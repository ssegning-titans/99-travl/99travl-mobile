# ntravl_api (EXPERIMENTAL)
The 99Travel API description

This Dart package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: 1.2.0
- Build package: org.openapitools.codegen.languages.DartDioNextClientCodegen

## Requirements

* Dart 2.12.0 or later OR Flutter 1.26.0 or later
* Dio 4.0.0+

## Installation & Usage

### pub.dev
To use the package from [pub.dev](https://pub.dev), please include the following in pubspec.yaml
```yaml
dependencies:
  ntravl_api: 1.1.4
```

### Github
If this Dart package is published to Github, please include the following in pubspec.yaml
```yaml
dependencies:
  ntravl_api:
    git:
      url: https://github.com/GIT_USER_ID/GIT_REPO_ID.git
      #ref: main
```

### Local development
To use the package from your local drive, please include the following in pubspec.yaml
```yaml
dependencies:
  ntravl_api:
    path: /path/to/ntravl_api
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```dart
import 'package:ntravl_api/ntravl_api.dart';


final api = NtravlApi().getAccountAddressApi();
final String accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final AccountAddress accountAddress = ; // AccountAddress | 

try {
    final response = await api.createAddress(accountId, accountAddress);
    print(response);
} catch on DioError (e) {
    print("Exception when calling AccountAddressApi->createAddress: $e\n");
}

```

## Documentation for API Endpoints

All URIs are relative to *http://localhost:8080*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
[*AccountAddressApi*](doc\AccountAddressApi.md) | [**createAddress**](doc\AccountAddressApi.md#createaddress) | **POST** /api/v1/accounts/{accountId}/addresses | Create address
[*AccountAddressApi*](doc\AccountAddressApi.md) | [**deleteAddressById**](doc\AccountAddressApi.md#deleteaddressbyid) | **DELETE** /api/v1/accounts/{accountId}/addresses/{addressId} | Delete address
[*AccountAddressApi*](doc\AccountAddressApi.md) | [**findAddressById**](doc\AccountAddressApi.md#findaddressbyid) | **GET** /api/v1/accounts/{accountId}/addresses/{addressId} | Get specific address
[*AccountAddressApi*](doc\AccountAddressApi.md) | [**findAddressesByAccountId**](doc\AccountAddressApi.md#findaddressesbyaccountid) | **GET** /api/v1/accounts/{accountId}/addresses | List addresses
[*AccountAddressApi*](doc\AccountAddressApi.md) | [**findLastAddressByAccountId**](doc\AccountAddressApi.md#findlastaddressbyaccountid) | **GET** /api/v1/accounts/{accountId}/addresses/last-added | Get address
[*AccountAddressApi*](doc\AccountAddressApi.md) | [**updateAddress**](doc\AccountAddressApi.md#updateaddress) | **PUT** /api/v1/accounts/{accountId}/addresses | Update address
[*AccountsApi*](doc\AccountsApi.md) | [**createAccount**](doc\AccountsApi.md#createaccount) | **POST** /api/v1/accounts | 
[*AccountsApi*](doc\AccountsApi.md) | [**getAccountByIdentifier**](doc\AccountsApi.md#getaccountbyidentifier) | **GET** /api/v1/accounts/{id} | 
[*AccountsApi*](doc\AccountsApi.md) | [**getAccounts**](doc\AccountsApi.md#getaccounts) | **GET** /api/v1/accounts | 
[*AccountsApi*](doc\AccountsApi.md) | [**updatePersonalData**](doc\AccountsApi.md#updatepersonaldata) | **PUT** /api/v1/accounts/{id}/personal-data | 
[*ChatMessagesApi*](doc\ChatMessagesApi.md) | [**createContactChatMessage**](doc\ChatMessagesApi.md#createcontactchatmessage) | **POST** /api/v1/messages/contacts | Create chat message
[*ChatMessagesApi*](doc\ChatMessagesApi.md) | [**createMediaChatMessage**](doc\ChatMessagesApi.md#createmediachatmessage) | **POST** /api/v1/messages/medias | Create chat message
[*ChatMessagesApi*](doc\ChatMessagesApi.md) | [**createPropositionChatMessage**](doc\ChatMessagesApi.md#createpropositionchatmessage) | **POST** /api/v1/messages/propositions | Create chat message
[*ChatMessagesApi*](doc\ChatMessagesApi.md) | [**createTextChatMessage**](doc\ChatMessagesApi.md#createtextchatmessage) | **POST** /api/v1/messages/texts | Create chat message
[*ChatMessagesApi*](doc\ChatMessagesApi.md) | [**deleteOneChatMessages**](doc\ChatMessagesApi.md#deleteonechatmessages) | **DELETE** /api/v1/messages/{messageId} | Delete chat message
[*ChatMessagesApi*](doc\ChatMessagesApi.md) | [**getAllChatMessages**](doc\ChatMessagesApi.md#getallchatmessages) | **GET** /api/v1/messages/_/chats/{chatId} | Get account&#39;s chat
[*ChatMessagesApi*](doc\ChatMessagesApi.md) | [**getOneChatMessage**](doc\ChatMessagesApi.md#getonechatmessage) | **GET** /api/v1/messages/{messageId} | Get account chat&#39;s message
[*ChatMessagesApi*](doc\ChatMessagesApi.md) | [**markMessageAsRead**](doc\ChatMessagesApi.md#markmessageasread) | **PUT** /api/v1/messages/{messageId}/read | Mark as read
[*ChatsApi*](doc\ChatsApi.md) | [**archiveChat**](doc\ChatsApi.md#archivechat) | **PUT** /api/v1/chats/{chatId}/status/archive | Archive chat
[*ChatsApi*](doc\ChatsApi.md) | [**deleteChat**](doc\ChatsApi.md#deletechat) | **DELETE** /api/v1/chats/{chatId} | Delete chat
[*ChatsApi*](doc\ChatsApi.md) | [**getAllChats**](doc\ChatsApi.md#getallchats) | **GET** /api/v1/chats/_/accounts/{accountId} | Get account&#39;s chat
[*ChatsApi*](doc\ChatsApi.md) | [**getChat**](doc\ChatsApi.md#getchat) | **GET** /api/v1/chats/{chatId} | Get chat
[*CredentialsApi*](doc\CredentialsApi.md) | [**createCredential**](doc\CredentialsApi.md#createcredential) | **POST** /api/v1/accounts/{id}/credentials/{type} | 
[*CredentialsApi*](doc\CredentialsApi.md) | [**disableCredential**](doc\CredentialsApi.md#disablecredential) | **PUT** /api/v1/accounts/{id}/credentials/{type}/disable | 
[*CredentialsApi*](doc\CredentialsApi.md) | [**hasCredential**](doc\CredentialsApi.md#hascredential) | **GET** /api/v1/accounts/{id}/credentials/{type} | 
[*CredentialsApi*](doc\CredentialsApi.md) | [**validateCredential**](doc\CredentialsApi.md#validatecredential) | **POST** /api/v1/accounts/{id}/credentials/{type}/verify | 
[*DocumentApi*](doc\DocumentApi.md) | [**getOnDocument**](doc\DocumentApi.md#getondocument) | **GET** /api/v1/documents/{documentId} | 
[*DocumentApi*](doc\DocumentApi.md) | [**uploadOne**](doc\DocumentApi.md#uploadone) | **POST** /api/v1/documents/upload | 
[*FavoritesApi*](doc\FavoritesApi.md) | [**createFavoriteAccount**](doc\FavoritesApi.md#createfavoriteaccount) | **POST** /api/v1/accounts/{accountId}/favorites/accounts/{personId} | Create person favorite
[*FavoritesApi*](doc\FavoritesApi.md) | [**createTravelFavorite**](doc\FavoritesApi.md#createtravelfavorite) | **POST** /api/v1/accounts/{accountId}/favorites/travels/{travelId} | Create travel favorite
[*FavoritesApi*](doc\FavoritesApi.md) | [**deleteFavoriteAccount**](doc\FavoritesApi.md#deletefavoriteaccount) | **DELETE** /api/v1/accounts/{accountId}/favorites/accounts/{personId} | Delete account travel
[*FavoritesApi*](doc\FavoritesApi.md) | [**deleteTravelFavorite**](doc\FavoritesApi.md#deletetravelfavorite) | **DELETE** /api/v1/accounts/{accountId}/favorites/travels/{travelId} | Delete favorite travel
[*FavoritesApi*](doc\FavoritesApi.md) | [**getFavoriteAccount**](doc\FavoritesApi.md#getfavoriteaccount) | **GET** /api/v1/accounts/{accountId}/favorites/accounts/{personId} | Get account favorite
[*FavoritesApi*](doc\FavoritesApi.md) | [**getOwnerFavorites**](doc\FavoritesApi.md#getownerfavorites) | **GET** /api/v1/accounts/{accountId}/favorites | Get owner&#39;s favorite
[*FavoritesApi*](doc\FavoritesApi.md) | [**getTravelFavorite**](doc\FavoritesApi.md#gettravelfavorite) | **GET** /api/v1/accounts/{accountId}/favorites/travels/{travelId} | Get travel favorite
[*FriendsApi*](doc\FriendsApi.md) | [**createOfflineFriend**](doc\FriendsApi.md#createofflinefriend) | **POST** /api/v1/accounts/friends/offline | 
[*FriendsApi*](doc\FriendsApi.md) | [**createOnlineFriend**](doc\FriendsApi.md#createonlinefriend) | **POST** /api/v1/accounts/friends/online | 
[*FriendsApi*](doc\FriendsApi.md) | [**deleteFriendById**](doc\FriendsApi.md#deletefriendbyid) | **DELETE** /api/v1/accounts/friends/{friendId} | 
[*FriendsApi*](doc\FriendsApi.md) | [**getFriendById**](doc\FriendsApi.md#getfriendbyid) | **GET** /api/v1/accounts/friends/{friendId} | 
[*FriendsApi*](doc\FriendsApi.md) | [**getFriends**](doc\FriendsApi.md#getfriends) | **GET** /api/v1/accounts/friends | 
[*FriendsApi*](doc\FriendsApi.md) | [**updateFriend**](doc\FriendsApi.md#updatefriend) | **PUT** /api/v1/accounts/friends/{friendId} | 
[*FriendsApi*](doc\FriendsApi.md) | [**updateFriendStatus**](doc\FriendsApi.md#updatefriendstatus) | **POST** /api/v1/accounts/friends/{friendId}/validate-online | 
[*FriendsApi*](doc\FriendsApi.md) | [**validateFriendShip**](doc\FriendsApi.md#validatefriendship) | **POST** /api/v1/accounts/friends/{friendId}/validate-offline | 
[*MapsApi*](doc\MapsApi.md) | [**autocomplete**](doc\MapsApi.md#autocomplete) | **GET** /api/v1/maps/autocomplete | 
[*MapsApi*](doc\MapsApi.md) | [**findPlaces**](doc\MapsApi.md#findplaces) | **GET** /api/v1/maps/by-formatted-name | Addresses by names
[*MapsApi*](doc\MapsApi.md) | [**getAddressByCoordinate**](doc\MapsApi.md#getaddressbycoordinate) | **GET** /api/v1/maps/by-coordinate | Address by coordinates
[*MapsApi*](doc\MapsApi.md) | [**getAddressByPlaceId**](doc\MapsApi.md#getaddressbyplaceid) | **GET** /api/v1/maps/by-place-id | Address by placeId
[*MapsApi*](doc\MapsApi.md) | [**routing**](doc\MapsApi.md#routing) | **GET** /api/v1/maps/routing | 
[*MapsApi*](doc\MapsApi.md) | [**sessionToken**](doc\MapsApi.md#sessiontoken) | **GET** /api/v1/maps/session-token | 
[*PhoneNumberApi*](doc\PhoneNumberApi.md) | [**getTan**](doc\PhoneNumberApi.md#gettan) | **POST** /api/v1/accounts/{id}/phone-number/tan | 
[*PhoneNumberApi*](doc\PhoneNumberApi.md) | [**validateTan**](doc\PhoneNumberApi.md#validatetan) | **PUT** /api/v1/accounts/{id}/phone-number/tan | 
[*PropositionsApi*](doc\PropositionsApi.md) | [**createChatAndTravelProposition**](doc\PropositionsApi.md#createchatandtravelproposition) | **POST** /api/v1/travels/{travelId}/propositions | Create proposition
[*PropositionsApi*](doc\PropositionsApi.md) | [**getOneTravelProposition**](doc\PropositionsApi.md#getonetravelproposition) | **GET** /api/v1/travels/_/propositions/{propositionId} | Get one travel proposition
[*PropositionsApi*](doc\PropositionsApi.md) | [**getTravelPropositions**](doc\PropositionsApi.md#gettravelpropositions) | **GET** /api/v1/travels/{travelId}/propositions | Get travel propositions
[*PropositionsApi*](doc\PropositionsApi.md) | [**updateTravelProposition**](doc\PropositionsApi.md#updatetravelproposition) | **PUT** /api/v1/travels/_/propositions/{propositionId} | Update proposition
[*RatingsApi*](doc\RatingsApi.md) | [**createPersonRating**](doc\RatingsApi.md#createpersonrating) | **POST** /api/v1/ratings/persons | Create person rating
[*RatingsApi*](doc\RatingsApi.md) | [**createTravelRating**](doc\RatingsApi.md#createtravelrating) | **POST** /api/v1/ratings/travels | Create travel rating
[*RatingsApi*](doc\RatingsApi.md) | [**deletePersonRating**](doc\RatingsApi.md#deletepersonrating) | **DELETE** /api/v1/ratings/persons/{personId} | Delete person rating
[*RatingsApi*](doc\RatingsApi.md) | [**deleteTravelRating**](doc\RatingsApi.md#deletetravelrating) | **DELETE** /api/v1/ratings/travels/{travelId} | Delete travel rating
[*RatingsApi*](doc\RatingsApi.md) | [**getAveragePersonRating**](doc\RatingsApi.md#getaveragepersonrating) | **GET** /api/v1/ratings/persons/{personId}/average | Get person average&#39;s ratings
[*RatingsApi*](doc\RatingsApi.md) | [**getAverageTravelRating**](doc\RatingsApi.md#getaveragetravelrating) | **GET** /api/v1/ratings/travels/{travelId}/average | Get travel average&#39;s ratings
[*RatingsApi*](doc\RatingsApi.md) | [**getCurrentAccountPersonRating**](doc\RatingsApi.md#getcurrentaccountpersonrating) | **GET** /api/v1/ratings/persons/{personId}/current_account | Get person rating
[*RatingsApi*](doc\RatingsApi.md) | [**getCurrentAccountTravelRating**](doc\RatingsApi.md#getcurrentaccounttravelrating) | **GET** /api/v1/ratings/travels/{travelId}/current_account | Get travel rating
[*RatingsApi*](doc\RatingsApi.md) | [**getOwnerRatings**](doc\RatingsApi.md#getownerratings) | **GET** /api/v1/accounts/{accountId}/ratings | Get owner&#39;s rating
[*RatingsApi*](doc\RatingsApi.md) | [**listPersonRating**](doc\RatingsApi.md#listpersonrating) | **GET** /api/v1/ratings/persons/{personId} | Get person ratings
[*RatingsApi*](doc\RatingsApi.md) | [**listTravelRating**](doc\RatingsApi.md#listtravelrating) | **GET** /api/v1/ratings/travels/{travelId} | Get travel ratings
[*ReportsApi*](doc\ReportsApi.md) | [**getAllBugReports**](doc\ReportsApi.md#getallbugreports) | **GET** /api/v1/reports/bugs | Get Report&#39;s bug
[*ReportsApi*](doc\ReportsApi.md) | [**getAllReportForAccount**](doc\ReportsApi.md#getallreportforaccount) | **GET** /api/v1/reports/accounts/{accountId} | Get Report&#39;s account
[*ReportsApi*](doc\ReportsApi.md) | [**getAllReportForTravel**](doc\ReportsApi.md#getallreportfortravel) | **GET** /api/v1/reports/travels/{travelId} | Get Report&#39;s Travel
[*ReportsApi*](doc\ReportsApi.md) | [**getAllReports**](doc\ReportsApi.md#getallreports) | **GET** /api/v1/reports/owners | Get report
[*ReportsApi*](doc\ReportsApi.md) | [**getOwnersReports**](doc\ReportsApi.md#getownersreports) | **GET** /api/v1/reports/owners/{ownerId} | Get owner&#39;s report
[*ReportsApi*](doc\ReportsApi.md) | [**getReportById**](doc\ReportsApi.md#getreportbyid) | **GET** /api/v1/reports/{reportId} | Get one report
[*ReportsApi*](doc\ReportsApi.md) | [**reportAccount**](doc\ReportsApi.md#reportaccount) | **POST** /api/v1/reports/accounts | Create account report&#39;s
[*ReportsApi*](doc\ReportsApi.md) | [**reportBug**](doc\ReportsApi.md#reportbug) | **POST** /api/v1/reports/bugs | Create bug report&#39;s
[*ReportsApi*](doc\ReportsApi.md) | [**reportTravel**](doc\ReportsApi.md#reporttravel) | **POST** /api/v1/reports/travels | Create travel report&#39;s
[*ReservationsApi*](doc\ReservationsApi.md) | [**countTravelReservation**](doc\ReservationsApi.md#counttravelreservation) | **GET** /api/v1/travels/{travelId}/reservations/count | Count travel&#39;s reservation
[*ReservationsApi*](doc\ReservationsApi.md) | [**createTravelReservation**](doc\ReservationsApi.md#createtravelreservation) | **POST** /api/v1/travels/{travelId}/reservations | Create Travel Reservation
[*ReservationsApi*](doc\ReservationsApi.md) | [**deleteReservationById**](doc\ReservationsApi.md#deletereservationbyid) | **DELETE** /api/v1/travels/{travelId}/reservations/{reservationId} | Delete travel
[*ReservationsApi*](doc\ReservationsApi.md) | [**getReservationById**](doc\ReservationsApi.md#getreservationbyid) | **GET** /api/v1/travels/{travelId}/reservations/{reservationId} | Get travel
[*ReservationsApi*](doc\ReservationsApi.md) | [**getReservationForTravel**](doc\ReservationsApi.md#getreservationfortravel) | **GET** /api/v1/travels/{travelId}/reservations | Get Reservation for travel
[*ReservationsApi*](doc\ReservationsApi.md) | [**updateReservationById**](doc\ReservationsApi.md#updatereservationbyid) | **PUT** /api/v1/travels/{travelId}/reservations | Update travel
[*TravelsApi*](doc\TravelsApi.md) | [**createTravel**](doc\TravelsApi.md#createtravel) | **POST** /api/v1/travels | Create travel
[*TravelsApi*](doc\TravelsApi.md) | [**deleteTravelById**](doc\TravelsApi.md#deletetravelbyid) | **DELETE** /api/v1/travels/{travelId} | Delete travel
[*TravelsApi*](doc\TravelsApi.md) | [**getAccountTravels**](doc\TravelsApi.md#getaccounttravels) | **GET** /api/v1/travels/accounts/{accountId} | Get accounts travels
[*TravelsApi*](doc\TravelsApi.md) | [**getRandomTravel**](doc\TravelsApi.md#getrandomtravel) | **GET** /api/v1/travels | Get travel randomly
[*TravelsApi*](doc\TravelsApi.md) | [**getTravelById**](doc\TravelsApi.md#gettravelbyid) | **GET** /api/v1/travels/{travelId} | Get travel
[*TravelsApi*](doc\TravelsApi.md) | [**searchTravel**](doc\TravelsApi.md#searchtravel) | **GET** /api/v1/travels/search | Search for travel between two points
[*TravelsApi*](doc\TravelsApi.md) | [**updateTravelById**](doc\TravelsApi.md#updatetravelbyid) | **PUT** /api/v1/travels | Update travel


## Documentation For Models

 - [Account](doc\Account.md)
 - [AccountAddress](doc\AccountAddress.md)
 - [AccountGender](doc\AccountGender.md)
 - [AccountStatus](doc\AccountStatus.md)
 - [AccountType](doc\AccountType.md)
 - [AddressComponent](doc\AddressComponent.md)
 - [AverageRatingPerson](doc\AverageRatingPerson.md)
 - [AverageRatingTravel](doc\AverageRatingTravel.md)
 - [BaseChatMessage](doc\BaseChatMessage.md)
 - [BaseFavorite](doc\BaseFavorite.md)
 - [BaseFriend](doc\BaseFriend.md)
 - [BaseRating](doc\BaseRating.md)
 - [BaseReport](doc\BaseReport.md)
 - [BugMedia](doc\BugMedia.md)
 - [ChangePersonalDataRequest](doc\ChangePersonalDataRequest.md)
 - [Chat](doc\Chat.md)
 - [ChatContact](doc\ChatContact.md)
 - [ChatMedia](doc\ChatMedia.md)
 - [ChatMessage](doc\ChatMessage.md)
 - [ChatMessageType](doc\ChatMessageType.md)
 - [ChatStatus](doc\ChatStatus.md)
 - [ContactChatMessage](doc\ContactChatMessage.md)
 - [ContactChatMessageAllOf](doc\ContactChatMessageAllOf.md)
 - [CreateAccountInput](doc\CreateAccountInput.md)
 - [CreateCredentialInput](doc\CreateCredentialInput.md)
 - [CreateCredentialResponse](doc\CreateCredentialResponse.md)
 - [CreateReportAccount](doc\CreateReportAccount.md)
 - [CreateReportBug](doc\CreateReportBug.md)
 - [CreateReportTravel](doc\CreateReportTravel.md)
 - [CreateTravel](doc\CreateTravel.md)
 - [CredentialType](doc\CredentialType.md)
 - [DisableCredentialResponse](doc\DisableCredentialResponse.md)
 - [Document](doc\Document.md)
 - [Favorite](doc\Favorite.md)
 - [FavoriteAccount](doc\FavoriteAccount.md)
 - [FavoriteTravel](doc\FavoriteTravel.md)
 - [FavoriteType](doc\FavoriteType.md)
 - [Friend](doc\Friend.md)
 - [FriendProcessType](doc\FriendProcessType.md)
 - [FriendShipStatus](doc\FriendShipStatus.md)
 - [FriendType](doc\FriendType.md)
 - [HasAccountPassword](doc\HasAccountPassword.md)
 - [LatLng](doc\LatLng.md)
 - [MapPlace](doc\MapPlace.md)
 - [MapsSession](doc\MapsSession.md)
 - [MediaChatMessage](doc\MediaChatMessage.md)
 - [MediaChatMessageAllOf](doc\MediaChatMessageAllOf.md)
 - [OfflineFriend](doc\OfflineFriend.md)
 - [OnlineFriend](doc\OnlineFriend.md)
 - [OnlineFriendAllOf](doc\OnlineFriendAllOf.md)
 - [PhoneTanRequest](doc\PhoneTanRequest.md)
 - [PhoneTanResponse](doc\PhoneTanResponse.md)
 - [PhoneTanStatus](doc\PhoneTanStatus.md)
 - [PhoneTanValidateRequest](doc\PhoneTanValidateRequest.md)
 - [PlaceType](doc\PlaceType.md)
 - [PropositionChatMessage](doc\PropositionChatMessage.md)
 - [PropositionChatMessageAllOf](doc\PropositionChatMessageAllOf.md)
 - [RatingAccount](doc\RatingAccount.md)
 - [RatingAccountAllOf](doc\RatingAccountAllOf.md)
 - [RatingTravel](doc\RatingTravel.md)
 - [RatingType](doc\RatingType.md)
 - [RatingValue](doc\RatingValue.md)
 - [ReportAccount](doc\ReportAccount.md)
 - [ReportAccountAllOf](doc\ReportAccountAllOf.md)
 - [ReportBug](doc\ReportBug.md)
 - [ReportStatus](doc\ReportStatus.md)
 - [ReportTravel](doc\ReportTravel.md)
 - [ReportTravelAllOf](doc\ReportTravelAllOf.md)
 - [ReportType](doc\ReportType.md)
 - [RequestOfflineFriendShip](doc\RequestOfflineFriendShip.md)
 - [RequestOnlineFriendShip](doc\RequestOnlineFriendShip.md)
 - [RoutingDirection](doc\RoutingDirection.md)
 - [RoutingMapPlace](doc\RoutingMapPlace.md)
 - [RoutingMapPlaceAllOf](doc\RoutingMapPlaceAllOf.md)
 - [SearchTravel](doc\SearchTravel.md)
 - [TextChatMessage](doc\TextChatMessage.md)
 - [TextChatMessageAllOf](doc\TextChatMessageAllOf.md)
 - [Travel](doc\Travel.md)
 - [TravelDays](doc\TravelDays.md)
 - [TravelLuggageDimension](doc\TravelLuggageDimension.md)
 - [TravelModeUnit](doc\TravelModeUnit.md)
 - [TravelPoint](doc\TravelPoint.md)
 - [TravelProposition](doc\TravelProposition.md)
 - [TravelQuantity](doc\TravelQuantity.md)
 - [TravelRepetition](doc\TravelRepetition.md)
 - [TravelReservation](doc\TravelReservation.md)
 - [TravelReservationCount](doc\TravelReservationCount.md)
 - [TravelWeightUnit](doc\TravelWeightUnit.md)
 - [UpdateFriend](doc\UpdateFriend.md)
 - [UpdateFriendStatus](doc\UpdateFriendStatus.md)
 - [UploadedResponse](doc\UploadedResponse.md)
 - [ValidateFriendShipCode](doc\ValidateFriendShipCode.md)
 - [ValidatePasswordInput](doc\ValidatePasswordInput.md)
 - [ValidatePasswordResponse](doc\ValidatePasswordResponse.md)


## Documentation For Authorization


## oauth2

- **Type**: OAuth
- **Flow**: application
- **Authorization URL**: 
- **Scopes**: 
 - **email**: Get account's email

## oauth2

- **Type**: OAuth
- **Flow**: accessCode
- **Authorization URL**: https://accounts.ssegning.com/auth/realms/99-travl/protocol/openid-connect/auth
- **Scopes**: 
 - **email**: Get account's email


## Author



