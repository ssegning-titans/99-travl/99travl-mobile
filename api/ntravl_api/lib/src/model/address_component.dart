//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:ntravl_api/src/model/place_type.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'address_component.g.dart';

/// AddressComponent
///
/// Properties:
/// * [longName] 
/// * [shortName] 
/// * [types] 
abstract class AddressComponent implements Built<AddressComponent, AddressComponentBuilder> {
    @BuiltValueField(wireName: r'longName')
    String? get longName;

    @BuiltValueField(wireName: r'shortName')
    String? get shortName;

    @BuiltValueField(wireName: r'types')
    BuiltList<PlaceType>? get types;

    AddressComponent._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(AddressComponentBuilder b) => b;

    factory AddressComponent([void updates(AddressComponentBuilder b)]) = _$AddressComponent;

    @BuiltValueSerializer(custom: true)
    static Serializer<AddressComponent> get serializer => _$AddressComponentSerializer();
}

class _$AddressComponentSerializer implements StructuredSerializer<AddressComponent> {
    @override
    final Iterable<Type> types = const [AddressComponent, _$AddressComponent];

    @override
    final String wireName = r'AddressComponent';

    @override
    Iterable<Object?> serialize(Serializers serializers, AddressComponent object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.longName != null) {
            result
                ..add(r'longName')
                ..add(serializers.serialize(object.longName,
                    specifiedType: const FullType(String)));
        }
        if (object.shortName != null) {
            result
                ..add(r'shortName')
                ..add(serializers.serialize(object.shortName,
                    specifiedType: const FullType(String)));
        }
        if (object.types != null) {
            result
                ..add(r'types')
                ..add(serializers.serialize(object.types,
                    specifiedType: const FullType(BuiltList, [FullType(PlaceType)])));
        }
        return result;
    }

    @override
    AddressComponent deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = AddressComponentBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'longName':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.longName = valueDes;
                    break;
                case r'shortName':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.shortName = valueDes;
                    break;
                case r'types':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(BuiltList, [FullType(PlaceType)])) as BuiltList<PlaceType>;
                    result.types.replace(valueDes);
                    break;
            }
        }
        return result.build();
    }
}

