// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search_travel.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$SearchTravel extends SearchTravel {
  @override
  final BuiltList<TravelPoint> points;

  factory _$SearchTravel([void Function(SearchTravelBuilder)? updates]) =>
      (new SearchTravelBuilder()..update(updates)).build();

  _$SearchTravel._({required this.points}) : super._() {
    BuiltValueNullFieldError.checkNotNull(points, 'SearchTravel', 'points');
  }

  @override
  SearchTravel rebuild(void Function(SearchTravelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SearchTravelBuilder toBuilder() => new SearchTravelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SearchTravel && points == other.points;
  }

  @override
  int get hashCode {
    return $jf($jc(0, points.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SearchTravel')..add('points', points))
        .toString();
  }
}

class SearchTravelBuilder
    implements Builder<SearchTravel, SearchTravelBuilder> {
  _$SearchTravel? _$v;

  ListBuilder<TravelPoint>? _points;
  ListBuilder<TravelPoint> get points =>
      _$this._points ??= new ListBuilder<TravelPoint>();
  set points(ListBuilder<TravelPoint>? points) => _$this._points = points;

  SearchTravelBuilder() {
    SearchTravel._defaults(this);
  }

  SearchTravelBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _points = $v.points.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SearchTravel other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SearchTravel;
  }

  @override
  void update(void Function(SearchTravelBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SearchTravel build() {
    _$SearchTravel _$result;
    try {
      _$result = _$v ?? new _$SearchTravel._(points: points.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'points';
        points.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'SearchTravel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
