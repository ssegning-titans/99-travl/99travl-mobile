// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'travel_proposition.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$TravelProposition extends TravelProposition {
  @override
  final String? id;
  @override
  final String? authorId;
  @override
  final String? travelId;
  @override
  final String? message;
  @override
  final String? startingPointId;
  @override
  final String? endingPointId;

  factory _$TravelProposition(
          [void Function(TravelPropositionBuilder)? updates]) =>
      (new TravelPropositionBuilder()..update(updates)).build();

  _$TravelProposition._(
      {this.id,
      this.authorId,
      this.travelId,
      this.message,
      this.startingPointId,
      this.endingPointId})
      : super._();

  @override
  TravelProposition rebuild(void Function(TravelPropositionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  TravelPropositionBuilder toBuilder() =>
      new TravelPropositionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TravelProposition &&
        id == other.id &&
        authorId == other.authorId &&
        travelId == other.travelId &&
        message == other.message &&
        startingPointId == other.startingPointId &&
        endingPointId == other.endingPointId;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, id.hashCode), authorId.hashCode),
                    travelId.hashCode),
                message.hashCode),
            startingPointId.hashCode),
        endingPointId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TravelProposition')
          ..add('id', id)
          ..add('authorId', authorId)
          ..add('travelId', travelId)
          ..add('message', message)
          ..add('startingPointId', startingPointId)
          ..add('endingPointId', endingPointId))
        .toString();
  }
}

class TravelPropositionBuilder
    implements Builder<TravelProposition, TravelPropositionBuilder> {
  _$TravelProposition? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _authorId;
  String? get authorId => _$this._authorId;
  set authorId(String? authorId) => _$this._authorId = authorId;

  String? _travelId;
  String? get travelId => _$this._travelId;
  set travelId(String? travelId) => _$this._travelId = travelId;

  String? _message;
  String? get message => _$this._message;
  set message(String? message) => _$this._message = message;

  String? _startingPointId;
  String? get startingPointId => _$this._startingPointId;
  set startingPointId(String? startingPointId) =>
      _$this._startingPointId = startingPointId;

  String? _endingPointId;
  String? get endingPointId => _$this._endingPointId;
  set endingPointId(String? endingPointId) =>
      _$this._endingPointId = endingPointId;

  TravelPropositionBuilder() {
    TravelProposition._defaults(this);
  }

  TravelPropositionBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _authorId = $v.authorId;
      _travelId = $v.travelId;
      _message = $v.message;
      _startingPointId = $v.startingPointId;
      _endingPointId = $v.endingPointId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TravelProposition other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$TravelProposition;
  }

  @override
  void update(void Function(TravelPropositionBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$TravelProposition build() {
    final _$result = _$v ??
        new _$TravelProposition._(
            id: id,
            authorId: authorId,
            travelId: travelId,
            message: message,
            startingPointId: startingPointId,
            endingPointId: endingPointId);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
