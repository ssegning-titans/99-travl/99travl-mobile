//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:ntravl_api/src/model/travel_proposition.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'proposition_chat_message_all_of.g.dart';

/// PropositionChatMessageAllOf
///
/// Properties:
/// * [proposition] 
abstract class PropositionChatMessageAllOf implements Built<PropositionChatMessageAllOf, PropositionChatMessageAllOfBuilder> {
    @BuiltValueField(wireName: r'proposition')
    TravelProposition? get proposition;

    PropositionChatMessageAllOf._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(PropositionChatMessageAllOfBuilder b) => b;

    factory PropositionChatMessageAllOf([void updates(PropositionChatMessageAllOfBuilder b)]) = _$PropositionChatMessageAllOf;

    @BuiltValueSerializer(custom: true)
    static Serializer<PropositionChatMessageAllOf> get serializer => _$PropositionChatMessageAllOfSerializer();
}

class _$PropositionChatMessageAllOfSerializer implements StructuredSerializer<PropositionChatMessageAllOf> {
    @override
    final Iterable<Type> types = const [PropositionChatMessageAllOf, _$PropositionChatMessageAllOf];

    @override
    final String wireName = r'PropositionChatMessageAllOf';

    @override
    Iterable<Object?> serialize(Serializers serializers, PropositionChatMessageAllOf object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.proposition != null) {
            result
                ..add(r'proposition')
                ..add(serializers.serialize(object.proposition,
                    specifiedType: const FullType(TravelProposition)));
        }
        return result;
    }

    @override
    PropositionChatMessageAllOf deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = PropositionChatMessageAllOfBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'proposition':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(TravelProposition)) as TravelProposition;
                    result.proposition.replace(valueDes);
                    break;
            }
        }
        return result.build();
    }
}

