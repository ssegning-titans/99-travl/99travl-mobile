// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'favorite_travel.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$FavoriteTravel extends FavoriteTravel {
  @override
  final String? id;
  @override
  final String ownerId;
  @override
  final int? createdAt;
  @override
  final FavoriteType favoriteType;
  @override
  final String travelId;

  factory _$FavoriteTravel([void Function(FavoriteTravelBuilder)? updates]) =>
      (new FavoriteTravelBuilder()..update(updates)).build();

  _$FavoriteTravel._(
      {this.id,
      required this.ownerId,
      this.createdAt,
      required this.favoriteType,
      required this.travelId})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(ownerId, 'FavoriteTravel', 'ownerId');
    BuiltValueNullFieldError.checkNotNull(
        favoriteType, 'FavoriteTravel', 'favoriteType');
    BuiltValueNullFieldError.checkNotNull(
        travelId, 'FavoriteTravel', 'travelId');
  }

  @override
  FavoriteTravel rebuild(void Function(FavoriteTravelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  FavoriteTravelBuilder toBuilder() =>
      new FavoriteTravelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is FavoriteTravel &&
        id == other.id &&
        ownerId == other.ownerId &&
        createdAt == other.createdAt &&
        favoriteType == other.favoriteType &&
        travelId == other.travelId;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc($jc(0, id.hashCode), ownerId.hashCode), createdAt.hashCode),
            favoriteType.hashCode),
        travelId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('FavoriteTravel')
          ..add('id', id)
          ..add('ownerId', ownerId)
          ..add('createdAt', createdAt)
          ..add('favoriteType', favoriteType)
          ..add('travelId', travelId))
        .toString();
  }
}

class FavoriteTravelBuilder
    implements Builder<FavoriteTravel, FavoriteTravelBuilder> {
  _$FavoriteTravel? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _ownerId;
  String? get ownerId => _$this._ownerId;
  set ownerId(String? ownerId) => _$this._ownerId = ownerId;

  int? _createdAt;
  int? get createdAt => _$this._createdAt;
  set createdAt(int? createdAt) => _$this._createdAt = createdAt;

  FavoriteType? _favoriteType;
  FavoriteType? get favoriteType => _$this._favoriteType;
  set favoriteType(FavoriteType? favoriteType) =>
      _$this._favoriteType = favoriteType;

  String? _travelId;
  String? get travelId => _$this._travelId;
  set travelId(String? travelId) => _$this._travelId = travelId;

  FavoriteTravelBuilder() {
    FavoriteTravel._defaults(this);
  }

  FavoriteTravelBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _ownerId = $v.ownerId;
      _createdAt = $v.createdAt;
      _favoriteType = $v.favoriteType;
      _travelId = $v.travelId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(FavoriteTravel other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$FavoriteTravel;
  }

  @override
  void update(void Function(FavoriteTravelBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$FavoriteTravel build() {
    final _$result = _$v ??
        new _$FavoriteTravel._(
            id: id,
            ownerId: BuiltValueNullFieldError.checkNotNull(
                ownerId, 'FavoriteTravel', 'ownerId'),
            createdAt: createdAt,
            favoriteType: BuiltValueNullFieldError.checkNotNull(
                favoriteType, 'FavoriteTravel', 'favoriteType'),
            travelId: BuiltValueNullFieldError.checkNotNull(
                travelId, 'FavoriteTravel', 'travelId'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
