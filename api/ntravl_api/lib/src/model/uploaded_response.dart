//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'uploaded_response.g.dart';

/// UploadedResponse
///
/// Properties:
/// * [id] 
/// * [path] 
/// * [key] 
abstract class UploadedResponse implements Built<UploadedResponse, UploadedResponseBuilder> {
    @BuiltValueField(wireName: r'id')
    String get id;

    @BuiltValueField(wireName: r'path')
    String? get path;

    @BuiltValueField(wireName: r'key')
    String get key;

    UploadedResponse._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(UploadedResponseBuilder b) => b;

    factory UploadedResponse([void updates(UploadedResponseBuilder b)]) = _$UploadedResponse;

    @BuiltValueSerializer(custom: true)
    static Serializer<UploadedResponse> get serializer => _$UploadedResponseSerializer();
}

class _$UploadedResponseSerializer implements StructuredSerializer<UploadedResponse> {
    @override
    final Iterable<Type> types = const [UploadedResponse, _$UploadedResponse];

    @override
    final String wireName = r'UploadedResponse';

    @override
    Iterable<Object?> serialize(Serializers serializers, UploadedResponse object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'id')
            ..add(serializers.serialize(object.id,
                specifiedType: const FullType(String)));
        if (object.path != null) {
            result
                ..add(r'path')
                ..add(serializers.serialize(object.path,
                    specifiedType: const FullType(String)));
        }
        result
            ..add(r'key')
            ..add(serializers.serialize(object.key,
                specifiedType: const FullType(String)));
        return result;
    }

    @override
    UploadedResponse deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = UploadedResponseBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'id':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.id = valueDes;
                    break;
                case r'path':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.path = valueDes;
                    break;
                case r'key':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.key = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

