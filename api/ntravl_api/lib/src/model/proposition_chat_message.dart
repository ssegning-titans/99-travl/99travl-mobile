//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:ntravl_api/src/model/proposition_chat_message_all_of.dart';
import 'package:ntravl_api/src/model/base_chat_message.dart';
import 'package:ntravl_api/src/model/travel_proposition.dart';
import 'package:ntravl_api/src/model/chat_message_type.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'proposition_chat_message.g.dart';

// ignore_for_file: unused_import

/// PropositionChatMessage
///
/// Properties:
/// * [id] 
/// * [createdAt] 
/// * [authorId] 
/// * [chatId] 
/// * [messageType] 
/// * [seen] 
/// * [proposition] 
abstract class PropositionChatMessage implements Built<PropositionChatMessage, PropositionChatMessageBuilder> {
    @BuiltValueField(wireName: r'id')
    String? get id;

    @BuiltValueField(wireName: r'createdAt')
    int? get createdAt;

    @BuiltValueField(wireName: r'authorId')
    String get authorId;

    @BuiltValueField(wireName: r'chatId')
    String get chatId;

    @BuiltValueField(wireName: r'messageType')
    ChatMessageType get messageType;
    // enum messageTypeEnum {  MEDIA,  TEXT,  PROPOSITION,  CONTACT,  };

    @BuiltValueField(wireName: r'seen')
    bool? get seen;

    @BuiltValueField(wireName: r'proposition')
    TravelProposition? get proposition;

    PropositionChatMessage._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(PropositionChatMessageBuilder b) => b
        ..seen = false;

    factory PropositionChatMessage([void updates(PropositionChatMessageBuilder b)]) = _$PropositionChatMessage;

    @BuiltValueSerializer(custom: true)
    static Serializer<PropositionChatMessage> get serializer => _$PropositionChatMessageSerializer();
}

class _$PropositionChatMessageSerializer implements StructuredSerializer<PropositionChatMessage> {
    @override
    final Iterable<Type> types = const [PropositionChatMessage, _$PropositionChatMessage];

    @override
    final String wireName = r'PropositionChatMessage';

    @override
    Iterable<Object?> serialize(Serializers serializers, PropositionChatMessage object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.id != null) {
            result
                ..add(r'id')
                ..add(serializers.serialize(object.id,
                    specifiedType: const FullType(String)));
        }
        if (object.createdAt != null) {
            result
                ..add(r'createdAt')
                ..add(serializers.serialize(object.createdAt,
                    specifiedType: const FullType(int)));
        }
        result
            ..add(r'authorId')
            ..add(serializers.serialize(object.authorId,
                specifiedType: const FullType(String)));
        result
            ..add(r'chatId')
            ..add(serializers.serialize(object.chatId,
                specifiedType: const FullType(String)));
        result
            ..add(r'messageType')
            ..add(serializers.serialize(object.messageType,
                specifiedType: const FullType(ChatMessageType)));
        if (object.seen != null) {
            result
                ..add(r'seen')
                ..add(serializers.serialize(object.seen,
                    specifiedType: const FullType(bool)));
        }
        if (object.proposition != null) {
            result
                ..add(r'proposition')
                ..add(serializers.serialize(object.proposition,
                    specifiedType: const FullType(TravelProposition)));
        }
        return result;
    }

    @override
    PropositionChatMessage deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = PropositionChatMessageBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'id':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.id = valueDes;
                    break;
                case r'createdAt':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    result.createdAt = valueDes;
                    break;
                case r'authorId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.authorId = valueDes;
                    break;
                case r'chatId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.chatId = valueDes;
                    break;
                case r'messageType':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(ChatMessageType)) as ChatMessageType;
                    result.messageType = valueDes;
                    break;
                case r'seen':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(bool)) as bool;
                    result.seen = valueDes;
                    break;
                case r'proposition':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(TravelProposition)) as TravelProposition;
                    result.proposition.replace(valueDes);
                    break;
            }
        }
        return result.build();
    }
}

