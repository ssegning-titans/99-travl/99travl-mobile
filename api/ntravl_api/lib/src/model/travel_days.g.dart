// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'travel_days.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const TravelDays _$MONDAY = const TravelDays._('MONDAY');
const TravelDays _$TUESDAY = const TravelDays._('TUESDAY');
const TravelDays _$WEDNESDAY = const TravelDays._('WEDNESDAY');
const TravelDays _$THURSDAY = const TravelDays._('THURSDAY');
const TravelDays _$FRIDAY = const TravelDays._('FRIDAY');
const TravelDays _$SATURDAY = const TravelDays._('SATURDAY');
const TravelDays _$SUNDAY = const TravelDays._('SUNDAY');

TravelDays _$valueOf(String name) {
  switch (name) {
    case 'MONDAY':
      return _$MONDAY;
    case 'TUESDAY':
      return _$TUESDAY;
    case 'WEDNESDAY':
      return _$WEDNESDAY;
    case 'THURSDAY':
      return _$THURSDAY;
    case 'FRIDAY':
      return _$FRIDAY;
    case 'SATURDAY':
      return _$SATURDAY;
    case 'SUNDAY':
      return _$SUNDAY;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<TravelDays> _$values =
    new BuiltSet<TravelDays>(const <TravelDays>[
  _$MONDAY,
  _$TUESDAY,
  _$WEDNESDAY,
  _$THURSDAY,
  _$FRIDAY,
  _$SATURDAY,
  _$SUNDAY,
]);

class _$TravelDaysMeta {
  const _$TravelDaysMeta();
  TravelDays get MONDAY => _$MONDAY;
  TravelDays get TUESDAY => _$TUESDAY;
  TravelDays get WEDNESDAY => _$WEDNESDAY;
  TravelDays get THURSDAY => _$THURSDAY;
  TravelDays get FRIDAY => _$FRIDAY;
  TravelDays get SATURDAY => _$SATURDAY;
  TravelDays get SUNDAY => _$SUNDAY;
  TravelDays valueOf(String name) => _$valueOf(name);
  BuiltSet<TravelDays> get values => _$values;
}

abstract class _$TravelDaysMixin {
  // ignore: non_constant_identifier_names
  _$TravelDaysMeta get TravelDays => const _$TravelDaysMeta();
}

Serializer<TravelDays> _$travelDaysSerializer = new _$TravelDaysSerializer();

class _$TravelDaysSerializer implements PrimitiveSerializer<TravelDays> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'MONDAY': 'MONDAY',
    'TUESDAY': 'TUESDAY',
    'WEDNESDAY': 'WEDNESDAY',
    'THURSDAY': 'THURSDAY',
    'FRIDAY': 'FRIDAY',
    'SATURDAY': 'SATURDAY',
    'SUNDAY': 'SUNDAY',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'MONDAY': 'MONDAY',
    'TUESDAY': 'TUESDAY',
    'WEDNESDAY': 'WEDNESDAY',
    'THURSDAY': 'THURSDAY',
    'FRIDAY': 'FRIDAY',
    'SATURDAY': 'SATURDAY',
    'SUNDAY': 'SUNDAY',
  };

  @override
  final Iterable<Type> types = const <Type>[TravelDays];
  @override
  final String wireName = 'TravelDays';

  @override
  Object serialize(Serializers serializers, TravelDays object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  TravelDays deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      TravelDays.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
