// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'uploaded_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$UploadedResponse extends UploadedResponse {
  @override
  final String id;
  @override
  final String? path;
  @override
  final String key;

  factory _$UploadedResponse(
          [void Function(UploadedResponseBuilder)? updates]) =>
      (new UploadedResponseBuilder()..update(updates)).build();

  _$UploadedResponse._({required this.id, this.path, required this.key})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(id, 'UploadedResponse', 'id');
    BuiltValueNullFieldError.checkNotNull(key, 'UploadedResponse', 'key');
  }

  @override
  UploadedResponse rebuild(void Function(UploadedResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UploadedResponseBuilder toBuilder() =>
      new UploadedResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UploadedResponse &&
        id == other.id &&
        path == other.path &&
        key == other.key;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, id.hashCode), path.hashCode), key.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UploadedResponse')
          ..add('id', id)
          ..add('path', path)
          ..add('key', key))
        .toString();
  }
}

class UploadedResponseBuilder
    implements Builder<UploadedResponse, UploadedResponseBuilder> {
  _$UploadedResponse? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _path;
  String? get path => _$this._path;
  set path(String? path) => _$this._path = path;

  String? _key;
  String? get key => _$this._key;
  set key(String? key) => _$this._key = key;

  UploadedResponseBuilder() {
    UploadedResponse._defaults(this);
  }

  UploadedResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _path = $v.path;
      _key = $v.key;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UploadedResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$UploadedResponse;
  }

  @override
  void update(void Function(UploadedResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$UploadedResponse build() {
    final _$result = _$v ??
        new _$UploadedResponse._(
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'UploadedResponse', 'id'),
            path: path,
            key: BuiltValueNullFieldError.checkNotNull(
                key, 'UploadedResponse', 'key'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
