//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'has_account_password.g.dart';

/// HasAccountPassword
///
/// Properties:
/// * [state] 
abstract class HasAccountPassword implements Built<HasAccountPassword, HasAccountPasswordBuilder> {
    @BuiltValueField(wireName: r'state')
    bool get state;

    HasAccountPassword._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(HasAccountPasswordBuilder b) => b;

    factory HasAccountPassword([void updates(HasAccountPasswordBuilder b)]) = _$HasAccountPassword;

    @BuiltValueSerializer(custom: true)
    static Serializer<HasAccountPassword> get serializer => _$HasAccountPasswordSerializer();
}

class _$HasAccountPasswordSerializer implements StructuredSerializer<HasAccountPassword> {
    @override
    final Iterable<Type> types = const [HasAccountPassword, _$HasAccountPassword];

    @override
    final String wireName = r'HasAccountPassword';

    @override
    Iterable<Object?> serialize(Serializers serializers, HasAccountPassword object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'state')
            ..add(serializers.serialize(object.state,
                specifiedType: const FullType(bool)));
        return result;
    }

    @override
    HasAccountPassword deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = HasAccountPasswordBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'state':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(bool)) as bool;
                    result.state = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

