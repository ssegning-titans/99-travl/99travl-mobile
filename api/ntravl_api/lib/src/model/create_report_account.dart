//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'create_report_account.g.dart';

/// CreateReportAccount
///
/// Properties:
/// * [authorId] 
/// * [accountId] 
/// * [description] 
abstract class CreateReportAccount implements Built<CreateReportAccount, CreateReportAccountBuilder> {
    @BuiltValueField(wireName: r'authorId')
    String get authorId;

    @BuiltValueField(wireName: r'accountId')
    String? get accountId;

    @BuiltValueField(wireName: r'description')
    String? get description;

    CreateReportAccount._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(CreateReportAccountBuilder b) => b;

    factory CreateReportAccount([void updates(CreateReportAccountBuilder b)]) = _$CreateReportAccount;

    @BuiltValueSerializer(custom: true)
    static Serializer<CreateReportAccount> get serializer => _$CreateReportAccountSerializer();
}

class _$CreateReportAccountSerializer implements StructuredSerializer<CreateReportAccount> {
    @override
    final Iterable<Type> types = const [CreateReportAccount, _$CreateReportAccount];

    @override
    final String wireName = r'CreateReportAccount';

    @override
    Iterable<Object?> serialize(Serializers serializers, CreateReportAccount object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'authorId')
            ..add(serializers.serialize(object.authorId,
                specifiedType: const FullType(String)));
        if (object.accountId != null) {
            result
                ..add(r'accountId')
                ..add(serializers.serialize(object.accountId,
                    specifiedType: const FullType(String)));
        }
        if (object.description != null) {
            result
                ..add(r'description')
                ..add(serializers.serialize(object.description,
                    specifiedType: const FullType(String)));
        }
        return result;
    }

    @override
    CreateReportAccount deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = CreateReportAccountBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'authorId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.authorId = valueDes;
                    break;
                case r'accountId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.accountId = valueDes;
                    break;
                case r'description':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.description = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

