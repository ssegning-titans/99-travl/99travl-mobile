// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'phone_tan_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$PhoneTanRequest extends PhoneTanRequest {
  @override
  final String? phoneNumber;

  factory _$PhoneTanRequest([void Function(PhoneTanRequestBuilder)? updates]) =>
      (new PhoneTanRequestBuilder()..update(updates)).build();

  _$PhoneTanRequest._({this.phoneNumber}) : super._();

  @override
  PhoneTanRequest rebuild(void Function(PhoneTanRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PhoneTanRequestBuilder toBuilder() =>
      new PhoneTanRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PhoneTanRequest && phoneNumber == other.phoneNumber;
  }

  @override
  int get hashCode {
    return $jf($jc(0, phoneNumber.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PhoneTanRequest')
          ..add('phoneNumber', phoneNumber))
        .toString();
  }
}

class PhoneTanRequestBuilder
    implements Builder<PhoneTanRequest, PhoneTanRequestBuilder> {
  _$PhoneTanRequest? _$v;

  String? _phoneNumber;
  String? get phoneNumber => _$this._phoneNumber;
  set phoneNumber(String? phoneNumber) => _$this._phoneNumber = phoneNumber;

  PhoneTanRequestBuilder() {
    PhoneTanRequest._defaults(this);
  }

  PhoneTanRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _phoneNumber = $v.phoneNumber;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PhoneTanRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$PhoneTanRequest;
  }

  @override
  void update(void Function(PhoneTanRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PhoneTanRequest build() {
    final _$result = _$v ?? new _$PhoneTanRequest._(phoneNumber: phoneNumber);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
