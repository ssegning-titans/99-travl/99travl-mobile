// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$Account extends Account {
  @override
  final int createdAt;
  @override
  final int updatedAt;
  @override
  final String id;
  @override
  final String? firstName;
  @override
  final String? lastName;
  @override
  final String? locale;
  @override
  final String? avatarUrl;
  @override
  final String email;
  @override
  final bool? emailVerified;
  @override
  final AccountGender? gender;
  @override
  final AccountStatus status;
  @override
  final String? phoneNumber;
  @override
  final bool? phoneNumberVerified;
  @override
  final String? bio;
  @override
  final AccountType? accountType;

  factory _$Account([void Function(AccountBuilder)? updates]) =>
      (new AccountBuilder()..update(updates)).build();

  _$Account._(
      {required this.createdAt,
      required this.updatedAt,
      required this.id,
      this.firstName,
      this.lastName,
      this.locale,
      this.avatarUrl,
      required this.email,
      this.emailVerified,
      this.gender,
      required this.status,
      this.phoneNumber,
      this.phoneNumberVerified,
      this.bio,
      this.accountType})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(createdAt, 'Account', 'createdAt');
    BuiltValueNullFieldError.checkNotNull(updatedAt, 'Account', 'updatedAt');
    BuiltValueNullFieldError.checkNotNull(id, 'Account', 'id');
    BuiltValueNullFieldError.checkNotNull(email, 'Account', 'email');
    BuiltValueNullFieldError.checkNotNull(status, 'Account', 'status');
  }

  @override
  Account rebuild(void Function(AccountBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AccountBuilder toBuilder() => new AccountBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Account &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt &&
        id == other.id &&
        firstName == other.firstName &&
        lastName == other.lastName &&
        locale == other.locale &&
        avatarUrl == other.avatarUrl &&
        email == other.email &&
        emailVerified == other.emailVerified &&
        gender == other.gender &&
        status == other.status &&
        phoneNumber == other.phoneNumber &&
        phoneNumberVerified == other.phoneNumberVerified &&
        bio == other.bio &&
        accountType == other.accountType;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc(
                                                    $jc(
                                                        $jc(
                                                            $jc(
                                                                0,
                                                                createdAt
                                                                    .hashCode),
                                                            updatedAt.hashCode),
                                                        id.hashCode),
                                                    firstName.hashCode),
                                                lastName.hashCode),
                                            locale.hashCode),
                                        avatarUrl.hashCode),
                                    email.hashCode),
                                emailVerified.hashCode),
                            gender.hashCode),
                        status.hashCode),
                    phoneNumber.hashCode),
                phoneNumberVerified.hashCode),
            bio.hashCode),
        accountType.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Account')
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt)
          ..add('id', id)
          ..add('firstName', firstName)
          ..add('lastName', lastName)
          ..add('locale', locale)
          ..add('avatarUrl', avatarUrl)
          ..add('email', email)
          ..add('emailVerified', emailVerified)
          ..add('gender', gender)
          ..add('status', status)
          ..add('phoneNumber', phoneNumber)
          ..add('phoneNumberVerified', phoneNumberVerified)
          ..add('bio', bio)
          ..add('accountType', accountType))
        .toString();
  }
}

class AccountBuilder implements Builder<Account, AccountBuilder> {
  _$Account? _$v;

  int? _createdAt;
  int? get createdAt => _$this._createdAt;
  set createdAt(int? createdAt) => _$this._createdAt = createdAt;

  int? _updatedAt;
  int? get updatedAt => _$this._updatedAt;
  set updatedAt(int? updatedAt) => _$this._updatedAt = updatedAt;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _firstName;
  String? get firstName => _$this._firstName;
  set firstName(String? firstName) => _$this._firstName = firstName;

  String? _lastName;
  String? get lastName => _$this._lastName;
  set lastName(String? lastName) => _$this._lastName = lastName;

  String? _locale;
  String? get locale => _$this._locale;
  set locale(String? locale) => _$this._locale = locale;

  String? _avatarUrl;
  String? get avatarUrl => _$this._avatarUrl;
  set avatarUrl(String? avatarUrl) => _$this._avatarUrl = avatarUrl;

  String? _email;
  String? get email => _$this._email;
  set email(String? email) => _$this._email = email;

  bool? _emailVerified;
  bool? get emailVerified => _$this._emailVerified;
  set emailVerified(bool? emailVerified) =>
      _$this._emailVerified = emailVerified;

  AccountGender? _gender;
  AccountGender? get gender => _$this._gender;
  set gender(AccountGender? gender) => _$this._gender = gender;

  AccountStatus? _status;
  AccountStatus? get status => _$this._status;
  set status(AccountStatus? status) => _$this._status = status;

  String? _phoneNumber;
  String? get phoneNumber => _$this._phoneNumber;
  set phoneNumber(String? phoneNumber) => _$this._phoneNumber = phoneNumber;

  bool? _phoneNumberVerified;
  bool? get phoneNumberVerified => _$this._phoneNumberVerified;
  set phoneNumberVerified(bool? phoneNumberVerified) =>
      _$this._phoneNumberVerified = phoneNumberVerified;

  String? _bio;
  String? get bio => _$this._bio;
  set bio(String? bio) => _$this._bio = bio;

  AccountType? _accountType;
  AccountType? get accountType => _$this._accountType;
  set accountType(AccountType? accountType) =>
      _$this._accountType = accountType;

  AccountBuilder() {
    Account._defaults(this);
  }

  AccountBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _createdAt = $v.createdAt;
      _updatedAt = $v.updatedAt;
      _id = $v.id;
      _firstName = $v.firstName;
      _lastName = $v.lastName;
      _locale = $v.locale;
      _avatarUrl = $v.avatarUrl;
      _email = $v.email;
      _emailVerified = $v.emailVerified;
      _gender = $v.gender;
      _status = $v.status;
      _phoneNumber = $v.phoneNumber;
      _phoneNumberVerified = $v.phoneNumberVerified;
      _bio = $v.bio;
      _accountType = $v.accountType;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Account other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Account;
  }

  @override
  void update(void Function(AccountBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Account build() {
    final _$result = _$v ??
        new _$Account._(
            createdAt: BuiltValueNullFieldError.checkNotNull(
                createdAt, 'Account', 'createdAt'),
            updatedAt: BuiltValueNullFieldError.checkNotNull(
                updatedAt, 'Account', 'updatedAt'),
            id: BuiltValueNullFieldError.checkNotNull(id, 'Account', 'id'),
            firstName: firstName,
            lastName: lastName,
            locale: locale,
            avatarUrl: avatarUrl,
            email: BuiltValueNullFieldError.checkNotNull(
                email, 'Account', 'email'),
            emailVerified: emailVerified,
            gender: gender,
            status: BuiltValueNullFieldError.checkNotNull(
                status, 'Account', 'status'),
            phoneNumber: phoneNumber,
            phoneNumberVerified: phoneNumberVerified,
            bio: bio,
            accountType: accountType);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
