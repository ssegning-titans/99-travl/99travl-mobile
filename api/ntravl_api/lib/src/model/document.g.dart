// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'document.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$Document extends Document {
  @override
  final String? bytes;
  @override
  final String? contentType;
  @override
  final String? filePath;
  @override
  final BuiltMap<String, String>? metaData;
  @override
  final BuiltMap<String, String>? headers;

  factory _$Document([void Function(DocumentBuilder)? updates]) =>
      (new DocumentBuilder()..update(updates)).build();

  _$Document._(
      {this.bytes,
      this.contentType,
      this.filePath,
      this.metaData,
      this.headers})
      : super._();

  @override
  Document rebuild(void Function(DocumentBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DocumentBuilder toBuilder() => new DocumentBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Document &&
        bytes == other.bytes &&
        contentType == other.contentType &&
        filePath == other.filePath &&
        metaData == other.metaData &&
        headers == other.headers;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc(0, bytes.hashCode), contentType.hashCode),
                filePath.hashCode),
            metaData.hashCode),
        headers.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Document')
          ..add('bytes', bytes)
          ..add('contentType', contentType)
          ..add('filePath', filePath)
          ..add('metaData', metaData)
          ..add('headers', headers))
        .toString();
  }
}

class DocumentBuilder implements Builder<Document, DocumentBuilder> {
  _$Document? _$v;

  String? _bytes;
  String? get bytes => _$this._bytes;
  set bytes(String? bytes) => _$this._bytes = bytes;

  String? _contentType;
  String? get contentType => _$this._contentType;
  set contentType(String? contentType) => _$this._contentType = contentType;

  String? _filePath;
  String? get filePath => _$this._filePath;
  set filePath(String? filePath) => _$this._filePath = filePath;

  MapBuilder<String, String>? _metaData;
  MapBuilder<String, String> get metaData =>
      _$this._metaData ??= new MapBuilder<String, String>();
  set metaData(MapBuilder<String, String>? metaData) =>
      _$this._metaData = metaData;

  MapBuilder<String, String>? _headers;
  MapBuilder<String, String> get headers =>
      _$this._headers ??= new MapBuilder<String, String>();
  set headers(MapBuilder<String, String>? headers) => _$this._headers = headers;

  DocumentBuilder() {
    Document._defaults(this);
  }

  DocumentBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _bytes = $v.bytes;
      _contentType = $v.contentType;
      _filePath = $v.filePath;
      _metaData = $v.metaData?.toBuilder();
      _headers = $v.headers?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Document other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Document;
  }

  @override
  void update(void Function(DocumentBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Document build() {
    _$Document _$result;
    try {
      _$result = _$v ??
          new _$Document._(
              bytes: bytes,
              contentType: contentType,
              filePath: filePath,
              metaData: _metaData?.build(),
              headers: _headers?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'metaData';
        _metaData?.build();
        _$failedField = 'headers';
        _headers?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'Document', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
