// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_message.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ChatMessage extends ChatMessage {
  @override
  final String? id;
  @override
  final int? createdAt;
  @override
  final String authorId;
  @override
  final String chatId;
  @override
  final ChatMessageType messageType;
  @override
  final bool? seen;
  @override
  final BuiltSet<ChatMedia> medias;
  @override
  final TravelProposition? proposition;
  @override
  final String message;
  @override
  final BuiltSet<ChatContact> contacts;

  factory _$ChatMessage([void Function(ChatMessageBuilder)? updates]) =>
      (new ChatMessageBuilder()..update(updates)).build();

  _$ChatMessage._(
      {this.id,
      this.createdAt,
      required this.authorId,
      required this.chatId,
      required this.messageType,
      this.seen,
      required this.medias,
      this.proposition,
      required this.message,
      required this.contacts})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(authorId, 'ChatMessage', 'authorId');
    BuiltValueNullFieldError.checkNotNull(chatId, 'ChatMessage', 'chatId');
    BuiltValueNullFieldError.checkNotNull(
        messageType, 'ChatMessage', 'messageType');
    BuiltValueNullFieldError.checkNotNull(medias, 'ChatMessage', 'medias');
    BuiltValueNullFieldError.checkNotNull(message, 'ChatMessage', 'message');
    BuiltValueNullFieldError.checkNotNull(contacts, 'ChatMessage', 'contacts');
  }

  @override
  ChatMessage rebuild(void Function(ChatMessageBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ChatMessageBuilder toBuilder() => new ChatMessageBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ChatMessage &&
        id == other.id &&
        createdAt == other.createdAt &&
        authorId == other.authorId &&
        chatId == other.chatId &&
        messageType == other.messageType &&
        seen == other.seen &&
        medias == other.medias &&
        proposition == other.proposition &&
        message == other.message &&
        contacts == other.contacts;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc($jc(0, id.hashCode),
                                        createdAt.hashCode),
                                    authorId.hashCode),
                                chatId.hashCode),
                            messageType.hashCode),
                        seen.hashCode),
                    medias.hashCode),
                proposition.hashCode),
            message.hashCode),
        contacts.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ChatMessage')
          ..add('id', id)
          ..add('createdAt', createdAt)
          ..add('authorId', authorId)
          ..add('chatId', chatId)
          ..add('messageType', messageType)
          ..add('seen', seen)
          ..add('medias', medias)
          ..add('proposition', proposition)
          ..add('message', message)
          ..add('contacts', contacts))
        .toString();
  }
}

class ChatMessageBuilder implements Builder<ChatMessage, ChatMessageBuilder> {
  _$ChatMessage? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  int? _createdAt;
  int? get createdAt => _$this._createdAt;
  set createdAt(int? createdAt) => _$this._createdAt = createdAt;

  String? _authorId;
  String? get authorId => _$this._authorId;
  set authorId(String? authorId) => _$this._authorId = authorId;

  String? _chatId;
  String? get chatId => _$this._chatId;
  set chatId(String? chatId) => _$this._chatId = chatId;

  ChatMessageType? _messageType;
  ChatMessageType? get messageType => _$this._messageType;
  set messageType(ChatMessageType? messageType) =>
      _$this._messageType = messageType;

  bool? _seen;
  bool? get seen => _$this._seen;
  set seen(bool? seen) => _$this._seen = seen;

  SetBuilder<ChatMedia>? _medias;
  SetBuilder<ChatMedia> get medias =>
      _$this._medias ??= new SetBuilder<ChatMedia>();
  set medias(SetBuilder<ChatMedia>? medias) => _$this._medias = medias;

  TravelPropositionBuilder? _proposition;
  TravelPropositionBuilder get proposition =>
      _$this._proposition ??= new TravelPropositionBuilder();
  set proposition(TravelPropositionBuilder? proposition) =>
      _$this._proposition = proposition;

  String? _message;
  String? get message => _$this._message;
  set message(String? message) => _$this._message = message;

  SetBuilder<ChatContact>? _contacts;
  SetBuilder<ChatContact> get contacts =>
      _$this._contacts ??= new SetBuilder<ChatContact>();
  set contacts(SetBuilder<ChatContact>? contacts) =>
      _$this._contacts = contacts;

  ChatMessageBuilder() {
    ChatMessage._defaults(this);
  }

  ChatMessageBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _createdAt = $v.createdAt;
      _authorId = $v.authorId;
      _chatId = $v.chatId;
      _messageType = $v.messageType;
      _seen = $v.seen;
      _medias = $v.medias.toBuilder();
      _proposition = $v.proposition?.toBuilder();
      _message = $v.message;
      _contacts = $v.contacts.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ChatMessage other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ChatMessage;
  }

  @override
  void update(void Function(ChatMessageBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ChatMessage build() {
    _$ChatMessage _$result;
    try {
      _$result = _$v ??
          new _$ChatMessage._(
              id: id,
              createdAt: createdAt,
              authorId: BuiltValueNullFieldError.checkNotNull(
                  authorId, 'ChatMessage', 'authorId'),
              chatId: BuiltValueNullFieldError.checkNotNull(
                  chatId, 'ChatMessage', 'chatId'),
              messageType: BuiltValueNullFieldError.checkNotNull(
                  messageType, 'ChatMessage', 'messageType'),
              seen: seen,
              medias: medias.build(),
              proposition: _proposition?.build(),
              message: BuiltValueNullFieldError.checkNotNull(
                  message, 'ChatMessage', 'message'),
              contacts: contacts.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'medias';
        medias.build();
        _$failedField = 'proposition';
        _proposition?.build();

        _$failedField = 'contacts';
        contacts.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ChatMessage', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
