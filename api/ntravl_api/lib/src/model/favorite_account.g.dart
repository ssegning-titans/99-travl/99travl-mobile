// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'favorite_account.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$FavoriteAccount extends FavoriteAccount {
  @override
  final String? id;
  @override
  final String ownerId;
  @override
  final int? createdAt;
  @override
  final FavoriteType favoriteType;
  @override
  final String accountId;

  factory _$FavoriteAccount([void Function(FavoriteAccountBuilder)? updates]) =>
      (new FavoriteAccountBuilder()..update(updates)).build();

  _$FavoriteAccount._(
      {this.id,
      required this.ownerId,
      this.createdAt,
      required this.favoriteType,
      required this.accountId})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        ownerId, 'FavoriteAccount', 'ownerId');
    BuiltValueNullFieldError.checkNotNull(
        favoriteType, 'FavoriteAccount', 'favoriteType');
    BuiltValueNullFieldError.checkNotNull(
        accountId, 'FavoriteAccount', 'accountId');
  }

  @override
  FavoriteAccount rebuild(void Function(FavoriteAccountBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  FavoriteAccountBuilder toBuilder() =>
      new FavoriteAccountBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is FavoriteAccount &&
        id == other.id &&
        ownerId == other.ownerId &&
        createdAt == other.createdAt &&
        favoriteType == other.favoriteType &&
        accountId == other.accountId;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc($jc(0, id.hashCode), ownerId.hashCode), createdAt.hashCode),
            favoriteType.hashCode),
        accountId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('FavoriteAccount')
          ..add('id', id)
          ..add('ownerId', ownerId)
          ..add('createdAt', createdAt)
          ..add('favoriteType', favoriteType)
          ..add('accountId', accountId))
        .toString();
  }
}

class FavoriteAccountBuilder
    implements Builder<FavoriteAccount, FavoriteAccountBuilder> {
  _$FavoriteAccount? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _ownerId;
  String? get ownerId => _$this._ownerId;
  set ownerId(String? ownerId) => _$this._ownerId = ownerId;

  int? _createdAt;
  int? get createdAt => _$this._createdAt;
  set createdAt(int? createdAt) => _$this._createdAt = createdAt;

  FavoriteType? _favoriteType;
  FavoriteType? get favoriteType => _$this._favoriteType;
  set favoriteType(FavoriteType? favoriteType) =>
      _$this._favoriteType = favoriteType;

  String? _accountId;
  String? get accountId => _$this._accountId;
  set accountId(String? accountId) => _$this._accountId = accountId;

  FavoriteAccountBuilder() {
    FavoriteAccount._defaults(this);
  }

  FavoriteAccountBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _ownerId = $v.ownerId;
      _createdAt = $v.createdAt;
      _favoriteType = $v.favoriteType;
      _accountId = $v.accountId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(FavoriteAccount other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$FavoriteAccount;
  }

  @override
  void update(void Function(FavoriteAccountBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$FavoriteAccount build() {
    final _$result = _$v ??
        new _$FavoriteAccount._(
            id: id,
            ownerId: BuiltValueNullFieldError.checkNotNull(
                ownerId, 'FavoriteAccount', 'ownerId'),
            createdAt: createdAt,
            favoriteType: BuiltValueNullFieldError.checkNotNull(
                favoriteType, 'FavoriteAccount', 'favoriteType'),
            accountId: BuiltValueNullFieldError.checkNotNull(
                accountId, 'FavoriteAccount', 'accountId'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
