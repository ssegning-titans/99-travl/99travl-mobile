// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'proposition_chat_message.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$PropositionChatMessage extends PropositionChatMessage {
  @override
  final String? id;
  @override
  final int? createdAt;
  @override
  final String authorId;
  @override
  final String chatId;
  @override
  final ChatMessageType messageType;
  @override
  final bool? seen;
  @override
  final TravelProposition? proposition;

  factory _$PropositionChatMessage(
          [void Function(PropositionChatMessageBuilder)? updates]) =>
      (new PropositionChatMessageBuilder()..update(updates)).build();

  _$PropositionChatMessage._(
      {this.id,
      this.createdAt,
      required this.authorId,
      required this.chatId,
      required this.messageType,
      this.seen,
      this.proposition})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        authorId, 'PropositionChatMessage', 'authorId');
    BuiltValueNullFieldError.checkNotNull(
        chatId, 'PropositionChatMessage', 'chatId');
    BuiltValueNullFieldError.checkNotNull(
        messageType, 'PropositionChatMessage', 'messageType');
  }

  @override
  PropositionChatMessage rebuild(
          void Function(PropositionChatMessageBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PropositionChatMessageBuilder toBuilder() =>
      new PropositionChatMessageBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PropositionChatMessage &&
        id == other.id &&
        createdAt == other.createdAt &&
        authorId == other.authorId &&
        chatId == other.chatId &&
        messageType == other.messageType &&
        seen == other.seen &&
        proposition == other.proposition;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, id.hashCode), createdAt.hashCode),
                        authorId.hashCode),
                    chatId.hashCode),
                messageType.hashCode),
            seen.hashCode),
        proposition.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PropositionChatMessage')
          ..add('id', id)
          ..add('createdAt', createdAt)
          ..add('authorId', authorId)
          ..add('chatId', chatId)
          ..add('messageType', messageType)
          ..add('seen', seen)
          ..add('proposition', proposition))
        .toString();
  }
}

class PropositionChatMessageBuilder
    implements Builder<PropositionChatMessage, PropositionChatMessageBuilder> {
  _$PropositionChatMessage? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  int? _createdAt;
  int? get createdAt => _$this._createdAt;
  set createdAt(int? createdAt) => _$this._createdAt = createdAt;

  String? _authorId;
  String? get authorId => _$this._authorId;
  set authorId(String? authorId) => _$this._authorId = authorId;

  String? _chatId;
  String? get chatId => _$this._chatId;
  set chatId(String? chatId) => _$this._chatId = chatId;

  ChatMessageType? _messageType;
  ChatMessageType? get messageType => _$this._messageType;
  set messageType(ChatMessageType? messageType) =>
      _$this._messageType = messageType;

  bool? _seen;
  bool? get seen => _$this._seen;
  set seen(bool? seen) => _$this._seen = seen;

  TravelPropositionBuilder? _proposition;
  TravelPropositionBuilder get proposition =>
      _$this._proposition ??= new TravelPropositionBuilder();
  set proposition(TravelPropositionBuilder? proposition) =>
      _$this._proposition = proposition;

  PropositionChatMessageBuilder() {
    PropositionChatMessage._defaults(this);
  }

  PropositionChatMessageBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _createdAt = $v.createdAt;
      _authorId = $v.authorId;
      _chatId = $v.chatId;
      _messageType = $v.messageType;
      _seen = $v.seen;
      _proposition = $v.proposition?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PropositionChatMessage other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$PropositionChatMessage;
  }

  @override
  void update(void Function(PropositionChatMessageBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PropositionChatMessage build() {
    _$PropositionChatMessage _$result;
    try {
      _$result = _$v ??
          new _$PropositionChatMessage._(
              id: id,
              createdAt: createdAt,
              authorId: BuiltValueNullFieldError.checkNotNull(
                  authorId, 'PropositionChatMessage', 'authorId'),
              chatId: BuiltValueNullFieldError.checkNotNull(
                  chatId, 'PropositionChatMessage', 'chatId'),
              messageType: BuiltValueNullFieldError.checkNotNull(
                  messageType, 'PropositionChatMessage', 'messageType'),
              seen: seen,
              proposition: _proposition?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'proposition';
        _proposition?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'PropositionChatMessage', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
