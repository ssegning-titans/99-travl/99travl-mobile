//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:ntravl_api/src/model/lat_lng.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'account_address.g.dart';

/// AccountAddress
///
/// Properties:
/// * [id] 
/// * [creationDate] 
/// * [phoneNumber] 
/// * [accountId] 
/// * [region] 
/// * [formattedName] 
/// * [street] 
/// * [houseNumber] 
/// * [city] 
/// * [zip] 
/// * [country] 
/// * [location] 
abstract class AccountAddress implements Built<AccountAddress, AccountAddressBuilder> {
    @BuiltValueField(wireName: r'id')
    String? get id;

    @BuiltValueField(wireName: r'creationDate')
    int? get creationDate;

    @BuiltValueField(wireName: r'phoneNumber')
    String? get phoneNumber;

    @BuiltValueField(wireName: r'accountId')
    String? get accountId;

    @BuiltValueField(wireName: r'region')
    String? get region;

    @BuiltValueField(wireName: r'formattedName')
    String? get formattedName;

    @BuiltValueField(wireName: r'street')
    String? get street;

    @BuiltValueField(wireName: r'houseNumber')
    String? get houseNumber;

    @BuiltValueField(wireName: r'city')
    String? get city;

    @BuiltValueField(wireName: r'zip')
    String? get zip;

    @BuiltValueField(wireName: r'country')
    String? get country;

    @BuiltValueField(wireName: r'location')
    LatLng? get location;

    AccountAddress._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(AccountAddressBuilder b) => b;

    factory AccountAddress([void updates(AccountAddressBuilder b)]) = _$AccountAddress;

    @BuiltValueSerializer(custom: true)
    static Serializer<AccountAddress> get serializer => _$AccountAddressSerializer();
}

class _$AccountAddressSerializer implements StructuredSerializer<AccountAddress> {
    @override
    final Iterable<Type> types = const [AccountAddress, _$AccountAddress];

    @override
    final String wireName = r'AccountAddress';

    @override
    Iterable<Object?> serialize(Serializers serializers, AccountAddress object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.id != null) {
            result
                ..add(r'id')
                ..add(serializers.serialize(object.id,
                    specifiedType: const FullType(String)));
        }
        if (object.creationDate != null) {
            result
                ..add(r'creationDate')
                ..add(serializers.serialize(object.creationDate,
                    specifiedType: const FullType(int)));
        }
        if (object.phoneNumber != null) {
            result
                ..add(r'phoneNumber')
                ..add(serializers.serialize(object.phoneNumber,
                    specifiedType: const FullType(String)));
        }
        if (object.accountId != null) {
            result
                ..add(r'accountId')
                ..add(serializers.serialize(object.accountId,
                    specifiedType: const FullType(String)));
        }
        if (object.region != null) {
            result
                ..add(r'region')
                ..add(serializers.serialize(object.region,
                    specifiedType: const FullType(String)));
        }
        if (object.formattedName != null) {
            result
                ..add(r'formattedName')
                ..add(serializers.serialize(object.formattedName,
                    specifiedType: const FullType(String)));
        }
        if (object.street != null) {
            result
                ..add(r'street')
                ..add(serializers.serialize(object.street,
                    specifiedType: const FullType(String)));
        }
        if (object.houseNumber != null) {
            result
                ..add(r'houseNumber')
                ..add(serializers.serialize(object.houseNumber,
                    specifiedType: const FullType(String)));
        }
        if (object.city != null) {
            result
                ..add(r'city')
                ..add(serializers.serialize(object.city,
                    specifiedType: const FullType(String)));
        }
        if (object.zip != null) {
            result
                ..add(r'zip')
                ..add(serializers.serialize(object.zip,
                    specifiedType: const FullType(String)));
        }
        if (object.country != null) {
            result
                ..add(r'country')
                ..add(serializers.serialize(object.country,
                    specifiedType: const FullType(String)));
        }
        if (object.location != null) {
            result
                ..add(r'location')
                ..add(serializers.serialize(object.location,
                    specifiedType: const FullType(LatLng)));
        }
        return result;
    }

    @override
    AccountAddress deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = AccountAddressBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'id':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.id = valueDes;
                    break;
                case r'creationDate':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    result.creationDate = valueDes;
                    break;
                case r'phoneNumber':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.phoneNumber = valueDes;
                    break;
                case r'accountId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.accountId = valueDes;
                    break;
                case r'region':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.region = valueDes;
                    break;
                case r'formattedName':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.formattedName = valueDes;
                    break;
                case r'street':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.street = valueDes;
                    break;
                case r'houseNumber':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.houseNumber = valueDes;
                    break;
                case r'city':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.city = valueDes;
                    break;
                case r'zip':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.zip = valueDes;
                    break;
                case r'country':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.country = valueDes;
                    break;
                case r'location':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(LatLng)) as LatLng;
                    result.location.replace(valueDes);
                    break;
            }
        }
        return result.build();
    }
}

