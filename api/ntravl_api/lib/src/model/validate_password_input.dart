//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'validate_password_input.g.dart';

/// ValidatePasswordInput
///
/// Properties:
/// * [challenge] 
abstract class ValidatePasswordInput implements Built<ValidatePasswordInput, ValidatePasswordInputBuilder> {
    @BuiltValueField(wireName: r'challenge')
    String get challenge;

    ValidatePasswordInput._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(ValidatePasswordInputBuilder b) => b;

    factory ValidatePasswordInput([void updates(ValidatePasswordInputBuilder b)]) = _$ValidatePasswordInput;

    @BuiltValueSerializer(custom: true)
    static Serializer<ValidatePasswordInput> get serializer => _$ValidatePasswordInputSerializer();
}

class _$ValidatePasswordInputSerializer implements StructuredSerializer<ValidatePasswordInput> {
    @override
    final Iterable<Type> types = const [ValidatePasswordInput, _$ValidatePasswordInput];

    @override
    final String wireName = r'ValidatePasswordInput';

    @override
    Iterable<Object?> serialize(Serializers serializers, ValidatePasswordInput object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'challenge')
            ..add(serializers.serialize(object.challenge,
                specifiedType: const FullType(String)));
        return result;
    }

    @override
    ValidatePasswordInput deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = ValidatePasswordInputBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'challenge':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.challenge = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

