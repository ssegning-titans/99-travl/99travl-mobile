//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'travel_weight_unit.g.dart';

class TravelWeightUnit extends EnumClass {

  @BuiltValueEnumConst(wireName: r'KG')
  static const TravelWeightUnit KG = _$KG;
  @BuiltValueEnumConst(wireName: r'Lb')
  static const TravelWeightUnit lb = _$lb;

  static Serializer<TravelWeightUnit> get serializer => _$travelWeightUnitSerializer;

  const TravelWeightUnit._(String name): super(name);

  static BuiltSet<TravelWeightUnit> get values => _$values;
  static TravelWeightUnit valueOf(String name) => _$valueOf(name);
}

/// Optionally, enum_class can generate a mixin to go with your enum for use
/// with Angular. It exposes your enum constants as getters. So, if you mix it
/// in to your Dart component class, the values become available to the
/// corresponding Angular template.
///
/// Trigger mixin generation by writing a line like this one next to your enum.
abstract class TravelWeightUnitMixin = Object with _$TravelWeightUnitMixin;

