//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'travel_days.g.dart';

class TravelDays extends EnumClass {

  @BuiltValueEnumConst(wireName: r'MONDAY')
  static const TravelDays MONDAY = _$MONDAY;
  @BuiltValueEnumConst(wireName: r'TUESDAY')
  static const TravelDays TUESDAY = _$TUESDAY;
  @BuiltValueEnumConst(wireName: r'WEDNESDAY')
  static const TravelDays WEDNESDAY = _$WEDNESDAY;
  @BuiltValueEnumConst(wireName: r'THURSDAY')
  static const TravelDays THURSDAY = _$THURSDAY;
  @BuiltValueEnumConst(wireName: r'FRIDAY')
  static const TravelDays FRIDAY = _$FRIDAY;
  @BuiltValueEnumConst(wireName: r'SATURDAY')
  static const TravelDays SATURDAY = _$SATURDAY;
  @BuiltValueEnumConst(wireName: r'SUNDAY')
  static const TravelDays SUNDAY = _$SUNDAY;

  static Serializer<TravelDays> get serializer => _$travelDaysSerializer;

  const TravelDays._(String name): super(name);

  static BuiltSet<TravelDays> get values => _$values;
  static TravelDays valueOf(String name) => _$valueOf(name);
}

/// Optionally, enum_class can generate a mixin to go with your enum for use
/// with Angular. It exposes your enum constants as getters. So, if you mix it
/// in to your Dart component class, the values become available to the
/// corresponding Angular template.
///
/// Trigger mixin generation by writing a line like this one next to your enum.
abstract class TravelDaysMixin = Object with _$TravelDaysMixin;

