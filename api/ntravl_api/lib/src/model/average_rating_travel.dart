//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'average_rating_travel.g.dart';

/// AverageRatingTravel
///
/// Properties:
/// * [value] 
/// * [count] 
/// * [travelId] 
abstract class AverageRatingTravel implements Built<AverageRatingTravel, AverageRatingTravelBuilder> {
    @BuiltValueField(wireName: r'value')
    double get value;

    @BuiltValueField(wireName: r'count')
    int get count;

    @BuiltValueField(wireName: r'travelId')
    String? get travelId;

    AverageRatingTravel._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(AverageRatingTravelBuilder b) => b;

    factory AverageRatingTravel([void updates(AverageRatingTravelBuilder b)]) = _$AverageRatingTravel;

    @BuiltValueSerializer(custom: true)
    static Serializer<AverageRatingTravel> get serializer => _$AverageRatingTravelSerializer();
}

class _$AverageRatingTravelSerializer implements StructuredSerializer<AverageRatingTravel> {
    @override
    final Iterable<Type> types = const [AverageRatingTravel, _$AverageRatingTravel];

    @override
    final String wireName = r'AverageRatingTravel';

    @override
    Iterable<Object?> serialize(Serializers serializers, AverageRatingTravel object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'value')
            ..add(serializers.serialize(object.value,
                specifiedType: const FullType(double)));
        result
            ..add(r'count')
            ..add(serializers.serialize(object.count,
                specifiedType: const FullType(int)));
        if (object.travelId != null) {
            result
                ..add(r'travelId')
                ..add(serializers.serialize(object.travelId,
                    specifiedType: const FullType(String)));
        }
        return result;
    }

    @override
    AverageRatingTravel deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = AverageRatingTravelBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'value':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(double)) as double;
                    result.value = valueDes;
                    break;
                case r'count':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    result.count = valueDes;
                    break;
                case r'travelId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.travelId = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

