// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_friend_status.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$UpdateFriendStatus extends UpdateFriendStatus {
  @override
  final FriendShipStatus? status;

  factory _$UpdateFriendStatus(
          [void Function(UpdateFriendStatusBuilder)? updates]) =>
      (new UpdateFriendStatusBuilder()..update(updates)).build();

  _$UpdateFriendStatus._({this.status}) : super._();

  @override
  UpdateFriendStatus rebuild(
          void Function(UpdateFriendStatusBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UpdateFriendStatusBuilder toBuilder() =>
      new UpdateFriendStatusBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UpdateFriendStatus && status == other.status;
  }

  @override
  int get hashCode {
    return $jf($jc(0, status.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UpdateFriendStatus')
          ..add('status', status))
        .toString();
  }
}

class UpdateFriendStatusBuilder
    implements Builder<UpdateFriendStatus, UpdateFriendStatusBuilder> {
  _$UpdateFriendStatus? _$v;

  FriendShipStatus? _status;
  FriendShipStatus? get status => _$this._status;
  set status(FriendShipStatus? status) => _$this._status = status;

  UpdateFriendStatusBuilder() {
    UpdateFriendStatus._defaults(this);
  }

  UpdateFriendStatusBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _status = $v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UpdateFriendStatus other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$UpdateFriendStatus;
  }

  @override
  void update(void Function(UpdateFriendStatusBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$UpdateFriendStatus build() {
    final _$result = _$v ?? new _$UpdateFriendStatus._(status: status);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
