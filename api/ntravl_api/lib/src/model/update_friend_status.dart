//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:ntravl_api/src/model/friend_ship_status.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'update_friend_status.g.dart';

/// UpdateFriendStatus
///
/// Properties:
/// * [status] 
abstract class UpdateFriendStatus implements Built<UpdateFriendStatus, UpdateFriendStatusBuilder> {
    @BuiltValueField(wireName: r'status')
    FriendShipStatus? get status;
    // enum statusEnum {  ACCEPTED,  REFUSED,  PENDING,  };

    UpdateFriendStatus._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(UpdateFriendStatusBuilder b) => b;

    factory UpdateFriendStatus([void updates(UpdateFriendStatusBuilder b)]) = _$UpdateFriendStatus;

    @BuiltValueSerializer(custom: true)
    static Serializer<UpdateFriendStatus> get serializer => _$UpdateFriendStatusSerializer();
}

class _$UpdateFriendStatusSerializer implements StructuredSerializer<UpdateFriendStatus> {
    @override
    final Iterable<Type> types = const [UpdateFriendStatus, _$UpdateFriendStatus];

    @override
    final String wireName = r'UpdateFriendStatus';

    @override
    Iterable<Object?> serialize(Serializers serializers, UpdateFriendStatus object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.status != null) {
            result
                ..add(r'status')
                ..add(serializers.serialize(object.status,
                    specifiedType: const FullType(FriendShipStatus)));
        }
        return result;
    }

    @override
    UpdateFriendStatus deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = UpdateFriendStatusBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'status':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(FriendShipStatus)) as FriendShipStatus;
                    result.status = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

