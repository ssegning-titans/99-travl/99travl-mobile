//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:ntravl_api/src/model/travel_quantity.dart';
import 'package:ntravl_api/src/model/travel_luggage_dimension.dart';
import 'package:built_collection/built_collection.dart';
import 'package:ntravl_api/src/model/travel_point.dart';
import 'package:ntravl_api/src/model/travel_mode_unit.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'travel.g.dart';

/// Travel
///
/// Properties:
/// * [createdAt] 
/// * [updatedAt] 
/// * [id] 
/// * [passagePoints] 
/// * [providerAccountId] 
/// * [maxQuantity] 
/// * [transportMode] 
/// * [luggageDimension] 
/// * [description] 
abstract class Travel implements Built<Travel, TravelBuilder> {
    @BuiltValueField(wireName: r'createdAt')
    int? get createdAt;

    @BuiltValueField(wireName: r'updatedAt')
    int? get updatedAt;

    @BuiltValueField(wireName: r'id')
    String? get id;

    @BuiltValueField(wireName: r'passagePoints')
    BuiltList<TravelPoint> get passagePoints;

    @BuiltValueField(wireName: r'providerAccountId')
    String get providerAccountId;

    @BuiltValueField(wireName: r'maxQuantity')
    TravelQuantity get maxQuantity;

    @BuiltValueField(wireName: r'transportMode')
    TravelModeUnit? get transportMode;
    // enum transportModeEnum {  PLANE,  CAR,  BOAT,  };

    @BuiltValueField(wireName: r'luggageDimension')
    TravelLuggageDimension get luggageDimension;
    // enum luggageDimensionEnum {  S,  M,  L,  XL,  };

    @BuiltValueField(wireName: r'description')
    String? get description;

    Travel._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(TravelBuilder b) => b;

    factory Travel([void updates(TravelBuilder b)]) = _$Travel;

    @BuiltValueSerializer(custom: true)
    static Serializer<Travel> get serializer => _$TravelSerializer();
}

class _$TravelSerializer implements StructuredSerializer<Travel> {
    @override
    final Iterable<Type> types = const [Travel, _$Travel];

    @override
    final String wireName = r'Travel';

    @override
    Iterable<Object?> serialize(Serializers serializers, Travel object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.createdAt != null) {
            result
                ..add(r'createdAt')
                ..add(serializers.serialize(object.createdAt,
                    specifiedType: const FullType(int)));
        }
        if (object.updatedAt != null) {
            result
                ..add(r'updatedAt')
                ..add(serializers.serialize(object.updatedAt,
                    specifiedType: const FullType(int)));
        }
        if (object.id != null) {
            result
                ..add(r'id')
                ..add(serializers.serialize(object.id,
                    specifiedType: const FullType(String)));
        }
        result
            ..add(r'passagePoints')
            ..add(serializers.serialize(object.passagePoints,
                specifiedType: const FullType(BuiltList, [FullType(TravelPoint)])));
        result
            ..add(r'providerAccountId')
            ..add(serializers.serialize(object.providerAccountId,
                specifiedType: const FullType(String)));
        result
            ..add(r'maxQuantity')
            ..add(serializers.serialize(object.maxQuantity,
                specifiedType: const FullType(TravelQuantity)));
        if (object.transportMode != null) {
            result
                ..add(r'transportMode')
                ..add(serializers.serialize(object.transportMode,
                    specifiedType: const FullType(TravelModeUnit)));
        }
        result
            ..add(r'luggageDimension')
            ..add(serializers.serialize(object.luggageDimension,
                specifiedType: const FullType(TravelLuggageDimension)));
        if (object.description != null) {
            result
                ..add(r'description')
                ..add(serializers.serialize(object.description,
                    specifiedType: const FullType(String)));
        }
        return result;
    }

    @override
    Travel deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = TravelBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'createdAt':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    result.createdAt = valueDes;
                    break;
                case r'updatedAt':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    result.updatedAt = valueDes;
                    break;
                case r'id':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.id = valueDes;
                    break;
                case r'passagePoints':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(BuiltList, [FullType(TravelPoint)])) as BuiltList<TravelPoint>;
                    result.passagePoints.replace(valueDes);
                    break;
                case r'providerAccountId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.providerAccountId = valueDes;
                    break;
                case r'maxQuantity':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(TravelQuantity)) as TravelQuantity;
                    result.maxQuantity.replace(valueDes);
                    break;
                case r'transportMode':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(TravelModeUnit)) as TravelModeUnit;
                    result.transportMode = valueDes;
                    break;
                case r'luggageDimension':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(TravelLuggageDimension)) as TravelLuggageDimension;
                    result.luggageDimension = valueDes;
                    break;
                case r'description':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.description = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

