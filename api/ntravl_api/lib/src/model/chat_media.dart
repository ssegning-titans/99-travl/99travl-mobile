//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'chat_media.g.dart';

/// ChatMedia
///
/// Properties:
/// * [id] 
/// * [creationDate] 
/// * [documentId] 
/// * [path] 
/// * [name] 
/// * [description] 
/// * [metaData] 
/// * [width] 
/// * [height] 
/// * [priority] 
abstract class ChatMedia implements Built<ChatMedia, ChatMediaBuilder> {
    @BuiltValueField(wireName: r'id')
    String? get id;

    @BuiltValueField(wireName: r'creationDate')
    int? get creationDate;

    @BuiltValueField(wireName: r'documentId')
    String? get documentId;

    @BuiltValueField(wireName: r'path')
    String? get path;

    @BuiltValueField(wireName: r'name')
    String? get name;

    @BuiltValueField(wireName: r'description')
    String? get description;

    @BuiltValueField(wireName: r'metaData')
    BuiltMap<String, String>? get metaData;

    @BuiltValueField(wireName: r'width')
    int get width;

    @BuiltValueField(wireName: r'height')
    int get height;

    @BuiltValueField(wireName: r'priority')
    int? get priority;

    ChatMedia._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(ChatMediaBuilder b) => b
        ..width = 1
        ..height = 1
        ..priority = 1;

    factory ChatMedia([void updates(ChatMediaBuilder b)]) = _$ChatMedia;

    @BuiltValueSerializer(custom: true)
    static Serializer<ChatMedia> get serializer => _$ChatMediaSerializer();
}

class _$ChatMediaSerializer implements StructuredSerializer<ChatMedia> {
    @override
    final Iterable<Type> types = const [ChatMedia, _$ChatMedia];

    @override
    final String wireName = r'ChatMedia';

    @override
    Iterable<Object?> serialize(Serializers serializers, ChatMedia object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.id != null) {
            result
                ..add(r'id')
                ..add(serializers.serialize(object.id,
                    specifiedType: const FullType(String)));
        }
        if (object.creationDate != null) {
            result
                ..add(r'creationDate')
                ..add(serializers.serialize(object.creationDate,
                    specifiedType: const FullType(int)));
        }
        if (object.documentId != null) {
            result
                ..add(r'documentId')
                ..add(serializers.serialize(object.documentId,
                    specifiedType: const FullType(String)));
        }
        if (object.path != null) {
            result
                ..add(r'path')
                ..add(serializers.serialize(object.path,
                    specifiedType: const FullType(String)));
        }
        if (object.name != null) {
            result
                ..add(r'name')
                ..add(serializers.serialize(object.name,
                    specifiedType: const FullType(String)));
        }
        if (object.description != null) {
            result
                ..add(r'description')
                ..add(serializers.serialize(object.description,
                    specifiedType: const FullType(String)));
        }
        if (object.metaData != null) {
            result
                ..add(r'metaData')
                ..add(serializers.serialize(object.metaData,
                    specifiedType: const FullType(BuiltMap, [FullType(String), FullType(String)])));
        }
        result
            ..add(r'width')
            ..add(serializers.serialize(object.width,
                specifiedType: const FullType(int)));
        result
            ..add(r'height')
            ..add(serializers.serialize(object.height,
                specifiedType: const FullType(int)));
        if (object.priority != null) {
            result
                ..add(r'priority')
                ..add(serializers.serialize(object.priority,
                    specifiedType: const FullType(int)));
        }
        return result;
    }

    @override
    ChatMedia deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = ChatMediaBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'id':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.id = valueDes;
                    break;
                case r'creationDate':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    result.creationDate = valueDes;
                    break;
                case r'documentId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.documentId = valueDes;
                    break;
                case r'path':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.path = valueDes;
                    break;
                case r'name':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.name = valueDes;
                    break;
                case r'description':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.description = valueDes;
                    break;
                case r'metaData':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(BuiltMap, [FullType(String), FullType(String)])) as BuiltMap<String, String>;
                    result.metaData.replace(valueDes);
                    break;
                case r'width':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    result.width = valueDes;
                    break;
                case r'height':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    result.height = valueDes;
                    break;
                case r'priority':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    result.priority = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

