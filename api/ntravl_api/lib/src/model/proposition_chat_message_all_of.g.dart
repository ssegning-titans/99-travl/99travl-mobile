// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'proposition_chat_message_all_of.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$PropositionChatMessageAllOf extends PropositionChatMessageAllOf {
  @override
  final TravelProposition? proposition;

  factory _$PropositionChatMessageAllOf(
          [void Function(PropositionChatMessageAllOfBuilder)? updates]) =>
      (new PropositionChatMessageAllOfBuilder()..update(updates)).build();

  _$PropositionChatMessageAllOf._({this.proposition}) : super._();

  @override
  PropositionChatMessageAllOf rebuild(
          void Function(PropositionChatMessageAllOfBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PropositionChatMessageAllOfBuilder toBuilder() =>
      new PropositionChatMessageAllOfBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PropositionChatMessageAllOf &&
        proposition == other.proposition;
  }

  @override
  int get hashCode {
    return $jf($jc(0, proposition.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PropositionChatMessageAllOf')
          ..add('proposition', proposition))
        .toString();
  }
}

class PropositionChatMessageAllOfBuilder
    implements
        Builder<PropositionChatMessageAllOf,
            PropositionChatMessageAllOfBuilder> {
  _$PropositionChatMessageAllOf? _$v;

  TravelPropositionBuilder? _proposition;
  TravelPropositionBuilder get proposition =>
      _$this._proposition ??= new TravelPropositionBuilder();
  set proposition(TravelPropositionBuilder? proposition) =>
      _$this._proposition = proposition;

  PropositionChatMessageAllOfBuilder() {
    PropositionChatMessageAllOf._defaults(this);
  }

  PropositionChatMessageAllOfBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _proposition = $v.proposition?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PropositionChatMessageAllOf other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$PropositionChatMessageAllOf;
  }

  @override
  void update(void Function(PropositionChatMessageAllOfBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PropositionChatMessageAllOf build() {
    _$PropositionChatMessageAllOf _$result;
    try {
      _$result = _$v ??
          new _$PropositionChatMessageAllOf._(
              proposition: _proposition?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'proposition';
        _proposition?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'PropositionChatMessageAllOf', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
