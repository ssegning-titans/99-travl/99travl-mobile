//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'report_travel_all_of.g.dart';

/// ReportTravelAllOf
///
/// Properties:
/// * [travelId] 
abstract class ReportTravelAllOf implements Built<ReportTravelAllOf, ReportTravelAllOfBuilder> {
    @BuiltValueField(wireName: r'travelId')
    String get travelId;

    ReportTravelAllOf._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(ReportTravelAllOfBuilder b) => b;

    factory ReportTravelAllOf([void updates(ReportTravelAllOfBuilder b)]) = _$ReportTravelAllOf;

    @BuiltValueSerializer(custom: true)
    static Serializer<ReportTravelAllOf> get serializer => _$ReportTravelAllOfSerializer();
}

class _$ReportTravelAllOfSerializer implements StructuredSerializer<ReportTravelAllOf> {
    @override
    final Iterable<Type> types = const [ReportTravelAllOf, _$ReportTravelAllOf];

    @override
    final String wireName = r'ReportTravelAllOf';

    @override
    Iterable<Object?> serialize(Serializers serializers, ReportTravelAllOf object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'travelId')
            ..add(serializers.serialize(object.travelId,
                specifiedType: const FullType(String)));
        return result;
    }

    @override
    ReportTravelAllOf deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = ReportTravelAllOfBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'travelId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.travelId = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

