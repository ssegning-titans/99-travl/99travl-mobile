// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'travel_weight_unit.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const TravelWeightUnit _$KG = const TravelWeightUnit._('KG');
const TravelWeightUnit _$lb = const TravelWeightUnit._('lb');

TravelWeightUnit _$valueOf(String name) {
  switch (name) {
    case 'KG':
      return _$KG;
    case 'lb':
      return _$lb;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<TravelWeightUnit> _$values =
    new BuiltSet<TravelWeightUnit>(const <TravelWeightUnit>[
  _$KG,
  _$lb,
]);

class _$TravelWeightUnitMeta {
  const _$TravelWeightUnitMeta();
  TravelWeightUnit get KG => _$KG;
  TravelWeightUnit get lb => _$lb;
  TravelWeightUnit valueOf(String name) => _$valueOf(name);
  BuiltSet<TravelWeightUnit> get values => _$values;
}

abstract class _$TravelWeightUnitMixin {
  // ignore: non_constant_identifier_names
  _$TravelWeightUnitMeta get TravelWeightUnit => const _$TravelWeightUnitMeta();
}

Serializer<TravelWeightUnit> _$travelWeightUnitSerializer =
    new _$TravelWeightUnitSerializer();

class _$TravelWeightUnitSerializer
    implements PrimitiveSerializer<TravelWeightUnit> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'KG': 'KG',
    'lb': 'Lb',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'KG': 'KG',
    'Lb': 'lb',
  };

  @override
  final Iterable<Type> types = const <Type>[TravelWeightUnit];
  @override
  final String wireName = 'TravelWeightUnit';

  @override
  Object serialize(Serializers serializers, TravelWeightUnit object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  TravelWeightUnit deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      TravelWeightUnit.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
