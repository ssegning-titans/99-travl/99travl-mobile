//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:ntravl_api/src/model/bug_media.dart';
import 'package:ntravl_api/src/model/base_report.dart';
import 'package:ntravl_api/src/model/report_type.dart';
import 'package:ntravl_api/src/model/create_report_bug.dart';
import 'package:ntravl_api/src/model/report_status.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'report_bug.g.dart';

// ignore_for_file: unused_import

/// ReportBug
///
/// Properties:
/// * [reportType] 
/// * [authorId] 
/// * [status] 
/// * [createdAt] 
/// * [id] 
/// * [description] 
/// * [title] 
/// * [deviceInfo] 
/// * [screenshot] 
abstract class ReportBug implements Built<ReportBug, ReportBugBuilder> {
    @BuiltValueField(wireName: r'reportType')
    ReportType get reportType;
    // enum reportTypeEnum {  TRAVEL,  ACCOUNT,  BUG,  };

    @BuiltValueField(wireName: r'authorId')
    String get authorId;

    @BuiltValueField(wireName: r'status')
    ReportStatus get status;
    // enum statusEnum {  PENDING,  HANDLED,  IN_PROGRESS,  };

    @BuiltValueField(wireName: r'createdAt')
    int? get createdAt;

    @BuiltValueField(wireName: r'id')
    String? get id;

    @BuiltValueField(wireName: r'description')
    String? get description;

    @BuiltValueField(wireName: r'title')
    String get title;

    @BuiltValueField(wireName: r'deviceInfo')
    BuiltMap<String, String> get deviceInfo;

    @BuiltValueField(wireName: r'screenshot')
    BuiltList<BugMedia> get screenshot;

    ReportBug._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(ReportBugBuilder b) => b;

    factory ReportBug([void updates(ReportBugBuilder b)]) = _$ReportBug;

    @BuiltValueSerializer(custom: true)
    static Serializer<ReportBug> get serializer => _$ReportBugSerializer();
}

class _$ReportBugSerializer implements StructuredSerializer<ReportBug> {
    @override
    final Iterable<Type> types = const [ReportBug, _$ReportBug];

    @override
    final String wireName = r'ReportBug';

    @override
    Iterable<Object?> serialize(Serializers serializers, ReportBug object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'reportType')
            ..add(serializers.serialize(object.reportType,
                specifiedType: const FullType(ReportType)));
        result
            ..add(r'authorId')
            ..add(serializers.serialize(object.authorId,
                specifiedType: const FullType(String)));
        result
            ..add(r'status')
            ..add(serializers.serialize(object.status,
                specifiedType: const FullType(ReportStatus)));
        if (object.createdAt != null) {
            result
                ..add(r'createdAt')
                ..add(serializers.serialize(object.createdAt,
                    specifiedType: const FullType(int)));
        }
        if (object.id != null) {
            result
                ..add(r'id')
                ..add(serializers.serialize(object.id,
                    specifiedType: const FullType(String)));
        }
        if (object.description != null) {
            result
                ..add(r'description')
                ..add(serializers.serialize(object.description,
                    specifiedType: const FullType(String)));
        }
        result
            ..add(r'title')
            ..add(serializers.serialize(object.title,
                specifiedType: const FullType(String)));
        result
            ..add(r'deviceInfo')
            ..add(serializers.serialize(object.deviceInfo,
                specifiedType: const FullType(BuiltMap, [FullType(String), FullType(String)])));
        result
            ..add(r'screenshot')
            ..add(serializers.serialize(object.screenshot,
                specifiedType: const FullType(BuiltList, [FullType(BugMedia)])));
        return result;
    }

    @override
    ReportBug deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = ReportBugBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'reportType':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(ReportType)) as ReportType;
                    result.reportType = valueDes;
                    break;
                case r'authorId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.authorId = valueDes;
                    break;
                case r'status':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(ReportStatus)) as ReportStatus;
                    result.status = valueDes;
                    break;
                case r'createdAt':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    result.createdAt = valueDes;
                    break;
                case r'id':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.id = valueDes;
                    break;
                case r'description':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.description = valueDes;
                    break;
                case r'title':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.title = valueDes;
                    break;
                case r'deviceInfo':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(BuiltMap, [FullType(String), FullType(String)])) as BuiltMap<String, String>;
                    result.deviceInfo.replace(valueDes);
                    break;
                case r'screenshot':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(BuiltList, [FullType(BugMedia)])) as BuiltList<BugMedia>;
                    result.screenshot.replace(valueDes);
                    break;
            }
        }
        return result.build();
    }
}

