//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'create_report_travel.g.dart';

/// CreateReportTravel
///
/// Properties:
/// * [authorId] 
/// * [travelId] 
/// * [description] 
abstract class CreateReportTravel implements Built<CreateReportTravel, CreateReportTravelBuilder> {
    @BuiltValueField(wireName: r'authorId')
    String get authorId;

    @BuiltValueField(wireName: r'travelId')
    String get travelId;

    @BuiltValueField(wireName: r'description')
    String? get description;

    CreateReportTravel._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(CreateReportTravelBuilder b) => b;

    factory CreateReportTravel([void updates(CreateReportTravelBuilder b)]) = _$CreateReportTravel;

    @BuiltValueSerializer(custom: true)
    static Serializer<CreateReportTravel> get serializer => _$CreateReportTravelSerializer();
}

class _$CreateReportTravelSerializer implements StructuredSerializer<CreateReportTravel> {
    @override
    final Iterable<Type> types = const [CreateReportTravel, _$CreateReportTravel];

    @override
    final String wireName = r'CreateReportTravel';

    @override
    Iterable<Object?> serialize(Serializers serializers, CreateReportTravel object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'authorId')
            ..add(serializers.serialize(object.authorId,
                specifiedType: const FullType(String)));
        result
            ..add(r'travelId')
            ..add(serializers.serialize(object.travelId,
                specifiedType: const FullType(String)));
        if (object.description != null) {
            result
                ..add(r'description')
                ..add(serializers.serialize(object.description,
                    specifiedType: const FullType(String)));
        }
        return result;
    }

    @override
    CreateReportTravel deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = CreateReportTravelBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'authorId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.authorId = valueDes;
                    break;
                case r'travelId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.travelId = valueDes;
                    break;
                case r'description':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.description = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

