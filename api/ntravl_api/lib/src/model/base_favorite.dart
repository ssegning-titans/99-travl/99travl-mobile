//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:ntravl_api/src/model/favorite_type.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'base_favorite.g.dart';

/// BaseFavorite
///
/// Properties:
/// * [id] 
/// * [ownerId] 
/// * [createdAt] 
/// * [favoriteType] 
abstract class BaseFavorite implements Built<BaseFavorite, BaseFavoriteBuilder> {
    @BuiltValueField(wireName: r'id')
    String? get id;

    @BuiltValueField(wireName: r'ownerId')
    String get ownerId;

    @BuiltValueField(wireName: r'createdAt')
    int? get createdAt;

    @BuiltValueField(wireName: r'favoriteType')
    FavoriteType get favoriteType;
    // enum favoriteTypeEnum {  TRAVEL,  ACCOUNT,  };

    BaseFavorite._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(BaseFavoriteBuilder b) => b;

    factory BaseFavorite([void updates(BaseFavoriteBuilder b)]) = _$BaseFavorite;

    @BuiltValueSerializer(custom: true)
    static Serializer<BaseFavorite> get serializer => _$BaseFavoriteSerializer();
}

class _$BaseFavoriteSerializer implements StructuredSerializer<BaseFavorite> {
    @override
    final Iterable<Type> types = const [BaseFavorite, _$BaseFavorite];

    @override
    final String wireName = r'BaseFavorite';

    @override
    Iterable<Object?> serialize(Serializers serializers, BaseFavorite object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.id != null) {
            result
                ..add(r'id')
                ..add(serializers.serialize(object.id,
                    specifiedType: const FullType(String)));
        }
        result
            ..add(r'ownerId')
            ..add(serializers.serialize(object.ownerId,
                specifiedType: const FullType(String)));
        if (object.createdAt != null) {
            result
                ..add(r'createdAt')
                ..add(serializers.serialize(object.createdAt,
                    specifiedType: const FullType(int)));
        }
        result
            ..add(r'favoriteType')
            ..add(serializers.serialize(object.favoriteType,
                specifiedType: const FullType(FavoriteType)));
        return result;
    }

    @override
    BaseFavorite deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = BaseFavoriteBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'id':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.id = valueDes;
                    break;
                case r'ownerId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.ownerId = valueDes;
                    break;
                case r'createdAt':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    result.createdAt = valueDes;
                    break;
                case r'favoriteType':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(FavoriteType)) as FavoriteType;
                    result.favoriteType = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

