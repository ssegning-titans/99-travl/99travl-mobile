// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'media_chat_message.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MediaChatMessage extends MediaChatMessage {
  @override
  final String? id;
  @override
  final int? createdAt;
  @override
  final String authorId;
  @override
  final String chatId;
  @override
  final ChatMessageType messageType;
  @override
  final bool? seen;
  @override
  final BuiltSet<ChatMedia> medias;

  factory _$MediaChatMessage(
          [void Function(MediaChatMessageBuilder)? updates]) =>
      (new MediaChatMessageBuilder()..update(updates)).build();

  _$MediaChatMessage._(
      {this.id,
      this.createdAt,
      required this.authorId,
      required this.chatId,
      required this.messageType,
      this.seen,
      required this.medias})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        authorId, 'MediaChatMessage', 'authorId');
    BuiltValueNullFieldError.checkNotNull(chatId, 'MediaChatMessage', 'chatId');
    BuiltValueNullFieldError.checkNotNull(
        messageType, 'MediaChatMessage', 'messageType');
    BuiltValueNullFieldError.checkNotNull(medias, 'MediaChatMessage', 'medias');
  }

  @override
  MediaChatMessage rebuild(void Function(MediaChatMessageBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MediaChatMessageBuilder toBuilder() =>
      new MediaChatMessageBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MediaChatMessage &&
        id == other.id &&
        createdAt == other.createdAt &&
        authorId == other.authorId &&
        chatId == other.chatId &&
        messageType == other.messageType &&
        seen == other.seen &&
        medias == other.medias;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, id.hashCode), createdAt.hashCode),
                        authorId.hashCode),
                    chatId.hashCode),
                messageType.hashCode),
            seen.hashCode),
        medias.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('MediaChatMessage')
          ..add('id', id)
          ..add('createdAt', createdAt)
          ..add('authorId', authorId)
          ..add('chatId', chatId)
          ..add('messageType', messageType)
          ..add('seen', seen)
          ..add('medias', medias))
        .toString();
  }
}

class MediaChatMessageBuilder
    implements Builder<MediaChatMessage, MediaChatMessageBuilder> {
  _$MediaChatMessage? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  int? _createdAt;
  int? get createdAt => _$this._createdAt;
  set createdAt(int? createdAt) => _$this._createdAt = createdAt;

  String? _authorId;
  String? get authorId => _$this._authorId;
  set authorId(String? authorId) => _$this._authorId = authorId;

  String? _chatId;
  String? get chatId => _$this._chatId;
  set chatId(String? chatId) => _$this._chatId = chatId;

  ChatMessageType? _messageType;
  ChatMessageType? get messageType => _$this._messageType;
  set messageType(ChatMessageType? messageType) =>
      _$this._messageType = messageType;

  bool? _seen;
  bool? get seen => _$this._seen;
  set seen(bool? seen) => _$this._seen = seen;

  SetBuilder<ChatMedia>? _medias;
  SetBuilder<ChatMedia> get medias =>
      _$this._medias ??= new SetBuilder<ChatMedia>();
  set medias(SetBuilder<ChatMedia>? medias) => _$this._medias = medias;

  MediaChatMessageBuilder() {
    MediaChatMessage._defaults(this);
  }

  MediaChatMessageBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _createdAt = $v.createdAt;
      _authorId = $v.authorId;
      _chatId = $v.chatId;
      _messageType = $v.messageType;
      _seen = $v.seen;
      _medias = $v.medias.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MediaChatMessage other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MediaChatMessage;
  }

  @override
  void update(void Function(MediaChatMessageBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$MediaChatMessage build() {
    _$MediaChatMessage _$result;
    try {
      _$result = _$v ??
          new _$MediaChatMessage._(
              id: id,
              createdAt: createdAt,
              authorId: BuiltValueNullFieldError.checkNotNull(
                  authorId, 'MediaChatMessage', 'authorId'),
              chatId: BuiltValueNullFieldError.checkNotNull(
                  chatId, 'MediaChatMessage', 'chatId'),
              messageType: BuiltValueNullFieldError.checkNotNull(
                  messageType, 'MediaChatMessage', 'messageType'),
              seen: seen,
              medias: medias.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'medias';
        medias.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'MediaChatMessage', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
