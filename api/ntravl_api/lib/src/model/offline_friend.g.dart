// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'offline_friend.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$OfflineFriend extends OfflineFriend {
  @override
  final String id;
  @override
  final int creationDate;
  @override
  final FriendShipStatus? status;
  @override
  final FriendType? type;
  @override
  final FriendProcessType processType;
  @override
  final String? phoneNumber;
  @override
  final String? description;
  @override
  final String? firstName;
  @override
  final String? lastName;
  @override
  final String? avatar;

  factory _$OfflineFriend([void Function(OfflineFriendBuilder)? updates]) =>
      (new OfflineFriendBuilder()..update(updates)).build();

  _$OfflineFriend._(
      {required this.id,
      required this.creationDate,
      this.status,
      this.type,
      required this.processType,
      this.phoneNumber,
      this.description,
      this.firstName,
      this.lastName,
      this.avatar})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(id, 'OfflineFriend', 'id');
    BuiltValueNullFieldError.checkNotNull(
        creationDate, 'OfflineFriend', 'creationDate');
    BuiltValueNullFieldError.checkNotNull(
        processType, 'OfflineFriend', 'processType');
  }

  @override
  OfflineFriend rebuild(void Function(OfflineFriendBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  OfflineFriendBuilder toBuilder() => new OfflineFriendBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is OfflineFriend &&
        id == other.id &&
        creationDate == other.creationDate &&
        status == other.status &&
        type == other.type &&
        processType == other.processType &&
        phoneNumber == other.phoneNumber &&
        description == other.description &&
        firstName == other.firstName &&
        lastName == other.lastName &&
        avatar == other.avatar;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc($jc(0, id.hashCode),
                                        creationDate.hashCode),
                                    status.hashCode),
                                type.hashCode),
                            processType.hashCode),
                        phoneNumber.hashCode),
                    description.hashCode),
                firstName.hashCode),
            lastName.hashCode),
        avatar.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('OfflineFriend')
          ..add('id', id)
          ..add('creationDate', creationDate)
          ..add('status', status)
          ..add('type', type)
          ..add('processType', processType)
          ..add('phoneNumber', phoneNumber)
          ..add('description', description)
          ..add('firstName', firstName)
          ..add('lastName', lastName)
          ..add('avatar', avatar))
        .toString();
  }
}

class OfflineFriendBuilder
    implements Builder<OfflineFriend, OfflineFriendBuilder> {
  _$OfflineFriend? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  int? _creationDate;
  int? get creationDate => _$this._creationDate;
  set creationDate(int? creationDate) => _$this._creationDate = creationDate;

  FriendShipStatus? _status;
  FriendShipStatus? get status => _$this._status;
  set status(FriendShipStatus? status) => _$this._status = status;

  FriendType? _type;
  FriendType? get type => _$this._type;
  set type(FriendType? type) => _$this._type = type;

  FriendProcessType? _processType;
  FriendProcessType? get processType => _$this._processType;
  set processType(FriendProcessType? processType) =>
      _$this._processType = processType;

  String? _phoneNumber;
  String? get phoneNumber => _$this._phoneNumber;
  set phoneNumber(String? phoneNumber) => _$this._phoneNumber = phoneNumber;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  String? _firstName;
  String? get firstName => _$this._firstName;
  set firstName(String? firstName) => _$this._firstName = firstName;

  String? _lastName;
  String? get lastName => _$this._lastName;
  set lastName(String? lastName) => _$this._lastName = lastName;

  String? _avatar;
  String? get avatar => _$this._avatar;
  set avatar(String? avatar) => _$this._avatar = avatar;

  OfflineFriendBuilder() {
    OfflineFriend._defaults(this);
  }

  OfflineFriendBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _creationDate = $v.creationDate;
      _status = $v.status;
      _type = $v.type;
      _processType = $v.processType;
      _phoneNumber = $v.phoneNumber;
      _description = $v.description;
      _firstName = $v.firstName;
      _lastName = $v.lastName;
      _avatar = $v.avatar;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(OfflineFriend other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$OfflineFriend;
  }

  @override
  void update(void Function(OfflineFriendBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$OfflineFriend build() {
    final _$result = _$v ??
        new _$OfflineFriend._(
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'OfflineFriend', 'id'),
            creationDate: BuiltValueNullFieldError.checkNotNull(
                creationDate, 'OfflineFriend', 'creationDate'),
            status: status,
            type: type,
            processType: BuiltValueNullFieldError.checkNotNull(
                processType, 'OfflineFriend', 'processType'),
            phoneNumber: phoneNumber,
            description: description,
            firstName: firstName,
            lastName: lastName,
            avatar: avatar);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
