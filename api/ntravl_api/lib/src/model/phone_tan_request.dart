//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'phone_tan_request.g.dart';

/// PhoneTanRequest
///
/// Properties:
/// * [phoneNumber] 
abstract class PhoneTanRequest implements Built<PhoneTanRequest, PhoneTanRequestBuilder> {
    @BuiltValueField(wireName: r'phoneNumber')
    String? get phoneNumber;

    PhoneTanRequest._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(PhoneTanRequestBuilder b) => b;

    factory PhoneTanRequest([void updates(PhoneTanRequestBuilder b)]) = _$PhoneTanRequest;

    @BuiltValueSerializer(custom: true)
    static Serializer<PhoneTanRequest> get serializer => _$PhoneTanRequestSerializer();
}

class _$PhoneTanRequestSerializer implements StructuredSerializer<PhoneTanRequest> {
    @override
    final Iterable<Type> types = const [PhoneTanRequest, _$PhoneTanRequest];

    @override
    final String wireName = r'PhoneTanRequest';

    @override
    Iterable<Object?> serialize(Serializers serializers, PhoneTanRequest object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.phoneNumber != null) {
            result
                ..add(r'phoneNumber')
                ..add(serializers.serialize(object.phoneNumber,
                    specifiedType: const FullType(String)));
        }
        return result;
    }

    @override
    PhoneTanRequest deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = PhoneTanRequestBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'phoneNumber':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.phoneNumber = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

