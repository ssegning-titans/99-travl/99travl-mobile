//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:ntravl_api/src/model/friend_type.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'update_friend.g.dart';

/// UpdateFriend
///
/// Properties:
/// * [type] 
abstract class UpdateFriend implements Built<UpdateFriend, UpdateFriendBuilder> {
    @BuiltValueField(wireName: r'type')
    FriendType? get type;
    // enum typeEnum {  RECEIVER_ONLY,  SENDER_ONLY,  FULL,  };

    UpdateFriend._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(UpdateFriendBuilder b) => b;

    factory UpdateFriend([void updates(UpdateFriendBuilder b)]) = _$UpdateFriend;

    @BuiltValueSerializer(custom: true)
    static Serializer<UpdateFriend> get serializer => _$UpdateFriendSerializer();
}

class _$UpdateFriendSerializer implements StructuredSerializer<UpdateFriend> {
    @override
    final Iterable<Type> types = const [UpdateFriend, _$UpdateFriend];

    @override
    final String wireName = r'UpdateFriend';

    @override
    Iterable<Object?> serialize(Serializers serializers, UpdateFriend object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.type != null) {
            result
                ..add(r'type')
                ..add(serializers.serialize(object.type,
                    specifiedType: const FullType(FriendType)));
        }
        return result;
    }

    @override
    UpdateFriend deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = UpdateFriendBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'type':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(FriendType)) as FriendType;
                    result.type = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

