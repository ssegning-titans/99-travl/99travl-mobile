// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'report_travel_all_of.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ReportTravelAllOf extends ReportTravelAllOf {
  @override
  final String travelId;

  factory _$ReportTravelAllOf(
          [void Function(ReportTravelAllOfBuilder)? updates]) =>
      (new ReportTravelAllOfBuilder()..update(updates)).build();

  _$ReportTravelAllOf._({required this.travelId}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        travelId, 'ReportTravelAllOf', 'travelId');
  }

  @override
  ReportTravelAllOf rebuild(void Function(ReportTravelAllOfBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ReportTravelAllOfBuilder toBuilder() =>
      new ReportTravelAllOfBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ReportTravelAllOf && travelId == other.travelId;
  }

  @override
  int get hashCode {
    return $jf($jc(0, travelId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ReportTravelAllOf')
          ..add('travelId', travelId))
        .toString();
  }
}

class ReportTravelAllOfBuilder
    implements Builder<ReportTravelAllOf, ReportTravelAllOfBuilder> {
  _$ReportTravelAllOf? _$v;

  String? _travelId;
  String? get travelId => _$this._travelId;
  set travelId(String? travelId) => _$this._travelId = travelId;

  ReportTravelAllOfBuilder() {
    ReportTravelAllOf._defaults(this);
  }

  ReportTravelAllOfBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _travelId = $v.travelId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ReportTravelAllOf other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ReportTravelAllOf;
  }

  @override
  void update(void Function(ReportTravelAllOfBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ReportTravelAllOf build() {
    final _$result = _$v ??
        new _$ReportTravelAllOf._(
            travelId: BuiltValueNullFieldError.checkNotNull(
                travelId, 'ReportTravelAllOf', 'travelId'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
