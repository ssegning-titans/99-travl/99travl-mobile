//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:ntravl_api/src/model/report_type.dart';
import 'package:ntravl_api/src/model/report_status.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'base_report.g.dart';

/// BaseReport
///
/// Properties:
/// * [reportType] 
/// * [authorId] 
/// * [status] 
/// * [createdAt] 
/// * [id] 
/// * [description] 
abstract class BaseReport implements Built<BaseReport, BaseReportBuilder> {
    @BuiltValueField(wireName: r'reportType')
    ReportType get reportType;
    // enum reportTypeEnum {  TRAVEL,  ACCOUNT,  BUG,  };

    @BuiltValueField(wireName: r'authorId')
    String get authorId;

    @BuiltValueField(wireName: r'status')
    ReportStatus get status;
    // enum statusEnum {  PENDING,  HANDLED,  IN_PROGRESS,  };

    @BuiltValueField(wireName: r'createdAt')
    int? get createdAt;

    @BuiltValueField(wireName: r'id')
    String? get id;

    @BuiltValueField(wireName: r'description')
    String? get description;

    BaseReport._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(BaseReportBuilder b) => b;

    factory BaseReport([void updates(BaseReportBuilder b)]) = _$BaseReport;

    @BuiltValueSerializer(custom: true)
    static Serializer<BaseReport> get serializer => _$BaseReportSerializer();
}

class _$BaseReportSerializer implements StructuredSerializer<BaseReport> {
    @override
    final Iterable<Type> types = const [BaseReport, _$BaseReport];

    @override
    final String wireName = r'BaseReport';

    @override
    Iterable<Object?> serialize(Serializers serializers, BaseReport object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'reportType')
            ..add(serializers.serialize(object.reportType,
                specifiedType: const FullType(ReportType)));
        result
            ..add(r'authorId')
            ..add(serializers.serialize(object.authorId,
                specifiedType: const FullType(String)));
        result
            ..add(r'status')
            ..add(serializers.serialize(object.status,
                specifiedType: const FullType(ReportStatus)));
        if (object.createdAt != null) {
            result
                ..add(r'createdAt')
                ..add(serializers.serialize(object.createdAt,
                    specifiedType: const FullType(int)));
        }
        if (object.id != null) {
            result
                ..add(r'id')
                ..add(serializers.serialize(object.id,
                    specifiedType: const FullType(String)));
        }
        if (object.description != null) {
            result
                ..add(r'description')
                ..add(serializers.serialize(object.description,
                    specifiedType: const FullType(String)));
        }
        return result;
    }

    @override
    BaseReport deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = BaseReportBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'reportType':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(ReportType)) as ReportType;
                    result.reportType = valueDes;
                    break;
                case r'authorId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.authorId = valueDes;
                    break;
                case r'status':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(ReportStatus)) as ReportStatus;
                    result.status = valueDes;
                    break;
                case r'createdAt':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    result.createdAt = valueDes;
                    break;
                case r'id':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.id = valueDes;
                    break;
                case r'description':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.description = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

