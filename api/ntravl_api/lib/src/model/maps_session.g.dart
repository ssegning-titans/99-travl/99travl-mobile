// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'maps_session.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapsSession extends MapsSession {
  @override
  final String? sessionToken;

  factory _$MapsSession([void Function(MapsSessionBuilder)? updates]) =>
      (new MapsSessionBuilder()..update(updates)).build();

  _$MapsSession._({this.sessionToken}) : super._();

  @override
  MapsSession rebuild(void Function(MapsSessionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapsSessionBuilder toBuilder() => new MapsSessionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MapsSession && sessionToken == other.sessionToken;
  }

  @override
  int get hashCode {
    return $jf($jc(0, sessionToken.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('MapsSession')
          ..add('sessionToken', sessionToken))
        .toString();
  }
}

class MapsSessionBuilder implements Builder<MapsSession, MapsSessionBuilder> {
  _$MapsSession? _$v;

  String? _sessionToken;
  String? get sessionToken => _$this._sessionToken;
  set sessionToken(String? sessionToken) => _$this._sessionToken = sessionToken;

  MapsSessionBuilder() {
    MapsSession._defaults(this);
  }

  MapsSessionBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _sessionToken = $v.sessionToken;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MapsSession other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MapsSession;
  }

  @override
  void update(void Function(MapsSessionBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$MapsSession build() {
    final _$result = _$v ?? new _$MapsSession._(sessionToken: sessionToken);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
