//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:ntravl_api/src/model/routing_map_place_all_of.dart';
import 'package:built_collection/built_collection.dart';
import 'package:ntravl_api/src/model/address_component.dart';
import 'package:ntravl_api/src/model/map_place.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'routing_map_place.g.dart';

/// RoutingMapPlace
///
/// Properties:
/// * [placeId] 
/// * [latitude] 
/// * [longitude] 
/// * [formattedAddress] 
/// * [addressComponent] 
/// * [order] 
abstract class RoutingMapPlace implements Built<RoutingMapPlace, RoutingMapPlaceBuilder> {
    @BuiltValueField(wireName: r'placeId')
    String? get placeId;

    @BuiltValueField(wireName: r'latitude')
    double? get latitude;

    @BuiltValueField(wireName: r'longitude')
    double? get longitude;

    @BuiltValueField(wireName: r'formattedAddress')
    String? get formattedAddress;

    @BuiltValueField(wireName: r'addressComponent')
    BuiltList<AddressComponent>? get addressComponent;

    @BuiltValueField(wireName: r'order')
    double? get order;

    RoutingMapPlace._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(RoutingMapPlaceBuilder b) => b;

    factory RoutingMapPlace([void updates(RoutingMapPlaceBuilder b)]) = _$RoutingMapPlace;

    @BuiltValueSerializer(custom: true)
    static Serializer<RoutingMapPlace> get serializer => _$RoutingMapPlaceSerializer();
}

class _$RoutingMapPlaceSerializer implements StructuredSerializer<RoutingMapPlace> {
    @override
    final Iterable<Type> types = const [RoutingMapPlace, _$RoutingMapPlace];

    @override
    final String wireName = r'RoutingMapPlace';

    @override
    Iterable<Object?> serialize(Serializers serializers, RoutingMapPlace object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.placeId != null) {
            result
                ..add(r'placeId')
                ..add(serializers.serialize(object.placeId,
                    specifiedType: const FullType(String)));
        }
        if (object.latitude != null) {
            result
                ..add(r'latitude')
                ..add(serializers.serialize(object.latitude,
                    specifiedType: const FullType(double)));
        }
        if (object.longitude != null) {
            result
                ..add(r'longitude')
                ..add(serializers.serialize(object.longitude,
                    specifiedType: const FullType(double)));
        }
        if (object.formattedAddress != null) {
            result
                ..add(r'formattedAddress')
                ..add(serializers.serialize(object.formattedAddress,
                    specifiedType: const FullType(String)));
        }
        if (object.addressComponent != null) {
            result
                ..add(r'addressComponent')
                ..add(serializers.serialize(object.addressComponent,
                    specifiedType: const FullType(BuiltList, [FullType(AddressComponent)])));
        }
        if (object.order != null) {
            result
                ..add(r'order')
                ..add(serializers.serialize(object.order,
                    specifiedType: const FullType(double)));
        }
        return result;
    }

    @override
    RoutingMapPlace deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = RoutingMapPlaceBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'placeId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.placeId = valueDes;
                    break;
                case r'latitude':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(double)) as double;
                    result.latitude = valueDes;
                    break;
                case r'longitude':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(double)) as double;
                    result.longitude = valueDes;
                    break;
                case r'formattedAddress':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.formattedAddress = valueDes;
                    break;
                case r'addressComponent':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(BuiltList, [FullType(AddressComponent)])) as BuiltList<AddressComponent>;
                    result.addressComponent.replace(valueDes);
                    break;
                case r'order':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(double)) as double;
                    result.order = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

