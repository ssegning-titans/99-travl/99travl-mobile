// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'online_friend.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$OnlineFriend extends OnlineFriend {
  @override
  final String id;
  @override
  final int creationDate;
  @override
  final FriendShipStatus? status;
  @override
  final FriendType? type;
  @override
  final FriendProcessType processType;
  @override
  final String? accountId;
  @override
  final String? authorId;
  @override
  final bool? isUserActive;

  factory _$OnlineFriend([void Function(OnlineFriendBuilder)? updates]) =>
      (new OnlineFriendBuilder()..update(updates)).build();

  _$OnlineFriend._(
      {required this.id,
      required this.creationDate,
      this.status,
      this.type,
      required this.processType,
      this.accountId,
      this.authorId,
      this.isUserActive})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(id, 'OnlineFriend', 'id');
    BuiltValueNullFieldError.checkNotNull(
        creationDate, 'OnlineFriend', 'creationDate');
    BuiltValueNullFieldError.checkNotNull(
        processType, 'OnlineFriend', 'processType');
  }

  @override
  OnlineFriend rebuild(void Function(OnlineFriendBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  OnlineFriendBuilder toBuilder() => new OnlineFriendBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is OnlineFriend &&
        id == other.id &&
        creationDate == other.creationDate &&
        status == other.status &&
        type == other.type &&
        processType == other.processType &&
        accountId == other.accountId &&
        authorId == other.authorId &&
        isUserActive == other.isUserActive;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc($jc($jc(0, id.hashCode), creationDate.hashCode),
                            status.hashCode),
                        type.hashCode),
                    processType.hashCode),
                accountId.hashCode),
            authorId.hashCode),
        isUserActive.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('OnlineFriend')
          ..add('id', id)
          ..add('creationDate', creationDate)
          ..add('status', status)
          ..add('type', type)
          ..add('processType', processType)
          ..add('accountId', accountId)
          ..add('authorId', authorId)
          ..add('isUserActive', isUserActive))
        .toString();
  }
}

class OnlineFriendBuilder
    implements Builder<OnlineFriend, OnlineFriendBuilder> {
  _$OnlineFriend? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  int? _creationDate;
  int? get creationDate => _$this._creationDate;
  set creationDate(int? creationDate) => _$this._creationDate = creationDate;

  FriendShipStatus? _status;
  FriendShipStatus? get status => _$this._status;
  set status(FriendShipStatus? status) => _$this._status = status;

  FriendType? _type;
  FriendType? get type => _$this._type;
  set type(FriendType? type) => _$this._type = type;

  FriendProcessType? _processType;
  FriendProcessType? get processType => _$this._processType;
  set processType(FriendProcessType? processType) =>
      _$this._processType = processType;

  String? _accountId;
  String? get accountId => _$this._accountId;
  set accountId(String? accountId) => _$this._accountId = accountId;

  String? _authorId;
  String? get authorId => _$this._authorId;
  set authorId(String? authorId) => _$this._authorId = authorId;

  bool? _isUserActive;
  bool? get isUserActive => _$this._isUserActive;
  set isUserActive(bool? isUserActive) => _$this._isUserActive = isUserActive;

  OnlineFriendBuilder() {
    OnlineFriend._defaults(this);
  }

  OnlineFriendBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _creationDate = $v.creationDate;
      _status = $v.status;
      _type = $v.type;
      _processType = $v.processType;
      _accountId = $v.accountId;
      _authorId = $v.authorId;
      _isUserActive = $v.isUserActive;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(OnlineFriend other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$OnlineFriend;
  }

  @override
  void update(void Function(OnlineFriendBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$OnlineFriend build() {
    final _$result = _$v ??
        new _$OnlineFriend._(
            id: BuiltValueNullFieldError.checkNotNull(id, 'OnlineFriend', 'id'),
            creationDate: BuiltValueNullFieldError.checkNotNull(
                creationDate, 'OnlineFriend', 'creationDate'),
            status: status,
            type: type,
            processType: BuiltValueNullFieldError.checkNotNull(
                processType, 'OnlineFriend', 'processType'),
            accountId: accountId,
            authorId: authorId,
            isUserActive: isUserActive);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
