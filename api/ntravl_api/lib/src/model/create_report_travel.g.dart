// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_report_travel.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$CreateReportTravel extends CreateReportTravel {
  @override
  final String authorId;
  @override
  final String travelId;
  @override
  final String? description;

  factory _$CreateReportTravel(
          [void Function(CreateReportTravelBuilder)? updates]) =>
      (new CreateReportTravelBuilder()..update(updates)).build();

  _$CreateReportTravel._(
      {required this.authorId, required this.travelId, this.description})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        authorId, 'CreateReportTravel', 'authorId');
    BuiltValueNullFieldError.checkNotNull(
        travelId, 'CreateReportTravel', 'travelId');
  }

  @override
  CreateReportTravel rebuild(
          void Function(CreateReportTravelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CreateReportTravelBuilder toBuilder() =>
      new CreateReportTravelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CreateReportTravel &&
        authorId == other.authorId &&
        travelId == other.travelId &&
        description == other.description;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, authorId.hashCode), travelId.hashCode),
        description.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CreateReportTravel')
          ..add('authorId', authorId)
          ..add('travelId', travelId)
          ..add('description', description))
        .toString();
  }
}

class CreateReportTravelBuilder
    implements Builder<CreateReportTravel, CreateReportTravelBuilder> {
  _$CreateReportTravel? _$v;

  String? _authorId;
  String? get authorId => _$this._authorId;
  set authorId(String? authorId) => _$this._authorId = authorId;

  String? _travelId;
  String? get travelId => _$this._travelId;
  set travelId(String? travelId) => _$this._travelId = travelId;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  CreateReportTravelBuilder() {
    CreateReportTravel._defaults(this);
  }

  CreateReportTravelBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _authorId = $v.authorId;
      _travelId = $v.travelId;
      _description = $v.description;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CreateReportTravel other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$CreateReportTravel;
  }

  @override
  void update(void Function(CreateReportTravelBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CreateReportTravel build() {
    final _$result = _$v ??
        new _$CreateReportTravel._(
            authorId: BuiltValueNullFieldError.checkNotNull(
                authorId, 'CreateReportTravel', 'authorId'),
            travelId: BuiltValueNullFieldError.checkNotNull(
                travelId, 'CreateReportTravel', 'travelId'),
            description: description);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
