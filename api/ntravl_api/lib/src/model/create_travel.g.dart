// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_travel.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$CreateTravel extends CreateTravel {
  @override
  final BuiltList<TravelPoint> passagePoints;
  @override
  final TravelRepetition? repeating;
  @override
  final TravelQuantity maxQuantity;
  @override
  final TravelModeUnit? transportMode;
  @override
  final TravelLuggageDimension luggageDimension;
  @override
  final String? description;

  factory _$CreateTravel([void Function(CreateTravelBuilder)? updates]) =>
      (new CreateTravelBuilder()..update(updates)).build();

  _$CreateTravel._(
      {required this.passagePoints,
      this.repeating,
      required this.maxQuantity,
      this.transportMode,
      required this.luggageDimension,
      this.description})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        passagePoints, 'CreateTravel', 'passagePoints');
    BuiltValueNullFieldError.checkNotNull(
        maxQuantity, 'CreateTravel', 'maxQuantity');
    BuiltValueNullFieldError.checkNotNull(
        luggageDimension, 'CreateTravel', 'luggageDimension');
  }

  @override
  CreateTravel rebuild(void Function(CreateTravelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CreateTravelBuilder toBuilder() => new CreateTravelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CreateTravel &&
        passagePoints == other.passagePoints &&
        repeating == other.repeating &&
        maxQuantity == other.maxQuantity &&
        transportMode == other.transportMode &&
        luggageDimension == other.luggageDimension &&
        description == other.description;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, passagePoints.hashCode), repeating.hashCode),
                    maxQuantity.hashCode),
                transportMode.hashCode),
            luggageDimension.hashCode),
        description.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CreateTravel')
          ..add('passagePoints', passagePoints)
          ..add('repeating', repeating)
          ..add('maxQuantity', maxQuantity)
          ..add('transportMode', transportMode)
          ..add('luggageDimension', luggageDimension)
          ..add('description', description))
        .toString();
  }
}

class CreateTravelBuilder
    implements Builder<CreateTravel, CreateTravelBuilder> {
  _$CreateTravel? _$v;

  ListBuilder<TravelPoint>? _passagePoints;
  ListBuilder<TravelPoint> get passagePoints =>
      _$this._passagePoints ??= new ListBuilder<TravelPoint>();
  set passagePoints(ListBuilder<TravelPoint>? passagePoints) =>
      _$this._passagePoints = passagePoints;

  TravelRepetitionBuilder? _repeating;
  TravelRepetitionBuilder get repeating =>
      _$this._repeating ??= new TravelRepetitionBuilder();
  set repeating(TravelRepetitionBuilder? repeating) =>
      _$this._repeating = repeating;

  TravelQuantityBuilder? _maxQuantity;
  TravelQuantityBuilder get maxQuantity =>
      _$this._maxQuantity ??= new TravelQuantityBuilder();
  set maxQuantity(TravelQuantityBuilder? maxQuantity) =>
      _$this._maxQuantity = maxQuantity;

  TravelModeUnit? _transportMode;
  TravelModeUnit? get transportMode => _$this._transportMode;
  set transportMode(TravelModeUnit? transportMode) =>
      _$this._transportMode = transportMode;

  TravelLuggageDimension? _luggageDimension;
  TravelLuggageDimension? get luggageDimension => _$this._luggageDimension;
  set luggageDimension(TravelLuggageDimension? luggageDimension) =>
      _$this._luggageDimension = luggageDimension;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  CreateTravelBuilder() {
    CreateTravel._defaults(this);
  }

  CreateTravelBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _passagePoints = $v.passagePoints.toBuilder();
      _repeating = $v.repeating?.toBuilder();
      _maxQuantity = $v.maxQuantity.toBuilder();
      _transportMode = $v.transportMode;
      _luggageDimension = $v.luggageDimension;
      _description = $v.description;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CreateTravel other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$CreateTravel;
  }

  @override
  void update(void Function(CreateTravelBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CreateTravel build() {
    _$CreateTravel _$result;
    try {
      _$result = _$v ??
          new _$CreateTravel._(
              passagePoints: passagePoints.build(),
              repeating: _repeating?.build(),
              maxQuantity: maxQuantity.build(),
              transportMode: transportMode,
              luggageDimension: BuiltValueNullFieldError.checkNotNull(
                  luggageDimension, 'CreateTravel', 'luggageDimension'),
              description: description);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'passagePoints';
        passagePoints.build();
        _$failedField = 'repeating';
        _repeating?.build();
        _$failedField = 'maxQuantity';
        maxQuantity.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CreateTravel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
