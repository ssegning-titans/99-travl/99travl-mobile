// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rating_account.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$RatingAccount extends RatingAccount {
  @override
  final String? id;
  @override
  final String? description;
  @override
  final String authorId;
  @override
  final RatingValue value;
  @override
  final int? createdAt;
  @override
  final RatingType type;
  @override
  final String personId;

  factory _$RatingAccount([void Function(RatingAccountBuilder)? updates]) =>
      (new RatingAccountBuilder()..update(updates)).build();

  _$RatingAccount._(
      {this.id,
      this.description,
      required this.authorId,
      required this.value,
      this.createdAt,
      required this.type,
      required this.personId})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        authorId, 'RatingAccount', 'authorId');
    BuiltValueNullFieldError.checkNotNull(value, 'RatingAccount', 'value');
    BuiltValueNullFieldError.checkNotNull(type, 'RatingAccount', 'type');
    BuiltValueNullFieldError.checkNotNull(
        personId, 'RatingAccount', 'personId');
  }

  @override
  RatingAccount rebuild(void Function(RatingAccountBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RatingAccountBuilder toBuilder() => new RatingAccountBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is RatingAccount &&
        id == other.id &&
        description == other.description &&
        authorId == other.authorId &&
        value == other.value &&
        createdAt == other.createdAt &&
        type == other.type &&
        personId == other.personId;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, id.hashCode), description.hashCode),
                        authorId.hashCode),
                    value.hashCode),
                createdAt.hashCode),
            type.hashCode),
        personId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('RatingAccount')
          ..add('id', id)
          ..add('description', description)
          ..add('authorId', authorId)
          ..add('value', value)
          ..add('createdAt', createdAt)
          ..add('type', type)
          ..add('personId', personId))
        .toString();
  }
}

class RatingAccountBuilder
    implements Builder<RatingAccount, RatingAccountBuilder> {
  _$RatingAccount? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  String? _authorId;
  String? get authorId => _$this._authorId;
  set authorId(String? authorId) => _$this._authorId = authorId;

  RatingValue? _value;
  RatingValue? get value => _$this._value;
  set value(RatingValue? value) => _$this._value = value;

  int? _createdAt;
  int? get createdAt => _$this._createdAt;
  set createdAt(int? createdAt) => _$this._createdAt = createdAt;

  RatingType? _type;
  RatingType? get type => _$this._type;
  set type(RatingType? type) => _$this._type = type;

  String? _personId;
  String? get personId => _$this._personId;
  set personId(String? personId) => _$this._personId = personId;

  RatingAccountBuilder() {
    RatingAccount._defaults(this);
  }

  RatingAccountBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _description = $v.description;
      _authorId = $v.authorId;
      _value = $v.value;
      _createdAt = $v.createdAt;
      _type = $v.type;
      _personId = $v.personId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RatingAccount other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$RatingAccount;
  }

  @override
  void update(void Function(RatingAccountBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$RatingAccount build() {
    final _$result = _$v ??
        new _$RatingAccount._(
            id: id,
            description: description,
            authorId: BuiltValueNullFieldError.checkNotNull(
                authorId, 'RatingAccount', 'authorId'),
            value: BuiltValueNullFieldError.checkNotNull(
                value, 'RatingAccount', 'value'),
            createdAt: createdAt,
            type: BuiltValueNullFieldError.checkNotNull(
                type, 'RatingAccount', 'type'),
            personId: BuiltValueNullFieldError.checkNotNull(
                personId, 'RatingAccount', 'personId'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
