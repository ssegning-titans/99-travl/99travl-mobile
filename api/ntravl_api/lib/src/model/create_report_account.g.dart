// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_report_account.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$CreateReportAccount extends CreateReportAccount {
  @override
  final String authorId;
  @override
  final String? accountId;
  @override
  final String? description;

  factory _$CreateReportAccount(
          [void Function(CreateReportAccountBuilder)? updates]) =>
      (new CreateReportAccountBuilder()..update(updates)).build();

  _$CreateReportAccount._(
      {required this.authorId, this.accountId, this.description})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        authorId, 'CreateReportAccount', 'authorId');
  }

  @override
  CreateReportAccount rebuild(
          void Function(CreateReportAccountBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CreateReportAccountBuilder toBuilder() =>
      new CreateReportAccountBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CreateReportAccount &&
        authorId == other.authorId &&
        accountId == other.accountId &&
        description == other.description;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, authorId.hashCode), accountId.hashCode),
        description.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CreateReportAccount')
          ..add('authorId', authorId)
          ..add('accountId', accountId)
          ..add('description', description))
        .toString();
  }
}

class CreateReportAccountBuilder
    implements Builder<CreateReportAccount, CreateReportAccountBuilder> {
  _$CreateReportAccount? _$v;

  String? _authorId;
  String? get authorId => _$this._authorId;
  set authorId(String? authorId) => _$this._authorId = authorId;

  String? _accountId;
  String? get accountId => _$this._accountId;
  set accountId(String? accountId) => _$this._accountId = accountId;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  CreateReportAccountBuilder() {
    CreateReportAccount._defaults(this);
  }

  CreateReportAccountBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _authorId = $v.authorId;
      _accountId = $v.accountId;
      _description = $v.description;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CreateReportAccount other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$CreateReportAccount;
  }

  @override
  void update(void Function(CreateReportAccountBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CreateReportAccount build() {
    final _$result = _$v ??
        new _$CreateReportAccount._(
            authorId: BuiltValueNullFieldError.checkNotNull(
                authorId, 'CreateReportAccount', 'authorId'),
            accountId: accountId,
            description: description);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
