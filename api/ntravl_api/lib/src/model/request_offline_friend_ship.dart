//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'request_offline_friend_ship.g.dart';

/// RequestOfflineFriendShip
///
/// Properties:
/// * [phoneNumber] 
/// * [description] 
/// * [firstName] 
/// * [lastName] 
/// * [avatar] 
abstract class RequestOfflineFriendShip implements Built<RequestOfflineFriendShip, RequestOfflineFriendShipBuilder> {
    @BuiltValueField(wireName: r'phoneNumber')
    String? get phoneNumber;

    @BuiltValueField(wireName: r'description')
    String? get description;

    @BuiltValueField(wireName: r'firstName')
    String? get firstName;

    @BuiltValueField(wireName: r'lastName')
    String? get lastName;

    @BuiltValueField(wireName: r'avatar')
    String? get avatar;

    RequestOfflineFriendShip._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(RequestOfflineFriendShipBuilder b) => b;

    factory RequestOfflineFriendShip([void updates(RequestOfflineFriendShipBuilder b)]) = _$RequestOfflineFriendShip;

    @BuiltValueSerializer(custom: true)
    static Serializer<RequestOfflineFriendShip> get serializer => _$RequestOfflineFriendShipSerializer();
}

class _$RequestOfflineFriendShipSerializer implements StructuredSerializer<RequestOfflineFriendShip> {
    @override
    final Iterable<Type> types = const [RequestOfflineFriendShip, _$RequestOfflineFriendShip];

    @override
    final String wireName = r'RequestOfflineFriendShip';

    @override
    Iterable<Object?> serialize(Serializers serializers, RequestOfflineFriendShip object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.phoneNumber != null) {
            result
                ..add(r'phoneNumber')
                ..add(serializers.serialize(object.phoneNumber,
                    specifiedType: const FullType(String)));
        }
        if (object.description != null) {
            result
                ..add(r'description')
                ..add(serializers.serialize(object.description,
                    specifiedType: const FullType(String)));
        }
        if (object.firstName != null) {
            result
                ..add(r'firstName')
                ..add(serializers.serialize(object.firstName,
                    specifiedType: const FullType(String)));
        }
        if (object.lastName != null) {
            result
                ..add(r'lastName')
                ..add(serializers.serialize(object.lastName,
                    specifiedType: const FullType(String)));
        }
        if (object.avatar != null) {
            result
                ..add(r'avatar')
                ..add(serializers.serialize(object.avatar,
                    specifiedType: const FullType(String)));
        }
        return result;
    }

    @override
    RequestOfflineFriendShip deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = RequestOfflineFriendShipBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'phoneNumber':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.phoneNumber = valueDes;
                    break;
                case r'description':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.description = valueDes;
                    break;
                case r'firstName':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.firstName = valueDes;
                    break;
                case r'lastName':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.lastName = valueDes;
                    break;
                case r'avatar':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.avatar = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

