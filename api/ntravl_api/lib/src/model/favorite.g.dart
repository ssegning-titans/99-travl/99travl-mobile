// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'favorite.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$Favorite extends Favorite {
  @override
  final String? id;
  @override
  final String ownerId;
  @override
  final int? createdAt;
  @override
  final FavoriteType favoriteType;
  @override
  final String accountId;
  @override
  final String travelId;

  factory _$Favorite([void Function(FavoriteBuilder)? updates]) =>
      (new FavoriteBuilder()..update(updates)).build();

  _$Favorite._(
      {this.id,
      required this.ownerId,
      this.createdAt,
      required this.favoriteType,
      required this.accountId,
      required this.travelId})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(ownerId, 'Favorite', 'ownerId');
    BuiltValueNullFieldError.checkNotNull(
        favoriteType, 'Favorite', 'favoriteType');
    BuiltValueNullFieldError.checkNotNull(accountId, 'Favorite', 'accountId');
    BuiltValueNullFieldError.checkNotNull(travelId, 'Favorite', 'travelId');
  }

  @override
  Favorite rebuild(void Function(FavoriteBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  FavoriteBuilder toBuilder() => new FavoriteBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Favorite &&
        id == other.id &&
        ownerId == other.ownerId &&
        createdAt == other.createdAt &&
        favoriteType == other.favoriteType &&
        accountId == other.accountId &&
        travelId == other.travelId;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, id.hashCode), ownerId.hashCode),
                    createdAt.hashCode),
                favoriteType.hashCode),
            accountId.hashCode),
        travelId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Favorite')
          ..add('id', id)
          ..add('ownerId', ownerId)
          ..add('createdAt', createdAt)
          ..add('favoriteType', favoriteType)
          ..add('accountId', accountId)
          ..add('travelId', travelId))
        .toString();
  }
}

class FavoriteBuilder implements Builder<Favorite, FavoriteBuilder> {
  _$Favorite? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _ownerId;
  String? get ownerId => _$this._ownerId;
  set ownerId(String? ownerId) => _$this._ownerId = ownerId;

  int? _createdAt;
  int? get createdAt => _$this._createdAt;
  set createdAt(int? createdAt) => _$this._createdAt = createdAt;

  FavoriteType? _favoriteType;
  FavoriteType? get favoriteType => _$this._favoriteType;
  set favoriteType(FavoriteType? favoriteType) =>
      _$this._favoriteType = favoriteType;

  String? _accountId;
  String? get accountId => _$this._accountId;
  set accountId(String? accountId) => _$this._accountId = accountId;

  String? _travelId;
  String? get travelId => _$this._travelId;
  set travelId(String? travelId) => _$this._travelId = travelId;

  FavoriteBuilder() {
    Favorite._defaults(this);
  }

  FavoriteBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _ownerId = $v.ownerId;
      _createdAt = $v.createdAt;
      _favoriteType = $v.favoriteType;
      _accountId = $v.accountId;
      _travelId = $v.travelId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Favorite other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Favorite;
  }

  @override
  void update(void Function(FavoriteBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Favorite build() {
    final _$result = _$v ??
        new _$Favorite._(
            id: id,
            ownerId: BuiltValueNullFieldError.checkNotNull(
                ownerId, 'Favorite', 'ownerId'),
            createdAt: createdAt,
            favoriteType: BuiltValueNullFieldError.checkNotNull(
                favoriteType, 'Favorite', 'favoriteType'),
            accountId: BuiltValueNullFieldError.checkNotNull(
                accountId, 'Favorite', 'accountId'),
            travelId: BuiltValueNullFieldError.checkNotNull(
                travelId, 'Favorite', 'travelId'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
