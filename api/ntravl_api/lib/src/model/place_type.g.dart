// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'place_type.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const PlaceType _$STREET_ADDRESS = const PlaceType._('STREET_ADDRESS');
const PlaceType _$ROUTE = const PlaceType._('ROUTE');
const PlaceType _$INTERSECTION = const PlaceType._('INTERSECTION');
const PlaceType _$CONTINENT = const PlaceType._('CONTINENT');
const PlaceType _$POLITICAL = const PlaceType._('POLITICAL');
const PlaceType _$COUNTRY = const PlaceType._('COUNTRY');
const PlaceType _$aDMINISTRATIVEAREALEVEL1 =
    const PlaceType._('aDMINISTRATIVEAREALEVEL1');
const PlaceType _$aDMINISTRATIVEAREALEVEL2 =
    const PlaceType._('aDMINISTRATIVEAREALEVEL2');
const PlaceType _$aDMINISTRATIVEAREALEVEL3 =
    const PlaceType._('aDMINISTRATIVEAREALEVEL3');
const PlaceType _$aDMINISTRATIVEAREALEVEL4 =
    const PlaceType._('aDMINISTRATIVEAREALEVEL4');
const PlaceType _$aDMINISTRATIVEAREALEVEL5 =
    const PlaceType._('aDMINISTRATIVEAREALEVEL5');
const PlaceType _$COLLOQUIAL_AREA = const PlaceType._('COLLOQUIAL_AREA');
const PlaceType _$LOCALITY = const PlaceType._('LOCALITY');
const PlaceType _$WARD = const PlaceType._('WARD');
const PlaceType _$SUBLOCALITY = const PlaceType._('SUBLOCALITY');
const PlaceType _$sUBLOCALITYLEVEL1 = const PlaceType._('sUBLOCALITYLEVEL1');
const PlaceType _$sUBLOCALITYLEVEL2 = const PlaceType._('sUBLOCALITYLEVEL2');
const PlaceType _$sUBLOCALITYLEVEL3 = const PlaceType._('sUBLOCALITYLEVEL3');
const PlaceType _$sUBLOCALITYLEVEL4 = const PlaceType._('sUBLOCALITYLEVEL4');
const PlaceType _$sUBLOCALITYLEVEL5 = const PlaceType._('sUBLOCALITYLEVEL5');
const PlaceType _$NEIGHBORHOOD = const PlaceType._('NEIGHBORHOOD');
const PlaceType _$PREMISE = const PlaceType._('PREMISE');
const PlaceType _$SUBPREMISE = const PlaceType._('SUBPREMISE');
const PlaceType _$POSTAL_CODE = const PlaceType._('POSTAL_CODE');
const PlaceType _$POSTAL_CODE_PREFIX = const PlaceType._('POSTAL_CODE_PREFIX');
const PlaceType _$POSTAL_CODE_SUFFIX = const PlaceType._('POSTAL_CODE_SUFFIX');
const PlaceType _$NATURAL_FEATURE = const PlaceType._('NATURAL_FEATURE');
const PlaceType _$AIRPORT = const PlaceType._('AIRPORT');
const PlaceType _$PARK = const PlaceType._('PARK');
const PlaceType _$POINT_OF_INTEREST = const PlaceType._('POINT_OF_INTEREST');
const PlaceType _$FLOOR = const PlaceType._('FLOOR');
const PlaceType _$ESTABLISHMENT = const PlaceType._('ESTABLISHMENT');
const PlaceType _$PARKING = const PlaceType._('PARKING');
const PlaceType _$POST_BOX = const PlaceType._('POST_BOX');
const PlaceType _$POSTAL_TOWN = const PlaceType._('POSTAL_TOWN');
const PlaceType _$ROOM = const PlaceType._('ROOM');
const PlaceType _$STREET_NUMBER = const PlaceType._('STREET_NUMBER');
const PlaceType _$BUS_STATION = const PlaceType._('BUS_STATION');
const PlaceType _$TRAIN_STATION = const PlaceType._('TRAIN_STATION');
const PlaceType _$SUBWAY_STATION = const PlaceType._('SUBWAY_STATION');
const PlaceType _$TRANSIT_STATION = const PlaceType._('TRANSIT_STATION');
const PlaceType _$LIGHT_RAIL_STATION = const PlaceType._('LIGHT_RAIL_STATION');
const PlaceType _$GENERAL_CONTRACTOR = const PlaceType._('GENERAL_CONTRACTOR');
const PlaceType _$FOOD = const PlaceType._('FOOD');
const PlaceType _$REAL_ESTATE_AGENCY = const PlaceType._('REAL_ESTATE_AGENCY');
const PlaceType _$CAR_RENTAL = const PlaceType._('CAR_RENTAL');
const PlaceType _$TRAVEL_AGENCY = const PlaceType._('TRAVEL_AGENCY');
const PlaceType _$ELECTRONICS_STORE = const PlaceType._('ELECTRONICS_STORE');
const PlaceType _$HOME_GOODS_STORE = const PlaceType._('HOME_GOODS_STORE');
const PlaceType _$SCHOOL = const PlaceType._('SCHOOL');
const PlaceType _$STORE = const PlaceType._('STORE');
const PlaceType _$SHOPPING_MALL = const PlaceType._('SHOPPING_MALL');
const PlaceType _$LODGING = const PlaceType._('LODGING');
const PlaceType _$ART_GALLERY = const PlaceType._('ART_GALLERY');
const PlaceType _$LAWYER = const PlaceType._('LAWYER');
const PlaceType _$RESTAURANT = const PlaceType._('RESTAURANT');
const PlaceType _$BAR = const PlaceType._('BAR');
const PlaceType _$MEAL_TAKEAWAY = const PlaceType._('MEAL_TAKEAWAY');
const PlaceType _$CLOTHING_STORE = const PlaceType._('CLOTHING_STORE');
const PlaceType _$LOCAL_GOVERNMENT_OFFICE =
    const PlaceType._('LOCAL_GOVERNMENT_OFFICE');
const PlaceType _$FINANCE = const PlaceType._('FINANCE');
const PlaceType _$MOVING_COMPANY = const PlaceType._('MOVING_COMPANY');
const PlaceType _$STORAGE = const PlaceType._('STORAGE');
const PlaceType _$CAFE = const PlaceType._('CAFE');
const PlaceType _$CAR_REPAIR = const PlaceType._('CAR_REPAIR');
const PlaceType _$HEALTH = const PlaceType._('HEALTH');
const PlaceType _$INSURANCE_AGENCY = const PlaceType._('INSURANCE_AGENCY');
const PlaceType _$PAINTER = const PlaceType._('PAINTER');
const PlaceType _$ARCHIPELAGO = const PlaceType._('ARCHIPELAGO');
const PlaceType _$MUSEUM = const PlaceType._('MUSEUM');
const PlaceType _$CAMPGROUND = const PlaceType._('CAMPGROUND');
const PlaceType _$RV_PARK = const PlaceType._('RV_PARK');
const PlaceType _$MEAL_DELIVERY = const PlaceType._('MEAL_DELIVERY');
const PlaceType _$PRIMARY_SCHOOL = const PlaceType._('PRIMARY_SCHOOL');
const PlaceType _$SECONDARY_SCHOOL = const PlaceType._('SECONDARY_SCHOOL');
const PlaceType _$TOWN_SQUARE = const PlaceType._('TOWN_SQUARE');
const PlaceType _$TOURIST_ATTRACTION = const PlaceType._('TOURIST_ATTRACTION');
const PlaceType _$PLUS_CODE = const PlaceType._('PLUS_CODE');
const PlaceType _$DRUGSTORE = const PlaceType._('DRUGSTORE');
const PlaceType _$UNKNOWN = const PlaceType._('UNKNOWN');

PlaceType _$valueOf(String name) {
  switch (name) {
    case 'STREET_ADDRESS':
      return _$STREET_ADDRESS;
    case 'ROUTE':
      return _$ROUTE;
    case 'INTERSECTION':
      return _$INTERSECTION;
    case 'CONTINENT':
      return _$CONTINENT;
    case 'POLITICAL':
      return _$POLITICAL;
    case 'COUNTRY':
      return _$COUNTRY;
    case 'aDMINISTRATIVEAREALEVEL1':
      return _$aDMINISTRATIVEAREALEVEL1;
    case 'aDMINISTRATIVEAREALEVEL2':
      return _$aDMINISTRATIVEAREALEVEL2;
    case 'aDMINISTRATIVEAREALEVEL3':
      return _$aDMINISTRATIVEAREALEVEL3;
    case 'aDMINISTRATIVEAREALEVEL4':
      return _$aDMINISTRATIVEAREALEVEL4;
    case 'aDMINISTRATIVEAREALEVEL5':
      return _$aDMINISTRATIVEAREALEVEL5;
    case 'COLLOQUIAL_AREA':
      return _$COLLOQUIAL_AREA;
    case 'LOCALITY':
      return _$LOCALITY;
    case 'WARD':
      return _$WARD;
    case 'SUBLOCALITY':
      return _$SUBLOCALITY;
    case 'sUBLOCALITYLEVEL1':
      return _$sUBLOCALITYLEVEL1;
    case 'sUBLOCALITYLEVEL2':
      return _$sUBLOCALITYLEVEL2;
    case 'sUBLOCALITYLEVEL3':
      return _$sUBLOCALITYLEVEL3;
    case 'sUBLOCALITYLEVEL4':
      return _$sUBLOCALITYLEVEL4;
    case 'sUBLOCALITYLEVEL5':
      return _$sUBLOCALITYLEVEL5;
    case 'NEIGHBORHOOD':
      return _$NEIGHBORHOOD;
    case 'PREMISE':
      return _$PREMISE;
    case 'SUBPREMISE':
      return _$SUBPREMISE;
    case 'POSTAL_CODE':
      return _$POSTAL_CODE;
    case 'POSTAL_CODE_PREFIX':
      return _$POSTAL_CODE_PREFIX;
    case 'POSTAL_CODE_SUFFIX':
      return _$POSTAL_CODE_SUFFIX;
    case 'NATURAL_FEATURE':
      return _$NATURAL_FEATURE;
    case 'AIRPORT':
      return _$AIRPORT;
    case 'PARK':
      return _$PARK;
    case 'POINT_OF_INTEREST':
      return _$POINT_OF_INTEREST;
    case 'FLOOR':
      return _$FLOOR;
    case 'ESTABLISHMENT':
      return _$ESTABLISHMENT;
    case 'PARKING':
      return _$PARKING;
    case 'POST_BOX':
      return _$POST_BOX;
    case 'POSTAL_TOWN':
      return _$POSTAL_TOWN;
    case 'ROOM':
      return _$ROOM;
    case 'STREET_NUMBER':
      return _$STREET_NUMBER;
    case 'BUS_STATION':
      return _$BUS_STATION;
    case 'TRAIN_STATION':
      return _$TRAIN_STATION;
    case 'SUBWAY_STATION':
      return _$SUBWAY_STATION;
    case 'TRANSIT_STATION':
      return _$TRANSIT_STATION;
    case 'LIGHT_RAIL_STATION':
      return _$LIGHT_RAIL_STATION;
    case 'GENERAL_CONTRACTOR':
      return _$GENERAL_CONTRACTOR;
    case 'FOOD':
      return _$FOOD;
    case 'REAL_ESTATE_AGENCY':
      return _$REAL_ESTATE_AGENCY;
    case 'CAR_RENTAL':
      return _$CAR_RENTAL;
    case 'TRAVEL_AGENCY':
      return _$TRAVEL_AGENCY;
    case 'ELECTRONICS_STORE':
      return _$ELECTRONICS_STORE;
    case 'HOME_GOODS_STORE':
      return _$HOME_GOODS_STORE;
    case 'SCHOOL':
      return _$SCHOOL;
    case 'STORE':
      return _$STORE;
    case 'SHOPPING_MALL':
      return _$SHOPPING_MALL;
    case 'LODGING':
      return _$LODGING;
    case 'ART_GALLERY':
      return _$ART_GALLERY;
    case 'LAWYER':
      return _$LAWYER;
    case 'RESTAURANT':
      return _$RESTAURANT;
    case 'BAR':
      return _$BAR;
    case 'MEAL_TAKEAWAY':
      return _$MEAL_TAKEAWAY;
    case 'CLOTHING_STORE':
      return _$CLOTHING_STORE;
    case 'LOCAL_GOVERNMENT_OFFICE':
      return _$LOCAL_GOVERNMENT_OFFICE;
    case 'FINANCE':
      return _$FINANCE;
    case 'MOVING_COMPANY':
      return _$MOVING_COMPANY;
    case 'STORAGE':
      return _$STORAGE;
    case 'CAFE':
      return _$CAFE;
    case 'CAR_REPAIR':
      return _$CAR_REPAIR;
    case 'HEALTH':
      return _$HEALTH;
    case 'INSURANCE_AGENCY':
      return _$INSURANCE_AGENCY;
    case 'PAINTER':
      return _$PAINTER;
    case 'ARCHIPELAGO':
      return _$ARCHIPELAGO;
    case 'MUSEUM':
      return _$MUSEUM;
    case 'CAMPGROUND':
      return _$CAMPGROUND;
    case 'RV_PARK':
      return _$RV_PARK;
    case 'MEAL_DELIVERY':
      return _$MEAL_DELIVERY;
    case 'PRIMARY_SCHOOL':
      return _$PRIMARY_SCHOOL;
    case 'SECONDARY_SCHOOL':
      return _$SECONDARY_SCHOOL;
    case 'TOWN_SQUARE':
      return _$TOWN_SQUARE;
    case 'TOURIST_ATTRACTION':
      return _$TOURIST_ATTRACTION;
    case 'PLUS_CODE':
      return _$PLUS_CODE;
    case 'DRUGSTORE':
      return _$DRUGSTORE;
    case 'UNKNOWN':
      return _$UNKNOWN;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<PlaceType> _$values = new BuiltSet<PlaceType>(const <PlaceType>[
  _$STREET_ADDRESS,
  _$ROUTE,
  _$INTERSECTION,
  _$CONTINENT,
  _$POLITICAL,
  _$COUNTRY,
  _$aDMINISTRATIVEAREALEVEL1,
  _$aDMINISTRATIVEAREALEVEL2,
  _$aDMINISTRATIVEAREALEVEL3,
  _$aDMINISTRATIVEAREALEVEL4,
  _$aDMINISTRATIVEAREALEVEL5,
  _$COLLOQUIAL_AREA,
  _$LOCALITY,
  _$WARD,
  _$SUBLOCALITY,
  _$sUBLOCALITYLEVEL1,
  _$sUBLOCALITYLEVEL2,
  _$sUBLOCALITYLEVEL3,
  _$sUBLOCALITYLEVEL4,
  _$sUBLOCALITYLEVEL5,
  _$NEIGHBORHOOD,
  _$PREMISE,
  _$SUBPREMISE,
  _$POSTAL_CODE,
  _$POSTAL_CODE_PREFIX,
  _$POSTAL_CODE_SUFFIX,
  _$NATURAL_FEATURE,
  _$AIRPORT,
  _$PARK,
  _$POINT_OF_INTEREST,
  _$FLOOR,
  _$ESTABLISHMENT,
  _$PARKING,
  _$POST_BOX,
  _$POSTAL_TOWN,
  _$ROOM,
  _$STREET_NUMBER,
  _$BUS_STATION,
  _$TRAIN_STATION,
  _$SUBWAY_STATION,
  _$TRANSIT_STATION,
  _$LIGHT_RAIL_STATION,
  _$GENERAL_CONTRACTOR,
  _$FOOD,
  _$REAL_ESTATE_AGENCY,
  _$CAR_RENTAL,
  _$TRAVEL_AGENCY,
  _$ELECTRONICS_STORE,
  _$HOME_GOODS_STORE,
  _$SCHOOL,
  _$STORE,
  _$SHOPPING_MALL,
  _$LODGING,
  _$ART_GALLERY,
  _$LAWYER,
  _$RESTAURANT,
  _$BAR,
  _$MEAL_TAKEAWAY,
  _$CLOTHING_STORE,
  _$LOCAL_GOVERNMENT_OFFICE,
  _$FINANCE,
  _$MOVING_COMPANY,
  _$STORAGE,
  _$CAFE,
  _$CAR_REPAIR,
  _$HEALTH,
  _$INSURANCE_AGENCY,
  _$PAINTER,
  _$ARCHIPELAGO,
  _$MUSEUM,
  _$CAMPGROUND,
  _$RV_PARK,
  _$MEAL_DELIVERY,
  _$PRIMARY_SCHOOL,
  _$SECONDARY_SCHOOL,
  _$TOWN_SQUARE,
  _$TOURIST_ATTRACTION,
  _$PLUS_CODE,
  _$DRUGSTORE,
  _$UNKNOWN,
]);

class _$PlaceTypeMeta {
  const _$PlaceTypeMeta();
  PlaceType get STREET_ADDRESS => _$STREET_ADDRESS;
  PlaceType get ROUTE => _$ROUTE;
  PlaceType get INTERSECTION => _$INTERSECTION;
  PlaceType get CONTINENT => _$CONTINENT;
  PlaceType get POLITICAL => _$POLITICAL;
  PlaceType get COUNTRY => _$COUNTRY;
  PlaceType get aDMINISTRATIVEAREALEVEL1 => _$aDMINISTRATIVEAREALEVEL1;
  PlaceType get aDMINISTRATIVEAREALEVEL2 => _$aDMINISTRATIVEAREALEVEL2;
  PlaceType get aDMINISTRATIVEAREALEVEL3 => _$aDMINISTRATIVEAREALEVEL3;
  PlaceType get aDMINISTRATIVEAREALEVEL4 => _$aDMINISTRATIVEAREALEVEL4;
  PlaceType get aDMINISTRATIVEAREALEVEL5 => _$aDMINISTRATIVEAREALEVEL5;
  PlaceType get COLLOQUIAL_AREA => _$COLLOQUIAL_AREA;
  PlaceType get LOCALITY => _$LOCALITY;
  PlaceType get WARD => _$WARD;
  PlaceType get SUBLOCALITY => _$SUBLOCALITY;
  PlaceType get sUBLOCALITYLEVEL1 => _$sUBLOCALITYLEVEL1;
  PlaceType get sUBLOCALITYLEVEL2 => _$sUBLOCALITYLEVEL2;
  PlaceType get sUBLOCALITYLEVEL3 => _$sUBLOCALITYLEVEL3;
  PlaceType get sUBLOCALITYLEVEL4 => _$sUBLOCALITYLEVEL4;
  PlaceType get sUBLOCALITYLEVEL5 => _$sUBLOCALITYLEVEL5;
  PlaceType get NEIGHBORHOOD => _$NEIGHBORHOOD;
  PlaceType get PREMISE => _$PREMISE;
  PlaceType get SUBPREMISE => _$SUBPREMISE;
  PlaceType get POSTAL_CODE => _$POSTAL_CODE;
  PlaceType get POSTAL_CODE_PREFIX => _$POSTAL_CODE_PREFIX;
  PlaceType get POSTAL_CODE_SUFFIX => _$POSTAL_CODE_SUFFIX;
  PlaceType get NATURAL_FEATURE => _$NATURAL_FEATURE;
  PlaceType get AIRPORT => _$AIRPORT;
  PlaceType get PARK => _$PARK;
  PlaceType get POINT_OF_INTEREST => _$POINT_OF_INTEREST;
  PlaceType get FLOOR => _$FLOOR;
  PlaceType get ESTABLISHMENT => _$ESTABLISHMENT;
  PlaceType get PARKING => _$PARKING;
  PlaceType get POST_BOX => _$POST_BOX;
  PlaceType get POSTAL_TOWN => _$POSTAL_TOWN;
  PlaceType get ROOM => _$ROOM;
  PlaceType get STREET_NUMBER => _$STREET_NUMBER;
  PlaceType get BUS_STATION => _$BUS_STATION;
  PlaceType get TRAIN_STATION => _$TRAIN_STATION;
  PlaceType get SUBWAY_STATION => _$SUBWAY_STATION;
  PlaceType get TRANSIT_STATION => _$TRANSIT_STATION;
  PlaceType get LIGHT_RAIL_STATION => _$LIGHT_RAIL_STATION;
  PlaceType get GENERAL_CONTRACTOR => _$GENERAL_CONTRACTOR;
  PlaceType get FOOD => _$FOOD;
  PlaceType get REAL_ESTATE_AGENCY => _$REAL_ESTATE_AGENCY;
  PlaceType get CAR_RENTAL => _$CAR_RENTAL;
  PlaceType get TRAVEL_AGENCY => _$TRAVEL_AGENCY;
  PlaceType get ELECTRONICS_STORE => _$ELECTRONICS_STORE;
  PlaceType get HOME_GOODS_STORE => _$HOME_GOODS_STORE;
  PlaceType get SCHOOL => _$SCHOOL;
  PlaceType get STORE => _$STORE;
  PlaceType get SHOPPING_MALL => _$SHOPPING_MALL;
  PlaceType get LODGING => _$LODGING;
  PlaceType get ART_GALLERY => _$ART_GALLERY;
  PlaceType get LAWYER => _$LAWYER;
  PlaceType get RESTAURANT => _$RESTAURANT;
  PlaceType get BAR => _$BAR;
  PlaceType get MEAL_TAKEAWAY => _$MEAL_TAKEAWAY;
  PlaceType get CLOTHING_STORE => _$CLOTHING_STORE;
  PlaceType get LOCAL_GOVERNMENT_OFFICE => _$LOCAL_GOVERNMENT_OFFICE;
  PlaceType get FINANCE => _$FINANCE;
  PlaceType get MOVING_COMPANY => _$MOVING_COMPANY;
  PlaceType get STORAGE => _$STORAGE;
  PlaceType get CAFE => _$CAFE;
  PlaceType get CAR_REPAIR => _$CAR_REPAIR;
  PlaceType get HEALTH => _$HEALTH;
  PlaceType get INSURANCE_AGENCY => _$INSURANCE_AGENCY;
  PlaceType get PAINTER => _$PAINTER;
  PlaceType get ARCHIPELAGO => _$ARCHIPELAGO;
  PlaceType get MUSEUM => _$MUSEUM;
  PlaceType get CAMPGROUND => _$CAMPGROUND;
  PlaceType get RV_PARK => _$RV_PARK;
  PlaceType get MEAL_DELIVERY => _$MEAL_DELIVERY;
  PlaceType get PRIMARY_SCHOOL => _$PRIMARY_SCHOOL;
  PlaceType get SECONDARY_SCHOOL => _$SECONDARY_SCHOOL;
  PlaceType get TOWN_SQUARE => _$TOWN_SQUARE;
  PlaceType get TOURIST_ATTRACTION => _$TOURIST_ATTRACTION;
  PlaceType get PLUS_CODE => _$PLUS_CODE;
  PlaceType get DRUGSTORE => _$DRUGSTORE;
  PlaceType get UNKNOWN => _$UNKNOWN;
  PlaceType valueOf(String name) => _$valueOf(name);
  BuiltSet<PlaceType> get values => _$values;
}

abstract class _$PlaceTypeMixin {
  // ignore: non_constant_identifier_names
  _$PlaceTypeMeta get PlaceType => const _$PlaceTypeMeta();
}

Serializer<PlaceType> _$placeTypeSerializer = new _$PlaceTypeSerializer();

class _$PlaceTypeSerializer implements PrimitiveSerializer<PlaceType> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'STREET_ADDRESS': 'STREET_ADDRESS',
    'ROUTE': 'ROUTE',
    'INTERSECTION': 'INTERSECTION',
    'CONTINENT': 'CONTINENT',
    'POLITICAL': 'POLITICAL',
    'COUNTRY': 'COUNTRY',
    'aDMINISTRATIVEAREALEVEL1': 'ADMINISTRATIVE_AREA_LEVEL_1',
    'aDMINISTRATIVEAREALEVEL2': 'ADMINISTRATIVE_AREA_LEVEL_2',
    'aDMINISTRATIVEAREALEVEL3': 'ADMINISTRATIVE_AREA_LEVEL_3',
    'aDMINISTRATIVEAREALEVEL4': 'ADMINISTRATIVE_AREA_LEVEL_4',
    'aDMINISTRATIVEAREALEVEL5': 'ADMINISTRATIVE_AREA_LEVEL_5',
    'COLLOQUIAL_AREA': 'COLLOQUIAL_AREA',
    'LOCALITY': 'LOCALITY',
    'WARD': 'WARD',
    'SUBLOCALITY': 'SUBLOCALITY',
    'sUBLOCALITYLEVEL1': 'SUBLOCALITY_LEVEL_1',
    'sUBLOCALITYLEVEL2': 'SUBLOCALITY_LEVEL_2',
    'sUBLOCALITYLEVEL3': 'SUBLOCALITY_LEVEL_3',
    'sUBLOCALITYLEVEL4': 'SUBLOCALITY_LEVEL_4',
    'sUBLOCALITYLEVEL5': 'SUBLOCALITY_LEVEL_5',
    'NEIGHBORHOOD': 'NEIGHBORHOOD',
    'PREMISE': 'PREMISE',
    'SUBPREMISE': 'SUBPREMISE',
    'POSTAL_CODE': 'POSTAL_CODE',
    'POSTAL_CODE_PREFIX': 'POSTAL_CODE_PREFIX',
    'POSTAL_CODE_SUFFIX': 'POSTAL_CODE_SUFFIX',
    'NATURAL_FEATURE': 'NATURAL_FEATURE',
    'AIRPORT': 'AIRPORT',
    'PARK': 'PARK',
    'POINT_OF_INTEREST': 'POINT_OF_INTEREST',
    'FLOOR': 'FLOOR',
    'ESTABLISHMENT': 'ESTABLISHMENT',
    'PARKING': 'PARKING',
    'POST_BOX': 'POST_BOX',
    'POSTAL_TOWN': 'POSTAL_TOWN',
    'ROOM': 'ROOM',
    'STREET_NUMBER': 'STREET_NUMBER',
    'BUS_STATION': 'BUS_STATION',
    'TRAIN_STATION': 'TRAIN_STATION',
    'SUBWAY_STATION': 'SUBWAY_STATION',
    'TRANSIT_STATION': 'TRANSIT_STATION',
    'LIGHT_RAIL_STATION': 'LIGHT_RAIL_STATION',
    'GENERAL_CONTRACTOR': 'GENERAL_CONTRACTOR',
    'FOOD': 'FOOD',
    'REAL_ESTATE_AGENCY': 'REAL_ESTATE_AGENCY',
    'CAR_RENTAL': 'CAR_RENTAL',
    'TRAVEL_AGENCY': 'TRAVEL_AGENCY',
    'ELECTRONICS_STORE': 'ELECTRONICS_STORE',
    'HOME_GOODS_STORE': 'HOME_GOODS_STORE',
    'SCHOOL': 'SCHOOL',
    'STORE': 'STORE',
    'SHOPPING_MALL': 'SHOPPING_MALL',
    'LODGING': 'LODGING',
    'ART_GALLERY': 'ART_GALLERY',
    'LAWYER': 'LAWYER',
    'RESTAURANT': 'RESTAURANT',
    'BAR': 'BAR',
    'MEAL_TAKEAWAY': 'MEAL_TAKEAWAY',
    'CLOTHING_STORE': 'CLOTHING_STORE',
    'LOCAL_GOVERNMENT_OFFICE': 'LOCAL_GOVERNMENT_OFFICE',
    'FINANCE': 'FINANCE',
    'MOVING_COMPANY': 'MOVING_COMPANY',
    'STORAGE': 'STORAGE',
    'CAFE': 'CAFE',
    'CAR_REPAIR': 'CAR_REPAIR',
    'HEALTH': 'HEALTH',
    'INSURANCE_AGENCY': 'INSURANCE_AGENCY',
    'PAINTER': 'PAINTER',
    'ARCHIPELAGO': 'ARCHIPELAGO',
    'MUSEUM': 'MUSEUM',
    'CAMPGROUND': 'CAMPGROUND',
    'RV_PARK': 'RV_PARK',
    'MEAL_DELIVERY': 'MEAL_DELIVERY',
    'PRIMARY_SCHOOL': 'PRIMARY_SCHOOL',
    'SECONDARY_SCHOOL': 'SECONDARY_SCHOOL',
    'TOWN_SQUARE': 'TOWN_SQUARE',
    'TOURIST_ATTRACTION': 'TOURIST_ATTRACTION',
    'PLUS_CODE': 'PLUS_CODE',
    'DRUGSTORE': 'DRUGSTORE',
    'UNKNOWN': 'UNKNOWN',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'STREET_ADDRESS': 'STREET_ADDRESS',
    'ROUTE': 'ROUTE',
    'INTERSECTION': 'INTERSECTION',
    'CONTINENT': 'CONTINENT',
    'POLITICAL': 'POLITICAL',
    'COUNTRY': 'COUNTRY',
    'ADMINISTRATIVE_AREA_LEVEL_1': 'aDMINISTRATIVEAREALEVEL1',
    'ADMINISTRATIVE_AREA_LEVEL_2': 'aDMINISTRATIVEAREALEVEL2',
    'ADMINISTRATIVE_AREA_LEVEL_3': 'aDMINISTRATIVEAREALEVEL3',
    'ADMINISTRATIVE_AREA_LEVEL_4': 'aDMINISTRATIVEAREALEVEL4',
    'ADMINISTRATIVE_AREA_LEVEL_5': 'aDMINISTRATIVEAREALEVEL5',
    'COLLOQUIAL_AREA': 'COLLOQUIAL_AREA',
    'LOCALITY': 'LOCALITY',
    'WARD': 'WARD',
    'SUBLOCALITY': 'SUBLOCALITY',
    'SUBLOCALITY_LEVEL_1': 'sUBLOCALITYLEVEL1',
    'SUBLOCALITY_LEVEL_2': 'sUBLOCALITYLEVEL2',
    'SUBLOCALITY_LEVEL_3': 'sUBLOCALITYLEVEL3',
    'SUBLOCALITY_LEVEL_4': 'sUBLOCALITYLEVEL4',
    'SUBLOCALITY_LEVEL_5': 'sUBLOCALITYLEVEL5',
    'NEIGHBORHOOD': 'NEIGHBORHOOD',
    'PREMISE': 'PREMISE',
    'SUBPREMISE': 'SUBPREMISE',
    'POSTAL_CODE': 'POSTAL_CODE',
    'POSTAL_CODE_PREFIX': 'POSTAL_CODE_PREFIX',
    'POSTAL_CODE_SUFFIX': 'POSTAL_CODE_SUFFIX',
    'NATURAL_FEATURE': 'NATURAL_FEATURE',
    'AIRPORT': 'AIRPORT',
    'PARK': 'PARK',
    'POINT_OF_INTEREST': 'POINT_OF_INTEREST',
    'FLOOR': 'FLOOR',
    'ESTABLISHMENT': 'ESTABLISHMENT',
    'PARKING': 'PARKING',
    'POST_BOX': 'POST_BOX',
    'POSTAL_TOWN': 'POSTAL_TOWN',
    'ROOM': 'ROOM',
    'STREET_NUMBER': 'STREET_NUMBER',
    'BUS_STATION': 'BUS_STATION',
    'TRAIN_STATION': 'TRAIN_STATION',
    'SUBWAY_STATION': 'SUBWAY_STATION',
    'TRANSIT_STATION': 'TRANSIT_STATION',
    'LIGHT_RAIL_STATION': 'LIGHT_RAIL_STATION',
    'GENERAL_CONTRACTOR': 'GENERAL_CONTRACTOR',
    'FOOD': 'FOOD',
    'REAL_ESTATE_AGENCY': 'REAL_ESTATE_AGENCY',
    'CAR_RENTAL': 'CAR_RENTAL',
    'TRAVEL_AGENCY': 'TRAVEL_AGENCY',
    'ELECTRONICS_STORE': 'ELECTRONICS_STORE',
    'HOME_GOODS_STORE': 'HOME_GOODS_STORE',
    'SCHOOL': 'SCHOOL',
    'STORE': 'STORE',
    'SHOPPING_MALL': 'SHOPPING_MALL',
    'LODGING': 'LODGING',
    'ART_GALLERY': 'ART_GALLERY',
    'LAWYER': 'LAWYER',
    'RESTAURANT': 'RESTAURANT',
    'BAR': 'BAR',
    'MEAL_TAKEAWAY': 'MEAL_TAKEAWAY',
    'CLOTHING_STORE': 'CLOTHING_STORE',
    'LOCAL_GOVERNMENT_OFFICE': 'LOCAL_GOVERNMENT_OFFICE',
    'FINANCE': 'FINANCE',
    'MOVING_COMPANY': 'MOVING_COMPANY',
    'STORAGE': 'STORAGE',
    'CAFE': 'CAFE',
    'CAR_REPAIR': 'CAR_REPAIR',
    'HEALTH': 'HEALTH',
    'INSURANCE_AGENCY': 'INSURANCE_AGENCY',
    'PAINTER': 'PAINTER',
    'ARCHIPELAGO': 'ARCHIPELAGO',
    'MUSEUM': 'MUSEUM',
    'CAMPGROUND': 'CAMPGROUND',
    'RV_PARK': 'RV_PARK',
    'MEAL_DELIVERY': 'MEAL_DELIVERY',
    'PRIMARY_SCHOOL': 'PRIMARY_SCHOOL',
    'SECONDARY_SCHOOL': 'SECONDARY_SCHOOL',
    'TOWN_SQUARE': 'TOWN_SQUARE',
    'TOURIST_ATTRACTION': 'TOURIST_ATTRACTION',
    'PLUS_CODE': 'PLUS_CODE',
    'DRUGSTORE': 'DRUGSTORE',
    'UNKNOWN': 'UNKNOWN',
  };

  @override
  final Iterable<Type> types = const <Type>[PlaceType];
  @override
  final String wireName = 'PlaceType';

  @override
  Object serialize(Serializers serializers, PlaceType object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  PlaceType deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      PlaceType.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
