//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:ntravl_api/src/model/account_type.dart';
import 'package:ntravl_api/src/model/account_gender.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'create_account_input.g.dart';

/// CreateAccountInput
///
/// Properties:
/// * [firstName] 
/// * [lastName] 
/// * [email] 
/// * [phoneNumber] 
/// * [gender] 
/// * [bio] 
/// * [avatarUrl] 
/// * [accountType] 
abstract class CreateAccountInput implements Built<CreateAccountInput, CreateAccountInputBuilder> {
    @BuiltValueField(wireName: r'firstName')
    String? get firstName;

    @BuiltValueField(wireName: r'lastName')
    String? get lastName;

    @BuiltValueField(wireName: r'email')
    String? get email;

    @BuiltValueField(wireName: r'phoneNumber')
    String? get phoneNumber;

    @BuiltValueField(wireName: r'gender')
    AccountGender? get gender;
    // enum genderEnum {  MALE,  FEMALE,  OTHER,  };

    @BuiltValueField(wireName: r'bio')
    String? get bio;

    @BuiltValueField(wireName: r'avatarUrl')
    String? get avatarUrl;

    @BuiltValueField(wireName: r'accountType')
    AccountType? get accountType;
    // enum accountTypeEnum {  C2C,  B2C,  B2B,  };

    CreateAccountInput._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(CreateAccountInputBuilder b) => b;

    factory CreateAccountInput([void updates(CreateAccountInputBuilder b)]) = _$CreateAccountInput;

    @BuiltValueSerializer(custom: true)
    static Serializer<CreateAccountInput> get serializer => _$CreateAccountInputSerializer();
}

class _$CreateAccountInputSerializer implements StructuredSerializer<CreateAccountInput> {
    @override
    final Iterable<Type> types = const [CreateAccountInput, _$CreateAccountInput];

    @override
    final String wireName = r'CreateAccountInput';

    @override
    Iterable<Object?> serialize(Serializers serializers, CreateAccountInput object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.firstName != null) {
            result
                ..add(r'firstName')
                ..add(serializers.serialize(object.firstName,
                    specifiedType: const FullType(String)));
        }
        if (object.lastName != null) {
            result
                ..add(r'lastName')
                ..add(serializers.serialize(object.lastName,
                    specifiedType: const FullType(String)));
        }
        if (object.email != null) {
            result
                ..add(r'email')
                ..add(serializers.serialize(object.email,
                    specifiedType: const FullType(String)));
        }
        if (object.phoneNumber != null) {
            result
                ..add(r'phoneNumber')
                ..add(serializers.serialize(object.phoneNumber,
                    specifiedType: const FullType(String)));
        }
        if (object.gender != null) {
            result
                ..add(r'gender')
                ..add(serializers.serialize(object.gender,
                    specifiedType: const FullType(AccountGender)));
        }
        if (object.bio != null) {
            result
                ..add(r'bio')
                ..add(serializers.serialize(object.bio,
                    specifiedType: const FullType(String)));
        }
        if (object.avatarUrl != null) {
            result
                ..add(r'avatarUrl')
                ..add(serializers.serialize(object.avatarUrl,
                    specifiedType: const FullType(String)));
        }
        if (object.accountType != null) {
            result
                ..add(r'accountType')
                ..add(serializers.serialize(object.accountType,
                    specifiedType: const FullType(AccountType)));
        }
        return result;
    }

    @override
    CreateAccountInput deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = CreateAccountInputBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'firstName':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.firstName = valueDes;
                    break;
                case r'lastName':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.lastName = valueDes;
                    break;
                case r'email':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.email = valueDes;
                    break;
                case r'phoneNumber':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.phoneNumber = valueDes;
                    break;
                case r'gender':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(AccountGender)) as AccountGender;
                    result.gender = valueDes;
                    break;
                case r'bio':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.bio = valueDes;
                    break;
                case r'avatarUrl':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.avatarUrl = valueDes;
                    break;
                case r'accountType':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(AccountType)) as AccountType;
                    result.accountType = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

