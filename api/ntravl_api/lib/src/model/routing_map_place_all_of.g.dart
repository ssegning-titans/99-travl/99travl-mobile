// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'routing_map_place_all_of.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$RoutingMapPlaceAllOf extends RoutingMapPlaceAllOf {
  @override
  final double? order;

  factory _$RoutingMapPlaceAllOf(
          [void Function(RoutingMapPlaceAllOfBuilder)? updates]) =>
      (new RoutingMapPlaceAllOfBuilder()..update(updates)).build();

  _$RoutingMapPlaceAllOf._({this.order}) : super._();

  @override
  RoutingMapPlaceAllOf rebuild(
          void Function(RoutingMapPlaceAllOfBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RoutingMapPlaceAllOfBuilder toBuilder() =>
      new RoutingMapPlaceAllOfBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is RoutingMapPlaceAllOf && order == other.order;
  }

  @override
  int get hashCode {
    return $jf($jc(0, order.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('RoutingMapPlaceAllOf')
          ..add('order', order))
        .toString();
  }
}

class RoutingMapPlaceAllOfBuilder
    implements Builder<RoutingMapPlaceAllOf, RoutingMapPlaceAllOfBuilder> {
  _$RoutingMapPlaceAllOf? _$v;

  double? _order;
  double? get order => _$this._order;
  set order(double? order) => _$this._order = order;

  RoutingMapPlaceAllOfBuilder() {
    RoutingMapPlaceAllOf._defaults(this);
  }

  RoutingMapPlaceAllOfBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _order = $v.order;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RoutingMapPlaceAllOf other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$RoutingMapPlaceAllOf;
  }

  @override
  void update(void Function(RoutingMapPlaceAllOfBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$RoutingMapPlaceAllOf build() {
    final _$result = _$v ?? new _$RoutingMapPlaceAllOf._(order: order);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
