//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'chat_status.g.dart';

class ChatStatus extends EnumClass {

  @BuiltValueEnumConst(wireName: r'ACTIVE')
  static const ChatStatus ACTIVE = _$ACTIVE;
  @BuiltValueEnumConst(wireName: r'ARCHIVED')
  static const ChatStatus ARCHIVED = _$ARCHIVED;

  static Serializer<ChatStatus> get serializer => _$chatStatusSerializer;

  const ChatStatus._(String name): super(name);

  static BuiltSet<ChatStatus> get values => _$values;
  static ChatStatus valueOf(String name) => _$valueOf(name);
}

/// Optionally, enum_class can generate a mixin to go with your enum for use
/// with Angular. It exposes your enum constants as getters. So, if you mix it
/// in to your Dart component class, the values become available to the
/// corresponding Angular template.
///
/// Trigger mixin generation by writing a line like this one next to your enum.
abstract class ChatStatusMixin = Object with _$ChatStatusMixin;

