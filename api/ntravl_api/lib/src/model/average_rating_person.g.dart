// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'average_rating_person.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$AverageRatingPerson extends AverageRatingPerson {
  @override
  final double value;
  @override
  final int count;
  @override
  final String personId;

  factory _$AverageRatingPerson(
          [void Function(AverageRatingPersonBuilder)? updates]) =>
      (new AverageRatingPersonBuilder()..update(updates)).build();

  _$AverageRatingPerson._(
      {required this.value, required this.count, required this.personId})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        value, 'AverageRatingPerson', 'value');
    BuiltValueNullFieldError.checkNotNull(
        count, 'AverageRatingPerson', 'count');
    BuiltValueNullFieldError.checkNotNull(
        personId, 'AverageRatingPerson', 'personId');
  }

  @override
  AverageRatingPerson rebuild(
          void Function(AverageRatingPersonBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AverageRatingPersonBuilder toBuilder() =>
      new AverageRatingPersonBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AverageRatingPerson &&
        value == other.value &&
        count == other.count &&
        personId == other.personId;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, value.hashCode), count.hashCode), personId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('AverageRatingPerson')
          ..add('value', value)
          ..add('count', count)
          ..add('personId', personId))
        .toString();
  }
}

class AverageRatingPersonBuilder
    implements Builder<AverageRatingPerson, AverageRatingPersonBuilder> {
  _$AverageRatingPerson? _$v;

  double? _value;
  double? get value => _$this._value;
  set value(double? value) => _$this._value = value;

  int? _count;
  int? get count => _$this._count;
  set count(int? count) => _$this._count = count;

  String? _personId;
  String? get personId => _$this._personId;
  set personId(String? personId) => _$this._personId = personId;

  AverageRatingPersonBuilder() {
    AverageRatingPerson._defaults(this);
  }

  AverageRatingPersonBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _value = $v.value;
      _count = $v.count;
      _personId = $v.personId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AverageRatingPerson other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$AverageRatingPerson;
  }

  @override
  void update(void Function(AverageRatingPersonBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$AverageRatingPerson build() {
    final _$result = _$v ??
        new _$AverageRatingPerson._(
            value: BuiltValueNullFieldError.checkNotNull(
                value, 'AverageRatingPerson', 'value'),
            count: BuiltValueNullFieldError.checkNotNull(
                count, 'AverageRatingPerson', 'count'),
            personId: BuiltValueNullFieldError.checkNotNull(
                personId, 'AverageRatingPerson', 'personId'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
