// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lat_lng.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$LatLng extends LatLng {
  @override
  final double? lat;
  @override
  final double? lng;

  factory _$LatLng([void Function(LatLngBuilder)? updates]) =>
      (new LatLngBuilder()..update(updates)).build();

  _$LatLng._({this.lat, this.lng}) : super._();

  @override
  LatLng rebuild(void Function(LatLngBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LatLngBuilder toBuilder() => new LatLngBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is LatLng && lat == other.lat && lng == other.lng;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, lat.hashCode), lng.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('LatLng')
          ..add('lat', lat)
          ..add('lng', lng))
        .toString();
  }
}

class LatLngBuilder implements Builder<LatLng, LatLngBuilder> {
  _$LatLng? _$v;

  double? _lat;
  double? get lat => _$this._lat;
  set lat(double? lat) => _$this._lat = lat;

  double? _lng;
  double? get lng => _$this._lng;
  set lng(double? lng) => _$this._lng = lng;

  LatLngBuilder() {
    LatLng._defaults(this);
  }

  LatLngBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _lat = $v.lat;
      _lng = $v.lng;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LatLng other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$LatLng;
  }

  @override
  void update(void Function(LatLngBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$LatLng build() {
    final _$result = _$v ?? new _$LatLng._(lat: lat, lng: lng);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
