// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contact_chat_message.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ContactChatMessage extends ContactChatMessage {
  @override
  final String? id;
  @override
  final int? createdAt;
  @override
  final String authorId;
  @override
  final String chatId;
  @override
  final ChatMessageType messageType;
  @override
  final bool? seen;
  @override
  final BuiltSet<ChatContact> contacts;

  factory _$ContactChatMessage(
          [void Function(ContactChatMessageBuilder)? updates]) =>
      (new ContactChatMessageBuilder()..update(updates)).build();

  _$ContactChatMessage._(
      {this.id,
      this.createdAt,
      required this.authorId,
      required this.chatId,
      required this.messageType,
      this.seen,
      required this.contacts})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        authorId, 'ContactChatMessage', 'authorId');
    BuiltValueNullFieldError.checkNotNull(
        chatId, 'ContactChatMessage', 'chatId');
    BuiltValueNullFieldError.checkNotNull(
        messageType, 'ContactChatMessage', 'messageType');
    BuiltValueNullFieldError.checkNotNull(
        contacts, 'ContactChatMessage', 'contacts');
  }

  @override
  ContactChatMessage rebuild(
          void Function(ContactChatMessageBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ContactChatMessageBuilder toBuilder() =>
      new ContactChatMessageBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ContactChatMessage &&
        id == other.id &&
        createdAt == other.createdAt &&
        authorId == other.authorId &&
        chatId == other.chatId &&
        messageType == other.messageType &&
        seen == other.seen &&
        contacts == other.contacts;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, id.hashCode), createdAt.hashCode),
                        authorId.hashCode),
                    chatId.hashCode),
                messageType.hashCode),
            seen.hashCode),
        contacts.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ContactChatMessage')
          ..add('id', id)
          ..add('createdAt', createdAt)
          ..add('authorId', authorId)
          ..add('chatId', chatId)
          ..add('messageType', messageType)
          ..add('seen', seen)
          ..add('contacts', contacts))
        .toString();
  }
}

class ContactChatMessageBuilder
    implements Builder<ContactChatMessage, ContactChatMessageBuilder> {
  _$ContactChatMessage? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  int? _createdAt;
  int? get createdAt => _$this._createdAt;
  set createdAt(int? createdAt) => _$this._createdAt = createdAt;

  String? _authorId;
  String? get authorId => _$this._authorId;
  set authorId(String? authorId) => _$this._authorId = authorId;

  String? _chatId;
  String? get chatId => _$this._chatId;
  set chatId(String? chatId) => _$this._chatId = chatId;

  ChatMessageType? _messageType;
  ChatMessageType? get messageType => _$this._messageType;
  set messageType(ChatMessageType? messageType) =>
      _$this._messageType = messageType;

  bool? _seen;
  bool? get seen => _$this._seen;
  set seen(bool? seen) => _$this._seen = seen;

  SetBuilder<ChatContact>? _contacts;
  SetBuilder<ChatContact> get contacts =>
      _$this._contacts ??= new SetBuilder<ChatContact>();
  set contacts(SetBuilder<ChatContact>? contacts) =>
      _$this._contacts = contacts;

  ContactChatMessageBuilder() {
    ContactChatMessage._defaults(this);
  }

  ContactChatMessageBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _createdAt = $v.createdAt;
      _authorId = $v.authorId;
      _chatId = $v.chatId;
      _messageType = $v.messageType;
      _seen = $v.seen;
      _contacts = $v.contacts.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ContactChatMessage other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ContactChatMessage;
  }

  @override
  void update(void Function(ContactChatMessageBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ContactChatMessage build() {
    _$ContactChatMessage _$result;
    try {
      _$result = _$v ??
          new _$ContactChatMessage._(
              id: id,
              createdAt: createdAt,
              authorId: BuiltValueNullFieldError.checkNotNull(
                  authorId, 'ContactChatMessage', 'authorId'),
              chatId: BuiltValueNullFieldError.checkNotNull(
                  chatId, 'ContactChatMessage', 'chatId'),
              messageType: BuiltValueNullFieldError.checkNotNull(
                  messageType, 'ContactChatMessage', 'messageType'),
              seen: seen,
              contacts: contacts.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'contacts';
        contacts.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ContactChatMessage', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
