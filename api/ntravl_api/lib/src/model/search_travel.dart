//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:ntravl_api/src/model/travel_point.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'search_travel.g.dart';

/// SearchTravel
///
/// Properties:
/// * [points] 
abstract class SearchTravel implements Built<SearchTravel, SearchTravelBuilder> {
    @BuiltValueField(wireName: r'points')
    BuiltList<TravelPoint> get points;

    SearchTravel._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(SearchTravelBuilder b) => b;

    factory SearchTravel([void updates(SearchTravelBuilder b)]) = _$SearchTravel;

    @BuiltValueSerializer(custom: true)
    static Serializer<SearchTravel> get serializer => _$SearchTravelSerializer();
}

class _$SearchTravelSerializer implements StructuredSerializer<SearchTravel> {
    @override
    final Iterable<Type> types = const [SearchTravel, _$SearchTravel];

    @override
    final String wireName = r'SearchTravel';

    @override
    Iterable<Object?> serialize(Serializers serializers, SearchTravel object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'points')
            ..add(serializers.serialize(object.points,
                specifiedType: const FullType(BuiltList, [FullType(TravelPoint)])));
        return result;
    }

    @override
    SearchTravel deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = SearchTravelBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'points':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(BuiltList, [FullType(TravelPoint)])) as BuiltList<TravelPoint>;
                    result.points.replace(valueDes);
                    break;
            }
        }
        return result.build();
    }
}

