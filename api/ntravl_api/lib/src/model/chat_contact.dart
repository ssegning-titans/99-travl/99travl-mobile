//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'chat_contact.g.dart';

/// This is either a reference to an account existing in app, or just a basic contact to someone existing outside the app. In the first case, `avatar` and `description` may be null. The `isAppUser` is the discriminant and is always required.
///
/// Properties:
/// * [id] 
/// * [creationDate] 
/// * [isAppUser] 
/// * [accountId] 
/// * [phoneNumber] 
/// * [description] 
/// * [firstName] 
/// * [lastName] 
/// * [avatar] 
abstract class ChatContact implements Built<ChatContact, ChatContactBuilder> {
    @BuiltValueField(wireName: r'id')
    String? get id;

    @BuiltValueField(wireName: r'creationDate')
    int? get creationDate;

    @BuiltValueField(wireName: r'isAppUser')
    bool get isAppUser;

    @BuiltValueField(wireName: r'accountId')
    String? get accountId;

    @BuiltValueField(wireName: r'phoneNumber')
    String get phoneNumber;

    @BuiltValueField(wireName: r'description')
    String? get description;

    @BuiltValueField(wireName: r'firstName')
    String get firstName;

    @BuiltValueField(wireName: r'lastName')
    String get lastName;

    @BuiltValueField(wireName: r'avatar')
    String? get avatar;

    ChatContact._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(ChatContactBuilder b) => b;

    factory ChatContact([void updates(ChatContactBuilder b)]) = _$ChatContact;

    @BuiltValueSerializer(custom: true)
    static Serializer<ChatContact> get serializer => _$ChatContactSerializer();
}

class _$ChatContactSerializer implements StructuredSerializer<ChatContact> {
    @override
    final Iterable<Type> types = const [ChatContact, _$ChatContact];

    @override
    final String wireName = r'ChatContact';

    @override
    Iterable<Object?> serialize(Serializers serializers, ChatContact object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.id != null) {
            result
                ..add(r'id')
                ..add(serializers.serialize(object.id,
                    specifiedType: const FullType(String)));
        }
        if (object.creationDate != null) {
            result
                ..add(r'creationDate')
                ..add(serializers.serialize(object.creationDate,
                    specifiedType: const FullType(int)));
        }
        result
            ..add(r'isAppUser')
            ..add(serializers.serialize(object.isAppUser,
                specifiedType: const FullType(bool)));
        if (object.accountId != null) {
            result
                ..add(r'accountId')
                ..add(serializers.serialize(object.accountId,
                    specifiedType: const FullType(String)));
        }
        result
            ..add(r'phoneNumber')
            ..add(serializers.serialize(object.phoneNumber,
                specifiedType: const FullType(String)));
        if (object.description != null) {
            result
                ..add(r'description')
                ..add(serializers.serialize(object.description,
                    specifiedType: const FullType(String)));
        }
        result
            ..add(r'firstName')
            ..add(serializers.serialize(object.firstName,
                specifiedType: const FullType(String)));
        result
            ..add(r'lastName')
            ..add(serializers.serialize(object.lastName,
                specifiedType: const FullType(String)));
        if (object.avatar != null) {
            result
                ..add(r'avatar')
                ..add(serializers.serialize(object.avatar,
                    specifiedType: const FullType(String)));
        }
        return result;
    }

    @override
    ChatContact deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = ChatContactBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'id':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.id = valueDes;
                    break;
                case r'creationDate':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    result.creationDate = valueDes;
                    break;
                case r'isAppUser':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(bool)) as bool;
                    result.isAppUser = valueDes;
                    break;
                case r'accountId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.accountId = valueDes;
                    break;
                case r'phoneNumber':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.phoneNumber = valueDes;
                    break;
                case r'description':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.description = valueDes;
                    break;
                case r'firstName':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.firstName = valueDes;
                    break;
                case r'lastName':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.lastName = valueDes;
                    break;
                case r'avatar':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.avatar = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

