//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:ntravl_api/src/model/account_type.dart';
import 'package:ntravl_api/src/model/account_gender.dart';
import 'package:ntravl_api/src/model/account_status.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'account.g.dart';

/// Account
///
/// Properties:
/// * [createdAt] 
/// * [updatedAt] 
/// * [id] 
/// * [firstName] 
/// * [lastName] 
/// * [locale] 
/// * [avatarUrl] 
/// * [email] 
/// * [emailVerified] 
/// * [gender] 
/// * [status] 
/// * [phoneNumber] 
/// * [phoneNumberVerified] 
/// * [bio] 
/// * [accountType] 
abstract class Account implements Built<Account, AccountBuilder> {
    @BuiltValueField(wireName: r'createdAt')
    int get createdAt;

    @BuiltValueField(wireName: r'updatedAt')
    int get updatedAt;

    @BuiltValueField(wireName: r'id')
    String get id;

    @BuiltValueField(wireName: r'firstName')
    String? get firstName;

    @BuiltValueField(wireName: r'lastName')
    String? get lastName;

    @BuiltValueField(wireName: r'locale')
    String? get locale;

    @BuiltValueField(wireName: r'avatarUrl')
    String? get avatarUrl;

    @BuiltValueField(wireName: r'email')
    String get email;

    @BuiltValueField(wireName: r'emailVerified')
    bool? get emailVerified;

    @BuiltValueField(wireName: r'gender')
    AccountGender? get gender;
    // enum genderEnum {  MALE,  FEMALE,  OTHER,  };

    @BuiltValueField(wireName: r'status')
    AccountStatus get status;
    // enum statusEnum {  ALLOWED,  BLOCKED,  };

    @BuiltValueField(wireName: r'phoneNumber')
    String? get phoneNumber;

    @BuiltValueField(wireName: r'phoneNumberVerified')
    bool? get phoneNumberVerified;

    @BuiltValueField(wireName: r'bio')
    String? get bio;

    @BuiltValueField(wireName: r'accountType')
    AccountType? get accountType;
    // enum accountTypeEnum {  C2C,  B2C,  B2B,  };

    Account._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(AccountBuilder b) => b;

    factory Account([void updates(AccountBuilder b)]) = _$Account;

    @BuiltValueSerializer(custom: true)
    static Serializer<Account> get serializer => _$AccountSerializer();
}

class _$AccountSerializer implements StructuredSerializer<Account> {
    @override
    final Iterable<Type> types = const [Account, _$Account];

    @override
    final String wireName = r'Account';

    @override
    Iterable<Object?> serialize(Serializers serializers, Account object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'createdAt')
            ..add(serializers.serialize(object.createdAt,
                specifiedType: const FullType(int)));
        result
            ..add(r'updatedAt')
            ..add(serializers.serialize(object.updatedAt,
                specifiedType: const FullType(int)));
        result
            ..add(r'id')
            ..add(serializers.serialize(object.id,
                specifiedType: const FullType(String)));
        if (object.firstName != null) {
            result
                ..add(r'firstName')
                ..add(serializers.serialize(object.firstName,
                    specifiedType: const FullType(String)));
        }
        if (object.lastName != null) {
            result
                ..add(r'lastName')
                ..add(serializers.serialize(object.lastName,
                    specifiedType: const FullType(String)));
        }
        if (object.locale != null) {
            result
                ..add(r'locale')
                ..add(serializers.serialize(object.locale,
                    specifiedType: const FullType(String)));
        }
        if (object.avatarUrl != null) {
            result
                ..add(r'avatarUrl')
                ..add(serializers.serialize(object.avatarUrl,
                    specifiedType: const FullType(String)));
        }
        result
            ..add(r'email')
            ..add(serializers.serialize(object.email,
                specifiedType: const FullType(String)));
        if (object.emailVerified != null) {
            result
                ..add(r'emailVerified')
                ..add(serializers.serialize(object.emailVerified,
                    specifiedType: const FullType(bool)));
        }
        if (object.gender != null) {
            result
                ..add(r'gender')
                ..add(serializers.serialize(object.gender,
                    specifiedType: const FullType(AccountGender)));
        }
        result
            ..add(r'status')
            ..add(serializers.serialize(object.status,
                specifiedType: const FullType(AccountStatus)));
        if (object.phoneNumber != null) {
            result
                ..add(r'phoneNumber')
                ..add(serializers.serialize(object.phoneNumber,
                    specifiedType: const FullType(String)));
        }
        if (object.phoneNumberVerified != null) {
            result
                ..add(r'phoneNumberVerified')
                ..add(serializers.serialize(object.phoneNumberVerified,
                    specifiedType: const FullType(bool)));
        }
        if (object.bio != null) {
            result
                ..add(r'bio')
                ..add(serializers.serialize(object.bio,
                    specifiedType: const FullType(String)));
        }
        if (object.accountType != null) {
            result
                ..add(r'accountType')
                ..add(serializers.serialize(object.accountType,
                    specifiedType: const FullType(AccountType)));
        }
        return result;
    }

    @override
    Account deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = AccountBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'createdAt':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    result.createdAt = valueDes;
                    break;
                case r'updatedAt':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    result.updatedAt = valueDes;
                    break;
                case r'id':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.id = valueDes;
                    break;
                case r'firstName':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.firstName = valueDes;
                    break;
                case r'lastName':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.lastName = valueDes;
                    break;
                case r'locale':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.locale = valueDes;
                    break;
                case r'avatarUrl':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.avatarUrl = valueDes;
                    break;
                case r'email':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.email = valueDes;
                    break;
                case r'emailVerified':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(bool)) as bool;
                    result.emailVerified = valueDes;
                    break;
                case r'gender':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(AccountGender)) as AccountGender;
                    result.gender = valueDes;
                    break;
                case r'status':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(AccountStatus)) as AccountStatus;
                    result.status = valueDes;
                    break;
                case r'phoneNumber':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.phoneNumber = valueDes;
                    break;
                case r'phoneNumberVerified':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(bool)) as bool;
                    result.phoneNumberVerified = valueDes;
                    break;
                case r'bio':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.bio = valueDes;
                    break;
                case r'accountType':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(AccountType)) as AccountType;
                    result.accountType = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

