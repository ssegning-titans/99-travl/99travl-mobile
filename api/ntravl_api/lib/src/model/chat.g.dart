// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$Chat extends Chat {
  @override
  final String? id;
  @override
  final int? createdAt;
  @override
  final BuiltList<String>? members;
  @override
  final String? travelId;
  @override
  final ChatStatus? status;

  factory _$Chat([void Function(ChatBuilder)? updates]) =>
      (new ChatBuilder()..update(updates)).build();

  _$Chat._({this.id, this.createdAt, this.members, this.travelId, this.status})
      : super._();

  @override
  Chat rebuild(void Function(ChatBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ChatBuilder toBuilder() => new ChatBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Chat &&
        id == other.id &&
        createdAt == other.createdAt &&
        members == other.members &&
        travelId == other.travelId &&
        status == other.status;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc($jc(0, id.hashCode), createdAt.hashCode), members.hashCode),
            travelId.hashCode),
        status.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Chat')
          ..add('id', id)
          ..add('createdAt', createdAt)
          ..add('members', members)
          ..add('travelId', travelId)
          ..add('status', status))
        .toString();
  }
}

class ChatBuilder implements Builder<Chat, ChatBuilder> {
  _$Chat? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  int? _createdAt;
  int? get createdAt => _$this._createdAt;
  set createdAt(int? createdAt) => _$this._createdAt = createdAt;

  ListBuilder<String>? _members;
  ListBuilder<String> get members =>
      _$this._members ??= new ListBuilder<String>();
  set members(ListBuilder<String>? members) => _$this._members = members;

  String? _travelId;
  String? get travelId => _$this._travelId;
  set travelId(String? travelId) => _$this._travelId = travelId;

  ChatStatus? _status;
  ChatStatus? get status => _$this._status;
  set status(ChatStatus? status) => _$this._status = status;

  ChatBuilder() {
    Chat._defaults(this);
  }

  ChatBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _createdAt = $v.createdAt;
      _members = $v.members?.toBuilder();
      _travelId = $v.travelId;
      _status = $v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Chat other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Chat;
  }

  @override
  void update(void Function(ChatBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Chat build() {
    _$Chat _$result;
    try {
      _$result = _$v ??
          new _$Chat._(
              id: id,
              createdAt: createdAt,
              members: _members?.build(),
              travelId: travelId,
              status: status);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'members';
        _members?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'Chat', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
