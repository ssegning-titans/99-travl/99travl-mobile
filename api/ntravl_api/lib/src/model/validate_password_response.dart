//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'validate_password_response.g.dart';

/// ValidatePasswordResponse
///
/// Properties:
/// * [valid] 
abstract class ValidatePasswordResponse implements Built<ValidatePasswordResponse, ValidatePasswordResponseBuilder> {
    @BuiltValueField(wireName: r'valid')
    bool get valid;

    ValidatePasswordResponse._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(ValidatePasswordResponseBuilder b) => b;

    factory ValidatePasswordResponse([void updates(ValidatePasswordResponseBuilder b)]) = _$ValidatePasswordResponse;

    @BuiltValueSerializer(custom: true)
    static Serializer<ValidatePasswordResponse> get serializer => _$ValidatePasswordResponseSerializer();
}

class _$ValidatePasswordResponseSerializer implements StructuredSerializer<ValidatePasswordResponse> {
    @override
    final Iterable<Type> types = const [ValidatePasswordResponse, _$ValidatePasswordResponse];

    @override
    final String wireName = r'ValidatePasswordResponse';

    @override
    Iterable<Object?> serialize(Serializers serializers, ValidatePasswordResponse object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'valid')
            ..add(serializers.serialize(object.valid,
                specifiedType: const FullType(bool)));
        return result;
    }

    @override
    ValidatePasswordResponse deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = ValidatePasswordResponseBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'valid':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(bool)) as bool;
                    result.valid = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

