// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'phone_tan_validate_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$PhoneTanValidateRequest extends PhoneTanValidateRequest {
  @override
  final String? secret;
  @override
  final String? phoneNumber;
  @override
  final int? code;

  factory _$PhoneTanValidateRequest(
          [void Function(PhoneTanValidateRequestBuilder)? updates]) =>
      (new PhoneTanValidateRequestBuilder()..update(updates)).build();

  _$PhoneTanValidateRequest._({this.secret, this.phoneNumber, this.code})
      : super._();

  @override
  PhoneTanValidateRequest rebuild(
          void Function(PhoneTanValidateRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PhoneTanValidateRequestBuilder toBuilder() =>
      new PhoneTanValidateRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PhoneTanValidateRequest &&
        secret == other.secret &&
        phoneNumber == other.phoneNumber &&
        code == other.code;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, secret.hashCode), phoneNumber.hashCode), code.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PhoneTanValidateRequest')
          ..add('secret', secret)
          ..add('phoneNumber', phoneNumber)
          ..add('code', code))
        .toString();
  }
}

class PhoneTanValidateRequestBuilder
    implements
        Builder<PhoneTanValidateRequest, PhoneTanValidateRequestBuilder> {
  _$PhoneTanValidateRequest? _$v;

  String? _secret;
  String? get secret => _$this._secret;
  set secret(String? secret) => _$this._secret = secret;

  String? _phoneNumber;
  String? get phoneNumber => _$this._phoneNumber;
  set phoneNumber(String? phoneNumber) => _$this._phoneNumber = phoneNumber;

  int? _code;
  int? get code => _$this._code;
  set code(int? code) => _$this._code = code;

  PhoneTanValidateRequestBuilder() {
    PhoneTanValidateRequest._defaults(this);
  }

  PhoneTanValidateRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _secret = $v.secret;
      _phoneNumber = $v.phoneNumber;
      _code = $v.code;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PhoneTanValidateRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$PhoneTanValidateRequest;
  }

  @override
  void update(void Function(PhoneTanValidateRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PhoneTanValidateRequest build() {
    final _$result = _$v ??
        new _$PhoneTanValidateRequest._(
            secret: secret, phoneNumber: phoneNumber, code: code);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
