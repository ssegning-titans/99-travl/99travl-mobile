//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:ntravl_api/src/model/address_component.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'map_place.g.dart';

/// MapPlace
///
/// Properties:
/// * [placeId] 
/// * [latitude] 
/// * [longitude] 
/// * [formattedAddress] 
/// * [addressComponent] 
abstract class MapPlace implements Built<MapPlace, MapPlaceBuilder> {
    @BuiltValueField(wireName: r'placeId')
    String? get placeId;

    @BuiltValueField(wireName: r'latitude')
    double? get latitude;

    @BuiltValueField(wireName: r'longitude')
    double? get longitude;

    @BuiltValueField(wireName: r'formattedAddress')
    String? get formattedAddress;

    @BuiltValueField(wireName: r'addressComponent')
    BuiltList<AddressComponent>? get addressComponent;

    MapPlace._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(MapPlaceBuilder b) => b;

    factory MapPlace([void updates(MapPlaceBuilder b)]) = _$MapPlace;

    @BuiltValueSerializer(custom: true)
    static Serializer<MapPlace> get serializer => _$MapPlaceSerializer();
}

class _$MapPlaceSerializer implements StructuredSerializer<MapPlace> {
    @override
    final Iterable<Type> types = const [MapPlace, _$MapPlace];

    @override
    final String wireName = r'MapPlace';

    @override
    Iterable<Object?> serialize(Serializers serializers, MapPlace object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.placeId != null) {
            result
                ..add(r'placeId')
                ..add(serializers.serialize(object.placeId,
                    specifiedType: const FullType(String)));
        }
        if (object.latitude != null) {
            result
                ..add(r'latitude')
                ..add(serializers.serialize(object.latitude,
                    specifiedType: const FullType(double)));
        }
        if (object.longitude != null) {
            result
                ..add(r'longitude')
                ..add(serializers.serialize(object.longitude,
                    specifiedType: const FullType(double)));
        }
        if (object.formattedAddress != null) {
            result
                ..add(r'formattedAddress')
                ..add(serializers.serialize(object.formattedAddress,
                    specifiedType: const FullType(String)));
        }
        if (object.addressComponent != null) {
            result
                ..add(r'addressComponent')
                ..add(serializers.serialize(object.addressComponent,
                    specifiedType: const FullType(BuiltList, [FullType(AddressComponent)])));
        }
        return result;
    }

    @override
    MapPlace deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = MapPlaceBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'placeId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.placeId = valueDes;
                    break;
                case r'latitude':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(double)) as double;
                    result.latitude = valueDes;
                    break;
                case r'longitude':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(double)) as double;
                    result.longitude = valueDes;
                    break;
                case r'formattedAddress':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.formattedAddress = valueDes;
                    break;
                case r'addressComponent':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(BuiltList, [FullType(AddressComponent)])) as BuiltList<AddressComponent>;
                    result.addressComponent.replace(valueDes);
                    break;
            }
        }
        return result.build();
    }
}

