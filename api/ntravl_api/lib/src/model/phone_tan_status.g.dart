// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'phone_tan_status.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$PhoneTanStatus extends PhoneTanStatus {
  @override
  final bool? status;

  factory _$PhoneTanStatus([void Function(PhoneTanStatusBuilder)? updates]) =>
      (new PhoneTanStatusBuilder()..update(updates)).build();

  _$PhoneTanStatus._({this.status}) : super._();

  @override
  PhoneTanStatus rebuild(void Function(PhoneTanStatusBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PhoneTanStatusBuilder toBuilder() =>
      new PhoneTanStatusBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PhoneTanStatus && status == other.status;
  }

  @override
  int get hashCode {
    return $jf($jc(0, status.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PhoneTanStatus')
          ..add('status', status))
        .toString();
  }
}

class PhoneTanStatusBuilder
    implements Builder<PhoneTanStatus, PhoneTanStatusBuilder> {
  _$PhoneTanStatus? _$v;

  bool? _status;
  bool? get status => _$this._status;
  set status(bool? status) => _$this._status = status;

  PhoneTanStatusBuilder() {
    PhoneTanStatus._defaults(this);
  }

  PhoneTanStatusBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _status = $v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PhoneTanStatus other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$PhoneTanStatus;
  }

  @override
  void update(void Function(PhoneTanStatusBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PhoneTanStatus build() {
    final _$result = _$v ?? new _$PhoneTanStatus._(status: status);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
