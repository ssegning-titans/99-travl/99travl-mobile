//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:ntravl_api/src/model/lat_lng.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'travel_point.g.dart';

/// TravelPoint
///
/// Properties:
/// * [id] 
/// * [priority] 
/// * [creationDate] 
/// * [passageTime] 
/// * [travelId] 
/// * [region] 
/// * [formattedName] 
/// * [street] 
/// * [houseNumber] 
/// * [city] 
/// * [zip] 
/// * [country] 
/// * [location] 
abstract class TravelPoint implements Built<TravelPoint, TravelPointBuilder> {
    @BuiltValueField(wireName: r'id')
    String? get id;

    @BuiltValueField(wireName: r'priority')
    int? get priority;

    @BuiltValueField(wireName: r'creationDate')
    int? get creationDate;

    @BuiltValueField(wireName: r'passageTime')
    int? get passageTime;

    @BuiltValueField(wireName: r'travelId')
    String? get travelId;

    @BuiltValueField(wireName: r'region')
    String? get region;

    @BuiltValueField(wireName: r'formattedName')
    String? get formattedName;

    @BuiltValueField(wireName: r'street')
    String? get street;

    @BuiltValueField(wireName: r'houseNumber')
    String? get houseNumber;

    @BuiltValueField(wireName: r'city')
    String? get city;

    @BuiltValueField(wireName: r'zip')
    String? get zip;

    @BuiltValueField(wireName: r'country')
    String get country;

    @BuiltValueField(wireName: r'location')
    LatLng get location;

    TravelPoint._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(TravelPointBuilder b) => b
        ..priority = 1;

    factory TravelPoint([void updates(TravelPointBuilder b)]) = _$TravelPoint;

    @BuiltValueSerializer(custom: true)
    static Serializer<TravelPoint> get serializer => _$TravelPointSerializer();
}

class _$TravelPointSerializer implements StructuredSerializer<TravelPoint> {
    @override
    final Iterable<Type> types = const [TravelPoint, _$TravelPoint];

    @override
    final String wireName = r'TravelPoint';

    @override
    Iterable<Object?> serialize(Serializers serializers, TravelPoint object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.id != null) {
            result
                ..add(r'id')
                ..add(serializers.serialize(object.id,
                    specifiedType: const FullType(String)));
        }
        if (object.priority != null) {
            result
                ..add(r'priority')
                ..add(serializers.serialize(object.priority,
                    specifiedType: const FullType(int)));
        }
        if (object.creationDate != null) {
            result
                ..add(r'creationDate')
                ..add(serializers.serialize(object.creationDate,
                    specifiedType: const FullType(int)));
        }
        if (object.passageTime != null) {
            result
                ..add(r'passageTime')
                ..add(serializers.serialize(object.passageTime,
                    specifiedType: const FullType(int)));
        }
        if (object.travelId != null) {
            result
                ..add(r'travelId')
                ..add(serializers.serialize(object.travelId,
                    specifiedType: const FullType(String)));
        }
        if (object.region != null) {
            result
                ..add(r'region')
                ..add(serializers.serialize(object.region,
                    specifiedType: const FullType(String)));
        }
        if (object.formattedName != null) {
            result
                ..add(r'formattedName')
                ..add(serializers.serialize(object.formattedName,
                    specifiedType: const FullType(String)));
        }
        if (object.street != null) {
            result
                ..add(r'street')
                ..add(serializers.serialize(object.street,
                    specifiedType: const FullType(String)));
        }
        if (object.houseNumber != null) {
            result
                ..add(r'houseNumber')
                ..add(serializers.serialize(object.houseNumber,
                    specifiedType: const FullType(String)));
        }
        if (object.city != null) {
            result
                ..add(r'city')
                ..add(serializers.serialize(object.city,
                    specifiedType: const FullType(String)));
        }
        if (object.zip != null) {
            result
                ..add(r'zip')
                ..add(serializers.serialize(object.zip,
                    specifiedType: const FullType(String)));
        }
        result
            ..add(r'country')
            ..add(serializers.serialize(object.country,
                specifiedType: const FullType(String)));
        result
            ..add(r'location')
            ..add(serializers.serialize(object.location,
                specifiedType: const FullType(LatLng)));
        return result;
    }

    @override
    TravelPoint deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = TravelPointBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'id':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.id = valueDes;
                    break;
                case r'priority':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    result.priority = valueDes;
                    break;
                case r'creationDate':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    result.creationDate = valueDes;
                    break;
                case r'passageTime':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    result.passageTime = valueDes;
                    break;
                case r'travelId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.travelId = valueDes;
                    break;
                case r'region':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.region = valueDes;
                    break;
                case r'formattedName':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.formattedName = valueDes;
                    break;
                case r'street':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.street = valueDes;
                    break;
                case r'houseNumber':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.houseNumber = valueDes;
                    break;
                case r'city':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.city = valueDes;
                    break;
                case r'zip':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.zip = valueDes;
                    break;
                case r'country':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.country = valueDes;
                    break;
                case r'location':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(LatLng)) as LatLng;
                    result.location.replace(valueDes);
                    break;
            }
        }
        return result.build();
    }
}

