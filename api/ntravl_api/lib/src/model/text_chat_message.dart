//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:ntravl_api/src/model/base_chat_message.dart';
import 'package:ntravl_api/src/model/chat_message_type.dart';
import 'package:ntravl_api/src/model/text_chat_message_all_of.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'text_chat_message.g.dart';

// ignore_for_file: unused_import

/// TextChatMessage
///
/// Properties:
/// * [id] 
/// * [createdAt] 
/// * [authorId] 
/// * [chatId] 
/// * [messageType] 
/// * [seen] 
/// * [message] 
abstract class TextChatMessage implements Built<TextChatMessage, TextChatMessageBuilder> {
    @BuiltValueField(wireName: r'id')
    String? get id;

    @BuiltValueField(wireName: r'createdAt')
    int? get createdAt;

    @BuiltValueField(wireName: r'authorId')
    String get authorId;

    @BuiltValueField(wireName: r'chatId')
    String get chatId;

    @BuiltValueField(wireName: r'messageType')
    ChatMessageType get messageType;
    // enum messageTypeEnum {  MEDIA,  TEXT,  PROPOSITION,  CONTACT,  };

    @BuiltValueField(wireName: r'seen')
    bool? get seen;

    @BuiltValueField(wireName: r'message')
    String get message;

    TextChatMessage._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(TextChatMessageBuilder b) => b
        ..seen = false;

    factory TextChatMessage([void updates(TextChatMessageBuilder b)]) = _$TextChatMessage;

    @BuiltValueSerializer(custom: true)
    static Serializer<TextChatMessage> get serializer => _$TextChatMessageSerializer();
}

class _$TextChatMessageSerializer implements StructuredSerializer<TextChatMessage> {
    @override
    final Iterable<Type> types = const [TextChatMessage, _$TextChatMessage];

    @override
    final String wireName = r'TextChatMessage';

    @override
    Iterable<Object?> serialize(Serializers serializers, TextChatMessage object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.id != null) {
            result
                ..add(r'id')
                ..add(serializers.serialize(object.id,
                    specifiedType: const FullType(String)));
        }
        if (object.createdAt != null) {
            result
                ..add(r'createdAt')
                ..add(serializers.serialize(object.createdAt,
                    specifiedType: const FullType(int)));
        }
        result
            ..add(r'authorId')
            ..add(serializers.serialize(object.authorId,
                specifiedType: const FullType(String)));
        result
            ..add(r'chatId')
            ..add(serializers.serialize(object.chatId,
                specifiedType: const FullType(String)));
        result
            ..add(r'messageType')
            ..add(serializers.serialize(object.messageType,
                specifiedType: const FullType(ChatMessageType)));
        if (object.seen != null) {
            result
                ..add(r'seen')
                ..add(serializers.serialize(object.seen,
                    specifiedType: const FullType(bool)));
        }
        result
            ..add(r'message')
            ..add(serializers.serialize(object.message,
                specifiedType: const FullType(String)));
        return result;
    }

    @override
    TextChatMessage deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = TextChatMessageBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'id':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.id = valueDes;
                    break;
                case r'createdAt':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    result.createdAt = valueDes;
                    break;
                case r'authorId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.authorId = valueDes;
                    break;
                case r'chatId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.chatId = valueDes;
                    break;
                case r'messageType':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(ChatMessageType)) as ChatMessageType;
                    result.messageType = valueDes;
                    break;
                case r'seen':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(bool)) as bool;
                    result.seen = valueDes;
                    break;
                case r'message':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.message = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

