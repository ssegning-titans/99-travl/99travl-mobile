// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'credential_type.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const CredentialType _$PASSWORD = const CredentialType._('PASSWORD');

CredentialType _$valueOf(String name) {
  switch (name) {
    case 'PASSWORD':
      return _$PASSWORD;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<CredentialType> _$values =
    new BuiltSet<CredentialType>(const <CredentialType>[
  _$PASSWORD,
]);

class _$CredentialTypeMeta {
  const _$CredentialTypeMeta();
  CredentialType get PASSWORD => _$PASSWORD;
  CredentialType valueOf(String name) => _$valueOf(name);
  BuiltSet<CredentialType> get values => _$values;
}

abstract class _$CredentialTypeMixin {
  // ignore: non_constant_identifier_names
  _$CredentialTypeMeta get CredentialType => const _$CredentialTypeMeta();
}

Serializer<CredentialType> _$credentialTypeSerializer =
    new _$CredentialTypeSerializer();

class _$CredentialTypeSerializer
    implements PrimitiveSerializer<CredentialType> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'PASSWORD': 'PASSWORD',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'PASSWORD': 'PASSWORD',
  };

  @override
  final Iterable<Type> types = const <Type>[CredentialType];
  @override
  final String wireName = 'CredentialType';

  @override
  Object serialize(Serializers serializers, CredentialType object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  CredentialType deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      CredentialType.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
