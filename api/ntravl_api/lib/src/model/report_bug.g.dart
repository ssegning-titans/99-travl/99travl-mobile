// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'report_bug.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ReportBug extends ReportBug {
  @override
  final ReportType reportType;
  @override
  final String authorId;
  @override
  final ReportStatus status;
  @override
  final int? createdAt;
  @override
  final String? id;
  @override
  final String? description;
  @override
  final String title;
  @override
  final BuiltMap<String, String> deviceInfo;
  @override
  final BuiltList<BugMedia> screenshot;

  factory _$ReportBug([void Function(ReportBugBuilder)? updates]) =>
      (new ReportBugBuilder()..update(updates)).build();

  _$ReportBug._(
      {required this.reportType,
      required this.authorId,
      required this.status,
      this.createdAt,
      this.id,
      this.description,
      required this.title,
      required this.deviceInfo,
      required this.screenshot})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        reportType, 'ReportBug', 'reportType');
    BuiltValueNullFieldError.checkNotNull(authorId, 'ReportBug', 'authorId');
    BuiltValueNullFieldError.checkNotNull(status, 'ReportBug', 'status');
    BuiltValueNullFieldError.checkNotNull(title, 'ReportBug', 'title');
    BuiltValueNullFieldError.checkNotNull(
        deviceInfo, 'ReportBug', 'deviceInfo');
    BuiltValueNullFieldError.checkNotNull(
        screenshot, 'ReportBug', 'screenshot');
  }

  @override
  ReportBug rebuild(void Function(ReportBugBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ReportBugBuilder toBuilder() => new ReportBugBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ReportBug &&
        reportType == other.reportType &&
        authorId == other.authorId &&
        status == other.status &&
        createdAt == other.createdAt &&
        id == other.id &&
        description == other.description &&
        title == other.title &&
        deviceInfo == other.deviceInfo &&
        screenshot == other.screenshot;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc($jc(0, reportType.hashCode),
                                    authorId.hashCode),
                                status.hashCode),
                            createdAt.hashCode),
                        id.hashCode),
                    description.hashCode),
                title.hashCode),
            deviceInfo.hashCode),
        screenshot.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ReportBug')
          ..add('reportType', reportType)
          ..add('authorId', authorId)
          ..add('status', status)
          ..add('createdAt', createdAt)
          ..add('id', id)
          ..add('description', description)
          ..add('title', title)
          ..add('deviceInfo', deviceInfo)
          ..add('screenshot', screenshot))
        .toString();
  }
}

class ReportBugBuilder implements Builder<ReportBug, ReportBugBuilder> {
  _$ReportBug? _$v;

  ReportType? _reportType;
  ReportType? get reportType => _$this._reportType;
  set reportType(ReportType? reportType) => _$this._reportType = reportType;

  String? _authorId;
  String? get authorId => _$this._authorId;
  set authorId(String? authorId) => _$this._authorId = authorId;

  ReportStatus? _status;
  ReportStatus? get status => _$this._status;
  set status(ReportStatus? status) => _$this._status = status;

  int? _createdAt;
  int? get createdAt => _$this._createdAt;
  set createdAt(int? createdAt) => _$this._createdAt = createdAt;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  String? _title;
  String? get title => _$this._title;
  set title(String? title) => _$this._title = title;

  MapBuilder<String, String>? _deviceInfo;
  MapBuilder<String, String> get deviceInfo =>
      _$this._deviceInfo ??= new MapBuilder<String, String>();
  set deviceInfo(MapBuilder<String, String>? deviceInfo) =>
      _$this._deviceInfo = deviceInfo;

  ListBuilder<BugMedia>? _screenshot;
  ListBuilder<BugMedia> get screenshot =>
      _$this._screenshot ??= new ListBuilder<BugMedia>();
  set screenshot(ListBuilder<BugMedia>? screenshot) =>
      _$this._screenshot = screenshot;

  ReportBugBuilder() {
    ReportBug._defaults(this);
  }

  ReportBugBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _reportType = $v.reportType;
      _authorId = $v.authorId;
      _status = $v.status;
      _createdAt = $v.createdAt;
      _id = $v.id;
      _description = $v.description;
      _title = $v.title;
      _deviceInfo = $v.deviceInfo.toBuilder();
      _screenshot = $v.screenshot.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ReportBug other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ReportBug;
  }

  @override
  void update(void Function(ReportBugBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ReportBug build() {
    _$ReportBug _$result;
    try {
      _$result = _$v ??
          new _$ReportBug._(
              reportType: BuiltValueNullFieldError.checkNotNull(
                  reportType, 'ReportBug', 'reportType'),
              authorId: BuiltValueNullFieldError.checkNotNull(
                  authorId, 'ReportBug', 'authorId'),
              status: BuiltValueNullFieldError.checkNotNull(
                  status, 'ReportBug', 'status'),
              createdAt: createdAt,
              id: id,
              description: description,
              title: BuiltValueNullFieldError.checkNotNull(
                  title, 'ReportBug', 'title'),
              deviceInfo: deviceInfo.build(),
              screenshot: screenshot.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'deviceInfo';
        deviceInfo.build();
        _$failedField = 'screenshot';
        screenshot.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ReportBug', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
