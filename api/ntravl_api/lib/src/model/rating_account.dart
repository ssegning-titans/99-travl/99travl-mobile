//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:ntravl_api/src/model/base_rating.dart';
import 'package:ntravl_api/src/model/rating_type.dart';
import 'package:ntravl_api/src/model/rating_value.dart';
import 'package:ntravl_api/src/model/rating_account_all_of.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'rating_account.g.dart';

// ignore_for_file: unused_import

/// RatingAccount
///
/// Properties:
/// * [id] 
/// * [description] 
/// * [authorId] 
/// * [value] 
/// * [createdAt] 
/// * [type] 
/// * [personId] 
abstract class RatingAccount implements Built<RatingAccount, RatingAccountBuilder> {
    @BuiltValueField(wireName: r'id')
    String? get id;

    @BuiltValueField(wireName: r'description')
    String? get description;

    @BuiltValueField(wireName: r'authorId')
    String get authorId;

    @BuiltValueField(wireName: r'value')
    RatingValue get value;
    // enum valueEnum {  ONE,  TWO,  THREE,  FOUR,  FIVE,  };

    @BuiltValueField(wireName: r'createdAt')
    int? get createdAt;

    @BuiltValueField(wireName: r'type')
    RatingType get type;
    // enum typeEnum {  TRAVEL,  ACCOUNT,  };

    @BuiltValueField(wireName: r'personId')
    String get personId;

    RatingAccount._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(RatingAccountBuilder b) => b;

    factory RatingAccount([void updates(RatingAccountBuilder b)]) = _$RatingAccount;

    @BuiltValueSerializer(custom: true)
    static Serializer<RatingAccount> get serializer => _$RatingAccountSerializer();
}

class _$RatingAccountSerializer implements StructuredSerializer<RatingAccount> {
    @override
    final Iterable<Type> types = const [RatingAccount, _$RatingAccount];

    @override
    final String wireName = r'RatingAccount';

    @override
    Iterable<Object?> serialize(Serializers serializers, RatingAccount object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.id != null) {
            result
                ..add(r'id')
                ..add(serializers.serialize(object.id,
                    specifiedType: const FullType(String)));
        }
        if (object.description != null) {
            result
                ..add(r'description')
                ..add(serializers.serialize(object.description,
                    specifiedType: const FullType(String)));
        }
        result
            ..add(r'authorId')
            ..add(serializers.serialize(object.authorId,
                specifiedType: const FullType(String)));
        result
            ..add(r'value')
            ..add(serializers.serialize(object.value,
                specifiedType: const FullType(RatingValue)));
        if (object.createdAt != null) {
            result
                ..add(r'createdAt')
                ..add(serializers.serialize(object.createdAt,
                    specifiedType: const FullType(int)));
        }
        result
            ..add(r'type')
            ..add(serializers.serialize(object.type,
                specifiedType: const FullType(RatingType)));
        result
            ..add(r'personId')
            ..add(serializers.serialize(object.personId,
                specifiedType: const FullType(String)));
        return result;
    }

    @override
    RatingAccount deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = RatingAccountBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'id':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.id = valueDes;
                    break;
                case r'description':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.description = valueDes;
                    break;
                case r'authorId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.authorId = valueDes;
                    break;
                case r'value':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(RatingValue)) as RatingValue;
                    result.value = valueDes;
                    break;
                case r'createdAt':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    result.createdAt = valueDes;
                    break;
                case r'type':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(RatingType)) as RatingType;
                    result.type = valueDes;
                    break;
                case r'personId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.personId = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

