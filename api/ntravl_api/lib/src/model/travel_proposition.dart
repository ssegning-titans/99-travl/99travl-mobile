//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'travel_proposition.g.dart';

/// TravelProposition
///
/// Properties:
/// * [id] 
/// * [authorId] 
/// * [travelId] 
/// * [message] 
/// * [startingPointId] 
/// * [endingPointId] 
abstract class TravelProposition implements Built<TravelProposition, TravelPropositionBuilder> {
    @BuiltValueField(wireName: r'id')
    String? get id;

    @BuiltValueField(wireName: r'authorId')
    String? get authorId;

    @BuiltValueField(wireName: r'travelId')
    String? get travelId;

    @BuiltValueField(wireName: r'message')
    String? get message;

    @BuiltValueField(wireName: r'startingPointId')
    String? get startingPointId;

    @BuiltValueField(wireName: r'endingPointId')
    String? get endingPointId;

    TravelProposition._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(TravelPropositionBuilder b) => b;

    factory TravelProposition([void updates(TravelPropositionBuilder b)]) = _$TravelProposition;

    @BuiltValueSerializer(custom: true)
    static Serializer<TravelProposition> get serializer => _$TravelPropositionSerializer();
}

class _$TravelPropositionSerializer implements StructuredSerializer<TravelProposition> {
    @override
    final Iterable<Type> types = const [TravelProposition, _$TravelProposition];

    @override
    final String wireName = r'TravelProposition';

    @override
    Iterable<Object?> serialize(Serializers serializers, TravelProposition object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.id != null) {
            result
                ..add(r'id')
                ..add(serializers.serialize(object.id,
                    specifiedType: const FullType(String)));
        }
        if (object.authorId != null) {
            result
                ..add(r'authorId')
                ..add(serializers.serialize(object.authorId,
                    specifiedType: const FullType(String)));
        }
        if (object.travelId != null) {
            result
                ..add(r'travelId')
                ..add(serializers.serialize(object.travelId,
                    specifiedType: const FullType(String)));
        }
        if (object.message != null) {
            result
                ..add(r'message')
                ..add(serializers.serialize(object.message,
                    specifiedType: const FullType(String)));
        }
        if (object.startingPointId != null) {
            result
                ..add(r'startingPointId')
                ..add(serializers.serialize(object.startingPointId,
                    specifiedType: const FullType(String)));
        }
        if (object.endingPointId != null) {
            result
                ..add(r'endingPointId')
                ..add(serializers.serialize(object.endingPointId,
                    specifiedType: const FullType(String)));
        }
        return result;
    }

    @override
    TravelProposition deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = TravelPropositionBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'id':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.id = valueDes;
                    break;
                case r'authorId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.authorId = valueDes;
                    break;
                case r'travelId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.travelId = valueDes;
                    break;
                case r'message':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.message = valueDes;
                    break;
                case r'startingPointId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.startingPointId = valueDes;
                    break;
                case r'endingPointId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.endingPointId = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

