//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:ntravl_api/src/model/rating_type.dart';
import 'package:ntravl_api/src/model/rating_value.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'base_rating.g.dart';

/// BaseRating
///
/// Properties:
/// * [id] 
/// * [description] 
/// * [authorId] 
/// * [value] 
/// * [createdAt] 
/// * [type] 
abstract class BaseRating implements Built<BaseRating, BaseRatingBuilder> {
    @BuiltValueField(wireName: r'id')
    String? get id;

    @BuiltValueField(wireName: r'description')
    String? get description;

    @BuiltValueField(wireName: r'authorId')
    String get authorId;

    @BuiltValueField(wireName: r'value')
    RatingValue get value;
    // enum valueEnum {  ONE,  TWO,  THREE,  FOUR,  FIVE,  };

    @BuiltValueField(wireName: r'createdAt')
    int? get createdAt;

    @BuiltValueField(wireName: r'type')
    RatingType get type;
    // enum typeEnum {  TRAVEL,  ACCOUNT,  };

    BaseRating._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(BaseRatingBuilder b) => b;

    factory BaseRating([void updates(BaseRatingBuilder b)]) = _$BaseRating;

    @BuiltValueSerializer(custom: true)
    static Serializer<BaseRating> get serializer => _$BaseRatingSerializer();
}

class _$BaseRatingSerializer implements StructuredSerializer<BaseRating> {
    @override
    final Iterable<Type> types = const [BaseRating, _$BaseRating];

    @override
    final String wireName = r'BaseRating';

    @override
    Iterable<Object?> serialize(Serializers serializers, BaseRating object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.id != null) {
            result
                ..add(r'id')
                ..add(serializers.serialize(object.id,
                    specifiedType: const FullType(String)));
        }
        if (object.description != null) {
            result
                ..add(r'description')
                ..add(serializers.serialize(object.description,
                    specifiedType: const FullType(String)));
        }
        result
            ..add(r'authorId')
            ..add(serializers.serialize(object.authorId,
                specifiedType: const FullType(String)));
        result
            ..add(r'value')
            ..add(serializers.serialize(object.value,
                specifiedType: const FullType(RatingValue)));
        if (object.createdAt != null) {
            result
                ..add(r'createdAt')
                ..add(serializers.serialize(object.createdAt,
                    specifiedType: const FullType(int)));
        }
        result
            ..add(r'type')
            ..add(serializers.serialize(object.type,
                specifiedType: const FullType(RatingType)));
        return result;
    }

    @override
    BaseRating deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = BaseRatingBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'id':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.id = valueDes;
                    break;
                case r'description':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.description = valueDes;
                    break;
                case r'authorId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.authorId = valueDes;
                    break;
                case r'value':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(RatingValue)) as RatingValue;
                    result.value = valueDes;
                    break;
                case r'createdAt':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    result.createdAt = valueDes;
                    break;
                case r'type':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(RatingType)) as RatingType;
                    result.type = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

