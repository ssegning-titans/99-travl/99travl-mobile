//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:ntravl_api/src/model/chat_status.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'chat.g.dart';

/// Chat
///
/// Properties:
/// * [id] 
/// * [createdAt] 
/// * [members] 
/// * [travelId] 
/// * [status] 
abstract class Chat implements Built<Chat, ChatBuilder> {
    @BuiltValueField(wireName: r'id')
    String? get id;

    @BuiltValueField(wireName: r'createdAt')
    int? get createdAt;

    @BuiltValueField(wireName: r'members')
    BuiltList<String>? get members;

    @BuiltValueField(wireName: r'travelId')
    String? get travelId;

    @BuiltValueField(wireName: r'status')
    ChatStatus? get status;
    // enum statusEnum {  ACTIVE,  ARCHIVED,  };

    Chat._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(ChatBuilder b) => b;

    factory Chat([void updates(ChatBuilder b)]) = _$Chat;

    @BuiltValueSerializer(custom: true)
    static Serializer<Chat> get serializer => _$ChatSerializer();
}

class _$ChatSerializer implements StructuredSerializer<Chat> {
    @override
    final Iterable<Type> types = const [Chat, _$Chat];

    @override
    final String wireName = r'Chat';

    @override
    Iterable<Object?> serialize(Serializers serializers, Chat object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.id != null) {
            result
                ..add(r'id')
                ..add(serializers.serialize(object.id,
                    specifiedType: const FullType(String)));
        }
        if (object.createdAt != null) {
            result
                ..add(r'createdAt')
                ..add(serializers.serialize(object.createdAt,
                    specifiedType: const FullType(int)));
        }
        if (object.members != null) {
            result
                ..add(r'members')
                ..add(serializers.serialize(object.members,
                    specifiedType: const FullType(BuiltList, [FullType(String)])));
        }
        if (object.travelId != null) {
            result
                ..add(r'travelId')
                ..add(serializers.serialize(object.travelId,
                    specifiedType: const FullType(String)));
        }
        if (object.status != null) {
            result
                ..add(r'status')
                ..add(serializers.serialize(object.status,
                    specifiedType: const FullType(ChatStatus)));
        }
        return result;
    }

    @override
    Chat deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = ChatBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'id':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.id = valueDes;
                    break;
                case r'createdAt':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    result.createdAt = valueDes;
                    break;
                case r'members':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(BuiltList, [FullType(String)])) as BuiltList<String>;
                    result.members.replace(valueDes);
                    break;
                case r'travelId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.travelId = valueDes;
                    break;
                case r'status':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(ChatStatus)) as ChatStatus;
                    result.status = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

