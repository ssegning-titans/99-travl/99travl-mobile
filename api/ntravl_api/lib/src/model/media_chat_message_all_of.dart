//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:ntravl_api/src/model/chat_media.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'media_chat_message_all_of.g.dart';

/// MediaChatMessageAllOf
///
/// Properties:
/// * [medias] 
abstract class MediaChatMessageAllOf implements Built<MediaChatMessageAllOf, MediaChatMessageAllOfBuilder> {
    @BuiltValueField(wireName: r'medias')
    BuiltSet<ChatMedia> get medias;

    MediaChatMessageAllOf._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(MediaChatMessageAllOfBuilder b) => b;

    factory MediaChatMessageAllOf([void updates(MediaChatMessageAllOfBuilder b)]) = _$MediaChatMessageAllOf;

    @BuiltValueSerializer(custom: true)
    static Serializer<MediaChatMessageAllOf> get serializer => _$MediaChatMessageAllOfSerializer();
}

class _$MediaChatMessageAllOfSerializer implements StructuredSerializer<MediaChatMessageAllOf> {
    @override
    final Iterable<Type> types = const [MediaChatMessageAllOf, _$MediaChatMessageAllOf];

    @override
    final String wireName = r'MediaChatMessageAllOf';

    @override
    Iterable<Object?> serialize(Serializers serializers, MediaChatMessageAllOf object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'medias')
            ..add(serializers.serialize(object.medias,
                specifiedType: const FullType(BuiltSet, [FullType(ChatMedia)])));
        return result;
    }

    @override
    MediaChatMessageAllOf deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = MediaChatMessageAllOfBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'medias':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(BuiltSet, [FullType(ChatMedia)])) as BuiltSet<ChatMedia>;
                    result.medias.replace(valueDes);
                    break;
            }
        }
        return result.build();
    }
}

