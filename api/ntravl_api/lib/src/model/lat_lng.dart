//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'lat_lng.g.dart';

/// LatLng
///
/// Properties:
/// * [lat] 
/// * [lng] 
abstract class LatLng implements Built<LatLng, LatLngBuilder> {
    @BuiltValueField(wireName: r'lat')
    double? get lat;

    @BuiltValueField(wireName: r'lng')
    double? get lng;

    LatLng._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(LatLngBuilder b) => b;

    factory LatLng([void updates(LatLngBuilder b)]) = _$LatLng;

    @BuiltValueSerializer(custom: true)
    static Serializer<LatLng> get serializer => _$LatLngSerializer();
}

class _$LatLngSerializer implements StructuredSerializer<LatLng> {
    @override
    final Iterable<Type> types = const [LatLng, _$LatLng];

    @override
    final String wireName = r'LatLng';

    @override
    Iterable<Object?> serialize(Serializers serializers, LatLng object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.lat != null) {
            result
                ..add(r'lat')
                ..add(serializers.serialize(object.lat,
                    specifiedType: const FullType(double)));
        }
        if (object.lng != null) {
            result
                ..add(r'lng')
                ..add(serializers.serialize(object.lng,
                    specifiedType: const FullType(double)));
        }
        return result;
    }

    @override
    LatLng deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = LatLngBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'lat':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(double)) as double;
                    result.lat = valueDes;
                    break;
                case r'lng':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(double)) as double;
                    result.lng = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

