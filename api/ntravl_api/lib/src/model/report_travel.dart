//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:ntravl_api/src/model/base_report.dart';
import 'package:ntravl_api/src/model/report_travel_all_of.dart';
import 'package:ntravl_api/src/model/report_type.dart';
import 'package:ntravl_api/src/model/report_status.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'report_travel.g.dart';

// ignore_for_file: unused_import

/// ReportTravel
///
/// Properties:
/// * [reportType] 
/// * [authorId] 
/// * [status] 
/// * [createdAt] 
/// * [id] 
/// * [description] 
/// * [travelId] 
abstract class ReportTravel implements Built<ReportTravel, ReportTravelBuilder> {
    @BuiltValueField(wireName: r'reportType')
    ReportType get reportType;
    // enum reportTypeEnum {  TRAVEL,  ACCOUNT,  BUG,  };

    @BuiltValueField(wireName: r'authorId')
    String get authorId;

    @BuiltValueField(wireName: r'status')
    ReportStatus get status;
    // enum statusEnum {  PENDING,  HANDLED,  IN_PROGRESS,  };

    @BuiltValueField(wireName: r'createdAt')
    int? get createdAt;

    @BuiltValueField(wireName: r'id')
    String? get id;

    @BuiltValueField(wireName: r'description')
    String? get description;

    @BuiltValueField(wireName: r'travelId')
    String get travelId;

    ReportTravel._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(ReportTravelBuilder b) => b;

    factory ReportTravel([void updates(ReportTravelBuilder b)]) = _$ReportTravel;

    @BuiltValueSerializer(custom: true)
    static Serializer<ReportTravel> get serializer => _$ReportTravelSerializer();
}

class _$ReportTravelSerializer implements StructuredSerializer<ReportTravel> {
    @override
    final Iterable<Type> types = const [ReportTravel, _$ReportTravel];

    @override
    final String wireName = r'ReportTravel';

    @override
    Iterable<Object?> serialize(Serializers serializers, ReportTravel object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'reportType')
            ..add(serializers.serialize(object.reportType,
                specifiedType: const FullType(ReportType)));
        result
            ..add(r'authorId')
            ..add(serializers.serialize(object.authorId,
                specifiedType: const FullType(String)));
        result
            ..add(r'status')
            ..add(serializers.serialize(object.status,
                specifiedType: const FullType(ReportStatus)));
        if (object.createdAt != null) {
            result
                ..add(r'createdAt')
                ..add(serializers.serialize(object.createdAt,
                    specifiedType: const FullType(int)));
        }
        if (object.id != null) {
            result
                ..add(r'id')
                ..add(serializers.serialize(object.id,
                    specifiedType: const FullType(String)));
        }
        if (object.description != null) {
            result
                ..add(r'description')
                ..add(serializers.serialize(object.description,
                    specifiedType: const FullType(String)));
        }
        result
            ..add(r'travelId')
            ..add(serializers.serialize(object.travelId,
                specifiedType: const FullType(String)));
        return result;
    }

    @override
    ReportTravel deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = ReportTravelBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'reportType':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(ReportType)) as ReportType;
                    result.reportType = valueDes;
                    break;
                case r'authorId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.authorId = valueDes;
                    break;
                case r'status':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(ReportStatus)) as ReportStatus;
                    result.status = valueDes;
                    break;
                case r'createdAt':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    result.createdAt = valueDes;
                    break;
                case r'id':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.id = valueDes;
                    break;
                case r'description':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.description = valueDes;
                    break;
                case r'travelId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.travelId = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

