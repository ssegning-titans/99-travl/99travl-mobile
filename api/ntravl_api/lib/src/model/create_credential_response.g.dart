// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_credential_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$CreateCredentialResponse extends CreateCredentialResponse {
  @override
  final bool state;

  factory _$CreateCredentialResponse(
          [void Function(CreateCredentialResponseBuilder)? updates]) =>
      (new CreateCredentialResponseBuilder()..update(updates)).build();

  _$CreateCredentialResponse._({required this.state}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        state, 'CreateCredentialResponse', 'state');
  }

  @override
  CreateCredentialResponse rebuild(
          void Function(CreateCredentialResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CreateCredentialResponseBuilder toBuilder() =>
      new CreateCredentialResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CreateCredentialResponse && state == other.state;
  }

  @override
  int get hashCode {
    return $jf($jc(0, state.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CreateCredentialResponse')
          ..add('state', state))
        .toString();
  }
}

class CreateCredentialResponseBuilder
    implements
        Builder<CreateCredentialResponse, CreateCredentialResponseBuilder> {
  _$CreateCredentialResponse? _$v;

  bool? _state;
  bool? get state => _$this._state;
  set state(bool? state) => _$this._state = state;

  CreateCredentialResponseBuilder() {
    CreateCredentialResponse._defaults(this);
  }

  CreateCredentialResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _state = $v.state;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CreateCredentialResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$CreateCredentialResponse;
  }

  @override
  void update(void Function(CreateCredentialResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CreateCredentialResponse build() {
    final _$result = _$v ??
        new _$CreateCredentialResponse._(
            state: BuiltValueNullFieldError.checkNotNull(
                state, 'CreateCredentialResponse', 'state'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
