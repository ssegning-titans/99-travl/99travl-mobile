// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_status.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const ChatStatus _$ACTIVE = const ChatStatus._('ACTIVE');
const ChatStatus _$ARCHIVED = const ChatStatus._('ARCHIVED');

ChatStatus _$valueOf(String name) {
  switch (name) {
    case 'ACTIVE':
      return _$ACTIVE;
    case 'ARCHIVED':
      return _$ARCHIVED;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<ChatStatus> _$values =
    new BuiltSet<ChatStatus>(const <ChatStatus>[
  _$ACTIVE,
  _$ARCHIVED,
]);

class _$ChatStatusMeta {
  const _$ChatStatusMeta();
  ChatStatus get ACTIVE => _$ACTIVE;
  ChatStatus get ARCHIVED => _$ARCHIVED;
  ChatStatus valueOf(String name) => _$valueOf(name);
  BuiltSet<ChatStatus> get values => _$values;
}

abstract class _$ChatStatusMixin {
  // ignore: non_constant_identifier_names
  _$ChatStatusMeta get ChatStatus => const _$ChatStatusMeta();
}

Serializer<ChatStatus> _$chatStatusSerializer = new _$ChatStatusSerializer();

class _$ChatStatusSerializer implements PrimitiveSerializer<ChatStatus> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'ACTIVE': 'ACTIVE',
    'ARCHIVED': 'ARCHIVED',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'ACTIVE': 'ACTIVE',
    'ARCHIVED': 'ARCHIVED',
  };

  @override
  final Iterable<Type> types = const <Type>[ChatStatus];
  @override
  final String wireName = 'ChatStatus';

  @override
  Object serialize(Serializers serializers, ChatStatus object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  ChatStatus deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      ChatStatus.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
