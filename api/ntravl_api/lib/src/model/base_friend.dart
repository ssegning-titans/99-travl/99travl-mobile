//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:ntravl_api/src/model/friend_ship_status.dart';
import 'package:ntravl_api/src/model/friend_type.dart';
import 'package:ntravl_api/src/model/friend_process_type.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'base_friend.g.dart';

/// BaseFriend
///
/// Properties:
/// * [id] 
/// * [creationDate] 
/// * [status] 
/// * [type] 
/// * [processType] 
abstract class BaseFriend implements Built<BaseFriend, BaseFriendBuilder> {
    @BuiltValueField(wireName: r'id')
    String get id;

    @BuiltValueField(wireName: r'creationDate')
    int get creationDate;

    @BuiltValueField(wireName: r'status')
    FriendShipStatus? get status;
    // enum statusEnum {  ACCEPTED,  REFUSED,  PENDING,  };

    @BuiltValueField(wireName: r'type')
    FriendType? get type;
    // enum typeEnum {  RECEIVER_ONLY,  SENDER_ONLY,  FULL,  };

    @BuiltValueField(wireName: r'processType')
    FriendProcessType get processType;
    // enum processTypeEnum {  OFFLINE,  ONLINE,  };

    BaseFriend._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(BaseFriendBuilder b) => b;

    factory BaseFriend([void updates(BaseFriendBuilder b)]) = _$BaseFriend;

    @BuiltValueSerializer(custom: true)
    static Serializer<BaseFriend> get serializer => _$BaseFriendSerializer();
}

class _$BaseFriendSerializer implements StructuredSerializer<BaseFriend> {
    @override
    final Iterable<Type> types = const [BaseFriend, _$BaseFriend];

    @override
    final String wireName = r'BaseFriend';

    @override
    Iterable<Object?> serialize(Serializers serializers, BaseFriend object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'id')
            ..add(serializers.serialize(object.id,
                specifiedType: const FullType(String)));
        result
            ..add(r'creationDate')
            ..add(serializers.serialize(object.creationDate,
                specifiedType: const FullType(int)));
        if (object.status != null) {
            result
                ..add(r'status')
                ..add(serializers.serialize(object.status,
                    specifiedType: const FullType(FriendShipStatus)));
        }
        if (object.type != null) {
            result
                ..add(r'type')
                ..add(serializers.serialize(object.type,
                    specifiedType: const FullType(FriendType)));
        }
        result
            ..add(r'processType')
            ..add(serializers.serialize(object.processType,
                specifiedType: const FullType(FriendProcessType)));
        return result;
    }

    @override
    BaseFriend deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = BaseFriendBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'id':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.id = valueDes;
                    break;
                case r'creationDate':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    result.creationDate = valueDes;
                    break;
                case r'status':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(FriendShipStatus)) as FriendShipStatus;
                    result.status = valueDes;
                    break;
                case r'type':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(FriendType)) as FriendType;
                    result.type = valueDes;
                    break;
                case r'processType':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(FriendProcessType)) as FriendProcessType;
                    result.processType = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

