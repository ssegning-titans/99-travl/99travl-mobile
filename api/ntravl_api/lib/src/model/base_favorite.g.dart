// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'base_favorite.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$BaseFavorite extends BaseFavorite {
  @override
  final String? id;
  @override
  final String ownerId;
  @override
  final int? createdAt;
  @override
  final FavoriteType favoriteType;

  factory _$BaseFavorite([void Function(BaseFavoriteBuilder)? updates]) =>
      (new BaseFavoriteBuilder()..update(updates)).build();

  _$BaseFavorite._(
      {this.id,
      required this.ownerId,
      this.createdAt,
      required this.favoriteType})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(ownerId, 'BaseFavorite', 'ownerId');
    BuiltValueNullFieldError.checkNotNull(
        favoriteType, 'BaseFavorite', 'favoriteType');
  }

  @override
  BaseFavorite rebuild(void Function(BaseFavoriteBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BaseFavoriteBuilder toBuilder() => new BaseFavoriteBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BaseFavorite &&
        id == other.id &&
        ownerId == other.ownerId &&
        createdAt == other.createdAt &&
        favoriteType == other.favoriteType;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, id.hashCode), ownerId.hashCode), createdAt.hashCode),
        favoriteType.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('BaseFavorite')
          ..add('id', id)
          ..add('ownerId', ownerId)
          ..add('createdAt', createdAt)
          ..add('favoriteType', favoriteType))
        .toString();
  }
}

class BaseFavoriteBuilder
    implements Builder<BaseFavorite, BaseFavoriteBuilder> {
  _$BaseFavorite? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _ownerId;
  String? get ownerId => _$this._ownerId;
  set ownerId(String? ownerId) => _$this._ownerId = ownerId;

  int? _createdAt;
  int? get createdAt => _$this._createdAt;
  set createdAt(int? createdAt) => _$this._createdAt = createdAt;

  FavoriteType? _favoriteType;
  FavoriteType? get favoriteType => _$this._favoriteType;
  set favoriteType(FavoriteType? favoriteType) =>
      _$this._favoriteType = favoriteType;

  BaseFavoriteBuilder() {
    BaseFavorite._defaults(this);
  }

  BaseFavoriteBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _ownerId = $v.ownerId;
      _createdAt = $v.createdAt;
      _favoriteType = $v.favoriteType;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(BaseFavorite other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$BaseFavorite;
  }

  @override
  void update(void Function(BaseFavoriteBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$BaseFavorite build() {
    final _$result = _$v ??
        new _$BaseFavorite._(
            id: id,
            ownerId: BuiltValueNullFieldError.checkNotNull(
                ownerId, 'BaseFavorite', 'ownerId'),
            createdAt: createdAt,
            favoriteType: BuiltValueNullFieldError.checkNotNull(
                favoriteType, 'BaseFavorite', 'favoriteType'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
