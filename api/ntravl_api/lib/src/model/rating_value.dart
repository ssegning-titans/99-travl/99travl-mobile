//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'rating_value.g.dart';

class RatingValue extends EnumClass {

  @BuiltValueEnumConst(wireName: r'ONE')
  static const RatingValue ONE = _$ONE;
  @BuiltValueEnumConst(wireName: r'TWO')
  static const RatingValue TWO = _$TWO;
  @BuiltValueEnumConst(wireName: r'THREE')
  static const RatingValue THREE = _$THREE;
  @BuiltValueEnumConst(wireName: r'FOUR')
  static const RatingValue FOUR = _$FOUR;
  @BuiltValueEnumConst(wireName: r'FIVE')
  static const RatingValue FIVE = _$FIVE;

  static Serializer<RatingValue> get serializer => _$ratingValueSerializer;

  const RatingValue._(String name): super(name);

  static BuiltSet<RatingValue> get values => _$values;
  static RatingValue valueOf(String name) => _$valueOf(name);
}

/// Optionally, enum_class can generate a mixin to go with your enum for use
/// with Angular. It exposes your enum constants as getters. So, if you mix it
/// in to your Dart component class, the values become available to the
/// corresponding Angular template.
///
/// Trigger mixin generation by writing a line like this one next to your enum.
abstract class RatingValueMixin = Object with _$RatingValueMixin;

