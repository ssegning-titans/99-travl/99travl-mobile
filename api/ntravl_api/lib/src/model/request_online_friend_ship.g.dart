// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'request_online_friend_ship.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$RequestOnlineFriendShip extends RequestOnlineFriendShip {
  @override
  final String? accountId;

  factory _$RequestOnlineFriendShip(
          [void Function(RequestOnlineFriendShipBuilder)? updates]) =>
      (new RequestOnlineFriendShipBuilder()..update(updates)).build();

  _$RequestOnlineFriendShip._({this.accountId}) : super._();

  @override
  RequestOnlineFriendShip rebuild(
          void Function(RequestOnlineFriendShipBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RequestOnlineFriendShipBuilder toBuilder() =>
      new RequestOnlineFriendShipBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is RequestOnlineFriendShip && accountId == other.accountId;
  }

  @override
  int get hashCode {
    return $jf($jc(0, accountId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('RequestOnlineFriendShip')
          ..add('accountId', accountId))
        .toString();
  }
}

class RequestOnlineFriendShipBuilder
    implements
        Builder<RequestOnlineFriendShip, RequestOnlineFriendShipBuilder> {
  _$RequestOnlineFriendShip? _$v;

  String? _accountId;
  String? get accountId => _$this._accountId;
  set accountId(String? accountId) => _$this._accountId = accountId;

  RequestOnlineFriendShipBuilder() {
    RequestOnlineFriendShip._defaults(this);
  }

  RequestOnlineFriendShipBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _accountId = $v.accountId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RequestOnlineFriendShip other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$RequestOnlineFriendShip;
  }

  @override
  void update(void Function(RequestOnlineFriendShipBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$RequestOnlineFriendShip build() {
    final _$result =
        _$v ?? new _$RequestOnlineFriendShip._(accountId: accountId);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
