// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rating_account_all_of.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$RatingAccountAllOf extends RatingAccountAllOf {
  @override
  final String personId;

  factory _$RatingAccountAllOf(
          [void Function(RatingAccountAllOfBuilder)? updates]) =>
      (new RatingAccountAllOfBuilder()..update(updates)).build();

  _$RatingAccountAllOf._({required this.personId}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        personId, 'RatingAccountAllOf', 'personId');
  }

  @override
  RatingAccountAllOf rebuild(
          void Function(RatingAccountAllOfBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RatingAccountAllOfBuilder toBuilder() =>
      new RatingAccountAllOfBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is RatingAccountAllOf && personId == other.personId;
  }

  @override
  int get hashCode {
    return $jf($jc(0, personId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('RatingAccountAllOf')
          ..add('personId', personId))
        .toString();
  }
}

class RatingAccountAllOfBuilder
    implements Builder<RatingAccountAllOf, RatingAccountAllOfBuilder> {
  _$RatingAccountAllOf? _$v;

  String? _personId;
  String? get personId => _$this._personId;
  set personId(String? personId) => _$this._personId = personId;

  RatingAccountAllOfBuilder() {
    RatingAccountAllOf._defaults(this);
  }

  RatingAccountAllOfBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _personId = $v.personId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RatingAccountAllOf other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$RatingAccountAllOf;
  }

  @override
  void update(void Function(RatingAccountAllOfBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$RatingAccountAllOf build() {
    final _$result = _$v ??
        new _$RatingAccountAllOf._(
            personId: BuiltValueNullFieldError.checkNotNull(
                personId, 'RatingAccountAllOf', 'personId'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
