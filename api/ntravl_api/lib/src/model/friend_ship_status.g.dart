// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'friend_ship_status.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const FriendShipStatus _$ACCEPTED = const FriendShipStatus._('ACCEPTED');
const FriendShipStatus _$REFUSED = const FriendShipStatus._('REFUSED');
const FriendShipStatus _$PENDING = const FriendShipStatus._('PENDING');

FriendShipStatus _$valueOf(String name) {
  switch (name) {
    case 'ACCEPTED':
      return _$ACCEPTED;
    case 'REFUSED':
      return _$REFUSED;
    case 'PENDING':
      return _$PENDING;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<FriendShipStatus> _$values =
    new BuiltSet<FriendShipStatus>(const <FriendShipStatus>[
  _$ACCEPTED,
  _$REFUSED,
  _$PENDING,
]);

class _$FriendShipStatusMeta {
  const _$FriendShipStatusMeta();
  FriendShipStatus get ACCEPTED => _$ACCEPTED;
  FriendShipStatus get REFUSED => _$REFUSED;
  FriendShipStatus get PENDING => _$PENDING;
  FriendShipStatus valueOf(String name) => _$valueOf(name);
  BuiltSet<FriendShipStatus> get values => _$values;
}

abstract class _$FriendShipStatusMixin {
  // ignore: non_constant_identifier_names
  _$FriendShipStatusMeta get FriendShipStatus => const _$FriendShipStatusMeta();
}

Serializer<FriendShipStatus> _$friendShipStatusSerializer =
    new _$FriendShipStatusSerializer();

class _$FriendShipStatusSerializer
    implements PrimitiveSerializer<FriendShipStatus> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'ACCEPTED': 'ACCEPTED',
    'REFUSED': 'REFUSED',
    'PENDING': 'PENDING',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'ACCEPTED': 'ACCEPTED',
    'REFUSED': 'REFUSED',
    'PENDING': 'PENDING',
  };

  @override
  final Iterable<Type> types = const <Type>[FriendShipStatus];
  @override
  final String wireName = 'FriendShipStatus';

  @override
  Object serialize(Serializers serializers, FriendShipStatus object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  FriendShipStatus deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      FriendShipStatus.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
