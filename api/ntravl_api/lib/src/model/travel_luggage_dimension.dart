//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'travel_luggage_dimension.g.dart';

class TravelLuggageDimension extends EnumClass {

  @BuiltValueEnumConst(wireName: r'S')
  static const TravelLuggageDimension S = _$S;
  @BuiltValueEnumConst(wireName: r'M')
  static const TravelLuggageDimension M = _$M;
  @BuiltValueEnumConst(wireName: r'L')
  static const TravelLuggageDimension L = _$L;
  @BuiltValueEnumConst(wireName: r'XL')
  static const TravelLuggageDimension XL = _$XL;

  static Serializer<TravelLuggageDimension> get serializer => _$travelLuggageDimensionSerializer;

  const TravelLuggageDimension._(String name): super(name);

  static BuiltSet<TravelLuggageDimension> get values => _$values;
  static TravelLuggageDimension valueOf(String name) => _$valueOf(name);
}

/// Optionally, enum_class can generate a mixin to go with your enum for use
/// with Angular. It exposes your enum constants as getters. So, if you mix it
/// in to your Dart component class, the values become available to the
/// corresponding Angular template.
///
/// Trigger mixin generation by writing a line like this one next to your enum.
abstract class TravelLuggageDimensionMixin = Object with _$TravelLuggageDimensionMixin;

