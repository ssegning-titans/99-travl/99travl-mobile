//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'create_credential_input.g.dart';

/// CreateCredentialInput
///
/// Properties:
/// * [challenge] 
/// * [data] 
abstract class CreateCredentialInput implements Built<CreateCredentialInput, CreateCredentialInputBuilder> {
    @BuiltValueField(wireName: r'challenge')
    String get challenge;

    @BuiltValueField(wireName: r'data')
    BuiltMap<String, String>? get data;

    CreateCredentialInput._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(CreateCredentialInputBuilder b) => b;

    factory CreateCredentialInput([void updates(CreateCredentialInputBuilder b)]) = _$CreateCredentialInput;

    @BuiltValueSerializer(custom: true)
    static Serializer<CreateCredentialInput> get serializer => _$CreateCredentialInputSerializer();
}

class _$CreateCredentialInputSerializer implements StructuredSerializer<CreateCredentialInput> {
    @override
    final Iterable<Type> types = const [CreateCredentialInput, _$CreateCredentialInput];

    @override
    final String wireName = r'CreateCredentialInput';

    @override
    Iterable<Object?> serialize(Serializers serializers, CreateCredentialInput object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'challenge')
            ..add(serializers.serialize(object.challenge,
                specifiedType: const FullType(String)));
        if (object.data != null) {
            result
                ..add(r'data')
                ..add(serializers.serialize(object.data,
                    specifiedType: const FullType(BuiltMap, [FullType(String), FullType(String)])));
        }
        return result;
    }

    @override
    CreateCredentialInput deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = CreateCredentialInputBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'challenge':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.challenge = valueDes;
                    break;
                case r'data':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(BuiltMap, [FullType(String), FullType(String)])) as BuiltMap<String, String>;
                    result.data.replace(valueDes);
                    break;
            }
        }
        return result.build();
    }
}

