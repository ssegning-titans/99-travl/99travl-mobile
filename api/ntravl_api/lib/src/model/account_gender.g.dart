// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account_gender.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const AccountGender _$MALE = const AccountGender._('MALE');
const AccountGender _$FEMALE = const AccountGender._('FEMALE');
const AccountGender _$OTHER = const AccountGender._('OTHER');

AccountGender _$valueOf(String name) {
  switch (name) {
    case 'MALE':
      return _$MALE;
    case 'FEMALE':
      return _$FEMALE;
    case 'OTHER':
      return _$OTHER;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<AccountGender> _$values =
    new BuiltSet<AccountGender>(const <AccountGender>[
  _$MALE,
  _$FEMALE,
  _$OTHER,
]);

class _$AccountGenderMeta {
  const _$AccountGenderMeta();
  AccountGender get MALE => _$MALE;
  AccountGender get FEMALE => _$FEMALE;
  AccountGender get OTHER => _$OTHER;
  AccountGender valueOf(String name) => _$valueOf(name);
  BuiltSet<AccountGender> get values => _$values;
}

abstract class _$AccountGenderMixin {
  // ignore: non_constant_identifier_names
  _$AccountGenderMeta get AccountGender => const _$AccountGenderMeta();
}

Serializer<AccountGender> _$accountGenderSerializer =
    new _$AccountGenderSerializer();

class _$AccountGenderSerializer implements PrimitiveSerializer<AccountGender> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'MALE': 'MALE',
    'FEMALE': 'FEMALE',
    'OTHER': 'OTHER',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'MALE': 'MALE',
    'FEMALE': 'FEMALE',
    'OTHER': 'OTHER',
  };

  @override
  final Iterable<Type> types = const <Type>[AccountGender];
  @override
  final String wireName = 'AccountGender';

  @override
  Object serialize(Serializers serializers, AccountGender object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  AccountGender deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      AccountGender.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
