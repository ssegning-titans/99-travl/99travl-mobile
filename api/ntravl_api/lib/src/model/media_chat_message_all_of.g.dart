// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'media_chat_message_all_of.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MediaChatMessageAllOf extends MediaChatMessageAllOf {
  @override
  final BuiltSet<ChatMedia> medias;

  factory _$MediaChatMessageAllOf(
          [void Function(MediaChatMessageAllOfBuilder)? updates]) =>
      (new MediaChatMessageAllOfBuilder()..update(updates)).build();

  _$MediaChatMessageAllOf._({required this.medias}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        medias, 'MediaChatMessageAllOf', 'medias');
  }

  @override
  MediaChatMessageAllOf rebuild(
          void Function(MediaChatMessageAllOfBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MediaChatMessageAllOfBuilder toBuilder() =>
      new MediaChatMessageAllOfBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MediaChatMessageAllOf && medias == other.medias;
  }

  @override
  int get hashCode {
    return $jf($jc(0, medias.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('MediaChatMessageAllOf')
          ..add('medias', medias))
        .toString();
  }
}

class MediaChatMessageAllOfBuilder
    implements Builder<MediaChatMessageAllOf, MediaChatMessageAllOfBuilder> {
  _$MediaChatMessageAllOf? _$v;

  SetBuilder<ChatMedia>? _medias;
  SetBuilder<ChatMedia> get medias =>
      _$this._medias ??= new SetBuilder<ChatMedia>();
  set medias(SetBuilder<ChatMedia>? medias) => _$this._medias = medias;

  MediaChatMessageAllOfBuilder() {
    MediaChatMessageAllOf._defaults(this);
  }

  MediaChatMessageAllOfBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _medias = $v.medias.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MediaChatMessageAllOf other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MediaChatMessageAllOf;
  }

  @override
  void update(void Function(MediaChatMessageAllOfBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$MediaChatMessageAllOf build() {
    _$MediaChatMessageAllOf _$result;
    try {
      _$result = _$v ?? new _$MediaChatMessageAllOf._(medias: medias.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'medias';
        medias.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'MediaChatMessageAllOf', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
