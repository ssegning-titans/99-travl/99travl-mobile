//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:ntravl_api/src/model/routing_map_place.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'routing_direction.g.dart';

/// RoutingDirection
///
/// Properties:
/// * [routes] 
abstract class RoutingDirection implements Built<RoutingDirection, RoutingDirectionBuilder> {
    @BuiltValueField(wireName: r'routes')
    BuiltList<RoutingMapPlace>? get routes;

    RoutingDirection._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(RoutingDirectionBuilder b) => b;

    factory RoutingDirection([void updates(RoutingDirectionBuilder b)]) = _$RoutingDirection;

    @BuiltValueSerializer(custom: true)
    static Serializer<RoutingDirection> get serializer => _$RoutingDirectionSerializer();
}

class _$RoutingDirectionSerializer implements StructuredSerializer<RoutingDirection> {
    @override
    final Iterable<Type> types = const [RoutingDirection, _$RoutingDirection];

    @override
    final String wireName = r'RoutingDirection';

    @override
    Iterable<Object?> serialize(Serializers serializers, RoutingDirection object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.routes != null) {
            result
                ..add(r'routes')
                ..add(serializers.serialize(object.routes,
                    specifiedType: const FullType(BuiltList, [FullType(RoutingMapPlace)])));
        }
        return result;
    }

    @override
    RoutingDirection deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = RoutingDirectionBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'routes':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(BuiltList, [FullType(RoutingMapPlace)])) as BuiltList<RoutingMapPlace>;
                    result.routes.replace(valueDes);
                    break;
            }
        }
        return result.build();
    }
}

