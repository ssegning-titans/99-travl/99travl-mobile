// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account_address.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$AccountAddress extends AccountAddress {
  @override
  final String? id;
  @override
  final int? creationDate;
  @override
  final String? phoneNumber;
  @override
  final String? accountId;
  @override
  final String? region;
  @override
  final String? formattedName;
  @override
  final String? street;
  @override
  final String? houseNumber;
  @override
  final String? city;
  @override
  final String? zip;
  @override
  final String? country;
  @override
  final LatLng? location;

  factory _$AccountAddress([void Function(AccountAddressBuilder)? updates]) =>
      (new AccountAddressBuilder()..update(updates)).build();

  _$AccountAddress._(
      {this.id,
      this.creationDate,
      this.phoneNumber,
      this.accountId,
      this.region,
      this.formattedName,
      this.street,
      this.houseNumber,
      this.city,
      this.zip,
      this.country,
      this.location})
      : super._();

  @override
  AccountAddress rebuild(void Function(AccountAddressBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AccountAddressBuilder toBuilder() =>
      new AccountAddressBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AccountAddress &&
        id == other.id &&
        creationDate == other.creationDate &&
        phoneNumber == other.phoneNumber &&
        accountId == other.accountId &&
        region == other.region &&
        formattedName == other.formattedName &&
        street == other.street &&
        houseNumber == other.houseNumber &&
        city == other.city &&
        zip == other.zip &&
        country == other.country &&
        location == other.location;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc($jc(0, id.hashCode),
                                                creationDate.hashCode),
                                            phoneNumber.hashCode),
                                        accountId.hashCode),
                                    region.hashCode),
                                formattedName.hashCode),
                            street.hashCode),
                        houseNumber.hashCode),
                    city.hashCode),
                zip.hashCode),
            country.hashCode),
        location.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('AccountAddress')
          ..add('id', id)
          ..add('creationDate', creationDate)
          ..add('phoneNumber', phoneNumber)
          ..add('accountId', accountId)
          ..add('region', region)
          ..add('formattedName', formattedName)
          ..add('street', street)
          ..add('houseNumber', houseNumber)
          ..add('city', city)
          ..add('zip', zip)
          ..add('country', country)
          ..add('location', location))
        .toString();
  }
}

class AccountAddressBuilder
    implements Builder<AccountAddress, AccountAddressBuilder> {
  _$AccountAddress? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  int? _creationDate;
  int? get creationDate => _$this._creationDate;
  set creationDate(int? creationDate) => _$this._creationDate = creationDate;

  String? _phoneNumber;
  String? get phoneNumber => _$this._phoneNumber;
  set phoneNumber(String? phoneNumber) => _$this._phoneNumber = phoneNumber;

  String? _accountId;
  String? get accountId => _$this._accountId;
  set accountId(String? accountId) => _$this._accountId = accountId;

  String? _region;
  String? get region => _$this._region;
  set region(String? region) => _$this._region = region;

  String? _formattedName;
  String? get formattedName => _$this._formattedName;
  set formattedName(String? formattedName) =>
      _$this._formattedName = formattedName;

  String? _street;
  String? get street => _$this._street;
  set street(String? street) => _$this._street = street;

  String? _houseNumber;
  String? get houseNumber => _$this._houseNumber;
  set houseNumber(String? houseNumber) => _$this._houseNumber = houseNumber;

  String? _city;
  String? get city => _$this._city;
  set city(String? city) => _$this._city = city;

  String? _zip;
  String? get zip => _$this._zip;
  set zip(String? zip) => _$this._zip = zip;

  String? _country;
  String? get country => _$this._country;
  set country(String? country) => _$this._country = country;

  LatLngBuilder? _location;
  LatLngBuilder get location => _$this._location ??= new LatLngBuilder();
  set location(LatLngBuilder? location) => _$this._location = location;

  AccountAddressBuilder() {
    AccountAddress._defaults(this);
  }

  AccountAddressBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _creationDate = $v.creationDate;
      _phoneNumber = $v.phoneNumber;
      _accountId = $v.accountId;
      _region = $v.region;
      _formattedName = $v.formattedName;
      _street = $v.street;
      _houseNumber = $v.houseNumber;
      _city = $v.city;
      _zip = $v.zip;
      _country = $v.country;
      _location = $v.location?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AccountAddress other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$AccountAddress;
  }

  @override
  void update(void Function(AccountAddressBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$AccountAddress build() {
    _$AccountAddress _$result;
    try {
      _$result = _$v ??
          new _$AccountAddress._(
              id: id,
              creationDate: creationDate,
              phoneNumber: phoneNumber,
              accountId: accountId,
              region: region,
              formattedName: formattedName,
              street: street,
              houseNumber: houseNumber,
              city: city,
              zip: zip,
              country: country,
              location: _location?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'location';
        _location?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'AccountAddress', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
