//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:ntravl_api/src/model/contact_chat_message_all_of.dart';
import 'package:built_collection/built_collection.dart';
import 'package:ntravl_api/src/model/base_chat_message.dart';
import 'package:ntravl_api/src/model/chat_message_type.dart';
import 'package:ntravl_api/src/model/chat_contact.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'contact_chat_message.g.dart';

// ignore_for_file: unused_import

/// ContactChatMessage
///
/// Properties:
/// * [id] 
/// * [createdAt] 
/// * [authorId] 
/// * [chatId] 
/// * [messageType] 
/// * [seen] 
/// * [contacts] 
abstract class ContactChatMessage implements Built<ContactChatMessage, ContactChatMessageBuilder> {
    @BuiltValueField(wireName: r'id')
    String? get id;

    @BuiltValueField(wireName: r'createdAt')
    int? get createdAt;

    @BuiltValueField(wireName: r'authorId')
    String get authorId;

    @BuiltValueField(wireName: r'chatId')
    String get chatId;

    @BuiltValueField(wireName: r'messageType')
    ChatMessageType get messageType;
    // enum messageTypeEnum {  MEDIA,  TEXT,  PROPOSITION,  CONTACT,  };

    @BuiltValueField(wireName: r'seen')
    bool? get seen;

    @BuiltValueField(wireName: r'contacts')
    BuiltSet<ChatContact> get contacts;

    ContactChatMessage._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(ContactChatMessageBuilder b) => b
        ..seen = false;

    factory ContactChatMessage([void updates(ContactChatMessageBuilder b)]) = _$ContactChatMessage;

    @BuiltValueSerializer(custom: true)
    static Serializer<ContactChatMessage> get serializer => _$ContactChatMessageSerializer();
}

class _$ContactChatMessageSerializer implements StructuredSerializer<ContactChatMessage> {
    @override
    final Iterable<Type> types = const [ContactChatMessage, _$ContactChatMessage];

    @override
    final String wireName = r'ContactChatMessage';

    @override
    Iterable<Object?> serialize(Serializers serializers, ContactChatMessage object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.id != null) {
            result
                ..add(r'id')
                ..add(serializers.serialize(object.id,
                    specifiedType: const FullType(String)));
        }
        if (object.createdAt != null) {
            result
                ..add(r'createdAt')
                ..add(serializers.serialize(object.createdAt,
                    specifiedType: const FullType(int)));
        }
        result
            ..add(r'authorId')
            ..add(serializers.serialize(object.authorId,
                specifiedType: const FullType(String)));
        result
            ..add(r'chatId')
            ..add(serializers.serialize(object.chatId,
                specifiedType: const FullType(String)));
        result
            ..add(r'messageType')
            ..add(serializers.serialize(object.messageType,
                specifiedType: const FullType(ChatMessageType)));
        if (object.seen != null) {
            result
                ..add(r'seen')
                ..add(serializers.serialize(object.seen,
                    specifiedType: const FullType(bool)));
        }
        result
            ..add(r'contacts')
            ..add(serializers.serialize(object.contacts,
                specifiedType: const FullType(BuiltSet, [FullType(ChatContact)])));
        return result;
    }

    @override
    ContactChatMessage deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = ContactChatMessageBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'id':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.id = valueDes;
                    break;
                case r'createdAt':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    result.createdAt = valueDes;
                    break;
                case r'authorId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.authorId = valueDes;
                    break;
                case r'chatId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.chatId = valueDes;
                    break;
                case r'messageType':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(ChatMessageType)) as ChatMessageType;
                    result.messageType = valueDes;
                    break;
                case r'seen':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(bool)) as bool;
                    result.seen = valueDes;
                    break;
                case r'contacts':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(BuiltSet, [FullType(ChatContact)])) as BuiltSet<ChatContact>;
                    result.contacts.replace(valueDes);
                    break;
            }
        }
        return result.build();
    }
}

