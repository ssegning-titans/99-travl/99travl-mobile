//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'travel_mode_unit.g.dart';

class TravelModeUnit extends EnumClass {

  @BuiltValueEnumConst(wireName: r'PLANE')
  static const TravelModeUnit PLANE = _$PLANE;
  @BuiltValueEnumConst(wireName: r'CAR')
  static const TravelModeUnit CAR = _$CAR;
  @BuiltValueEnumConst(wireName: r'BOAT')
  static const TravelModeUnit BOAT = _$BOAT;

  static Serializer<TravelModeUnit> get serializer => _$travelModeUnitSerializer;

  const TravelModeUnit._(String name): super(name);

  static BuiltSet<TravelModeUnit> get values => _$values;
  static TravelModeUnit valueOf(String name) => _$valueOf(name);
}

/// Optionally, enum_class can generate a mixin to go with your enum for use
/// with Angular. It exposes your enum constants as getters. So, if you mix it
/// in to your Dart component class, the values become available to the
/// corresponding Angular template.
///
/// Trigger mixin generation by writing a line like this one next to your enum.
abstract class TravelModeUnitMixin = Object with _$TravelModeUnitMixin;

