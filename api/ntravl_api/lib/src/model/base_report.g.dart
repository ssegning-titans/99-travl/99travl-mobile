// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'base_report.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$BaseReport extends BaseReport {
  @override
  final ReportType reportType;
  @override
  final String authorId;
  @override
  final ReportStatus status;
  @override
  final int? createdAt;
  @override
  final String? id;
  @override
  final String? description;

  factory _$BaseReport([void Function(BaseReportBuilder)? updates]) =>
      (new BaseReportBuilder()..update(updates)).build();

  _$BaseReport._(
      {required this.reportType,
      required this.authorId,
      required this.status,
      this.createdAt,
      this.id,
      this.description})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        reportType, 'BaseReport', 'reportType');
    BuiltValueNullFieldError.checkNotNull(authorId, 'BaseReport', 'authorId');
    BuiltValueNullFieldError.checkNotNull(status, 'BaseReport', 'status');
  }

  @override
  BaseReport rebuild(void Function(BaseReportBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BaseReportBuilder toBuilder() => new BaseReportBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BaseReport &&
        reportType == other.reportType &&
        authorId == other.authorId &&
        status == other.status &&
        createdAt == other.createdAt &&
        id == other.id &&
        description == other.description;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, reportType.hashCode), authorId.hashCode),
                    status.hashCode),
                createdAt.hashCode),
            id.hashCode),
        description.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('BaseReport')
          ..add('reportType', reportType)
          ..add('authorId', authorId)
          ..add('status', status)
          ..add('createdAt', createdAt)
          ..add('id', id)
          ..add('description', description))
        .toString();
  }
}

class BaseReportBuilder implements Builder<BaseReport, BaseReportBuilder> {
  _$BaseReport? _$v;

  ReportType? _reportType;
  ReportType? get reportType => _$this._reportType;
  set reportType(ReportType? reportType) => _$this._reportType = reportType;

  String? _authorId;
  String? get authorId => _$this._authorId;
  set authorId(String? authorId) => _$this._authorId = authorId;

  ReportStatus? _status;
  ReportStatus? get status => _$this._status;
  set status(ReportStatus? status) => _$this._status = status;

  int? _createdAt;
  int? get createdAt => _$this._createdAt;
  set createdAt(int? createdAt) => _$this._createdAt = createdAt;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  BaseReportBuilder() {
    BaseReport._defaults(this);
  }

  BaseReportBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _reportType = $v.reportType;
      _authorId = $v.authorId;
      _status = $v.status;
      _createdAt = $v.createdAt;
      _id = $v.id;
      _description = $v.description;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(BaseReport other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$BaseReport;
  }

  @override
  void update(void Function(BaseReportBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$BaseReport build() {
    final _$result = _$v ??
        new _$BaseReport._(
            reportType: BuiltValueNullFieldError.checkNotNull(
                reportType, 'BaseReport', 'reportType'),
            authorId: BuiltValueNullFieldError.checkNotNull(
                authorId, 'BaseReport', 'authorId'),
            status: BuiltValueNullFieldError.checkNotNull(
                status, 'BaseReport', 'status'),
            createdAt: createdAt,
            id: id,
            description: description);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
