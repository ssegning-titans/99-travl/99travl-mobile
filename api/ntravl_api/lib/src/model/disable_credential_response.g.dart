// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'disable_credential_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$DisableCredentialResponse extends DisableCredentialResponse {
  @override
  final bool state;

  factory _$DisableCredentialResponse(
          [void Function(DisableCredentialResponseBuilder)? updates]) =>
      (new DisableCredentialResponseBuilder()..update(updates)).build();

  _$DisableCredentialResponse._({required this.state}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        state, 'DisableCredentialResponse', 'state');
  }

  @override
  DisableCredentialResponse rebuild(
          void Function(DisableCredentialResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DisableCredentialResponseBuilder toBuilder() =>
      new DisableCredentialResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DisableCredentialResponse && state == other.state;
  }

  @override
  int get hashCode {
    return $jf($jc(0, state.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('DisableCredentialResponse')
          ..add('state', state))
        .toString();
  }
}

class DisableCredentialResponseBuilder
    implements
        Builder<DisableCredentialResponse, DisableCredentialResponseBuilder> {
  _$DisableCredentialResponse? _$v;

  bool? _state;
  bool? get state => _$this._state;
  set state(bool? state) => _$this._state = state;

  DisableCredentialResponseBuilder() {
    DisableCredentialResponse._defaults(this);
  }

  DisableCredentialResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _state = $v.state;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(DisableCredentialResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$DisableCredentialResponse;
  }

  @override
  void update(void Function(DisableCredentialResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$DisableCredentialResponse build() {
    final _$result = _$v ??
        new _$DisableCredentialResponse._(
            state: BuiltValueNullFieldError.checkNotNull(
                state, 'DisableCredentialResponse', 'state'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
