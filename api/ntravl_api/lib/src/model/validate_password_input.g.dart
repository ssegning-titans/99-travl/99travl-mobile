// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'validate_password_input.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ValidatePasswordInput extends ValidatePasswordInput {
  @override
  final String challenge;

  factory _$ValidatePasswordInput(
          [void Function(ValidatePasswordInputBuilder)? updates]) =>
      (new ValidatePasswordInputBuilder()..update(updates)).build();

  _$ValidatePasswordInput._({required this.challenge}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        challenge, 'ValidatePasswordInput', 'challenge');
  }

  @override
  ValidatePasswordInput rebuild(
          void Function(ValidatePasswordInputBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ValidatePasswordInputBuilder toBuilder() =>
      new ValidatePasswordInputBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ValidatePasswordInput && challenge == other.challenge;
  }

  @override
  int get hashCode {
    return $jf($jc(0, challenge.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ValidatePasswordInput')
          ..add('challenge', challenge))
        .toString();
  }
}

class ValidatePasswordInputBuilder
    implements Builder<ValidatePasswordInput, ValidatePasswordInputBuilder> {
  _$ValidatePasswordInput? _$v;

  String? _challenge;
  String? get challenge => _$this._challenge;
  set challenge(String? challenge) => _$this._challenge = challenge;

  ValidatePasswordInputBuilder() {
    ValidatePasswordInput._defaults(this);
  }

  ValidatePasswordInputBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _challenge = $v.challenge;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ValidatePasswordInput other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ValidatePasswordInput;
  }

  @override
  void update(void Function(ValidatePasswordInputBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ValidatePasswordInput build() {
    final _$result = _$v ??
        new _$ValidatePasswordInput._(
            challenge: BuiltValueNullFieldError.checkNotNull(
                challenge, 'ValidatePasswordInput', 'challenge'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
