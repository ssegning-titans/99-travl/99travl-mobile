//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'bug_media.g.dart';

/// BugMedia
///
/// Properties:
/// * [id] 
/// * [creationDate] 
/// * [documentId] 
/// * [bugId] 
abstract class BugMedia implements Built<BugMedia, BugMediaBuilder> {
    @BuiltValueField(wireName: r'id')
    String? get id;

    @BuiltValueField(wireName: r'creationDate')
    int? get creationDate;

    @BuiltValueField(wireName: r'documentId')
    String get documentId;

    @BuiltValueField(wireName: r'bugId')
    String? get bugId;

    BugMedia._();

    @BuiltValueHook(initializeBuilder: true)
    static void _defaults(BugMediaBuilder b) => b;

    factory BugMedia([void updates(BugMediaBuilder b)]) = _$BugMedia;

    @BuiltValueSerializer(custom: true)
    static Serializer<BugMedia> get serializer => _$BugMediaSerializer();
}

class _$BugMediaSerializer implements StructuredSerializer<BugMedia> {
    @override
    final Iterable<Type> types = const [BugMedia, _$BugMedia];

    @override
    final String wireName = r'BugMedia';

    @override
    Iterable<Object?> serialize(Serializers serializers, BugMedia object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.id != null) {
            result
                ..add(r'id')
                ..add(serializers.serialize(object.id,
                    specifiedType: const FullType(String)));
        }
        if (object.creationDate != null) {
            result
                ..add(r'creationDate')
                ..add(serializers.serialize(object.creationDate,
                    specifiedType: const FullType(int)));
        }
        result
            ..add(r'documentId')
            ..add(serializers.serialize(object.documentId,
                specifiedType: const FullType(String)));
        if (object.bugId != null) {
            result
                ..add(r'bugId')
                ..add(serializers.serialize(object.bugId,
                    specifiedType: const FullType(String)));
        }
        return result;
    }

    @override
    BugMedia deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = BugMediaBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            
            switch (key) {
                case r'id':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.id = valueDes;
                    break;
                case r'creationDate':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    result.creationDate = valueDes;
                    break;
                case r'documentId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.documentId = valueDes;
                    break;
                case r'bugId':
                    final valueDes = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    result.bugId = valueDes;
                    break;
            }
        }
        return result.build();
    }
}

