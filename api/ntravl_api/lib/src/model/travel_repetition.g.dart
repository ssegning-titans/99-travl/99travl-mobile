// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'travel_repetition.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$TravelRepetition extends TravelRepetition {
  @override
  final int startingRepetitionDate;
  @override
  final int endingRepetitionDate;
  @override
  final BuiltList<TravelDays> days;

  factory _$TravelRepetition(
          [void Function(TravelRepetitionBuilder)? updates]) =>
      (new TravelRepetitionBuilder()..update(updates)).build();

  _$TravelRepetition._(
      {required this.startingRepetitionDate,
      required this.endingRepetitionDate,
      required this.days})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        startingRepetitionDate, 'TravelRepetition', 'startingRepetitionDate');
    BuiltValueNullFieldError.checkNotNull(
        endingRepetitionDate, 'TravelRepetition', 'endingRepetitionDate');
    BuiltValueNullFieldError.checkNotNull(days, 'TravelRepetition', 'days');
  }

  @override
  TravelRepetition rebuild(void Function(TravelRepetitionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  TravelRepetitionBuilder toBuilder() =>
      new TravelRepetitionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TravelRepetition &&
        startingRepetitionDate == other.startingRepetitionDate &&
        endingRepetitionDate == other.endingRepetitionDate &&
        days == other.days;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, startingRepetitionDate.hashCode),
            endingRepetitionDate.hashCode),
        days.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TravelRepetition')
          ..add('startingRepetitionDate', startingRepetitionDate)
          ..add('endingRepetitionDate', endingRepetitionDate)
          ..add('days', days))
        .toString();
  }
}

class TravelRepetitionBuilder
    implements Builder<TravelRepetition, TravelRepetitionBuilder> {
  _$TravelRepetition? _$v;

  int? _startingRepetitionDate;
  int? get startingRepetitionDate => _$this._startingRepetitionDate;
  set startingRepetitionDate(int? startingRepetitionDate) =>
      _$this._startingRepetitionDate = startingRepetitionDate;

  int? _endingRepetitionDate;
  int? get endingRepetitionDate => _$this._endingRepetitionDate;
  set endingRepetitionDate(int? endingRepetitionDate) =>
      _$this._endingRepetitionDate = endingRepetitionDate;

  ListBuilder<TravelDays>? _days;
  ListBuilder<TravelDays> get days =>
      _$this._days ??= new ListBuilder<TravelDays>();
  set days(ListBuilder<TravelDays>? days) => _$this._days = days;

  TravelRepetitionBuilder() {
    TravelRepetition._defaults(this);
  }

  TravelRepetitionBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _startingRepetitionDate = $v.startingRepetitionDate;
      _endingRepetitionDate = $v.endingRepetitionDate;
      _days = $v.days.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TravelRepetition other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$TravelRepetition;
  }

  @override
  void update(void Function(TravelRepetitionBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$TravelRepetition build() {
    _$TravelRepetition _$result;
    try {
      _$result = _$v ??
          new _$TravelRepetition._(
              startingRepetitionDate: BuiltValueNullFieldError.checkNotNull(
                  startingRepetitionDate,
                  'TravelRepetition',
                  'startingRepetitionDate'),
              endingRepetitionDate: BuiltValueNullFieldError.checkNotNull(
                  endingRepetitionDate,
                  'TravelRepetition',
                  'endingRepetitionDate'),
              days: days.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'days';
        days.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'TravelRepetition', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
