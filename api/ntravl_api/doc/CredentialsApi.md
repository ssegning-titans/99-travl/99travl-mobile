# ntravl_api.api.CredentialsApi

## Load the API package
```dart
import 'package:ntravl_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createCredential**](CredentialsApi.md#createcredential) | **POST** /api/v1/accounts/{id}/credentials/{type} | 
[**disableCredential**](CredentialsApi.md#disablecredential) | **PUT** /api/v1/accounts/{id}/credentials/{type}/disable | 
[**hasCredential**](CredentialsApi.md#hascredential) | **GET** /api/v1/accounts/{id}/credentials/{type} | 
[**validateCredential**](CredentialsApi.md#validatecredential) | **POST** /api/v1/accounts/{id}/credentials/{type}/verify | 


# **createCredential**
> CreateCredentialResponse createCredential(id, type, createCredentialInput)



### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getCredentialsApi();
final String id = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final CredentialType type = ; // CredentialType | 
final CreateCredentialInput createCredentialInput = ; // CreateCredentialInput | 

try {
    final response = api.createCredential(id, type, createCredentialInput);
    print(response);
} catch on DioError (e) {
    print('Exception when calling CredentialsApi->createCredential: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **type** | [**CredentialType**](.md)|  | 
 **createCredentialInput** | [**CreateCredentialInput**](CreateCredentialInput.md)|  | 

### Return type

[**CreateCredentialResponse**](CreateCredentialResponse.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **disableCredential**
> DisableCredentialResponse disableCredential(id, type, requestBody)



### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getCredentialsApi();
final String id = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final CredentialType type = ; // CredentialType | 
final BuiltMap<String, String> requestBody = ; // BuiltMap<String, String> | 

try {
    final response = api.disableCredential(id, type, requestBody);
    print(response);
} catch on DioError (e) {
    print('Exception when calling CredentialsApi->disableCredential: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **type** | [**CredentialType**](.md)|  | 
 **requestBody** | [**BuiltMap&lt;String, String&gt;**](String.md)|  | 

### Return type

[**DisableCredentialResponse**](DisableCredentialResponse.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **hasCredential**
> HasAccountPassword hasCredential(id, type)



### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getCredentialsApi();
final String id = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final CredentialType type = ; // CredentialType | 

try {
    final response = api.hasCredential(id, type);
    print(response);
} catch on DioError (e) {
    print('Exception when calling CredentialsApi->hasCredential: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **type** | [**CredentialType**](.md)|  | 

### Return type

[**HasAccountPassword**](HasAccountPassword.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **validateCredential**
> ValidatePasswordResponse validateCredential(id, type, validatePasswordInput)



### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getCredentialsApi();
final String id = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final CredentialType type = ; // CredentialType | 
final ValidatePasswordInput validatePasswordInput = ; // ValidatePasswordInput | 

try {
    final response = api.validateCredential(id, type, validatePasswordInput);
    print(response);
} catch on DioError (e) {
    print('Exception when calling CredentialsApi->validateCredential: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **type** | [**CredentialType**](.md)|  | 
 **validatePasswordInput** | [**ValidatePasswordInput**](ValidatePasswordInput.md)|  | 

### Return type

[**ValidatePasswordResponse**](ValidatePasswordResponse.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

