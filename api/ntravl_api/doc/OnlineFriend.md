# ntravl_api.model.OnlineFriend

## Load the model package
```dart
import 'package:ntravl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**creationDate** | **int** |  | 
**status** | [**FriendShipStatus**](FriendShipStatus.md) |  | [optional] 
**type** | [**FriendType**](FriendType.md) |  | [optional] 
**processType** | [**FriendProcessType**](FriendProcessType.md) |  | 
**accountId** | **String** |  | [optional] 
**authorId** | **String** |  | [optional] 
**isUserActive** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


