# ntravl_api.api.RatingsApi

## Load the API package
```dart
import 'package:ntravl_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPersonRating**](RatingsApi.md#createpersonrating) | **POST** /api/v1/ratings/persons | Create person rating
[**createTravelRating**](RatingsApi.md#createtravelrating) | **POST** /api/v1/ratings/travels | Create travel rating
[**deletePersonRating**](RatingsApi.md#deletepersonrating) | **DELETE** /api/v1/ratings/persons/{personId} | Delete person rating
[**deleteTravelRating**](RatingsApi.md#deletetravelrating) | **DELETE** /api/v1/ratings/travels/{travelId} | Delete travel rating
[**getAveragePersonRating**](RatingsApi.md#getaveragepersonrating) | **GET** /api/v1/ratings/persons/{personId}/average | Get person average&#39;s ratings
[**getAverageTravelRating**](RatingsApi.md#getaveragetravelrating) | **GET** /api/v1/ratings/travels/{travelId}/average | Get travel average&#39;s ratings
[**getCurrentAccountPersonRating**](RatingsApi.md#getcurrentaccountpersonrating) | **GET** /api/v1/ratings/persons/{personId}/current_account | Get person rating
[**getCurrentAccountTravelRating**](RatingsApi.md#getcurrentaccounttravelrating) | **GET** /api/v1/ratings/travels/{travelId}/current_account | Get travel rating
[**getOwnerRatings**](RatingsApi.md#getownerratings) | **GET** /api/v1/accounts/{accountId}/ratings | Get owner&#39;s rating
[**listPersonRating**](RatingsApi.md#listpersonrating) | **GET** /api/v1/ratings/persons/{personId} | Get person ratings
[**listTravelRating**](RatingsApi.md#listtravelrating) | **GET** /api/v1/ratings/travels/{travelId} | Get travel ratings


# **createPersonRating**
> BaseRating createPersonRating(ratingAccount)

Create person rating

Create a person's rating

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getRatingsApi();
final RatingAccount ratingAccount = ; // RatingAccount | 

try {
    final response = api.createPersonRating(ratingAccount);
    print(response);
} catch on DioError (e) {
    print('Exception when calling RatingsApi->createPersonRating: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ratingAccount** | [**RatingAccount**](RatingAccount.md)|  | 

### Return type

[**BaseRating**](BaseRating.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **createTravelRating**
> BaseRating createTravelRating(ratingTravel)

Create travel rating

Create a travel's rating

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getRatingsApi();
final RatingTravel ratingTravel = ; // RatingTravel | 

try {
    final response = api.createTravelRating(ratingTravel);
    print(response);
} catch on DioError (e) {
    print('Exception when calling RatingsApi->createTravelRating: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ratingTravel** | [**RatingTravel**](RatingTravel.md)|  | 

### Return type

[**BaseRating**](BaseRating.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deletePersonRating**
> deletePersonRating(personId)

Delete person rating

Get a person's rating

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getRatingsApi();
final String personId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    api.deletePersonRating(personId);
} catch on DioError (e) {
    print('Exception when calling RatingsApi->deletePersonRating: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **personId** | **String**|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deleteTravelRating**
> deleteTravelRating(travelId)

Delete travel rating

Get a travel's rating

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getRatingsApi();
final String travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    api.deleteTravelRating(travelId);
} catch on DioError (e) {
    print('Exception when calling RatingsApi->deleteTravelRating: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **travelId** | **String**|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAveragePersonRating**
> AverageRatingPerson getAveragePersonRating(personId)

Get person average's ratings

Get person average's ratings

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getRatingsApi();
final String personId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    final response = api.getAveragePersonRating(personId);
    print(response);
} catch on DioError (e) {
    print('Exception when calling RatingsApi->getAveragePersonRating: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **personId** | **String**|  | 

### Return type

[**AverageRatingPerson**](AverageRatingPerson.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAverageTravelRating**
> AverageRatingTravel getAverageTravelRating(travelId)

Get travel average's ratings

Get travel average's ratings

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getRatingsApi();
final String travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    final response = api.getAverageTravelRating(travelId);
    print(response);
} catch on DioError (e) {
    print('Exception when calling RatingsApi->getAverageTravelRating: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **travelId** | **String**|  | 

### Return type

[**AverageRatingTravel**](AverageRatingTravel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getCurrentAccountPersonRating**
> RatingAccount getCurrentAccountPersonRating(personId)

Get person rating

Get a person's rating by the current account

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getRatingsApi();
final String personId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    final response = api.getCurrentAccountPersonRating(personId);
    print(response);
} catch on DioError (e) {
    print('Exception when calling RatingsApi->getCurrentAccountPersonRating: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **personId** | **String**|  | 

### Return type

[**RatingAccount**](RatingAccount.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getCurrentAccountTravelRating**
> RatingTravel getCurrentAccountTravelRating(travelId)

Get travel rating

Get a travel's rating by the current account

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getRatingsApi();
final String travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    final response = api.getCurrentAccountTravelRating(travelId);
    print(response);
} catch on DioError (e) {
    print('Exception when calling RatingsApi->getCurrentAccountTravelRating: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **travelId** | **String**|  | 

### Return type

[**RatingTravel**](RatingTravel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getOwnerRatings**
> BuiltList<BaseRating> getOwnerRatings(accountId, size, page, sort)

Get owner's rating

Get owner's account all ratings

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getRatingsApi();
final String accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final int size = 56; // int | In the backend, this would always be defaulted at 20
final int page = 56; // int | In the backend, this would always be defaulted at 0
final BuiltList<String> sort = ; // BuiltList<String> | Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.

try {
    final response = api.getOwnerRatings(accountId, size, page, sort);
    print(response);
} catch on DioError (e) {
    print('Exception when calling RatingsApi->getOwnerRatings: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**|  | 
 **size** | **int**| In the backend, this would always be defaulted at 20 | [optional] [default to 20]
 **page** | **int**| In the backend, this would always be defaulted at 0 | [optional] 
 **sort** | [**BuiltList&lt;String&gt;**](String.md)| Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] [default to ListBuilder()]

### Return type

[**BuiltList&lt;BaseRating&gt;**](BaseRating.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **listPersonRating**
> BuiltList<RatingAccount> listPersonRating(personId, size, page, sort)

Get person ratings

Get all person's ratings

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getRatingsApi();
final String personId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final int size = 56; // int | In the backend, this would always be defaulted at 20
final int page = 56; // int | In the backend, this would always be defaulted at 0
final BuiltList<String> sort = ; // BuiltList<String> | Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.

try {
    final response = api.listPersonRating(personId, size, page, sort);
    print(response);
} catch on DioError (e) {
    print('Exception when calling RatingsApi->listPersonRating: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **personId** | **String**|  | 
 **size** | **int**| In the backend, this would always be defaulted at 20 | [optional] [default to 20]
 **page** | **int**| In the backend, this would always be defaulted at 0 | [optional] 
 **sort** | [**BuiltList&lt;String&gt;**](String.md)| Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] [default to ListBuilder()]

### Return type

[**BuiltList&lt;RatingAccount&gt;**](RatingAccount.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **listTravelRating**
> BuiltList<RatingTravel> listTravelRating(travelId, size, page, sort)

Get travel ratings

Get all travel's ratings

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getRatingsApi();
final String travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final int size = 56; // int | In the backend, this would always be defaulted at 20
final int page = 56; // int | In the backend, this would always be defaulted at 0
final BuiltList<String> sort = ; // BuiltList<String> | Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.

try {
    final response = api.listTravelRating(travelId, size, page, sort);
    print(response);
} catch on DioError (e) {
    print('Exception when calling RatingsApi->listTravelRating: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **travelId** | **String**|  | 
 **size** | **int**| In the backend, this would always be defaulted at 20 | [optional] [default to 20]
 **page** | **int**| In the backend, this would always be defaulted at 0 | [optional] 
 **sort** | [**BuiltList&lt;String&gt;**](String.md)| Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] [default to ListBuilder()]

### Return type

[**BuiltList&lt;RatingTravel&gt;**](RatingTravel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

