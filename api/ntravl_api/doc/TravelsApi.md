# ntravl_api.api.TravelsApi

## Load the API package
```dart
import 'package:ntravl_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createTravel**](TravelsApi.md#createtravel) | **POST** /api/v1/travels | Create travel
[**deleteTravelById**](TravelsApi.md#deletetravelbyid) | **DELETE** /api/v1/travels/{travelId} | Delete travel
[**getAccountTravels**](TravelsApi.md#getaccounttravels) | **GET** /api/v1/travels/accounts/{accountId} | Get accounts travels
[**getRandomTravel**](TravelsApi.md#getrandomtravel) | **GET** /api/v1/travels | Get travel randomly
[**getTravelById**](TravelsApi.md#gettravelbyid) | **GET** /api/v1/travels/{travelId} | Get travel
[**searchTravel**](TravelsApi.md#searchtravel) | **GET** /api/v1/travels/search | Search for travel between two points
[**updateTravelById**](TravelsApi.md#updatetravelbyid) | **PUT** /api/v1/travels | Update travel


# **createTravel**
> Travel createTravel(createTravel)

Create travel

### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getTravelsApi();
final CreateTravel createTravel = ; // CreateTravel | 

try {
    final response = api.createTravel(createTravel);
    print(response);
} catch on DioError (e) {
    print('Exception when calling TravelsApi->createTravel: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createTravel** | [**CreateTravel**](CreateTravel.md)|  | 

### Return type

[**Travel**](Travel.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deleteTravelById**
> deleteTravelById(travelId)

Delete travel

Delete one travel

### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getTravelsApi();
final String travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    api.deleteTravelById(travelId);
} catch on DioError (e) {
    print('Exception when calling TravelsApi->deleteTravelById: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **travelId** | **String**|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAccountTravels**
> BuiltList<Travel> getAccountTravels(accountId, size, page, sort)

Get accounts travels

### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getTravelsApi();
final String accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final int size = 56; // int | In the backend, this would always be defaulted at 20
final int page = 56; // int | In the backend, this would always be defaulted at 0
final BuiltList<String> sort = ; // BuiltList<String> | Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.

try {
    final response = api.getAccountTravels(accountId, size, page, sort);
    print(response);
} catch on DioError (e) {
    print('Exception when calling TravelsApi->getAccountTravels: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**|  | 
 **size** | **int**| In the backend, this would always be defaulted at 20 | [optional] [default to 20]
 **page** | **int**| In the backend, this would always be defaulted at 0 | [optional] 
 **sort** | [**BuiltList&lt;String&gt;**](String.md)| Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] [default to ListBuilder()]

### Return type

[**BuiltList&lt;Travel&gt;**](Travel.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getRandomTravel**
> BuiltList<TravelPoint> getRandomTravel(coord, radius, size, page, sort)

Get travel randomly

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getTravelsApi();
final BuiltList<double> coord = ; // BuiltList<double> | The first position in the array is the lat, the second is the lng. In the future, the third will be the altitude
final double radius = 1.2; // double | Know if these coordinates are starting or ending points
final int size = 56; // int | In the backend, this would always be defaulted at 20
final int page = 56; // int | In the backend, this would always be defaulted at 0
final BuiltList<String> sort = ; // BuiltList<String> | Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.

try {
    final response = api.getRandomTravel(coord, radius, size, page, sort);
    print(response);
} catch on DioError (e) {
    print('Exception when calling TravelsApi->getRandomTravel: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **coord** | [**BuiltList&lt;double&gt;**](double.md)| The first position in the array is the lat, the second is the lng. In the future, the third will be the altitude | 
 **radius** | **double**| Know if these coordinates are starting or ending points | 
 **size** | **int**| In the backend, this would always be defaulted at 20 | [optional] [default to 20]
 **page** | **int**| In the backend, this would always be defaulted at 0 | [optional] 
 **sort** | [**BuiltList&lt;String&gt;**](String.md)| Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] [default to ListBuilder()]

### Return type

[**BuiltList&lt;TravelPoint&gt;**](TravelPoint.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getTravelById**
> Travel getTravelById(travelId)

Get travel

Get one travel

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getTravelsApi();
final String travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    final response = api.getTravelById(travelId);
    print(response);
} catch on DioError (e) {
    print('Exception when calling TravelsApi->getTravelById: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **travelId** | **String**|  | 

### Return type

[**Travel**](Travel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **searchTravel**
> BuiltList<SearchTravel> searchTravel(startCoord, endCoord, startRadius, endRadius, startDate, endDate, size, page, sort)

Search for travel between two points

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getTravelsApi();
final BuiltList<double> startCoord = ; // BuiltList<double> | The first position in the array is the lat, the second is the lng. In the future, the third will be the altitude
final BuiltList<double> endCoord = ; // BuiltList<double> | The first position in the array is the lat, the second is the lng. In the future, the third will be the altitude
final double startRadius = 1.2; // double | 
final double endRadius = 1.2; // double | 
final int startDate = 789; // int | 
final int endDate = 789; // int | 
final int size = 56; // int | In the backend, this would always be defaulted at 20
final int page = 56; // int | In the backend, this would always be defaulted at 0
final BuiltList<String> sort = ; // BuiltList<String> | Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.

try {
    final response = api.searchTravel(startCoord, endCoord, startRadius, endRadius, startDate, endDate, size, page, sort);
    print(response);
} catch on DioError (e) {
    print('Exception when calling TravelsApi->searchTravel: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startCoord** | [**BuiltList&lt;double&gt;**](double.md)| The first position in the array is the lat, the second is the lng. In the future, the third will be the altitude | 
 **endCoord** | [**BuiltList&lt;double&gt;**](double.md)| The first position in the array is the lat, the second is the lng. In the future, the third will be the altitude | 
 **startRadius** | **double**|  | 
 **endRadius** | **double**|  | 
 **startDate** | **int**|  | 
 **endDate** | **int**|  | 
 **size** | **int**| In the backend, this would always be defaulted at 20 | [optional] [default to 20]
 **page** | **int**| In the backend, this would always be defaulted at 0 | [optional] 
 **sort** | [**BuiltList&lt;String&gt;**](String.md)| Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] [default to ListBuilder()]

### Return type

[**BuiltList&lt;SearchTravel&gt;**](SearchTravel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateTravelById**
> Travel updateTravelById(travel)

Update travel

Update one travel

### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getTravelsApi();
final Travel travel = ; // Travel | 

try {
    final response = api.updateTravelById(travel);
    print(response);
} catch on DioError (e) {
    print('Exception when calling TravelsApi->updateTravelById: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **travel** | [**Travel**](Travel.md)|  | 

### Return type

[**Travel**](Travel.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

