# ntravl_api.model.Document

## Load the model package
```dart
import 'package:ntravl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bytes** | **String** |  | [optional] 
**contentType** | **String** |  | [optional] 
**filePath** | **String** |  | [optional] 
**metaData** | **BuiltMap&lt;String, String&gt;** |  | [optional] 
**headers** | **BuiltMap&lt;String, String&gt;** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


