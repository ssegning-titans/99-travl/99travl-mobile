# ntravl_api.model.Favorite

## Load the model package
```dart
import 'package:ntravl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**ownerId** | **String** |  | 
**createdAt** | **int** |  | [optional] 
**favoriteType** | [**FavoriteType**](FavoriteType.md) |  | 
**accountId** | **String** |  | 
**travelId** | **String** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


