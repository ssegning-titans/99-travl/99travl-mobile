# ntravl_api.api.FavoritesApi

## Load the API package
```dart
import 'package:ntravl_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createFavoriteAccount**](FavoritesApi.md#createfavoriteaccount) | **POST** /api/v1/accounts/{accountId}/favorites/accounts/{personId} | Create person favorite
[**createTravelFavorite**](FavoritesApi.md#createtravelfavorite) | **POST** /api/v1/accounts/{accountId}/favorites/travels/{travelId} | Create travel favorite
[**deleteFavoriteAccount**](FavoritesApi.md#deletefavoriteaccount) | **DELETE** /api/v1/accounts/{accountId}/favorites/accounts/{personId} | Delete account travel
[**deleteTravelFavorite**](FavoritesApi.md#deletetravelfavorite) | **DELETE** /api/v1/accounts/{accountId}/favorites/travels/{travelId} | Delete favorite travel
[**getFavoriteAccount**](FavoritesApi.md#getfavoriteaccount) | **GET** /api/v1/accounts/{accountId}/favorites/accounts/{personId} | Get account favorite
[**getOwnerFavorites**](FavoritesApi.md#getownerfavorites) | **GET** /api/v1/accounts/{accountId}/favorites | Get owner&#39;s favorite
[**getTravelFavorite**](FavoritesApi.md#gettravelfavorite) | **GET** /api/v1/accounts/{accountId}/favorites/travels/{travelId} | Get travel favorite


# **createFavoriteAccount**
> FavoriteAccount createFavoriteAccount(accountId, personId)

Create person favorite

Create a person's favorite

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getFavoritesApi();
final String accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final String personId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    final response = api.createFavoriteAccount(accountId, personId);
    print(response);
} catch on DioError (e) {
    print('Exception when calling FavoritesApi->createFavoriteAccount: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**|  | 
 **personId** | **String**|  | 

### Return type

[**FavoriteAccount**](FavoriteAccount.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **createTravelFavorite**
> FavoriteTravel createTravelFavorite(accountId, travelId)

Create travel favorite

Create a travel's favorite

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getFavoritesApi();
final String accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final String travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    final response = api.createTravelFavorite(accountId, travelId);
    print(response);
} catch on DioError (e) {
    print('Exception when calling FavoritesApi->createTravelFavorite: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**|  | 
 **travelId** | **String**|  | 

### Return type

[**FavoriteTravel**](FavoriteTravel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deleteFavoriteAccount**
> deleteFavoriteAccount(accountId, personId)

Delete account travel

Get a account's favorite

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getFavoritesApi();
final String accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final String personId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    api.deleteFavoriteAccount(accountId, personId);
} catch on DioError (e) {
    print('Exception when calling FavoritesApi->deleteFavoriteAccount: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**|  | 
 **personId** | **String**|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deleteTravelFavorite**
> deleteTravelFavorite(accountId, travelId)

Delete favorite travel

Get a travel's favorite

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getFavoritesApi();
final String accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final String travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    api.deleteTravelFavorite(accountId, travelId);
} catch on DioError (e) {
    print('Exception when calling FavoritesApi->deleteTravelFavorite: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**|  | 
 **travelId** | **String**|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getFavoriteAccount**
> FavoriteAccount getFavoriteAccount(accountId, personId)

Get account favorite

Get a account's favorite

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getFavoritesApi();
final String accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final String personId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    final response = api.getFavoriteAccount(accountId, personId);
    print(response);
} catch on DioError (e) {
    print('Exception when calling FavoritesApi->getFavoriteAccount: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**|  | 
 **personId** | **String**|  | 

### Return type

[**FavoriteAccount**](FavoriteAccount.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getOwnerFavorites**
> BuiltList<Favorite> getOwnerFavorites(accountId, size, page, sort)

Get owner's favorite

Get owner's account all favorites

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getFavoritesApi();
final String accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final int size = 56; // int | In the backend, this would always be defaulted at 20
final int page = 56; // int | In the backend, this would always be defaulted at 0
final BuiltList<String> sort = ; // BuiltList<String> | Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.

try {
    final response = api.getOwnerFavorites(accountId, size, page, sort);
    print(response);
} catch on DioError (e) {
    print('Exception when calling FavoritesApi->getOwnerFavorites: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**|  | 
 **size** | **int**| In the backend, this would always be defaulted at 20 | [optional] [default to 20]
 **page** | **int**| In the backend, this would always be defaulted at 0 | [optional] 
 **sort** | [**BuiltList&lt;String&gt;**](String.md)| Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] [default to ListBuilder()]

### Return type

[**BuiltList&lt;Favorite&gt;**](Favorite.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getTravelFavorite**
> FavoriteTravel getTravelFavorite(accountId, travelId)

Get travel favorite

Get a travel's favorite

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getFavoritesApi();
final String accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final String travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    final response = api.getTravelFavorite(accountId, travelId);
    print(response);
} catch on DioError (e) {
    print('Exception when calling FavoritesApi->getTravelFavorite: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**|  | 
 **travelId** | **String**|  | 

### Return type

[**FavoriteTravel**](FavoriteTravel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

