# ntravl_api.model.ChatMessage

## Load the model package
```dart
import 'package:ntravl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**createdAt** | **int** |  | [optional] 
**authorId** | **String** |  | 
**chatId** | **String** |  | 
**messageType** | [**ChatMessageType**](ChatMessageType.md) |  | 
**seen** | **bool** |  | [optional] [default to false]
**medias** | [**BuiltSet&lt;ChatMedia&gt;**](ChatMedia.md) |  | 
**proposition** | [**TravelProposition**](TravelProposition.md) |  | [optional] 
**message** | **String** |  | 
**contacts** | [**BuiltSet&lt;ChatContact&gt;**](ChatContact.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


