# ntravl_api.model.ChatMedia

## Load the model package
```dart
import 'package:ntravl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**creationDate** | **int** |  | [optional] 
**documentId** | **String** |  | [optional] 
**path** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**metaData** | **BuiltMap&lt;String, String&gt;** |  | [optional] 
**width** | **int** |  | [default to 1]
**height** | **int** |  | [default to 1]
**priority** | **int** |  | [optional] [default to 1]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


