# ntravl_api.api.ReservationsApi

## Load the API package
```dart
import 'package:ntravl_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**countTravelReservation**](ReservationsApi.md#counttravelreservation) | **GET** /api/v1/travels/{travelId}/reservations/count | Count travel&#39;s reservation
[**createTravelReservation**](ReservationsApi.md#createtravelreservation) | **POST** /api/v1/travels/{travelId}/reservations | Create Travel Reservation
[**deleteReservationById**](ReservationsApi.md#deletereservationbyid) | **DELETE** /api/v1/travels/{travelId}/reservations/{reservationId} | Delete travel
[**getReservationById**](ReservationsApi.md#getreservationbyid) | **GET** /api/v1/travels/{travelId}/reservations/{reservationId} | Get travel
[**getReservationForTravel**](ReservationsApi.md#getreservationfortravel) | **GET** /api/v1/travels/{travelId}/reservations | Get Reservation for travel
[**updateReservationById**](ReservationsApi.md#updatereservationbyid) | **PUT** /api/v1/travels/{travelId}/reservations | Update travel


# **countTravelReservation**
> TravelReservationCount countTravelReservation(travelId)

Count travel's reservation

Count all travel's reservations

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getReservationsApi();
final String travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    final response = api.countTravelReservation(travelId);
    print(response);
} catch on DioError (e) {
    print('Exception when calling ReservationsApi->countTravelReservation: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **travelId** | **String**|  | 

### Return type

[**TravelReservationCount**](TravelReservationCount.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **createTravelReservation**
> TravelReservation createTravelReservation(travelId, travelReservation)

Create Travel Reservation

### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getReservationsApi();
final String travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final TravelReservation travelReservation = ; // TravelReservation | 

try {
    final response = api.createTravelReservation(travelId, travelReservation);
    print(response);
} catch on DioError (e) {
    print('Exception when calling ReservationsApi->createTravelReservation: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **travelId** | **String**|  | 
 **travelReservation** | [**TravelReservation**](TravelReservation.md)|  | 

### Return type

[**TravelReservation**](TravelReservation.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deleteReservationById**
> deleteReservationById(travelId, reservationId)

Delete travel

Delete one travel

### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getReservationsApi();
final String travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final String reservationId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    api.deleteReservationById(travelId, reservationId);
} catch on DioError (e) {
    print('Exception when calling ReservationsApi->deleteReservationById: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **travelId** | **String**|  | 
 **reservationId** | **String**|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getReservationById**
> TravelReservation getReservationById(travelId, reservationId)

Get travel

Get one travel

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getReservationsApi();
final String travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final String reservationId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    final response = api.getReservationById(travelId, reservationId);
    print(response);
} catch on DioError (e) {
    print('Exception when calling ReservationsApi->getReservationById: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **travelId** | **String**|  | 
 **reservationId** | **String**|  | 

### Return type

[**TravelReservation**](TravelReservation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getReservationForTravel**
> BuiltList<TravelReservation> getReservationForTravel(travelId, size, page, sort)

Get Reservation for travel

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getReservationsApi();
final String travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final int size = 56; // int | In the backend, this would always be defaulted at 20
final int page = 56; // int | In the backend, this would always be defaulted at 0
final BuiltList<String> sort = ; // BuiltList<String> | Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.

try {
    final response = api.getReservationForTravel(travelId, size, page, sort);
    print(response);
} catch on DioError (e) {
    print('Exception when calling ReservationsApi->getReservationForTravel: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **travelId** | **String**|  | 
 **size** | **int**| In the backend, this would always be defaulted at 20 | [optional] [default to 20]
 **page** | **int**| In the backend, this would always be defaulted at 0 | [optional] 
 **sort** | [**BuiltList&lt;String&gt;**](String.md)| Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] [default to ListBuilder()]

### Return type

[**BuiltList&lt;TravelReservation&gt;**](TravelReservation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateReservationById**
> TravelReservation updateReservationById(travelId, travelReservation)

Update travel

Update one travel

### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getReservationsApi();
final String travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final TravelReservation travelReservation = ; // TravelReservation | 

try {
    final response = api.updateReservationById(travelId, travelReservation);
    print(response);
} catch on DioError (e) {
    print('Exception when calling ReservationsApi->updateReservationById: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **travelId** | **String**|  | 
 **travelReservation** | [**TravelReservation**](TravelReservation.md)|  | 

### Return type

[**TravelReservation**](TravelReservation.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

