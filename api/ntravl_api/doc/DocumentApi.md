# ntravl_api.api.DocumentApi

## Load the API package
```dart
import 'package:ntravl_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getOnDocument**](DocumentApi.md#getondocument) | **GET** /api/v1/documents/{documentId} | 
[**uploadOne**](DocumentApi.md#uploadone) | **POST** /api/v1/documents/upload | 


# **getOnDocument**
> Document getOnDocument(documentId)



### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getDocumentApi();
final String documentId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    final response = api.getOnDocument(documentId);
    print(response);
} catch on DioError (e) {
    print('Exception when calling DocumentApi->getOnDocument: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **documentId** | **String**|  | 

### Return type

[**Document**](Document.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **uploadOne**
> UploadedResponse uploadOne(file)



### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getDocumentApi();
final MultipartFile file = BINARY_DATA_HERE; // MultipartFile | 

try {
    final response = api.uploadOne(file);
    print(response);
} catch on DioError (e) {
    print('Exception when calling DocumentApi->uploadOne: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **MultipartFile**|  | [optional] 

### Return type

[**UploadedResponse**](UploadedResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

