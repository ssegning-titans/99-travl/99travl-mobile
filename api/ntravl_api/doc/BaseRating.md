# ntravl_api.model.BaseRating

## Load the model package
```dart
import 'package:ntravl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**authorId** | **String** |  | 
**value** | [**RatingValue**](RatingValue.md) |  | 
**createdAt** | **int** |  | [optional] 
**type** | [**RatingType**](RatingType.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


