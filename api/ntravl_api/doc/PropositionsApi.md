# ntravl_api.api.PropositionsApi

## Load the API package
```dart
import 'package:ntravl_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createChatAndTravelProposition**](PropositionsApi.md#createchatandtravelproposition) | **POST** /api/v1/travels/{travelId}/propositions | Create proposition
[**getOneTravelProposition**](PropositionsApi.md#getonetravelproposition) | **GET** /api/v1/travels/_/propositions/{propositionId} | Get one travel proposition
[**getTravelPropositions**](PropositionsApi.md#gettravelpropositions) | **GET** /api/v1/travels/{travelId}/propositions | Get travel propositions
[**updateTravelProposition**](PropositionsApi.md#updatetravelproposition) | **PUT** /api/v1/travels/_/propositions/{propositionId} | Update proposition


# **createChatAndTravelProposition**
> TravelProposition createChatAndTravelProposition(travelId, travelProposition)

Create proposition

Create one travel proposition and add it into a chat

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getPropositionsApi();
final String travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final TravelProposition travelProposition = ; // TravelProposition | The travel propositions new body

try {
    final response = api.createChatAndTravelProposition(travelId, travelProposition);
    print(response);
} catch on DioError (e) {
    print('Exception when calling PropositionsApi->createChatAndTravelProposition: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **travelId** | **String**|  | 
 **travelProposition** | [**TravelProposition**](TravelProposition.md)| The travel propositions new body | [optional] 

### Return type

[**TravelProposition**](TravelProposition.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getOneTravelProposition**
> TravelProposition getOneTravelProposition(propositionId)

Get one travel proposition

Get one travel proposition by id

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getPropositionsApi();
final String propositionId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    final response = api.getOneTravelProposition(propositionId);
    print(response);
} catch on DioError (e) {
    print('Exception when calling PropositionsApi->getOneTravelProposition: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **propositionId** | **String**|  | 

### Return type

[**TravelProposition**](TravelProposition.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getTravelPropositions**
> BuiltList<TravelProposition> getTravelPropositions(travelId, size, page, sort)

Get travel propositions

Get all travel's propositions

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getPropositionsApi();
final String travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final int size = 56; // int | In the backend, this would always be defaulted at 20
final int page = 56; // int | In the backend, this would always be defaulted at 0
final BuiltList<String> sort = ; // BuiltList<String> | Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.

try {
    final response = api.getTravelPropositions(travelId, size, page, sort);
    print(response);
} catch on DioError (e) {
    print('Exception when calling PropositionsApi->getTravelPropositions: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **travelId** | **String**|  | 
 **size** | **int**| In the backend, this would always be defaulted at 20 | [optional] [default to 20]
 **page** | **int**| In the backend, this would always be defaulted at 0 | [optional] 
 **sort** | [**BuiltList&lt;String&gt;**](String.md)| Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] [default to ListBuilder()]

### Return type

[**BuiltList&lt;TravelProposition&gt;**](TravelProposition.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateTravelProposition**
> TravelProposition updateTravelProposition(propositionId, travelProposition)

Update proposition

Update one travel proposition by id

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getPropositionsApi();
final String propositionId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final TravelProposition travelProposition = ; // TravelProposition | The travel propositions new body

try {
    final response = api.updateTravelProposition(propositionId, travelProposition);
    print(response);
} catch on DioError (e) {
    print('Exception when calling PropositionsApi->updateTravelProposition: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **propositionId** | **String**|  | 
 **travelProposition** | [**TravelProposition**](TravelProposition.md)| The travel propositions new body | [optional] 

### Return type

[**TravelProposition**](TravelProposition.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

