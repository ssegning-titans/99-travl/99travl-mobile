# ntravl_api.api.AccountAddressApi

## Load the API package
```dart
import 'package:ntravl_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createAddress**](AccountAddressApi.md#createaddress) | **POST** /api/v1/accounts/{accountId}/addresses | Create address
[**deleteAddressById**](AccountAddressApi.md#deleteaddressbyid) | **DELETE** /api/v1/accounts/{accountId}/addresses/{addressId} | Delete address
[**findAddressById**](AccountAddressApi.md#findaddressbyid) | **GET** /api/v1/accounts/{accountId}/addresses/{addressId} | Get specific address
[**findAddressesByAccountId**](AccountAddressApi.md#findaddressesbyaccountid) | **GET** /api/v1/accounts/{accountId}/addresses | List addresses
[**findLastAddressByAccountId**](AccountAddressApi.md#findlastaddressbyaccountid) | **GET** /api/v1/accounts/{accountId}/addresses/last-added | Get address
[**updateAddress**](AccountAddressApi.md#updateaddress) | **PUT** /api/v1/accounts/{accountId}/addresses | Update address


# **createAddress**
> AccountAddress createAddress(accountId, accountAddress)

Create address

Create an address for an account

### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getAccountAddressApi();
final String accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final AccountAddress accountAddress = ; // AccountAddress | 

try {
    final response = api.createAddress(accountId, accountAddress);
    print(response);
} catch on DioError (e) {
    print('Exception when calling AccountAddressApi->createAddress: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**|  | 
 **accountAddress** | [**AccountAddress**](AccountAddress.md)|  | 

### Return type

[**AccountAddress**](AccountAddress.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deleteAddressById**
> deleteAddressById(accountId, addressId)

Delete address

Delete an address for an account

### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getAccountAddressApi();
final String accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final String addressId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    api.deleteAddressById(accountId, addressId);
} catch on DioError (e) {
    print('Exception when calling AccountAddressApi->deleteAddressById: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**|  | 
 **addressId** | **String**|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **findAddressById**
> AccountAddress findAddressById(accountId, addressId)

Get specific address

Get one address for an account by it's Id

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getAccountAddressApi();
final String accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final String addressId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    final response = api.findAddressById(accountId, addressId);
    print(response);
} catch on DioError (e) {
    print('Exception when calling AccountAddressApi->findAddressById: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**|  | 
 **addressId** | **String**|  | 

### Return type

[**AccountAddress**](AccountAddress.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **findAddressesByAccountId**
> BuiltList<AccountAddress> findAddressesByAccountId(accountId, size, page, sort)

List addresses

List accounts addresses using a pageable

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getAccountAddressApi();
final String accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final int size = 56; // int | In the backend, this would always be defaulted at 20
final int page = 56; // int | In the backend, this would always be defaulted at 0
final BuiltList<String> sort = ; // BuiltList<String> | Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.

try {
    final response = api.findAddressesByAccountId(accountId, size, page, sort);
    print(response);
} catch on DioError (e) {
    print('Exception when calling AccountAddressApi->findAddressesByAccountId: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**|  | 
 **size** | **int**| In the backend, this would always be defaulted at 20 | [optional] [default to 20]
 **page** | **int**| In the backend, this would always be defaulted at 0 | [optional] 
 **sort** | [**BuiltList&lt;String&gt;**](String.md)| Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] [default to ListBuilder()]

### Return type

[**BuiltList&lt;AccountAddress&gt;**](AccountAddress.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **findLastAddressByAccountId**
> AccountAddress findLastAddressByAccountId(accountId)

Get address

Get one address for an account

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getAccountAddressApi();
final String accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    final response = api.findLastAddressByAccountId(accountId);
    print(response);
} catch on DioError (e) {
    print('Exception when calling AccountAddressApi->findLastAddressByAccountId: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**|  | 

### Return type

[**AccountAddress**](AccountAddress.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateAddress**
> AccountAddress updateAddress(accountId, accountAddress)

Update address

Update an address for an account

### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getAccountAddressApi();
final String accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final AccountAddress accountAddress = ; // AccountAddress | 

try {
    final response = api.updateAddress(accountId, accountAddress);
    print(response);
} catch on DioError (e) {
    print('Exception when calling AccountAddressApi->updateAddress: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**|  | 
 **accountAddress** | [**AccountAddress**](AccountAddress.md)|  | 

### Return type

[**AccountAddress**](AccountAddress.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

