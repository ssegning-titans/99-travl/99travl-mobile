# ntravl_api.model.TravelReservation

## Load the model package
```dart
import 'package:ntravl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**creationDate** | **int** |  | [optional] 
**travelId** | **String** |  | [optional] 
**startingReservationPoint** | **String** |  | [optional] 
**endingReservationPoint** | **String** |  | [optional] 
**reserverAccountId** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


