# ntravl_api.api.ReportsApi

## Load the API package
```dart
import 'package:ntravl_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAllBugReports**](ReportsApi.md#getallbugreports) | **GET** /api/v1/reports/bugs | Get Report&#39;s bug
[**getAllReportForAccount**](ReportsApi.md#getallreportforaccount) | **GET** /api/v1/reports/accounts/{accountId} | Get Report&#39;s account
[**getAllReportForTravel**](ReportsApi.md#getallreportfortravel) | **GET** /api/v1/reports/travels/{travelId} | Get Report&#39;s Travel
[**getAllReports**](ReportsApi.md#getallreports) | **GET** /api/v1/reports/owners | Get report
[**getOwnersReports**](ReportsApi.md#getownersreports) | **GET** /api/v1/reports/owners/{ownerId} | Get owner&#39;s report
[**getReportById**](ReportsApi.md#getreportbyid) | **GET** /api/v1/reports/{reportId} | Get one report
[**reportAccount**](ReportsApi.md#reportaccount) | **POST** /api/v1/reports/accounts | Create account report&#39;s
[**reportBug**](ReportsApi.md#reportbug) | **POST** /api/v1/reports/bugs | Create bug report&#39;s
[**reportTravel**](ReportsApi.md#reporttravel) | **POST** /api/v1/reports/travels | Create travel report&#39;s


# **getAllBugReports**
> BuiltList<ReportBug> getAllBugReports(accountId, size, page, sort)

Get Report's bug

Get all report

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getReportsApi();
final String accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final int size = 56; // int | In the backend, this would always be defaulted at 20
final int page = 56; // int | In the backend, this would always be defaulted at 0
final BuiltList<String> sort = ; // BuiltList<String> | Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.

try {
    final response = api.getAllBugReports(accountId, size, page, sort);
    print(response);
} catch on DioError (e) {
    print('Exception when calling ReportsApi->getAllBugReports: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**|  | [optional] 
 **size** | **int**| In the backend, this would always be defaulted at 20 | [optional] [default to 20]
 **page** | **int**| In the backend, this would always be defaulted at 0 | [optional] 
 **sort** | [**BuiltList&lt;String&gt;**](String.md)| Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] [default to ListBuilder()]

### Return type

[**BuiltList&lt;ReportBug&gt;**](ReportBug.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAllReportForAccount**
> BuiltList<ReportAccount> getAllReportForAccount(accountId, size, page, sort)

Get Report's account

Get all report for a account

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getReportsApi();
final String accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final int size = 56; // int | In the backend, this would always be defaulted at 20
final int page = 56; // int | In the backend, this would always be defaulted at 0
final BuiltList<String> sort = ; // BuiltList<String> | Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.

try {
    final response = api.getAllReportForAccount(accountId, size, page, sort);
    print(response);
} catch on DioError (e) {
    print('Exception when calling ReportsApi->getAllReportForAccount: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**|  | 
 **size** | **int**| In the backend, this would always be defaulted at 20 | [optional] [default to 20]
 **page** | **int**| In the backend, this would always be defaulted at 0 | [optional] 
 **sort** | [**BuiltList&lt;String&gt;**](String.md)| Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] [default to ListBuilder()]

### Return type

[**BuiltList&lt;ReportAccount&gt;**](ReportAccount.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAllReportForTravel**
> BuiltList<ReportTravel> getAllReportForTravel(travelId, size, page, sort)

Get Report's Travel

Get all report for a travel

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getReportsApi();
final String travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final int size = 56; // int | In the backend, this would always be defaulted at 20
final int page = 56; // int | In the backend, this would always be defaulted at 0
final BuiltList<String> sort = ; // BuiltList<String> | Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.

try {
    final response = api.getAllReportForTravel(travelId, size, page, sort);
    print(response);
} catch on DioError (e) {
    print('Exception when calling ReportsApi->getAllReportForTravel: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **travelId** | **String**|  | 
 **size** | **int**| In the backend, this would always be defaulted at 20 | [optional] [default to 20]
 **page** | **int**| In the backend, this would always be defaulted at 0 | [optional] 
 **sort** | [**BuiltList&lt;String&gt;**](String.md)| Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] [default to ListBuilder()]

### Return type

[**BuiltList&lt;ReportTravel&gt;**](ReportTravel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAllReports**
> BuiltList<BaseReport> getAllReports(status, size, page, sort)

Get report

Get all report for an account

### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getReportsApi();
final ReportStatus status = ; // ReportStatus | 
final int size = 56; // int | In the backend, this would always be defaulted at 20
final int page = 56; // int | In the backend, this would always be defaulted at 0
final BuiltList<String> sort = ; // BuiltList<String> | Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.

try {
    final response = api.getAllReports(status, size, page, sort);
    print(response);
} catch on DioError (e) {
    print('Exception when calling ReportsApi->getAllReports: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **status** | [**ReportStatus**](.md)|  | 
 **size** | **int**| In the backend, this would always be defaulted at 20 | [optional] [default to 20]
 **page** | **int**| In the backend, this would always be defaulted at 0 | [optional] 
 **sort** | [**BuiltList&lt;String&gt;**](String.md)| Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] [default to ListBuilder()]

### Return type

[**BuiltList&lt;BaseReport&gt;**](BaseReport.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getOwnersReports**
> BuiltList<BaseReport> getOwnersReports(ownerId, status, size, page, sort)

Get owner's report

Get all report for a account

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getReportsApi();
final String ownerId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final ReportStatus status = ; // ReportStatus | 
final int size = 56; // int | In the backend, this would always be defaulted at 20
final int page = 56; // int | In the backend, this would always be defaulted at 0
final BuiltList<String> sort = ; // BuiltList<String> | Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.

try {
    final response = api.getOwnersReports(ownerId, status, size, page, sort);
    print(response);
} catch on DioError (e) {
    print('Exception when calling ReportsApi->getOwnersReports: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ownerId** | **String**|  | 
 **status** | [**ReportStatus**](.md)|  | 
 **size** | **int**| In the backend, this would always be defaulted at 20 | [optional] [default to 20]
 **page** | **int**| In the backend, this would always be defaulted at 0 | [optional] 
 **sort** | [**BuiltList&lt;String&gt;**](String.md)| Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] [default to ListBuilder()]

### Return type

[**BuiltList&lt;BaseReport&gt;**](BaseReport.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getReportById**
> BaseReport getReportById(reportId)

Get one report

Use the ID to get one single report

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getReportsApi();
final String reportId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    final response = api.getReportById(reportId);
    print(response);
} catch on DioError (e) {
    print('Exception when calling ReportsApi->getReportById: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reportId** | **String**|  | 

### Return type

[**BaseReport**](BaseReport.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **reportAccount**
> ReportAccount reportAccount(createReportAccount)

Create account report's

Create a account's report

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getReportsApi();
final CreateReportAccount createReportAccount = ; // CreateReportAccount | 

try {
    final response = api.reportAccount(createReportAccount);
    print(response);
} catch on DioError (e) {
    print('Exception when calling ReportsApi->reportAccount: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createReportAccount** | [**CreateReportAccount**](CreateReportAccount.md)|  | 

### Return type

[**ReportAccount**](ReportAccount.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **reportBug**
> ReportBug reportBug(createReportBug, accountId)

Create bug report's

Create a bug's report

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getReportsApi();
final CreateReportBug createReportBug = ; // CreateReportBug | 
final String accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    final response = api.reportBug(createReportBug, accountId);
    print(response);
} catch on DioError (e) {
    print('Exception when calling ReportsApi->reportBug: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createReportBug** | [**CreateReportBug**](CreateReportBug.md)|  | 
 **accountId** | **String**|  | [optional] 

### Return type

[**ReportBug**](ReportBug.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **reportTravel**
> ReportTravel reportTravel(createReportTravel)

Create travel report's

Create a travel's report

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getReportsApi();
final CreateReportTravel createReportTravel = ; // CreateReportTravel | 

try {
    final response = api.reportTravel(createReportTravel);
    print(response);
} catch on DioError (e) {
    print('Exception when calling ReportsApi->reportTravel: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createReportTravel** | [**CreateReportTravel**](CreateReportTravel.md)|  | 

### Return type

[**ReportTravel**](ReportTravel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

