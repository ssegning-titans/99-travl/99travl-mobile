# ntravl_api.api.ChatsApi

## Load the API package
```dart
import 'package:ntravl_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**archiveChat**](ChatsApi.md#archivechat) | **PUT** /api/v1/chats/{chatId}/status/archive | Archive chat
[**deleteChat**](ChatsApi.md#deletechat) | **DELETE** /api/v1/chats/{chatId} | Delete chat
[**getAllChats**](ChatsApi.md#getallchats) | **GET** /api/v1/chats/_/accounts/{accountId} | Get account&#39;s chat
[**getChat**](ChatsApi.md#getchat) | **GET** /api/v1/chats/{chatId} | Get chat


# **archiveChat**
> archiveChat(chatId)

Archive chat

Archive one chat's status

### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getChatsApi();
final String chatId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    api.archiveChat(chatId);
} catch on DioError (e) {
    print('Exception when calling ChatsApi->archiveChat: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **chatId** | **String**|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deleteChat**
> deleteChat(chatId)

Delete chat

Delete one chat by params

### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getChatsApi();
final String chatId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    api.deleteChat(chatId);
} catch on DioError (e) {
    print('Exception when calling ChatsApi->deleteChat: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **chatId** | **String**|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAllChats**
> BuiltList<Chat> getAllChats(accountId, status, size, page, sort)

Get account's chat

Get all chats for an account

### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getChatsApi();
final String accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final ChatStatus status = ; // ChatStatus | 
final int size = 56; // int | In the backend, this would always be defaulted at 20
final int page = 56; // int | In the backend, this would always be defaulted at 0
final BuiltList<String> sort = ; // BuiltList<String> | Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.

try {
    final response = api.getAllChats(accountId, status, size, page, sort);
    print(response);
} catch on DioError (e) {
    print('Exception when calling ChatsApi->getAllChats: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | **String**|  | 
 **status** | [**ChatStatus**](.md)|  | 
 **size** | **int**| In the backend, this would always be defaulted at 20 | [optional] [default to 20]
 **page** | **int**| In the backend, this would always be defaulted at 0 | [optional] 
 **sort** | [**BuiltList&lt;String&gt;**](String.md)| Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] [default to ListBuilder()]

### Return type

[**BuiltList&lt;Chat&gt;**](Chat.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getChat**
> Chat getChat(chatId)

Get chat

Get one chat by params

### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getChatsApi();
final String chatId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    final response = api.getChat(chatId);
    print(response);
} catch on DioError (e) {
    print('Exception when calling ChatsApi->getChat: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **chatId** | **String**|  | 

### Return type

[**Chat**](Chat.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

