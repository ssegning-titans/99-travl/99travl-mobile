# ntravl_api.model.Friend

## Load the model package
```dart
import 'package:ntravl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**creationDate** | **int** |  | 
**status** | [**FriendShipStatus**](FriendShipStatus.md) |  | [optional] 
**type** | [**FriendType**](FriendType.md) |  | [optional] 
**processType** | [**FriendProcessType**](FriendProcessType.md) |  | 
**accountId** | **String** |  | [optional] 
**authorId** | **String** |  | [optional] 
**isUserActive** | **bool** |  | [optional] 
**phoneNumber** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**firstName** | **String** |  | [optional] 
**lastName** | **String** |  | [optional] 
**avatar** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


