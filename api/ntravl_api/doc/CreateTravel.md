# ntravl_api.model.CreateTravel

## Load the model package
```dart
import 'package:ntravl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**passagePoints** | [**BuiltList&lt;TravelPoint&gt;**](TravelPoint.md) |  | 
**repeating** | [**TravelRepetition**](TravelRepetition.md) |  | [optional] 
**maxQuantity** | [**TravelQuantity**](TravelQuantity.md) |  | 
**transportMode** | [**TravelModeUnit**](TravelModeUnit.md) |  | [optional] 
**luggageDimension** | [**TravelLuggageDimension**](TravelLuggageDimension.md) |  | 
**description** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


