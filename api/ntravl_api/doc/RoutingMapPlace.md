# ntravl_api.model.RoutingMapPlace

## Load the model package
```dart
import 'package:ntravl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**placeId** | **String** |  | [optional] 
**latitude** | **double** |  | [optional] 
**longitude** | **double** |  | [optional] 
**formattedAddress** | **String** |  | [optional] 
**addressComponent** | [**BuiltList&lt;AddressComponent&gt;**](AddressComponent.md) |  | [optional] 
**order** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


