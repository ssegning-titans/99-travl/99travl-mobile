# ntravl_api.model.BaseChatMessage

## Load the model package
```dart
import 'package:ntravl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**createdAt** | **int** |  | [optional] 
**authorId** | **String** |  | 
**chatId** | **String** |  | 
**messageType** | [**ChatMessageType**](ChatMessageType.md) |  | 
**seen** | **bool** |  | [optional] [default to false]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


