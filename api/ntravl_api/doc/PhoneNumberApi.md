# ntravl_api.api.PhoneNumberApi

## Load the API package
```dart
import 'package:ntravl_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getTan**](PhoneNumberApi.md#gettan) | **POST** /api/v1/accounts/{id}/phone-number/tan | 
[**validateTan**](PhoneNumberApi.md#validatetan) | **PUT** /api/v1/accounts/{id}/phone-number/tan | 


# **getTan**
> PhoneTanResponse getTan(id, phoneTanRequest)



### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getPhoneNumberApi();
final String id = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final PhoneTanRequest phoneTanRequest = ; // PhoneTanRequest | 

try {
    final response = api.getTan(id, phoneTanRequest);
    print(response);
} catch on DioError (e) {
    print('Exception when calling PhoneNumberApi->getTan: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **phoneTanRequest** | [**PhoneTanRequest**](PhoneTanRequest.md)|  | 

### Return type

[**PhoneTanResponse**](PhoneTanResponse.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **validateTan**
> PhoneTanStatus validateTan(id, phoneTanValidateRequest)



### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getPhoneNumberApi();
final String id = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final PhoneTanValidateRequest phoneTanValidateRequest = ; // PhoneTanValidateRequest | 

try {
    final response = api.validateTan(id, phoneTanValidateRequest);
    print(response);
} catch on DioError (e) {
    print('Exception when calling PhoneNumberApi->validateTan: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **phoneTanValidateRequest** | [**PhoneTanValidateRequest**](PhoneTanValidateRequest.md)|  | 

### Return type

[**PhoneTanStatus**](PhoneTanStatus.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

