# ntravl_api.api.FriendsApi

## Load the API package
```dart
import 'package:ntravl_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createOfflineFriend**](FriendsApi.md#createofflinefriend) | **POST** /api/v1/accounts/friends/offline | 
[**createOnlineFriend**](FriendsApi.md#createonlinefriend) | **POST** /api/v1/accounts/friends/online | 
[**deleteFriendById**](FriendsApi.md#deletefriendbyid) | **DELETE** /api/v1/accounts/friends/{friendId} | 
[**getFriendById**](FriendsApi.md#getfriendbyid) | **GET** /api/v1/accounts/friends/{friendId} | 
[**getFriends**](FriendsApi.md#getfriends) | **GET** /api/v1/accounts/friends | 
[**updateFriend**](FriendsApi.md#updatefriend) | **PUT** /api/v1/accounts/friends/{friendId} | 
[**updateFriendStatus**](FriendsApi.md#updatefriendstatus) | **POST** /api/v1/accounts/friends/{friendId}/validate-online | 
[**validateFriendShip**](FriendsApi.md#validatefriendship) | **POST** /api/v1/accounts/friends/{friendId}/validate-offline | 


# **createOfflineFriend**
> OfflineFriend createOfflineFriend(requestOfflineFriendShip)



### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getFriendsApi();
final RequestOfflineFriendShip requestOfflineFriendShip = ; // RequestOfflineFriendShip | 

try {
    final response = api.createOfflineFriend(requestOfflineFriendShip);
    print(response);
} catch on DioError (e) {
    print('Exception when calling FriendsApi->createOfflineFriend: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestOfflineFriendShip** | [**RequestOfflineFriendShip**](RequestOfflineFriendShip.md)|  | [optional] 

### Return type

[**OfflineFriend**](OfflineFriend.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **createOnlineFriend**
> OnlineFriend createOnlineFriend(requestOnlineFriendShip)



### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getFriendsApi();
final RequestOnlineFriendShip requestOnlineFriendShip = ; // RequestOnlineFriendShip | 

try {
    final response = api.createOnlineFriend(requestOnlineFriendShip);
    print(response);
} catch on DioError (e) {
    print('Exception when calling FriendsApi->createOnlineFriend: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestOnlineFriendShip** | [**RequestOnlineFriendShip**](RequestOnlineFriendShip.md)|  | [optional] 

### Return type

[**OnlineFriend**](OnlineFriend.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deleteFriendById**
> deleteFriendById(friendId)



### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getFriendsApi();
final String friendId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    api.deleteFriendById(friendId);
} catch on DioError (e) {
    print('Exception when calling FriendsApi->deleteFriendById: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **friendId** | **String**|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getFriendById**
> Friend getFriendById(friendId)



### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getFriendsApi();
final String friendId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    final response = api.getFriendById(friendId);
    print(response);
} catch on DioError (e) {
    print('Exception when calling FriendsApi->getFriendById: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **friendId** | **String**|  | 

### Return type

[**Friend**](Friend.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getFriends**
> BuiltList<Friend> getFriends(type, status, size, page, sort)



### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getFriendsApi();
final FriendType type = ; // FriendType | 
final FriendShipStatus status = ; // FriendShipStatus | 
final int size = 56; // int | In the backend, this would always be defaulted at 20
final int page = 56; // int | In the backend, this would always be defaulted at 0
final BuiltList<String> sort = ; // BuiltList<String> | Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.

try {
    final response = api.getFriends(type, status, size, page, sort);
    print(response);
} catch on DioError (e) {
    print('Exception when calling FriendsApi->getFriends: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | [**FriendType**](.md)|  | [optional] 
 **status** | [**FriendShipStatus**](.md)|  | [optional] 
 **size** | **int**| In the backend, this would always be defaulted at 20 | [optional] [default to 20]
 **page** | **int**| In the backend, this would always be defaulted at 0 | [optional] 
 **sort** | [**BuiltList&lt;String&gt;**](String.md)| Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] [default to ListBuilder()]

### Return type

[**BuiltList&lt;Friend&gt;**](Friend.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateFriend**
> Friend updateFriend(friendId, updateFriend)



### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getFriendsApi();
final String friendId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final UpdateFriend updateFriend = ; // UpdateFriend | 

try {
    final response = api.updateFriend(friendId, updateFriend);
    print(response);
} catch on DioError (e) {
    print('Exception when calling FriendsApi->updateFriend: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **friendId** | **String**|  | 
 **updateFriend** | [**UpdateFriend**](UpdateFriend.md)|  | [optional] 

### Return type

[**Friend**](Friend.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateFriendStatus**
> Friend updateFriendStatus(friendId, updateFriendStatus)



The receiver which in this case uses the app is notified to accept the relationship

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getFriendsApi();
final String friendId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final UpdateFriendStatus updateFriendStatus = ; // UpdateFriendStatus | 

try {
    final response = api.updateFriendStatus(friendId, updateFriendStatus);
    print(response);
} catch on DioError (e) {
    print('Exception when calling FriendsApi->updateFriendStatus: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **friendId** | **String**|  | 
 **updateFriendStatus** | [**UpdateFriendStatus**](UpdateFriendStatus.md)|  | [optional] 

### Return type

[**Friend**](Friend.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **validateFriendShip**
> Friend validateFriendShip(friendId, validateFriendShipCode)



The requester is given the code which is send to the friend through offline process

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getFriendsApi();
final String friendId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final ValidateFriendShipCode validateFriendShipCode = ; // ValidateFriendShipCode | 

try {
    final response = api.validateFriendShip(friendId, validateFriendShipCode);
    print(response);
} catch on DioError (e) {
    print('Exception when calling FriendsApi->validateFriendShip: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **friendId** | **String**|  | 
 **validateFriendShipCode** | [**ValidateFriendShipCode**](ValidateFriendShipCode.md)|  | [optional] 

### Return type

[**Friend**](Friend.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

