# ntravl_api.model.AddressComponent

## Load the model package
```dart
import 'package:ntravl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**longName** | **String** |  | [optional] 
**shortName** | **String** |  | [optional] 
**types** | [**BuiltList&lt;PlaceType&gt;**](PlaceType.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


