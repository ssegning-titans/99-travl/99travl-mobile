# ntravl_api.model.Chat

## Load the model package
```dart
import 'package:ntravl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**createdAt** | **int** |  | [optional] 
**members** | **BuiltList&lt;String&gt;** |  | [optional] 
**travelId** | **String** |  | [optional] 
**status** | [**ChatStatus**](ChatStatus.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


