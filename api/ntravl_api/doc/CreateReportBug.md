# ntravl_api.model.CreateReportBug

## Load the model package
```dart
import 'package:ntravl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** |  | 
**description** | **String** |  | [optional] 
**deviceInfo** | **BuiltMap&lt;String, String&gt;** |  | 
**screenshot** | [**BuiltList&lt;BugMedia&gt;**](BugMedia.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


