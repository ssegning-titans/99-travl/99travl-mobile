# ntravl_api.model.TravelRepetition

## Load the model package
```dart
import 'package:ntravl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**startingRepetitionDate** | **int** |  | 
**endingRepetitionDate** | **int** |  | 
**days** | [**BuiltList&lt;TravelDays&gt;**](TravelDays.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


