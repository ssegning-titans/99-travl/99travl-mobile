# ntravl_api.model.Account

## Load the model package
```dart
import 'package:ntravl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdAt** | **int** |  | 
**updatedAt** | **int** |  | 
**id** | **String** |  | 
**firstName** | **String** |  | [optional] 
**lastName** | **String** |  | [optional] 
**locale** | **String** |  | [optional] 
**avatarUrl** | **String** |  | [optional] 
**email** | **String** |  | 
**emailVerified** | **bool** |  | [optional] 
**gender** | [**AccountGender**](AccountGender.md) |  | [optional] 
**status** | [**AccountStatus**](AccountStatus.md) |  | 
**phoneNumber** | **String** |  | [optional] 
**phoneNumberVerified** | **bool** |  | [optional] 
**bio** | **String** |  | [optional] 
**accountType** | [**AccountType**](AccountType.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


