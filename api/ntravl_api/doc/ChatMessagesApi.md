# ntravl_api.api.ChatMessagesApi

## Load the API package
```dart
import 'package:ntravl_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createContactChatMessage**](ChatMessagesApi.md#createcontactchatmessage) | **POST** /api/v1/messages/contacts | Create chat message
[**createMediaChatMessage**](ChatMessagesApi.md#createmediachatmessage) | **POST** /api/v1/messages/medias | Create chat message
[**createPropositionChatMessage**](ChatMessagesApi.md#createpropositionchatmessage) | **POST** /api/v1/messages/propositions | Create chat message
[**createTextChatMessage**](ChatMessagesApi.md#createtextchatmessage) | **POST** /api/v1/messages/texts | Create chat message
[**deleteOneChatMessages**](ChatMessagesApi.md#deleteonechatmessages) | **DELETE** /api/v1/messages/{messageId} | Delete chat message
[**getAllChatMessages**](ChatMessagesApi.md#getallchatmessages) | **GET** /api/v1/messages/_/chats/{chatId} | Get account&#39;s chat
[**getOneChatMessage**](ChatMessagesApi.md#getonechatmessage) | **GET** /api/v1/messages/{messageId} | Get account chat&#39;s message
[**markMessageAsRead**](ChatMessagesApi.md#markmessageasread) | **PUT** /api/v1/messages/{messageId}/read | Mark as read


# **createContactChatMessage**
> ChatMessage createContactChatMessage(contactChatMessage)

Create chat message

Create chat message

### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getChatMessagesApi();
final ContactChatMessage contactChatMessage = ; // ContactChatMessage | 

try {
    final response = api.createContactChatMessage(contactChatMessage);
    print(response);
} catch on DioError (e) {
    print('Exception when calling ChatMessagesApi->createContactChatMessage: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contactChatMessage** | [**ContactChatMessage**](ContactChatMessage.md)|  | 

### Return type

[**ChatMessage**](ChatMessage.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **createMediaChatMessage**
> ChatMessage createMediaChatMessage(mediaChatMessage)

Create chat message

Create chat message

### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getChatMessagesApi();
final MediaChatMessage mediaChatMessage = ; // MediaChatMessage | 

try {
    final response = api.createMediaChatMessage(mediaChatMessage);
    print(response);
} catch on DioError (e) {
    print('Exception when calling ChatMessagesApi->createMediaChatMessage: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mediaChatMessage** | [**MediaChatMessage**](MediaChatMessage.md)|  | 

### Return type

[**ChatMessage**](ChatMessage.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **createPropositionChatMessage**
> ChatMessage createPropositionChatMessage(propositionChatMessage)

Create chat message

Create chat message

### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getChatMessagesApi();
final PropositionChatMessage propositionChatMessage = ; // PropositionChatMessage | 

try {
    final response = api.createPropositionChatMessage(propositionChatMessage);
    print(response);
} catch on DioError (e) {
    print('Exception when calling ChatMessagesApi->createPropositionChatMessage: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **propositionChatMessage** | [**PropositionChatMessage**](PropositionChatMessage.md)|  | 

### Return type

[**ChatMessage**](ChatMessage.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **createTextChatMessage**
> ChatMessage createTextChatMessage(textChatMessage)

Create chat message

Create chat message

### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getChatMessagesApi();
final TextChatMessage textChatMessage = ; // TextChatMessage | 

try {
    final response = api.createTextChatMessage(textChatMessage);
    print(response);
} catch on DioError (e) {
    print('Exception when calling ChatMessagesApi->createTextChatMessage: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **textChatMessage** | [**TextChatMessage**](TextChatMessage.md)|  | 

### Return type

[**ChatMessage**](ChatMessage.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deleteOneChatMessages**
> deleteOneChatMessages(messageId)

Delete chat message

Find and delete one chat message by Id

### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getChatMessagesApi();
final String messageId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    api.deleteOneChatMessages(messageId);
} catch on DioError (e) {
    print('Exception when calling ChatMessagesApi->deleteOneChatMessages: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **messageId** | **String**|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAllChatMessages**
> BuiltList<ChatMessage> getAllChatMessages(chatId, size, page, sort)

Get account's chat

Get all chats for an account

### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getChatMessagesApi();
final String chatId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
final int size = 56; // int | In the backend, this would always be defaulted at 20
final int page = 56; // int | In the backend, this would always be defaulted at 0
final BuiltList<String> sort = ; // BuiltList<String> | Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.

try {
    final response = api.getAllChatMessages(chatId, size, page, sort);
    print(response);
} catch on DioError (e) {
    print('Exception when calling ChatMessagesApi->getAllChatMessages: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **chatId** | **String**|  | 
 **size** | **int**| In the backend, this would always be defaulted at 20 | [optional] [default to 20]
 **page** | **int**| In the backend, this would always be defaulted at 0 | [optional] 
 **sort** | [**BuiltList&lt;String&gt;**](String.md)| Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] [default to ListBuilder()]

### Return type

[**BuiltList&lt;ChatMessage&gt;**](ChatMessage.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getOneChatMessage**
> ChatMessage getOneChatMessage(messageId)

Get account chat's message

Get all chats for an account

### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getChatMessagesApi();
final String messageId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    final response = api.getOneChatMessage(messageId);
    print(response);
} catch on DioError (e) {
    print('Exception when calling ChatMessagesApi->getOneChatMessage: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **messageId** | **String**|  | 

### Return type

[**ChatMessage**](ChatMessage.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **markMessageAsRead**
> markMessageAsRead(messageId)

Mark as read

Mark as read

### Example
```dart
import 'package:ntravl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api = NtravlApi().getChatMessagesApi();
final String messageId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    api.markMessageAsRead(messageId);
} catch on DioError (e) {
    print('Exception when calling ChatMessagesApi->markMessageAsRead: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **messageId** | **String**|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

