# ntravl_api.model.ReportAccount

## Load the model package
```dart
import 'package:ntravl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reportType** | [**ReportType**](ReportType.md) |  | 
**authorId** | **String** |  | 
**status** | [**ReportStatus**](ReportStatus.md) |  | 
**createdAt** | **int** |  | [optional] 
**id** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**accountId** | **String** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


