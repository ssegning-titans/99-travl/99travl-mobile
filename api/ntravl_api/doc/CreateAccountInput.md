# ntravl_api.model.CreateAccountInput

## Load the model package
```dart
import 'package:ntravl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**firstName** | **String** |  | [optional] 
**lastName** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**phoneNumber** | **String** |  | [optional] 
**gender** | [**AccountGender**](AccountGender.md) |  | [optional] 
**bio** | **String** |  | [optional] 
**avatarUrl** | **String** |  | [optional] 
**accountType** | [**AccountType**](AccountType.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


