# ntravl_api.model.TravelProposition

## Load the model package
```dart
import 'package:ntravl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**authorId** | **String** |  | [optional] 
**travelId** | **String** |  | [optional] 
**message** | **String** |  | [optional] 
**startingPointId** | **String** |  | [optional] 
**endingPointId** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


