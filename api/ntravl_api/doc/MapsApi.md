# ntravl_api.api.MapsApi

## Load the API package
```dart
import 'package:ntravl_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**autocomplete**](MapsApi.md#autocomplete) | **GET** /api/v1/maps/autocomplete | 
[**findPlaces**](MapsApi.md#findplaces) | **GET** /api/v1/maps/by-formatted-name | Addresses by names
[**getAddressByCoordinate**](MapsApi.md#getaddressbycoordinate) | **GET** /api/v1/maps/by-coordinate | Address by coordinates
[**getAddressByPlaceId**](MapsApi.md#getaddressbyplaceid) | **GET** /api/v1/maps/by-place-id | Address by placeId
[**routing**](MapsApi.md#routing) | **GET** /api/v1/maps/routing | 
[**sessionToken**](MapsApi.md#sessiontoken) | **GET** /api/v1/maps/session-token | 


# **autocomplete**
> BuiltList<MapPlace> autocomplete(formattedAddress, acceptLanguage, sessionToken)



### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getMapsApi();
final String formattedAddress = formattedAddress_example; // String | 
final String acceptLanguage = acceptLanguage_example; // String | 
final String sessionToken = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try {
    final response = api.autocomplete(formattedAddress, acceptLanguage, sessionToken);
    print(response);
} catch on DioError (e) {
    print('Exception when calling MapsApi->autocomplete: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **formattedAddress** | **String**|  | 
 **acceptLanguage** | **String**|  | 
 **sessionToken** | **String**|  | 

### Return type

[**BuiltList&lt;MapPlace&gt;**](MapPlace.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **findPlaces**
> BuiltList<MapPlace> findPlaces(formattedAddress, acceptLanguage)

Addresses by names

Get address by place id

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getMapsApi();
final String formattedAddress = formattedAddress_example; // String | 
final String acceptLanguage = acceptLanguage_example; // String | 

try {
    final response = api.findPlaces(formattedAddress, acceptLanguage);
    print(response);
} catch on DioError (e) {
    print('Exception when calling MapsApi->findPlaces: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **formattedAddress** | **String**|  | 
 **acceptLanguage** | **String**|  | 

### Return type

[**BuiltList&lt;MapPlace&gt;**](MapPlace.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAddressByCoordinate**
> MapPlace getAddressByCoordinate(latitude, longitude, acceptLanguage)

Address by coordinates

Get address by coordinates

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getMapsApi();
final double latitude = 1.2; // double | 
final double longitude = 1.2; // double | 
final String acceptLanguage = acceptLanguage_example; // String | 

try {
    final response = api.getAddressByCoordinate(latitude, longitude, acceptLanguage);
    print(response);
} catch on DioError (e) {
    print('Exception when calling MapsApi->getAddressByCoordinate: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **latitude** | **double**|  | [default to 10]
 **longitude** | **double**|  | [default to 10]
 **acceptLanguage** | **String**|  | 

### Return type

[**MapPlace**](MapPlace.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAddressByPlaceId**
> MapPlace getAddressByPlaceId(placeId, acceptLanguage)

Address by placeId

Get address by place id

### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getMapsApi();
final String placeId = placeId_example; // String | 
final String acceptLanguage = acceptLanguage_example; // String | 

try {
    final response = api.getAddressByPlaceId(placeId, acceptLanguage);
    print(response);
} catch on DioError (e) {
    print('Exception when calling MapsApi->getAddressByPlaceId: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **placeId** | **String**|  | 
 **acceptLanguage** | **String**|  | 

### Return type

[**MapPlace**](MapPlace.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **routing**
> BuiltList<RoutingDirection> routing(startCoord, endCoord, acceptLanguage)



### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getMapsApi();
final BuiltList<double> startCoord = ; // BuiltList<double> | The first position in the array is the lat, the second is the lng. In the future, the third will be the altitude
final BuiltList<double> endCoord = ; // BuiltList<double> | The first position in the array is the lat, the second is the lng. In the future, the third will be the altitude
final String acceptLanguage = acceptLanguage_example; // String | 

try {
    final response = api.routing(startCoord, endCoord, acceptLanguage);
    print(response);
} catch on DioError (e) {
    print('Exception when calling MapsApi->routing: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startCoord** | [**BuiltList&lt;double&gt;**](double.md)| The first position in the array is the lat, the second is the lng. In the future, the third will be the altitude | 
 **endCoord** | [**BuiltList&lt;double&gt;**](double.md)| The first position in the array is the lat, the second is the lng. In the future, the third will be the altitude | 
 **acceptLanguage** | **String**|  | 

### Return type

[**BuiltList&lt;RoutingDirection&gt;**](RoutingDirection.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **sessionToken**
> MapsSession sessionToken()



### Example
```dart
import 'package:ntravl_api/api.dart';

final api = NtravlApi().getMapsApi();

try {
    final response = api.sessionToken();
    print(response);
} catch on DioError (e) {
    print('Exception when calling MapsApi->sessionToken: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**MapsSession**](MapsSession.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

