import 'package:test/test.dart';
import 'package:ntravl_api/ntravl_api.dart';


/// tests for ReportsApi
void main() {
  final instance = NtravlApi().getReportsApi();

  group(ReportsApi, () {
    // Get Report's bug
    //
    // Get all report
    //
    //Future<BuiltList<ReportBug>> getAllBugReports({ String accountId, int size, int page, BuiltList<String> sort }) async
    test('test getAllBugReports', () async {
      // TODO
    });

    // Get Report's account
    //
    // Get all report for a account
    //
    //Future<BuiltList<ReportAccount>> getAllReportForAccount(String accountId, { int size, int page, BuiltList<String> sort }) async
    test('test getAllReportForAccount', () async {
      // TODO
    });

    // Get Report's Travel
    //
    // Get all report for a travel
    //
    //Future<BuiltList<ReportTravel>> getAllReportForTravel(String travelId, { int size, int page, BuiltList<String> sort }) async
    test('test getAllReportForTravel', () async {
      // TODO
    });

    // Get report
    //
    // Get all report for an account
    //
    //Future<BuiltList<BaseReport>> getAllReports(ReportStatus status, { int size, int page, BuiltList<String> sort }) async
    test('test getAllReports', () async {
      // TODO
    });

    // Get owner's report
    //
    // Get all report for a account
    //
    //Future<BuiltList<BaseReport>> getOwnersReports(String ownerId, ReportStatus status, { int size, int page, BuiltList<String> sort }) async
    test('test getOwnersReports', () async {
      // TODO
    });

    // Get one report
    //
    // Use the ID to get one single report
    //
    //Future<BaseReport> getReportById(String reportId) async
    test('test getReportById', () async {
      // TODO
    });

    // Create account report's
    //
    // Create a account's report
    //
    //Future<ReportAccount> reportAccount(String accountId, CreateReportAccount createReportAccount) async
    test('test reportAccount', () async {
      // TODO
    });

    // Create bug report's
    //
    // Create a bug's report
    //
    //Future<ReportBug> reportBug(CreateReportBug createReportBug, { String accountId }) async
    test('test reportBug', () async {
      // TODO
    });

    // Create travel report's
    //
    // Create a travel's report
    //
    //Future<ReportTravel> reportTravel(String travelId, CreateReportTravel createReportTravel) async
    test('test reportTravel', () async {
      // TODO
    });

  });
}
