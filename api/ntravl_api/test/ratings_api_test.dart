import 'package:test/test.dart';
import 'package:ntravl_api/ntravl_api.dart';


/// tests for RatingsApi
void main() {
  final instance = NtravlApi().getRatingsApi();

  group(RatingsApi, () {
    // Create person rating
    //
    // Create a person's rating
    //
    //Future<BaseRating> createPersonRating(RatingAccount ratingAccount) async
    test('test createPersonRating', () async {
      // TODO
    });

    // Create travel rating
    //
    // Create a travel's rating
    //
    //Future<BaseRating> createTravelRating(RatingTravel ratingTravel) async
    test('test createTravelRating', () async {
      // TODO
    });

    // Delete person rating
    //
    // Get a person's rating
    //
    //Future deletePersonRating(String personId) async
    test('test deletePersonRating', () async {
      // TODO
    });

    // Delete travel rating
    //
    // Get a travel's rating
    //
    //Future deleteTravelRating(String travelId) async
    test('test deleteTravelRating', () async {
      // TODO
    });

    // Get person average's ratings
    //
    // Get person average's ratings
    //
    //Future<AverageRatingPerson> getAveragePersonRating(String personId) async
    test('test getAveragePersonRating', () async {
      // TODO
    });

    // Get travel average's ratings
    //
    // Get travel average's ratings
    //
    //Future<AverageRatingTravel> getAverageTravelRating(String travelId) async
    test('test getAverageTravelRating', () async {
      // TODO
    });

    // Get person rating
    //
    // Get a person's rating by the current account
    //
    //Future<RatingAccount> getCurrentAccountPersonRating(String personId) async
    test('test getCurrentAccountPersonRating', () async {
      // TODO
    });

    // Get travel rating
    //
    // Get a travel's rating by the current account
    //
    //Future<RatingTravel> getCurrentAccountTravelRating(String travelId) async
    test('test getCurrentAccountTravelRating', () async {
      // TODO
    });

    // Get owner's rating
    //
    // Get owner's account all ratings
    //
    //Future<BuiltList<BaseRating>> getOwnerRatings(String accountId, { int size, int page, BuiltList<String> sort }) async
    test('test getOwnerRatings', () async {
      // TODO
    });

    // Get person ratings
    //
    // Get all person's ratings
    //
    //Future<BuiltList<RatingAccount>> listPersonRating(String personId, { int size, int page, BuiltList<String> sort }) async
    test('test listPersonRating', () async {
      // TODO
    });

    // Get travel ratings
    //
    // Get all travel's ratings
    //
    //Future<BuiltList<RatingTravel>> listTravelRating(String travelId, { int size, int page, BuiltList<String> sort }) async
    test('test listTravelRating', () async {
      // TODO
    });

  });
}
