import 'package:test/test.dart';
import 'package:ntravl_api/ntravl_api.dart';


/// tests for ReservationsApi
void main() {
  final instance = NtravlApi().getReservationsApi();

  group(ReservationsApi, () {
    // Count travel's reservation
    //
    // Count all travel's reservations
    //
    //Future<TravelReservationCount> countTravelReservation(String travelId) async
    test('test countTravelReservation', () async {
      // TODO
    });

    // Create Travel Reservation
    //
    //Future<TravelReservation> createTravelReservation(String travelId, TravelReservation travelReservation) async
    test('test createTravelReservation', () async {
      // TODO
    });

    // Delete travel
    //
    // Delete one travel
    //
    //Future deleteReservationById(String travelId, String reservationId) async
    test('test deleteReservationById', () async {
      // TODO
    });

    // Get travel
    //
    // Get one travel
    //
    //Future<TravelReservation> getReservationById(String travelId, String reservationId) async
    test('test getReservationById', () async {
      // TODO
    });

    // Get Reservation for travel
    //
    //Future<BuiltList<TravelReservation>> getReservationForTravel(String travelId, { int size, int page, BuiltList<String> sort }) async
    test('test getReservationForTravel', () async {
      // TODO
    });

    // Update travel
    //
    // Update one travel
    //
    //Future<TravelReservation> updateReservationById(String travelId, TravelReservation travelReservation) async
    test('test updateReservationById', () async {
      // TODO
    });

  });
}
