import 'package:test/test.dart';
import 'package:ntravl_api/ntravl_api.dart';


/// tests for ChatsApi
void main() {
  final instance = NtravlApi().getChatsApi();

  group(ChatsApi, () {
    // Archive chat
    //
    // Archive one chat's status
    //
    //Future archiveChat(String chatId) async
    test('test archiveChat', () async {
      // TODO
    });

    // Delete chat
    //
    // Delete one chat by params
    //
    //Future deleteChat(String chatId) async
    test('test deleteChat', () async {
      // TODO
    });

    // Get account's chat
    //
    // Get all chats for an account
    //
    //Future<BuiltList<Chat>> getAllChats(String accountId, ChatStatus status, { int size, int page, BuiltList<String> sort }) async
    test('test getAllChats', () async {
      // TODO
    });

    // Get chat
    //
    // Get one chat by params
    //
    //Future<Chat> getChat(String chatId) async
    test('test getChat', () async {
      // TODO
    });

  });
}
