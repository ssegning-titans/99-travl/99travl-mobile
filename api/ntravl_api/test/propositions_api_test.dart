import 'package:test/test.dart';
import 'package:ntravl_api/ntravl_api.dart';


/// tests for PropositionsApi
void main() {
  final instance = NtravlApi().getPropositionsApi();

  group(PropositionsApi, () {
    // Get one travel proposition
    //
    // Get one travel proposition by id
    //
    //Future<TravelProposition> getOneTravelProposition(String propositionId) async
    test('test getOneTravelProposition', () async {
      // TODO
    });

    // Get travel propositions
    //
    // Get all travel's propositions
    //
    //Future<BuiltList<TravelProposition>> getTravelPropositions(String travelId, { int size, int page, BuiltList<String> sort }) async
    test('test getTravelPropositions', () async {
      // TODO
    });

    // Update proposition
    //
    // Update one travel proposition by id
    //
    //Future<TravelProposition> updateTravelProposition(String propositionId, { TravelProposition travelProposition }) async
    test('test updateTravelProposition', () async {
      // TODO
    });

  });
}
