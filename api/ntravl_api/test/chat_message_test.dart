import 'package:test/test.dart';
import 'package:ntravl_api/ntravl_api.dart';

// tests for ChatMessage
void main() {
  final instance = ChatMessageBuilder();
  // TODO add properties to the builder and call build()

  group(ChatMessage, () {
    // String id
    test('to test the property `id`', () async {
      // TODO
    });

    // int createdAt
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // String authorId
    test('to test the property `authorId`', () async {
      // TODO
    });

    // String chatId
    test('to test the property `chatId`', () async {
      // TODO
    });

    // ChatMessageType messageType
    test('to test the property `messageType`', () async {
      // TODO
    });

    // bool seen (default value: false)
    test('to test the property `seen`', () async {
      // TODO
    });

    // BuiltSet<ChatMedia> medias
    test('to test the property `medias`', () async {
      // TODO
    });

    // TravelProposition proposition
    test('to test the property `proposition`', () async {
      // TODO
    });

    // String message
    test('to test the property `message`', () async {
      // TODO
    });

    // BuiltSet<ChatContact> contacts
    test('to test the property `contacts`', () async {
      // TODO
    });

  });
}
