import 'package:test/test.dart';
import 'package:ntravl_api/ntravl_api.dart';


/// tests for MapsApi
void main() {
  final instance = NtravlApi().getMapsApi();

  group(MapsApi, () {
    //Future<BuiltList<MapPlace>> autocomplete(String formattedAddress, String acceptLanguage, String sessionToken) async
    test('test autocomplete', () async {
      // TODO
    });

    // Addresses by names
    //
    // Get address by place id
    //
    //Future<BuiltList<MapPlace>> findPlaces(String formattedAddress, String acceptLanguage) async
    test('test findPlaces', () async {
      // TODO
    });

    // Address by coordinates
    //
    // Get address by coordinates
    //
    //Future<MapPlace> getAddressByCoordinate(double latitude, double longitude, String acceptLanguage) async
    test('test getAddressByCoordinate', () async {
      // TODO
    });

    // Address by placeId
    //
    // Get address by place id
    //
    //Future<MapPlace> getAddressByPlaceId(String placeId, String acceptLanguage) async
    test('test getAddressByPlaceId', () async {
      // TODO
    });

    //Future<BuiltList<RoutingDirection>> routing(BuiltList<double> startCoord, BuiltList<double> endCoord, String acceptLanguage) async
    test('test routing', () async {
      // TODO
    });

    //Future<MapsSession> sessionToken() async
    test('test sessionToken', () async {
      // TODO
    });

  });
}
