import 'package:test/test.dart';
import 'package:ntravl_api/ntravl_api.dart';

// tests for TravelPoint
void main() {
  final instance = TravelPointBuilder();
  // TODO add properties to the builder and call build()

  group(TravelPoint, () {
    // String id
    test('to test the property `id`', () async {
      // TODO
    });

    // int priority (default value: 1)
    test('to test the property `priority`', () async {
      // TODO
    });

    // int creationDate
    test('to test the property `creationDate`', () async {
      // TODO
    });

    // int passageTime
    test('to test the property `passageTime`', () async {
      // TODO
    });

    // String travelId
    test('to test the property `travelId`', () async {
      // TODO
    });

    // String region
    test('to test the property `region`', () async {
      // TODO
    });

    // String formattedName
    test('to test the property `formattedName`', () async {
      // TODO
    });

    // String street
    test('to test the property `street`', () async {
      // TODO
    });

    // String houseNumber
    test('to test the property `houseNumber`', () async {
      // TODO
    });

    // String city
    test('to test the property `city`', () async {
      // TODO
    });

    // String zip
    test('to test the property `zip`', () async {
      // TODO
    });

    // String country
    test('to test the property `country`', () async {
      // TODO
    });

    // LatLng location
    test('to test the property `location`', () async {
      // TODO
    });

  });
}
