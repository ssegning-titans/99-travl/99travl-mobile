import 'package:test/test.dart';
import 'package:ntravl_api/ntravl_api.dart';


/// tests for FavoritesApi
void main() {
  final instance = NtravlApi().getFavoritesApi();

  group(FavoritesApi, () {
    // Create person favorite
    //
    // Create a person's favorite
    //
    //Future<FavoriteAccount> createFavoriteAccount(String accountId, String personId) async
    test('test createFavoriteAccount', () async {
      // TODO
    });

    // Create travel favorite
    //
    // Create a travel's favorite
    //
    //Future<FavoriteTravel> createTravelFavorite(String accountId, String travelId) async
    test('test createTravelFavorite', () async {
      // TODO
    });

    // Delete account travel
    //
    // Get a account's favorite
    //
    //Future deleteFavoriteAccount(String accountId, String personId) async
    test('test deleteFavoriteAccount', () async {
      // TODO
    });

    // Delete favorite travel
    //
    // Get a travel's favorite
    //
    //Future deleteTravelFavorite(String accountId, String travelId) async
    test('test deleteTravelFavorite', () async {
      // TODO
    });

    // Get account favorite
    //
    // Get a account's favorite
    //
    //Future<FavoriteAccount> getFavoriteAccount(String accountId, String personId) async
    test('test getFavoriteAccount', () async {
      // TODO
    });

    // Get owner's favorite
    //
    // Get owner's account all favorites
    //
    //Future<BuiltList<Favorite>> getOwnerFavorites(String accountId, { int size, int page, BuiltList<String> sort }) async
    test('test getOwnerFavorites', () async {
      // TODO
    });

    // Get travel favorite
    //
    // Get a travel's favorite
    //
    //Future<FavoriteTravel> getTravelFavorite(String accountId, String travelId) async
    test('test getTravelFavorite', () async {
      // TODO
    });

  });
}
