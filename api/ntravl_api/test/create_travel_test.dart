import 'package:test/test.dart';
import 'package:ntravl_api/ntravl_api.dart';

// tests for CreateTravel
void main() {
  final instance = CreateTravelBuilder();
  // TODO add properties to the builder and call build()

  group(CreateTravel, () {
    // BuiltList<TravelPoint> passagePoints
    test('to test the property `passagePoints`', () async {
      // TODO
    });

    // TravelRepetition repeating
    test('to test the property `repeating`', () async {
      // TODO
    });

    // TravelQuantity maxQuantity
    test('to test the property `maxQuantity`', () async {
      // TODO
    });

    // TravelModeUnit transportMode
    test('to test the property `transportMode`', () async {
      // TODO
    });

    // TravelLuggageDimension luggageDimension
    test('to test the property `luggageDimension`', () async {
      // TODO
    });

    // String description
    test('to test the property `description`', () async {
      // TODO
    });

  });
}
