import 'package:test/test.dart';
import 'package:ntravl_api/ntravl_api.dart';

// tests for MediaChatMessage
void main() {
  final instance = MediaChatMessageBuilder();
  // TODO add properties to the builder and call build()

  group(MediaChatMessage, () {
    // String id
    test('to test the property `id`', () async {
      // TODO
    });

    // int createdAt
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // String authorId
    test('to test the property `authorId`', () async {
      // TODO
    });

    // String chatId
    test('to test the property `chatId`', () async {
      // TODO
    });

    // ChatMessageType messageType
    test('to test the property `messageType`', () async {
      // TODO
    });

    // bool seen (default value: false)
    test('to test the property `seen`', () async {
      // TODO
    });

    // BuiltSet<ChatMedia> medias
    test('to test the property `medias`', () async {
      // TODO
    });

  });
}
