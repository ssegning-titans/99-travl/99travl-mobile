part of '../../main.dart';

class WelcomeLocation extends BeamLocation {
  WelcomeLocation({BeamState? state}) : super(state);

  @override
  List<String> get pathBlueprints => [
        '/welcome',
      ];

  @override
  List<BeamPage> buildPages(BuildContext context, BeamState state) => [
        AppBeamPage(
          key: ValueKey('welcome'),
          child: WelcomePage(),
        ),
      ];
}
