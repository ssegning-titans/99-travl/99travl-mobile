part of '../../main.dart';

class AuthLocation extends BeamLocation {
  AuthLocation({BeamState? state}) : super(state);

  @override
  List<String> get pathBlueprints => ['/auth'];

  @override
  List<BeamPage> buildPages(BuildContext context, BeamState state) => [
        AppBeamPage(
          key: ValueKey('auth'),
          child: AuthPage(),
        ),
      ];
}
