part of '../../main.dart';

class MainLocation extends BeamLocation {
  MainLocation({BeamState? state}) : super(state);

  @override
  List<String> get pathBlueprints => [
        '/main/home',
        '/main/add_travel',
        '/main/friends',
        '/main/account_options/phone_number',
        '/main/account_options/password',
        '/main/account_options/payment/setup_stripe',
        '/main/notification_options',
        '/main/privacy_options',
        '/main/appearance_options',
        '/main/chat_options',
        '/main/support_options',
        '/main/search_result/:travelId',
      ];

  @override
  List<BeamPage> buildPages(BuildContext context, BeamState state) => [
        AppBeamPage(
          key: ValueKey('main'),
          child: MainPage(
              tab: state.queryParameters.containsKey('t')
                  ? int.parse(state.queryParameters['t'] as String)
                  : null),
        ),
        if (state.uri.pathSegments.contains('add_travel'))
          AppBeamPage(
            key: ValueKey('add_travel'),
            child: const AddTravelPage(),
            animationType: BeamAnimationType.zAxis,
          ),
        if (state.uri.pathSegments.contains('search_result'))
          AppBeamPage(
            key: ValueKey('search_result'),
            child: SearchResultPage(
              searchModel: state.data['search_model'],
            ),
            animationType: BeamAnimationType.zAxis,
          ),
        if (state.uri.pathSegments.contains('friends'))
          AppBeamPage(
            key: ValueKey('friends'),
            child: FriendsPage(),
          ),
        if (state.uri.pathSegments.contains('account_options'))
          AppBeamPage(
            key: ValueKey('account_options'),
            child: AccountScreen(),
          ),
        if (state.uri.pathSegments.contains('phone_number'))
          AppBeamPage(
            key: ValueKey('phone_number'),
            child: PhoneNumberScreen(),
          ),
        if (state.uri.pathSegments.contains('notification_options'))
          AppBeamPage(
            key: ValueKey('notification_options'),
            child: NotificationSettingScreen(),
          ),
        if (state.uri.pathSegments.contains('privacy_options'))
          AppBeamPage(
            key: ValueKey('privacy_options'),
            child: PrivacyScreen(),
          ),
        if (state.uri.pathSegments.contains('appearance_options'))
          AppBeamPage(
            key: ValueKey('appearance_options'),
            child: AppearanceScreen(),
          ),
        if (state.uri.pathSegments.contains('chat_options'))
          AppBeamPage(
            key: ValueKey('chat_options'),
            child: ChatSettingScreen(),
          ),
        if (state.uri.pathSegments.contains('support_options'))
          AppBeamPage(
            key: ValueKey('support_options'),
            child: SupportScreen(),
          ),
      ];
}
