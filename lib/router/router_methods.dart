part of ntravl;

void gotoSearchResult(
  BuildContext context, {
  required MapPlace startingPoint,
  required MapPlace endingPoint,
  required DateTimeRange range,
}) {
  final model = SearchModel(
    startRadius: 30,
    endRadius: 30,
    range: range,
    start: SearchPoint(
      name: startingPoint.formattedAddress!,
      latitude: startingPoint.latitude!,
      longitude: startingPoint.longitude!,
    ),
    end: SearchPoint(
      name: endingPoint.formattedAddress!,
      latitude: endingPoint.latitude!,
      longitude: endingPoint.longitude!,
    ),
  );

  Beamer.of(context).beamToNamed(
    '/main/search_result',
    data: {
      'search_model': model,
    },
  );
}
