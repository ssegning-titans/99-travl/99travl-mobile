part of ntravl;

Future<MapPlace?> pickAddress(BuildContext context) =>
    showMaterialModalBottomSheet<MapPlace>(
      context: context,
      enableDrag: false,
      useRootNavigator: true,
      builder: (_) {
        final mapsApi = ApiProvider.of(context).mapsApi;
        return Material(
          child: SelectPlaceModal(
            mapsApi: mapsApi,
          ),
        );
      },
    );

Future<dynamic> pickDate(
  BuildContext context, {
  bool multiple = false,
}) =>
    showMaterialModalBottomSheet<dynamic>(
      context: context,
      enableDrag: false,
      useRootNavigator: true,
      builder: (_) => Material(child: SelectDateModal(multiple: multiple)),
    );

Future<Contact?> pickContact(BuildContext context) =>
    showMaterialModalBottomSheet<Contact>(
      context: context,
      enableDrag: false,
      useRootNavigator: true,
      builder: (_) => Material(child: SelectContactModal()),
    );

Future<SearchModel?> searchOptions(BuildContext context,
        {required SearchModel initialValue}) =>
    showMaterialModalBottomSheet<SearchModel>(
      context: context,
      enableDrag: false,
      useRootNavigator: true,
      builder: (_) => Material(
        child: SearchOptionModal(initialValue: initialValue),
      ),
    );
