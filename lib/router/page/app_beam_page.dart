part of '../../main.dart';

enum BeamAnimationType {
  swipe,
  zAxis,
  none,
}

class AppBeamPage extends BeamPage {
  final BeamAnimationType animationType;

  AppBeamPage({
    required LocalKey key,
    required Widget child,
    String? name,
    bool keepQueryOnPop = false,
    this.animationType = BeamAnimationType.swipe,
  }) : super(
          key: key,
          name: name,
          child: child,
          keepQueryOnPop: keepQueryOnPop,
        );

  @override
  Route createRoute(BuildContext context) {
    switch(animationType) {

      case BeamAnimationType.swipe:
        return SwipeablePageRoute(
          settings: this,
          builder: (_) => child,
        );
      case BeamAnimationType.zAxis:
        return SharedAxisPageRoute(
          page: child,
          transitionType: SharedAxisTransitionType.scaled,
          settings: this,
        );
      case BeamAnimationType.none:
        return PageRouteBuilder(
          settings: this,
          pageBuilder: (context, animation, secondaryAnimation) => child,
        );
    }
  }
}

class SharedAxisPageRoute extends PageRouteBuilder<Object> {
  SharedAxisPageRoute({
    required Widget page,
    required SharedAxisTransitionType transitionType,
    required RouteSettings? settings,
  }) : super(
    pageBuilder: (context, primaryAnimation, secondaryAnimation) => page,
    settings: settings,
    transitionsBuilder: (
        context,
        primaryAnimation,
        secondaryAnimation,
        child,
        ) {
      return SharedAxisTransition(
        animation: primaryAnimation,
        secondaryAnimation: secondaryAnimation,
        transitionType: transitionType,
        child: child,
      );
    },
  );
}
