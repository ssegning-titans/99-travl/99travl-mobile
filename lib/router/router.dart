part of ntravl;

final _routerDelegate = BeamerDelegate(
  initialPath: '/main',
  locationBuilder: BeamerLocationBuilder(
    beamLocations: [
      MainLocation(),
      WelcomeLocation(),
      AuthLocation(),
    ],
  ),
  notFoundPage: AppBeamPage(
    key: ValueKey('not_found'),
    child: NotFoundPage(),
  ),
  guards: [
    BeamGuard(
      pathBlueprints: ['/main', '/main/*'],
      check: (context, location) => AuthProvider.of(context).isAuth,
      beamToNamed: '/auth',
    ),
    BeamGuard(
      pathBlueprints: ['/main', '/auth', '/main/*', '/auth/*'],
      check: (context, location) => WelcomeProvider.of(context).hasSeenWelcome,
      beamToNamed: '/welcome',
    ),
  ],
);

final _backButtonDispatcher =
    BeamerBackButtonDispatcher(delegate: _routerDelegate);
