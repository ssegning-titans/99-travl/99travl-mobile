part of ntravl;

class Debouncer {
  final int milliseconds;
  Timer? _timer;
  VoidCallback? action;

  Debouncer({
    required this.milliseconds,
    this.action,
  });

  void run(VoidCallback? action) {
    if (_timer != null) {
      _timer!.cancel();
    }

    assert(this.action != null || action != null);

    _timer = Timer(
      Duration(milliseconds: milliseconds),
      action != null ? action : this.action!,
    );
  }
}
