part of ntravl;

void showScaffoldError(
  BuildContext context, {
  required String title,
  String? subtitle,
}) {
  final color = Colors.white;
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    content: ListTile(
      title: AppText(
        text: title,
        paddingHorizontal: 0,
        paddingVertical: 0,
        fontSize: 16,
        color: color,
      ),
      subtitle: subtitle != null
          ? AppText(
              text: subtitle,
              paddingHorizontal: 0,
              paddingVertical: 0,
              fontSize: 12,
              color: color,
            )
          : null,
      trailing: IconButton(
        onPressed: () {
          ScaffoldMessenger.maybeOf(context)?.clearSnackBars();
        },
        icon: Icon(
          Icons.close_outlined,
          color: color,
        ),
      ),
    ),
    backgroundColor: Colors.deepOrangeAccent,
  ));
}
