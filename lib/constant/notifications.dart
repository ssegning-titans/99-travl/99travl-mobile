part of ntravl;

// Declared as global, outside of any class
Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await Firebase.initializeApp();

  print("Handling a background message: ${message.messageId}");

  // Use this method to automatically convert the push data, in case you gonna use our data standard
  AwesomeNotifications().createNotificationFromJsonData(message.data);
}

void _initNotif() {
  final notif = AwesomeNotifications();
  notif.initialize(
    // set the icon to null if you want to use the default app icon
    'resource://drawable/res_app_icon',
    [
      NotificationChannel(
        channelKey: 'travel_channel',
        channelName: 'Travel notifications',
        channelDescription: 'Notification channel for Travels',
        defaultColor: AppThemeData.normalTheme.primaryColor,
        ledColor: Colors.white,
      ),
    ],
  );

}
