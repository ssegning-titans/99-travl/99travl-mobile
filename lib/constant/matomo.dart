part of ntravl;

Future<void> initMatomo() async {
  await MatomoTracker().initialize(
    siteId: EnvironmentConfig.matomoSiteId.toInt(),
    url: EnvironmentConfig.matomoUrl,
  );
}
