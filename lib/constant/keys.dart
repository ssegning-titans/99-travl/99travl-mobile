part of ntravl;

const kBoxPreviousSuggestionKey = 'previous_suggestion';
const kBoxPreviousSearchKey = 'previous_search';

const kBoxThemeKey = 'theme';
const kThemeModeKey = 'theme_mode';

const kSecureTokenKey = 'access_token';
const kSeenWelcomeKey = 'seen_welcome';
const kBoxWelcomeKey = 'welcome';

const kBoxHeaderKey = 'xHeader';

const kBoxSavedSearchKey = 'searches';
const kSavedSearchKey = 'saved_search';

final df = DateFormat('MMMM dd');
