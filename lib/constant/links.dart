part of ntravl;

void openUrl(String url) async {
  await canLaunch(url)
      ? await launch(url)
      : throw Exception('Could not launch $url');
}

void openPrivacyPolicy() {
  final Uri uri = Uri(
    scheme: 'https',
    host: '99travl.com',
    path: 'resources/privacy-policy',
    queryParameters: {
      'visitorId': MatomoTracker().visitor.id,
    },
  );

  openUrl(uri.toString());
}

void openTermsOfUse() {
  final Uri uri = Uri(
    scheme: 'https',
    host: '99travl.com',
    path: 'resources/terms-of-use',
    queryParameters: {
      'visitorId': MatomoTracker().visitor.id,
    },
  );

  openUrl(uri.toString());
}

