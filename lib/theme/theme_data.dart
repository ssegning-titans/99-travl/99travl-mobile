part of ntravl;

class AppThemeData {
  static ThemeData normalTheme = ThemeData(
    primarySwatch: Colors.purple,
    fontFamily: 'Louis George Cafe',
    brightness: Brightness.light,
    buttonTheme: ButtonThemeData(height: 64.0),
  );

  static ThemeData darkTheme = ThemeData(
    primarySwatch: Colors.purple,
    primaryColor: Colors.purple,
    primaryColorLight: Colors.purple[100],
    primaryColorDark: Colors.purple[700],
    accentColor: Colors.purple[500],
    toggleableActiveColor: Colors.purple[600],
    fontFamily: 'Louis George Cafe',
    brightness: Brightness.dark,
  );

}
