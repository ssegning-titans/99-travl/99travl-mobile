part of ntravl;

class MainPage extends TraceableStatefulWidget {
  final int? tab;

  @override
  State<StatefulWidget> createState() {
    return _MainPageState();
  }

  const MainPage({this.tab = 1});
}

class _MainPageState extends State<MainPage> {
  late int _currentIndex;

  int get stackIdx {
    switch (_currentIndex) {
      case 0:
      case 1:
        return _currentIndex;
      case 3:
      case 4:
        return _currentIndex - 1;
      default:
        throw Exception();
    }
  }


  @override
  void initState() {
    super.initState();
    _currentIndex = widget.tab ?? 1;
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      BoxConstraints(
          maxWidth: MediaQuery.of(context).size.width,
          maxHeight: MediaQuery.of(context).size.height),
      designSize: Size(360, 690),
      orientation: Orientation.portrait,
    );

    return Scaffold(
      body: IndexedStack(
        index: stackIdx,
        children: const [
          HomePage(),
          SearchPage(),
          ChatPage(),
          ProfilePage(),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _currentIndex,
        selectedFontSize: 12,
        unselectedFontSize: 12,
        items: [
          BottomNavigationBarItem(
            icon: Icon(_currentIndex == 0 ? Icons.home : Icons.home_outlined),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon:
                Icon(_currentIndex == 1 ? Icons.search : Icons.search_outlined),
            label: 'Search',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.add),
            label: 'Propose',
          ),
          BottomNavigationBarItem(
            icon: Icon(_currentIndex == 3
                ? Icons.message
                : Icons.message_outlined),
            label: 'Chats',
          ),
          BottomNavigationBarItem(
            icon:
                Icon(_currentIndex == 4 ? Icons.person : Icons.person_outline),
            label: 'Options',
          ),
        ],
        onTap: (idx) {
          if (idx != _currentIndex) {
            switch (idx) {
              case 0:
              case 1:
              case 3:
              case 4:
                _chooseIdx(idx);
                break;
              case 2:
                _goToNew(context);
                break;
            }
          }
        },
      ),
    );
  }

  void _chooseIdx(int idx) {
    setState(() => _currentIndex = idx);
  }

  void _goToNew(BuildContext context) {
    Beamer.of(context).beamToNamed('/main/add_travel');
  }
}
