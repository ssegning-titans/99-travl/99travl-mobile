part of ntravl;

IconData iconFromTravelModeUnit(TravelModeUnit mode) {
  switch (mode) {
    case TravelModeUnit.PLANE:
      return Icons.airplanemode_active;
    case TravelModeUnit.CAR:
      return Icons.directions_car_outlined;
    case TravelModeUnit.BOAT:
      return Icons.directions_boat_outlined;
    default:
      throw Exception('Unknown type');
  }
}

class AddTravelPage extends TraceableStatefulWidget {
  @override
  State<StatefulWidget> createState() => _AddTravelPageState();

  const AddTravelPage();
}

class _AddTravelPageState extends State<AddTravelPage> {
  int _index = 0;
  MapPlace? startingPoint;
  DateTime startDate = DateTime.now();

  MapPlace? endingPoint;
  DateTime endDate = DateTime.now();

  List<MapPlace> passBy = [];

  DateTime date = DateTime.now();
  TravelModeUnit travelModeUnit = TravelModeUnit.PLANE;
  final descriptionController = TextEditingController();
  final PageController _pageController = PageController();

  @override
  void dispose() {
    _pageController.dispose();
    descriptionController.dispose();
    super.dispose();
  }

  bool get isValid => startingPoint != null && endingPoint != null;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        body: Stack(
          children: [
            SafeArea(
              child: Column(
                children: [
                  Expanded(
                    child: PageView(
                      physics: NeverScrollableScrollPhysics(),
                      controller: _pageController,
                      children: [
                        AppWrapper(child: _buildStartingPage(context)),
                        AppWrapper(child: _buildEndingPage(context)),
                        AppWrapper(child: _buildModePage(context)),
                        if (travelModeUnit == TravelModeUnit.CAR ||
                            travelModeUnit == TravelModeUnit.BOAT)
                          AppWrapper(child: _buildPassagePoints(context)),
                        AppWrapper(child: _buildDescriptionPage(context)),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SafeArea(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  AppWrapper(
                    child: IconButton(
                      onPressed: () async {
                        if (await _onWillPop(context)) {
                          Navigator.pop(context);
                        }
                      },
                      icon: Icon(Icons.close),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      onWillPop: () => _onWillPop(context),
    );
  }

  Widget _buildStartingPage(BuildContext context) {
    return _buildPage(
      title: 'Starting point',
      child: Column(
        children: [
          PlacePicker(
            initialValue: startingPoint?.formattedAddress,
            paddingHorizontal: 0,
            labelText: 'Departure',
            hintText: 'Where are travelling from?',
            placeCallback: (place) {
              setState(() {
                startingPoint = place;
              });
            },
          ),
          const SizedBox(
            height: 24.0,
          ),
          DatePicker(
            multiple: false,
            onChange: (value) {
              setState(() {
                startDate = value;
              });
            },
            hint: AppText(
              text: 'At',
              paddingHorizontal: 0,
              paddingVertical: 0,
              fontSize: 16.0,
            ),
          ),
        ],
      ),
      showBack: false,
      showContinue: true,
      enableContinue: startingPoint != null,
    );
  }

  Widget _buildEndingPage(BuildContext context) {
    return _buildPage(
      title: 'Ending point',
      child: Column(
        children: [
          PlacePicker(
            initialValue: endingPoint?.formattedAddress,
            paddingHorizontal: 0,
            labelText: 'Destination',
            hintText: 'Where are you travelling to?',
            placeCallback: (place) {
              setState(() {
                endingPoint = place;
              });
            },
          ),
          const SizedBox(
            height: 24.0,
          ),
          DatePicker(
            multiple: false,
            onChange: (value) {
              setState(() {
                endDate = value;
              });
            },
            hint: AppText(
              text: 'At',
              paddingHorizontal: 0,
              paddingVertical: 0,
              fontSize: 16.0,
            ),
          ),
        ],
      ),
      showBack: true,
      showContinue: true,
      enableContinue: endingPoint != null,
    );
  }

  Widget _buildModePage(BuildContext context) {
    final theme = Theme.of(context);
    return _buildPage(
      title: 'How are you going to travel?',
      child: Column(
        children: TravelModeUnit.values
            .map(
              (mode) => AppCard(
                paddingHorizontal: 0,
                child: Padding(
                  padding: EdgeInsets.all(24),
                  child: Center(
                    child: Icon(
                      iconFromTravelModeUnit(mode),
                      color: travelModeUnit == mode ? theme.primaryColor : null,
                      size: 48.r,
                    ),
                  ),
                ),
                onTap: () {
                  setState(() {
                    travelModeUnit = mode;
                  });
                },
              ),
            )
            .toList(),
      ),
      showBack: true,
      showContinue: true,
      enableContinue: endingPoint != null,
    );
  }

  Widget _buildPassagePoints(BuildContext context) {
    return _buildPage(
      title: 'Where are you passage points?',
      child: Column(
        children: [
          PlacePicker(
            paddingHorizontal: 0,
            labelText: 'Departure',
            hintText: 'Where are travelling from?',
            placeCallback: (place) {
              setState(() {
                passBy.add(place);
              });
            },
          ),
          Expanded(
            child: ListView.builder(
              shrinkWrap: true,
              itemBuilder: (context, index) {
                final value = passBy[index];
                return MapPlaceListTile(
                  place: value,
                  padding: EdgeInsets.zero,
                  icon: Icon(Icons.close),
                  onIconTap: () {
                    setState(() {
                      passBy = passBy..removeAt(index);
                    });
                  },
                );
              },
              itemCount: passBy.length,
            ),
          )
        ],
      ),
      showBack: true,
      showContinue: true,
      enableContinue: true,
    );
  }

  Widget _buildDescriptionPage(BuildContext context) {
    return _buildPage(
      title: 'Any last message to other before contacting you?',
      child: Column(
        children: [
          AppWrapper(
            paddingHorizontal: 0,
            paddingVertical: 0,
            child: TextFormField(
              controller: descriptionController,
              decoration: InputDecoration(
                labelText: 'Description',
                hintText:
                    'What information does everyone must know before contacting you?',
                border: OutlineInputBorder(
                  borderRadius: const BorderRadius.all(Radius.circular(8.0)),
                ),
              ),
              maxLines: 6,
              autocorrect: true,
            ),
          ),
        ],
      ),
      showBack: true,
      showContinue: false,
      enableContinue: startingPoint != null,
      showSubmit: true,
    );
  }

  Widget _buildPage({
    required String title,
    required Widget child,
    required bool showContinue,
    required bool showBack,
    bool showSubmit = false,
    bool enableContinue = false,
  }) {
    return Builder(
      builder: (context) {
        final theme = Theme.of(context);
        return Container(
          child: Column(
            children: [
              AppText(
                text: title,
                paddingHorizontal: 4,
                fontSize: 24.0,
              ),
              Expanded(child: child),
              if (showBack)
                Row(
                  children: [
                    Expanded(
                      child: AppWrapper(
                        paddingHorizontal: 0,
                        child: OutlinedButton(
                          onPressed: () => _previousPage(),
                          child: Text('Back'),
                          style: TextButton.styleFrom(
                            textStyle: theme.textTheme.headline6,
                            shape: StadiumBorder(),
                            padding: EdgeInsets.symmetric(
                                horizontal: 24.0.w, vertical: 12.0.h),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              if (showContinue)
                Row(
                  children: [
                    Expanded(
                      child: AppWrapper(
                        paddingHorizontal: 0,
                        child: ElevatedButton(
                          onPressed:
                              enableContinue ? (() => _nextPage()) : null,
                          child: Text('Continue'),
                          style: ElevatedButton.styleFrom(
                            textStyle: theme.textTheme.headline6,
                            shape: StadiumBorder(),
                            padding: EdgeInsets.symmetric(
                              horizontal: 24.0.w,
                              vertical: 12.0.h,
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              if (showSubmit)
                Row(
                  children: [
                    Expanded(
                      child: AppWrapper(
                        paddingHorizontal: 0,
                        child: ElevatedButton(
                          onPressed: () => submit(context),
                          child: Text('Submit'),
                          style: ElevatedButton.styleFrom(
                            textStyle: theme.textTheme.headline6,
                            shape: StadiumBorder(),
                            padding: EdgeInsets.symmetric(
                              horizontal: 24.0.w,
                              vertical: 12.0.h,
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
            ],
          ),
        );
      },
    );
  }

  void _nextPage() {
    _pageController.nextPage(
      duration: Duration(milliseconds: 250),
      curve: Curves.ease,
    );
  }

  void _previousPage() {
    _pageController.previousPage(
      duration: Duration(milliseconds: 250),
      curve: Curves.ease,
    );
  }

  static TravelPoint toPoint(MapPlace point, int idx, DateTime date) {
    final formattedAddress = point.formattedAddress!;
    final splitted = formattedAddress.split(', ');

    final builder = TravelPointBuilder()
      ..passageTime = date.millisecondsSinceEpoch
      ..formattedName = point.formattedAddress
      ..location.lat = point.latitude
      ..location.lng = point.longitude
      ..priority = idx
      ..city = splitted.first
      ..country = splitted.last;
    return builder.build();
  }

  void submit(BuildContext context) async {
    final apiProvider = ApiProvider.of(context, listen: false);

    try {
      final description = descriptionController.text;
      final List<TravelPoint> passagePoints = [
        toPoint(startingPoint!, 0, startDate),
        for (int i = 0; i < passBy.length; i++)
          toPoint(passBy[i], i + 1, startDate.add(Duration(hours: i))),
        toPoint(endingPoint!, passBy.length + 1, endDate),
      ];

      final toCreate = CreateTravelBuilder()
        ..description = description
        ..transportMode = travelModeUnit
        // TODO Make this Selectable
        ..maxQuantity.pricePerUnit = 1234
        ..maxQuantity.weight = 23
        ..maxQuantity.unit = TravelWeightUnit.KG
        ..luggageDimension = TravelLuggageDimension.M
        ..passagePoints.addAll(passagePoints);

      final result = await apiProvider.travelsApi.createTravel(
        createTravel: toCreate.build(),
      );
      final travel = result.data!;
      context.read<CurrentUserTravelProvider>().add(travel);
      _showSuccessModal(context);
    } on DioError catch (e, s) {
      showScaffoldError(
        context,
        title: 'An error occurred',
        subtitle: 'Please try again later',
      );
    }
  }

  VoidCallback? onStepCancel() {
    return _index <= 0
        ? null
        : () {
            if (_index > 0) {
              setState(() {
                _index -= 1;
              });
            }
          };
  }

  Widget controlsBuilder(
    BuildContext context, {
    VoidCallback? onStepContinue,
    VoidCallback? onStepCancel,
  }) {
    return Row(
      children: [
        if (onStepContinue != null)
          ElevatedButton(
            onPressed: onStepContinue,
            child: Text('Continue'),
            style: ElevatedButton.styleFrom(
              textStyle: Theme.of(context).textTheme.headline6,
              shape: StadiumBorder(),
              padding: EdgeInsets.symmetric(
                horizontal: 24.0.w,
                vertical: 12.0.h,
              ),
            ),
          ),
        if (onStepCancel != null)
          TextButton(
            onPressed: onStepCancel,
            child: Text('Cancel'),
            style: TextButton.styleFrom(
              textStyle: Theme.of(context).textTheme.headline6,
              shape: StadiumBorder(),
              padding: EdgeInsets.symmetric(
                horizontal: 24.0.w,
                vertical: 12.0.h,
              ),
            ),
          ),
      ],
    );
  }

  Future<bool> _onWillPop(BuildContext context) async {
    if (startingPoint == null && endingPoint == null) {
      return true;
    }

    final result = await showModal<bool>(
      context: context,
      builder: (context) => const _BeforeCloseDialog(),
    );
    return result != null ? result : false;
  }

  void _showSuccessModal(BuildContext context) async {
    final result = await showModal<String>(
      context: context,
      builder: (context) => const _AfterSuccessDialog(),
      useRootNavigator: true,
    );
    if (result == null) {
      Navigator.pop(context);
    }
  }
}

class _BeforeCloseDialog extends TraceableStatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Leave creation page?'),
      content: Text(
          'This will reset your inputs and take you back to the previous page'),
      actions: [
        TextButton(
          onPressed: () => Navigator.pop(context, false),
          child: Text('Stay'),
          style: TextButton.styleFrom(
            textStyle: Theme.of(context).textTheme.headline6,
            shape: StadiumBorder(),
            padding: EdgeInsets.symmetric(
              horizontal: 24.0.w,
              vertical: 12.0.h,
            ),
          ),
        ),
        ElevatedButton(
          onPressed: () => Navigator.pop(context, true),
          child: Text('Leave'),
          style: ElevatedButton.styleFrom(
            textStyle: Theme.of(context).textTheme.headline6,
            shape: StadiumBorder(),
            padding: EdgeInsets.symmetric(
              horizontal: 24.0.w,
              vertical: 12.0.h,
            ),
          ),
        ),
      ],
    );
  }

  const _BeforeCloseDialog();
}

class _AfterSuccessDialog extends TraceableStatelessWidget {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return AlertDialog(
      title: Text('Successfully saved!'),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            width: 100.r,
            height: 100.r,
            child: Icon(
              Icons.check,
              size: 50.r,
              color: theme.colorScheme.onBackground,
            ),
            decoration: ShapeDecoration(
              color: Colors.green,
              shape: CircleBorder(),
            ),
          ),
          SizedBox(height: 24),
          Text(
              'Your announce was saved right. You can view it right now inside your home page'),
        ],
      ),
      actions: [
        TextButton(
          onPressed: () => Navigator.pop(context),
          child: Text('Back'),
          style: TextButton.styleFrom(
            textStyle: Theme.of(context).textTheme.headline6,
            shape: StadiumBorder(),
            padding: EdgeInsets.symmetric(
              horizontal: 24.0.w,
              vertical: 12.0.h,
            ),
          ),
        ),
        ElevatedButton(
          onPressed: () {
            Navigator.pop(context, '/main');
            Beamer.of(context).root.popToNamed('/main?t=0');
          },
          child: Text('Home'),
          style: ElevatedButton.styleFrom(
            textStyle: Theme.of(context).textTheme.headline6,
            shape: StadiumBorder(),
            padding: EdgeInsets.symmetric(
              horizontal: 24.0.w,
              vertical: 12.0.h,
            ),
          ),
        ),
      ],
    );
  }

  const _AfterSuccessDialog();
}
