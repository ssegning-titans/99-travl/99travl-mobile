part of ntravl;

class SearchResultPage extends TraceableStatefulWidget {
  final SearchModel searchModel;

  SearchResultPage({
    required this.searchModel,
  });

  @override
  State<StatefulWidget> createState() =>
      _SearchResultPageState(this.searchModel);
}

class _SearchResultPageState extends State<SearchResultPage> {
  SearchModel searchModel;

  _SearchResultPageState(this.searchModel);

  @override
  Widget build(BuildContext context) {
    final apiProvider = ApiProvider.of(context);
    final theme = Theme.of(context);
    final titleSize = 18.0;
    return Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AppText(
              text: searchModel.start.name,
              subTitle: Text(
                searchModel.end.name,
                style:
                    theme.textTheme.headline2!.copyWith(fontSize: titleSize.sp),
                textScaleFactor: 1.0,
              ),
              fontSize: titleSize,
              prefix: SizedBox(
                width: 24.0,
                height: 24.0,
                child: IconButton(
                  padding: EdgeInsets.all(0.0),
                  icon: Icon(Icons.arrow_back_ios_outlined),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
              suffix: SizedBox(
                width: 24.0,
                height: 24.0,
                child: IconButton(
                  padding: EdgeInsets.all(0.0),
                  icon: Icon(Icons.filter_list_outlined),
                  onPressed: () => _openFilter(context),
                ),
              ),
            ),
            AppWrapper(
              paddingVertical: 0,
              child: Wrap(
                children: [
                  ActionChip(
                    avatar: Icon(Icons.calendar_today_outlined),
                    label: Text(df.format(searchModel.range.start) +
                        ' - ' +
                        df.format(searchModel.range.end)),
                    onPressed: () => _datePicker(context),
                  ),
                ],
              ),
            ),
            Expanded(
              child: _ResultList(
                travelsApi: apiProvider.travelsApi,
                searchModel: searchModel,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _datePicker(BuildContext context) async {
    dynamic result = await pickDate(
      context,
      multiple: true,
    );
    if (result != null && mounted) {
      setState(() {
        searchModel.range = result;
      });
    }
  }

  void _openFilter(BuildContext context) async {
    final result = await searchOptions(
      context,
      initialValue: searchModel,
    );
    if (result != null) {
      setState(() {
        searchModel = result;
      });
    }
  }
}

class _ResultList extends TraceableStatelessWidget {
  final TravelsApi travelsApi;
  final SearchModel searchModel;

  const _ResultList({
    required this.travelsApi,
    required this.searchModel,
  });

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Iterable<SearchTravel>?>(
        future: _findResult(context),
        builder: (context, snapshot) {
          if (snapshot.connectionState != ConnectionState.done) {
            return Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }

          if (snapshot.hasError) {
            return Container(
              child: AppText(
                text:
                    'An error occurred when trying to find more data. Please retry later',
                fontSize: 16.0,
                wrapper: (children) => Wrap(children: children),
              ),
            );
          }

          if ((!snapshot.hasData) || (snapshot.data!.isEmpty)) {
            return Container(
              child: AppText(
                text: 'No data found',
                fontSize: 16.0,
                wrapper: (children) => Wrap(children: children),
              ),
            );
          }

          final list = snapshot.data!;
          return ListView.builder(
            shrinkWrap: true,
            itemCount: list.length,
            itemBuilder: (context, index) {
              final value = list.elementAt(index);
              return SearchResultCard(road: value);
            },
          );
        });
  }

  Future<Iterable<SearchTravel>?> _findResult(BuildContext context) async {
    try {
      final result = await travelsApi.searchTravel(
        startCoord: BuiltList([
          searchModel.start.latitude,
          searchModel.start.longitude,
        ]),
        endCoord: BuiltList([
          searchModel.end.latitude,
          searchModel.end.longitude,
        ]),
        startRadius: searchModel.startRadius.toDouble(),
        endRadius: searchModel.endRadius.toDouble(),
        startDate: searchModel.range.start.millisecondsSinceEpoch,
        endDate: searchModel.range.end.millisecondsSinceEpoch,
      );
      return result.data!;
    } on DioError catch (e, s) {
      print('e--> $e');
      print('s--> $s');

      showScaffoldError(
        context,
        title: 'An error occurred',
        subtitle: 'Please try again later',
      );
      rethrow;
    }
  }
}
