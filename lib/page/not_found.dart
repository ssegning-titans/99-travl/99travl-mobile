part of ntravl;

class NotFoundPage extends TraceableStatefulWidget {
  @override
  State<StatefulWidget> createState() => _NotFoundPageState();
}

class _NotFoundPageState extends State<NotFoundPage> {
  @override
  Widget build(BuildContext context) {
    final uri = Beamer.of(context).currentConfiguration!.uri;
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text('Not found'),
              Text('$uri'),
              OutlinedButton(
                onPressed: () {
                  Beamer.of(context).beamToNamed('/main');
                },
                child: Text('Go to main'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
