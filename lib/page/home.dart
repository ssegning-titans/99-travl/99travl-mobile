part of ntravl;

class HomePage extends TraceableStatefulWidget {
  @override
  State<StatefulWidget> createState() => _HomePageState();

  const HomePage();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            Consumer<CurrentUserTravelProvider>(
              builder: (context, provider, _) {
                return SliverList(
                  delegate: SliverChildBuilderDelegate((context, i) {
                    if (i == 0) {
                      return AppText(
                        text: 'Home',
                        fontSize: 24.0,
                        textAlign: TextAlign.start,
                      );
                    } else if (i == 1) {
                      if (provider.error)
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            AppWrapper(
                              paddingHorizontal: 24,
                              paddingVertical: 12,
                              child: Icon(
                                Icons.warning_amber,
                                color: Theme.of(context).errorColor,
                              ),
                            ),
                            AppText(
                              text:
                              'There was an error while looking for your chats.',
                              fontSize: 16.0,
                              wrapper: (children) => Wrap(children: children),
                            ),
                          ],
                        );
                      if (provider.loading)
                        return Container(
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        );
                      if (provider.previousList.isEmpty)
                        return AppText(
                          text:
                              'You have no travels yet. Create one and come back here',
                          fontSize: 16.0,
                          wrapper: (children) => Wrap(children: children),
                        );
                      else
                        return AppText(
                          text: 'Here are your travels',
                          fontSize: 16.0,
                          wrapper: (children) => Wrap(children: children),
                        );
                    } else {
                      if (provider.previousList.length > i - 2) {
                        return TravelItem(travel: provider.previousList[i - 2]);
                      }
                    }

                    return null;
                  }),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
