part of ntravl;

class WelcomePage extends TraceableStatefulWidget {
  @override
  State<StatefulWidget> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  final controller = PageController();

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      BoxConstraints(
          maxWidth: MediaQuery.of(context).size.width,
          maxHeight: MediaQuery.of(context).size.height),
      designSize: Size(360, 690),
      orientation: Orientation.portrait,
    );
    final theme = Theme.of(context);

    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: Stack(
              children: [
                Container(
                  child: PageView(
                    controller: controller,
                    children: [
                      buildPage1(context),
                      buildPage2(context),
                      buildPage3(context),
                      buildPage4(context),
                    ],
                  ),
                ),
                Positioned(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Image.asset(
                        'assets/images/logo.png',
                        width: 96.w,
                      ),
                      SmoothPageIndicator(
                        effect: ExpandingDotsEffect(
                          activeDotColor: theme.colorScheme.primary,
                          dotColor: theme.colorScheme.background,
                        ),
                        controller: controller,
                        count: 4,
                        onDotClicked: (index) {
                          controller.animateToPage(
                            index,
                            duration: Duration(milliseconds: 250),
                            curve: Curves.decelerate,
                          );
                        },
                      ),
                    ],
                  ),
                  bottom: 24.0,
                  left: 24.0,
                  right: 24.0,
                ),
              ],
            ),
          ),
          SafeArea(
            top: false,
            child: Container(
              padding: const EdgeInsets.only(
                left: 24.0,
                right: 24.0,
                top: 12.0,
                bottom: 0.0,
              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const AppThemeToggle(),
                      TextButton(
                        onPressed: () => _continue(context),
                        child: Row(
                          children: [
                            Text('Skip'),
                            SizedBox(width: 12.0),
                            Icon(Icons.arrow_forward_ios_sharp),
                          ],
                        ),
                        style: TextButton.styleFrom(
                          textStyle: theme.textTheme.headline5,
                          shape: StadiumBorder(),
                          padding: EdgeInsets.all(12.0.h),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildPage1(BuildContext context) => IntroPage(
        title: 'Find a destination',
        subTitle: 'We operate throughout our users all over the world',
        imageUrl: 'assets/images/find_destination.png',
      );

  Widget buildPage2(BuildContext context) => IntroPage(
        title: 'Find a traveller',
        subTitle:
            'We offer a vast network of traveller which give their time for such task',
        imageUrl: 'assets/images/find_sender.png',
      );

  Widget buildPage3(BuildContext context) => IntroPage(
        title: 'Ship a parcel',
        subTitle:
            'A traveller take your parcel, travel with it and give it to the receiver',
        imageUrl: 'assets/images/send.png',
      );

  Widget buildPage4(BuildContext context) => IntroPage(
        title: 'First step!',
        subTitle:
            'First things first, you need to configure the app for us to deliver the best service possible',
        imageUrl: 'assets/images/efficiency.png',
        more: [
          SizedBox(height: 12.0),
          ElevatedButton(
            onPressed: () => _continue(context),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text('Continue'),
                SizedBox(width: 12.0),
                Icon(Icons.arrow_forward_ios_sharp),
              ],
            ),
            style: ElevatedButton.styleFrom(
              textStyle: Theme.of(context).textTheme.headline5,
              shape: StadiumBorder(),
              padding: EdgeInsets.symmetric(horizontal: 24.0.w, vertical: 12.0.h),
            ),
          )
        ],
      );

  void _continue(BuildContext context) async {
    await WelcomeProvider.of(context, listen: false).seenWelcome();
    Beamer.of(context).beamToNamed('/main');
  }
}

class IntroPage extends TraceableStatelessWidget {
  final String title;
  final String subTitle;
  final String imageUrl;
  final List<Widget>? more;

  const IntroPage({
    required this.title,
    required this.subTitle,
    required this.imageUrl,
    this.more,
  });

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Container(
      color: theme.colorScheme.surface,
      padding: const EdgeInsets.symmetric(horizontal: 24.0),
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Image.asset(imageUrl, width: 0.5.sw),
            SizedBox(height: 6.0.h),
            AppText(
              text: title,
              fontSize: 32,
              textAlign: TextAlign.center,
            ),
            AppText(
              text: subTitle,
              fontSize: 16,
              textAlign: TextAlign.center,
              wrapper: (children) => Wrap(children: children),
            ),
            if (more != null)
              Container(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: more!,
                ),
              ),
          ],
        ),
      ),
    );
  }
}
