part of ntravl;

class AuthPage extends TraceableStatelessWidget {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final authProvider = AuthProvider.of(context);
    ScreenUtil.init(
      BoxConstraints(
          maxWidth: MediaQuery.of(context).size.width,
          maxHeight: MediaQuery.of(context).size.height),
      designSize: Size(360, 690),
      orientation: Orientation.portrait,
    );

    return Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(
                left: 24.0,
                top: 24.0,
              ),
              child: AppThemeToggle(),
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(
                  left: 12.0,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Image.asset(
                      'assets/images/logo.png',
                      width: 0.7.sw,
                    ),
                  ],
                ),
              ),
            ),
            AppText(
              text: 'Please login before going further',
              fontSize: 32,
            ),
            AppText(
              text: 'Before going further, please authenticate yourself',
              fontSize: 16,
            ),
            Row(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 24.0, right: 24.0, top: 12.0, bottom: 12.0),
                    child: ElevatedButton(
                      onPressed: () async {
                        await authProvider.login();
                        Beamer.of(context).root.beamToNamed(
                              '/main',
                              replaceCurrent: true,
                              stacked: false,
                            );
                      },
                      child: Text('Login'),
                      style: ElevatedButton.styleFrom(
                        textStyle: theme.textTheme.headline5,
                        shape: StadiumBorder(),
                        padding: EdgeInsets.symmetric(horizontal: 24.0.w, vertical: 12.0.h),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(
                bottom: 24.0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
