part of '../../main.dart';

class AppearanceScreen extends TraceableStatefulWidget {
  const AppearanceScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _AppearanceScreenState();
}

class _AppearanceScreenState extends State<AppearanceScreen> {
  @override
  Widget build(BuildContext context) {
    ThemeProvider themeProvider = ThemeProvider.of(context);
    final localization = AppLocalizations.of(context)!;
    final current = localization.localeName;
    return Scaffold(
      appBar: AppBar(
        title: Text('Appearance'),
      ),
      body: ListView(
        children: [
          ListTile(
            title: Text('Theme'),
          ),
          RadioListTile<ThemeMode>(
            title: Text('Light theme'),
            subtitle: Text("Better for bad sight people"),
            value: ThemeMode.light,
            onChanged: (value) {
              themeProvider.setTheme(value!);
            },
            groupValue: themeProvider.themeMode,
          ),
          RadioListTile<ThemeMode>(
            title: Text('Dark theme'),
            subtitle: Text("Better for screen savings and phone's life"),
            value: ThemeMode.dark,
            onChanged: (value) {
              themeProvider.setTheme(value!);
            },
            groupValue: themeProvider.themeMode,
          ),
          RadioListTile<ThemeMode>(
            title: Text('System theme'),
            subtitle: Text("Adapt theme using the system's theme"),
            value: ThemeMode.system,
            onChanged: (value) {
              themeProvider.setTheme(value!);
            },
            groupValue: themeProvider.themeMode,
          ),
          Divider(),
          ListTile(
            title: Text('Language'),
          ),
          RadioListTile<String>(
            title: Text('Français'),
            value: 'fr',
            onChanged: (v) {},
            groupValue: current,
          ),
          RadioListTile<String>(
            title: Text('English'),
            value: 'en',
            onChanged: (v) {},
            groupValue: current,
          ),
          RadioListTile<String>(
            title: Text('Deutsch'),
            value: 'de',
            onChanged: (v) {},
            groupValue: current,
          ),
          RadioListTile<String>(
            title: Text('عرب'),
            value: 'ar',
            onChanged: (v) {},
            groupValue: current,
          ),
        ],
      ),
    );
  }
}
