part of '../../main.dart';

class AccountScreen extends TraceableStatefulWidget {
  const AccountScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Account'),
      ),
      body: ListView(
        children: [
          ListTile(
            leading: Icon(Icons.vpn_key_outlined),
            title: Text('Phone number'),
            subtitle: Text('Update your Phone number'),
            onTap: () {
              Beamer.of(context).root.beamToNamed('/main/account_options/phone_number');
            },
          ),
        ],
      ),
    );
  }
}
