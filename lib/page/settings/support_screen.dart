part of '../../main.dart';

class SupportScreen extends TraceableStatefulWidget {
  const SupportScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SupportScreenState();
}

class _SupportScreenState extends State<SupportScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Support'),
      ),
      body: Center(
        child: Text('Support'),
      ),
    );
  }
}
