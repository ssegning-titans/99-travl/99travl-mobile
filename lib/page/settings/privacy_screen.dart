part of '../../main.dart';

class PrivacyScreen extends TraceableStatefulWidget {
  const PrivacyScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _PrivacyScreenState();
}

class _PrivacyScreenState extends State<PrivacyScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Privacy'),
      ),
      body: ListView(
        children: [
          ListTile(
            title: Text('Privacy policy'),
            trailing: Icon(Icons.open_in_new_outlined),
            onTap: openPrivacyPolicy,
          ),
          ListTile(
            title: Text('Terms of use'),
            trailing: Icon(Icons.open_in_new_outlined),
            onTap: openTermsOfUse,
          ),
          Divider(),
          ListTile(
            title: Text('Data collection'),
          ),
          Builder(
            builder: (context) => ChangeNotifierProvider(
              create: (_) => AnalyticProvider(),
              builder: (context, child) {
                final provider = context.watch<AnalyticProvider>();

                return SwitchListTile.adaptive(
                  value: provider.optIn,
                  onChanged: provider.init ? provider.setOptIn : null,
                  title: Text('Enabled'),
                  subtitle: Text(
                    'We collect usage data anonymously to understand '
                    'user\'s workflows and better respond to their need. '
                    'No personal information is provided.',
                  ),
                );
              },
            ),
          ),
          Builder(
            builder: (context) => ListTile(
              title: Text('Cookies'),
              subtitle: Text(
                'We uses only necessary cookie to guarantee a minimum of services, calling our APIs, authenticating users. These are saved locally and are encrypted. More inside the terms of use and Privacy policy',
              ),
            ),
          ),
        ],
      ),
    );
  }
}
