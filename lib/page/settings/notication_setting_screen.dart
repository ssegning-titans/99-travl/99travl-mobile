part of '../../main.dart';

class NotificationSettingScreen extends TraceableStatefulWidget {
  const NotificationSettingScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _NotificationSettingScreenState();
}

class _NotificationSettingScreenState extends State<NotificationSettingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Notification'),
      ),
      body: ListView(
        children: [
          ListTile(
            title: Text('Global notifications'),
          ),
          /*
          SwitchListTile.adaptive(
            title: Text('Notification'),
            onChanged: provider.toggleEnabled,
            value: provider.enabled,
            shape: RoundedRectangleBorder(),
          ),
          Divider(),
          ListTile(
            title: Text('Travel notifications'),
            enabled: provider.enabled,
          ),
          SwitchListTile.adaptive(
            title: Text("Notify new travel's request"),
            onChanged:
                provider.enabled ? provider.toggleTravelRequestEnabled : null,
            value: provider.travelRequestEnabled,
          ),
          SwitchListTile.adaptive(
            title: Text("Notify accepted requests"),
            onChanged:
                provider.enabled ? provider.toggleAcceptedRequestEnabled : null,
            value: provider.acceptedRequestEnabled,
          ),
          SwitchListTile.adaptive(
            title: Text("Notify pending requests"),
            onChanged:
                provider.enabled ? provider.togglePendingRequestEnabled : null,
            value: provider.pendingRequestEnabled,
          ),
          Divider(),
          ListTile(
            title: Text('Chat notifications'),
            enabled: provider.enabled,
          ),
          SwitchListTile.adaptive(
            title: Text("Notify new travel's request"),
            onChanged:
                provider.enabled ? provider.toggleChatMessageEnabled : null,
            value: provider.chatMessageEnabled,
          ),
           */
        ],
      ),
    );
  }
}
