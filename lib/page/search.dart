part of ntravl;

class SearchPage extends TraceableStatefulWidget {
  @override
  State<StatefulWidget> createState() => _SearchPageState();

  const SearchPage();
}

class _SearchPageState extends State<SearchPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: ListView(
          shrinkWrap: true,
          children: [
            AppText(
              text: 'Search',
              fontSize: 24.0,
              textAlign: TextAlign.start,
            ),
            _SearchHeader(),
            _SearchPrevious(),
          ],
        ),
      ),
    );
  }
}

class _SearchHeader extends TraceableStatefulWidget {
  @override
  State<StatefulWidget> createState() => _SearchHeaderState();

  const _SearchHeader();
}

class _SearchHeaderState extends State<_SearchHeader> {
  MapPlace? startingPoint;
  MapPlace? endingPoint;
  DateTimeRange range = DateTimeRange(
    start: DateTime.now(),
    end: DateTime.now().add(Duration(days: 7)),
  );

  bool get isValid => startingPoint != null && endingPoint != null;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        PlacePicker(
          labelText: 'From',
          hintText: 'Where are you sending from?',
          placeCallback: (place) {
            setState(() {
              startingPoint = place;
            });
          },
        ),
        PlacePicker(
          labelText: 'To',
          hintText: 'Where are you sending to?',
          placeCallback: (place) {
            setState(() {
              endingPoint = place;
            });
          },
        ),
        AppWrapper(
          child: Column(
            children: [
              DatePicker(
                multiple: true,
                onChange: (range) {
                  if (range is DateTimeRange) {
                    if (mounted) {
                      setState(() {
                        this.range = range;
                      });
                    }
                  } else {
                    throw Exception('range is not DateTimeRange');
                  }
                },
              ),
              Divider(),
              if (isValid)
                ElevatedButton(
                  onPressed: () {
                    final previousSearchProvider =
                        PreviousSearchProvider.of(context, listen: false);
                    previousSearchProvider.put(
                      SingleSearch(
                        startingPoint: startingPoint!,
                        endingPoint: endingPoint!,
                        range: range,
                      ),
                    );

                    gotoSearchResult(
                      context,
                      startingPoint: startingPoint!,
                      endingPoint: endingPoint!,
                      range: range,
                    );
                  },
                  child: Text('Search'),
                  style: ElevatedButton.styleFrom(
                    textStyle: Theme.of(context).textTheme.headline6,
                    shape: StadiumBorder(),
                    padding: EdgeInsets.symmetric(
                      horizontal: 24.0.w,
                      vertical: 12.0.h,
                    ),
                  ),
                ),
            ],
          ),
        ),
      ],
    );
  }
}

class _SearchPrevious extends TraceableStatelessWidget {
  @override
  Widget build(BuildContext context) {
    final provider = PreviousSearchProvider.of(context);
    final allPrevious = provider.getAll();
    if (allPrevious.isEmpty) {
      return AppText(text: 'No previous search', fontSize: 16);
    }
    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        final value = allPrevious[index];
        return AppWrapper(
          child: TravelSearch(value),
          paddingVertical: 0,
        );
      },
      itemCount: allPrevious.length,
    );
  }
}
