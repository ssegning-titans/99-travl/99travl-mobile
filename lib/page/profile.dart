part of ntravl;

class ProfilePage extends TraceableStatefulWidget {
  @override
  State<StatefulWidget> createState() => _ProfilePageState();

  const ProfilePage();
}

class _ProfilePageState extends State<ProfilePage> {
  final _controller = ScrollController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AppText(
              text: 'Profile',
              fontSize: 24.0,
              textAlign: TextAlign.start,
            ),
            Expanded(
              child: ListView(
                controller: _controller,
                shrinkWrap: true,
                children: [
                  Builder(
                    builder: (context) {
                      final authProvider = AuthProvider.of(context);
                      // TODO final map = authProvider.getMap();

                      return AppListTile(
                        leading: CircleAvatar(
                          backgroundImage: NetworkImage(
                              "https://images.pexels.com/photos/4675531/pexels-photo-4675531.jpeg?crop=entropy&cs=srgb&dl=pexels-kehn-hermano-4675531.jpg&fit=crop&fm=jpg&h=1919&w=1280"),
                        ),
                        title: Text('Selast Lambou'),
                        subtitle: Text('+1234567890'),
                      );
                    },
                  ),
                  _buildLine(
                    context,
                    title: 'Friends',
                    subTitle: 'Manage your parcel receivers',
                    icon: Icons.group_outlined,
                    uri: '/main/friends',
                  ),
                  _buildLine(
                    context,
                    title: 'Accounts',
                    subTitle: 'Password, Payments, Phone number',
                    icon: Icons.vpn_key_outlined,
                    uri: '/main/account_options',
                  ),
                  _buildLine(
                    context,
                    title: 'Notifications',
                    subTitle: 'Chat & Requests notification',
                    icon: Icons.notifications_none_outlined,
                    uri: '/main/notification_options',
                  ),
                  _buildLine(
                    context,
                    title: 'Privacy',
                    subTitle: 'Cookie, Analytics, Terms of use',
                    icon: Icons.lock_open_outlined,
                    uri: '/main/privacy_options',
                  ),
                  _buildLine(
                    context,
                    title: 'Appearance',
                    subTitle: 'Theme, Language',
                    icon: Icons.wb_sunny_outlined,
                    uri: '/main/appearance_options',
                  ),
                  _buildLine(
                    context,
                    title: 'Chats',
                    subTitle: 'Archives, Encryption, Storage',
                    icon: Icons.chat_bubble_outline,
                    uri: '/main/chat_options',
                  ),
                  _buildLine(
                    context,
                    title: 'Support',
                    subTitle: 'Help, Reports, Feedback',
                    icon: Icons.help_outline,
                    uri: '/main/support_options',
                  ),
                  Consumer<AppConfigProvider>(
                    builder: (context, provider, child) {
                      if (!provider.ready) {
                        return Container();
                      }

                      String applicationName = '99Travl Mobile App';
                      String applicationVersion = provider.packageInfo.version +
                          ' ' +
                          provider.packageInfo.buildNumber;
                      applicationName = provider.packageInfo.appName;

                      final ThemeData theme = Theme.of(context);
                      final TextStyle textStyle = theme.textTheme.bodyText2!;
                      final List<Widget> aboutBoxChildren = <Widget>[
                        const SizedBox(height: 24),
                        RichText(
                          text: TextSpan(
                            children: <TextSpan>[
                              TextSpan(
                                style: textStyle,
                                text: 'Logistics reinvented at lower cost. For mo info, contact us at '
                              ),
                              TextSpan(
                                style: textStyle.copyWith(color: theme.colorScheme.primary),
                                text: 'https://ssegning.com'
                              ),
                              TextSpan(
                                style: textStyle,
                                text: '.'
                              ),
                            ],
                          ),
                        ),
                      ];

                      return AppListTile(
                        title: Text('About $applicationName'),
                        leading: const Icon(Icons.info_outline),
                        subtitle: Text(applicationVersion),
                        onTap: () => showAboutDialog(
                          context: context,
                          applicationIcon: Image.asset(
                            "assets/images/favicon.png",
                            width: 42,
                          ),
                          applicationName: applicationName,
                          applicationVersion: applicationVersion,
                          applicationLegalese: '\u{a9} 2021 SSegning Inc',
                          children: aboutBoxChildren,
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildLine(
    BuildContext context, {
    required IconData icon,
    required String title,
    required String subTitle,
    required String uri,
  }) =>
      AppListTile(
        onTap: () => Beamer.of(context).root.beamToNamed(uri),
        leading: Icon(icon),
        title: Text(title),
        subtitle: Text(subTitle),
      );
}
