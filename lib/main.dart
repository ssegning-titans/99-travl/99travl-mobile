library ntravl;

import 'dart:async';
import 'dart:io';

import 'package:animations/animations.dart';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:beamer/beamer.dart';
import 'package:built_collection/built_collection.dart';
import 'package:catcher/catcher.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:dio/dio.dart';
import 'package:dio_cache_interceptor/dio_cache_interceptor.dart';
import 'package:dio_cache_interceptor_hive_store/dio_cache_interceptor_hive_store.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_contact/contacts.dart';
import 'package:flutter_contact/flutter_contact.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:intl/intl.dart';
import 'package:jwt_decode/jwt_decode.dart';
import 'package:kiwi/kiwi.dart';
import 'package:matomo/matomo.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:nested/nested.dart';
import 'package:ntravl_api/ntravl_api.dart';
import 'package:oauth2_client/oauth2_client.dart';
import 'package:oauth2_client/oauth2_helper.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:quiver/collection.dart';
import 'package:sentry/sentry.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:swipeable_page_route/swipeable_page_route.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:timeline_tile/timeline_tile.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:uuid/uuid.dart';
import 'package:wakelock/wakelock.dart';

import 'environment_config.dart';

part 'app.dart';

part 'components/account_line_item.dart';

part 'components/bottom_navigation.dart';

part 'components/card.dart';

part 'components/chat_item.dart';

part 'components/date_picker.dart';

part 'components/location.dart';

part 'components/payment_method_choose_screen.dart';

part 'components/payments.dart';

part 'components/place_picker.dart';

part 'components/radius_picker.dart';

part 'components/search_option_modal.dart';

part 'components/search_result_card.dart';

part 'components/select_contact_modal.dart';

part 'components/select_date_modal.dart';

part 'components/select_place_modal.dart';

part 'components/text.dart';

part 'components/theme_toggle.dart';

part 'components/wrapper.dart';

part 'constant/debouncer.dart';

part 'constant/error.dart';

part 'constant/keys.dart';

part 'constant/links.dart';

part 'constant/matomo.dart';

part 'constant/notifications.dart';

part 'model/built_list.dart';

part 'model/search_model.dart';

part 'page/add_travel.dart';

part 'page/auth.dart';

part 'page/chats.dart';

part 'page/friends.dart';

part 'page/home.dart';

part 'page/main.dart';

part 'page/not_found.dart';

part 'page/profile.dart';

part 'page/search.dart';

part 'page/search_result.dart';

part 'page/settings/account_screen.dart';

part 'page/settings/appearance_screen.dart';

part 'page/settings/archives_screen.dart';

part 'page/settings/avatar_screen.dart';

part 'page/settings/chat_setting_screen.dart';

part 'page/settings/feedback_screen.dart';

part 'page/settings/notication_setting_screen.dart';

part 'page/settings/phone_number_screen.dart';

part 'page/settings/privacy_screen.dart';

part 'page/settings/support_screen.dart';

part 'page/welcome.dart';

part 'provider/analytic_provider.dart';

part 'provider/api_provider.dart';

part 'provider/app_config_provider.dart';

part 'provider/auth_provider.dart';

part 'provider/current_user_chats_provider.dart';

part 'provider/current_user_travel_provider.dart';

part 'provider/previous_search_provider.dart';

part 'provider/previous_suggestion_provider.dart';

part 'provider/theme_provider.dart';

part 'provider/welcome_provider.dart';

part 'router/context/auth_beam.dart';

part 'router/context/main_beam.dart';

part 'router/context/welcome_beam.dart';

part 'router/modal.dart';

part 'router/page/app_beam_page.dart';

part 'router/router.dart';

part 'router/router_methods.dart';

part 'theme/theme_data.dart';

void main() async => await app();
