part of ntravl;

class SearchModel {
  SearchPoint start;
  SearchPoint end;
  DateTimeRange range;
  int startRadius;
  int endRadius;

  SearchModel({
    required this.start,
    required this.end,
    required this.range,
    required this.startRadius,
    required this.endRadius,
  });

  SearchModel.fromJson(Map<String, dynamic> json)
      : start = SearchPoint.fromJson(json['start_point']),
        end = SearchPoint.fromJson(json['end_point']),
        range = DateTimeRange(
          start: DateTime.fromMillisecondsSinceEpoch(json['start_date']),
          end: DateTime.fromMillisecondsSinceEpoch(json['end_date']),
        ),
        startRadius = json['start_radius'] as int,
        endRadius = json['end_radius'] as int
  //
  ;

  Map<String, dynamic> toJson() => {
        'start_point': start.toJson(),
        'end_point': end.toJson(),
        'start_date': range.start.millisecondsSinceEpoch,
        'end_date': range.end.millisecondsSinceEpoch,
        'start_radius': startRadius,
        'end_radius': endRadius,
      };

  @override
  String toString() {
    return 'SearchModel{start: $start, end: $end, range: $range, startRadius: $startRadius, endRadius: $endRadius}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SearchModel &&
          runtimeType == other.runtimeType &&
          start == other.start &&
          end == other.end &&
          range == other.range &&
          startRadius == other.startRadius &&
          endRadius == other.endRadius;

  @override
  int get hashCode =>
      start.hashCode ^
      end.hashCode ^
      range.hashCode ^
      startRadius.hashCode ^
      endRadius.hashCode;
}

class SearchPoint {
  String name;
  double latitude;
  double longitude;

  SearchPoint({
    required this.name,
    required this.latitude,
    required this.longitude,
  });

  SearchPoint.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        latitude = json['latitude'] as double,
        longitude = json['longitude'] as double;

  Map<String, dynamic> toJson() => {
        'name': name,
        'latitude': latitude,
        'longitude': longitude,
      };

  @override
  String toString() {
    return 'SearchPoint{name: $name, latitude: $latitude, longitude: $longitude}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SearchPoint &&
          runtimeType == other.runtimeType &&
          name == other.name &&
          latitude == other.latitude &&
          longitude == other.longitude;

  @override
  int get hashCode => name.hashCode ^ latitude.hashCode ^ longitude.hashCode;
}
