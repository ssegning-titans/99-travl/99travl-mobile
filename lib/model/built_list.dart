part of ntravl;

class QueryList<E> extends DelegatingList<E> {
  final List<E> list;

  QueryList(this.list);

  @override
  List<E> get delegate => list;

  @override
  String toString() {
    return list.join(',');
  }
}
