part of ntravl;

typedef TextWrapper = MultiChildRenderObjectWidget Function(
    List<Widget> children);

class AppText extends StatelessWidget {
  final String text;
  final Widget? subTitle;
  final double fontSize;
  final double paddingHorizontal;
  final double paddingVertical;
  final TextAlign? textAlign;
  final Widget? prefix;
  final Widget? suffix;
  final TextWrapper? wrapper;
  final bool expandText;
  final Color? color;

  AppText({
    required this.text,
    this.subTitle,
    this.fontSize = 12,
    this.paddingHorizontal = 24.0,
    this.paddingVertical = 12.0,
    TextWrapper? wrapper,
    this.textAlign,
    this.prefix,
    this.suffix,
    this.color,
  })  : this.expandText = wrapper == null,
        this.wrapper = wrapper ?? ((children) => Row(children: children));

  @override
  Widget build(BuildContext context) {
    return Nested(
      children: [
        AppWrapper(
          paddingHorizontal: paddingHorizontal,
          paddingVertical: paddingVertical,
        ),
      ],
      child: wrapper!(_buildChildren(context)),
    );
  }

  List<Widget> _buildChildren(BuildContext context) {
    final theme = Theme.of(context);
    final hasSubtitle = subTitle != null;
    final simpleText = Text(
      text,
      style: theme.textTheme.headline2!
          .copyWith(fontSize: fontSize.sp, color: color),
      textScaleFactor: 1.0,
      textAlign: textAlign,
      maxLines: hasSubtitle ? 1 : null,
      overflow: hasSubtitle ? TextOverflow.ellipsis : null,
    );
    final textWidget = hasSubtitle
        ? Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              simpleText,
              subTitle!,
            ],
          )
        : simpleText;
    return [
      if (prefix != null)
        Padding(
          padding: EdgeInsets.only(right: 24.0),
          child: prefix!,
        ),
      if (expandText) Expanded(child: textWidget) else textWidget,
      if (suffix != null)
        Padding(
          padding: EdgeInsets.only(left: 24.0),
          child: suffix!,
        ),
    ];
  }
}
