part of ntravl;

class RadiusPicker extends StatefulWidget {
  final String label;
  final int initialValue;
  final ValueChanged<int> onRadiusChange;
  final int max;

  RadiusPicker({
    required this.label,
    required this.initialValue,
    required this.onRadiusChange,
    this.max = 60,
  });

  @override
  State createState() {
    return _RadiusPickerState(this.initialValue);
  }
}

class _RadiusPickerState extends State<RadiusPicker> {
  int _index;

  _RadiusPickerState(this._index);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        AppText(text: widget.label, fontSize: 16),
        Slider(
          value: _index.toDouble(),
          min: 10,
          max: widget.max.toDouble(),
          divisions: widget.max ~/ 10,
          label: '${_index.round()} km',
          onChanged: (double d) {
            final value = d.toInt();
            if (!mounted) {
              return;
            }

            setState(() {
              _index = value;
            });
            widget.onRadiusChange(value);
          },
        ),
      ],
    );
  }
}
