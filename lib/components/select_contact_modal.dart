part of ntravl;

class SelectContactModal extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SelectContactModalState();
}

class _SelectContactModalState extends State<SelectContactModal> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            AppText(
              text: 'Contacts',
              fontSize: 24.0,
              textAlign: TextAlign.start,
              prefix: SizedBox(
                width: 24.0,
                height: 24.0,
                child: IconButton(
                  padding: EdgeInsets.all(0.0),
                  icon: Icon(Icons.close),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
            ),
            FutureBuilder<Stream<Contact>?>(
              future: _findContacts(context),
              builder: (context, snapshot) {
                if (snapshot.connectionState != ConnectionState.none) {
                  return Container(
                    child: AppText(
                      text: 'We need permission to look for your contact',
                      fontSize: 16.0,
                      wrapper: (children) => Wrap(children: children),
                    ),
                  );
                }
                if (snapshot.connectionState != ConnectionState.done) {
                  return Container(
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
                }

                if (snapshot.hasError) {
                  return Container(
                    child: AppText(
                      text:
                      'An error occurred when trying to find your contacts. Please retry later',
                      fontSize: 16.0,
                      wrapper: (children) => Wrap(children: children),
                    ),
                  );
                }

                if (!snapshot.hasData) {
                  return Container(
                    child: AppText(
                      text: 'No data found. Please retry later or restart the app',
                      fontSize: 16.0,
                      wrapper: (children) => Wrap(children: children),
                    ),
                  );
                }

                final stream = snapshot.data!;
                return StreamBuilder<Contact>(
                  stream: stream,
                  builder: (context, snapshot) {
                    final item = snapshot.data!;
                    return ListTile(
                      leading: CircleAvatar(
                        child: Image.memory(item.avatar!),
                      ),
                      title: Text(item.displayName!),
                      subtitle: Wrap(
                        children: item.phones
                            .map(
                              (e) => Chip(
                            label: Text('${e.label}-${e.value}'),
                          ),
                        )
                            .toList(),
                      ),
                      onTap: () => Navigator.pop(context, item),
                    );
                  },
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  Future<Stream<Contact>?>? _findContacts(BuildContext context) async {
    if (!await _verifyPermissions(context)) {
      return null;
    }
    return Contacts.streamContacts();
  }

  Future<bool> _verifyPermissions(BuildContext context) async {
    try {
      final result = await Permission.contacts.request();
      return result.isGranted;
    } catch (e, s) {
      print(e);
      print(s);
      return false;
    }
  }
}
