part of ntravl;

enum Month {
  january,
  february,
  march,
  april,
  mai,
  june,
  july,
  august,
  september,
  october,
  november,
  december
}

enum Day { M, Tu, W, Th, F, Sa, Su }

int numDayPerMonth(Month month, int year) {
  switch (month) {
    case Month.january:
    case Month.march:
    case Month.mai:
    case Month.july:
    case Month.august:
    case Month.october:
    case Month.december:
      return 31;
    case Month.february:
      return year % 4 == 0 ? 29 : 28;
    case Month.april:
    case Month.june:
    case Month.september:
    case Month.november:
      return 30;
  }
}

final allMonths = Month.values;
final allDays = Day.values;

class SelectDateModal extends StatefulWidget {
  final bool multiple;

  SelectDateModal({required this.multiple});

  @override
  State<StatefulWidget> createState() => _SelectDateModalState();
}

class _SelectDateModalState extends State<SelectDateModal> {
  DateTimeRange? dateTimeRange;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        top: true,
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AppText(
                text: 'Pick date',
                fontSize: 24.0,
                textAlign: TextAlign.start,
                prefix: SizedBox(
                  width: 24.0,
                  height: 24.0,
                  child: IconButton(
                    padding: EdgeInsets.all(0.0),
                    icon: Icon(Icons.close),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ),
              ),
              Expanded(
                child: AppWrapper(
                  paddingHorizontal: 16.0,
                  child: SfDateRangePicker(
                    enableMultiView: true,
                    minDate: DateTime.now(),
                    maxDate: DateTime.now().add(Duration(days: 365)),
                    headerStyle: DateRangePickerHeaderStyle(
                      backgroundColor: Theme.of(context).canvasColor,
                      textStyle: Theme.of(context).textTheme.headline6,
                    ),
                    monthViewSettings: DateRangePickerMonthViewSettings(
                      enableSwipeSelection: false,
                    ),
                    navigationDirection:
                        DateRangePickerNavigationDirection.vertical,
                    navigationMode: DateRangePickerNavigationMode.scroll,
                    selectionMode: widget.multiple
                        ? DateRangePickerSelectionMode.range
                        : DateRangePickerSelectionMode.single,
                    onSelectionChanged: (args) {
                      if (args.value != null) {
                        if (args.value is PickerDateRange) {
                          final date = args.value;
                          if (date.startDate != null && date.endDate != null) {
                            setState(() {
                              dateTimeRange = DateTimeRange(
                                start: date.startDate!,
                                end: date.endDate!,
                              );
                            });
                          }
                        } else if (args.value is DateTime) {
                          final date = args.value;
                          Navigator.pop(context, date);
                        }
                      }
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: widget.multiple ? AppWrapper(
        child: ElevatedButton(
          onPressed: dateTimeRange == null
              ? null
              : () => Navigator.pop(context, dateTimeRange),
          child: Text('Continue'),
          style: ElevatedButton.styleFrom(
            textStyle: Theme.of(context).textTheme.headline5,
            shape: StadiumBorder(),
            padding: EdgeInsets.symmetric(horizontal: 24.0.w, vertical: 12.0.h),
          ),
        ),
      ) : null,
    );
  }
}
