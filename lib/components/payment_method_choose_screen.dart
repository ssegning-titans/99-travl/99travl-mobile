part of ntravl;

class PaymentMethodScreen extends StatelessWidget {
  PaymentMethodScreen({Key? key}) : super(key: key);
  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          AppText(
            text: 'Choose a method to setup',
            fontSize: 24.0,
            textAlign: TextAlign.start,
          ),
          Expanded(
            child: ListView(
              controller: _scrollController,
              children: [
                SizedBox(height: 16),
                AppListTile(
                  title: Text(
                    'Save a payment method and you will '
                    'use it to make or receive payments',
                  ),
                ),
                SizedBox(height: 16),
                Container(
                  child: Column(
                    children: PaymentMethodType.available
                        .map(
                          (methodType) => _PaymentMethodWidget(
                            methodType: methodType,
                          ),
                        )
                        .toList(),
                  ),
                ),
                SizedBox(height: 16),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class _PaymentMethodWidget extends StatelessWidget {
  final PaymentMethodType methodType;
  final String? subtitle;

  _PaymentMethodWidget({Key? key, required this.methodType, this.subtitle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    const padding = const EdgeInsets.symmetric(vertical: 4.0);

    return Padding(
      padding: padding,
      child: ListTile(
        leading: Card(
          child: methodType.buildCard(context),
        ),
        title: Text(methodType.name),
        subtitle: subtitle != null ? Text(subtitle!) : null,
        onTap: () {
          Navigator.pop(context, methodType);
        },
        trailing: Icon(
          Icons.add,
          color: theme.primaryColor,
        ),
      ),
    );
  }
}
