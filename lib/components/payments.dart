part of ntravl;

enum PaymentType {
  google,
  apple,
  masterCard,
  ec,
}

class PaymentMethodType {
  static final PaymentMethodType google = PaymentMethodType._(
    name: 'Google Pay',
    asset: "assets/payment/google-pay.svg",
    isSvg: true,
    paymentType: PaymentType.google,
  );
  static final PaymentMethodType apple = PaymentMethodType._(
    name: 'Apple',
    asset: "assets/payment/apple-pay.svg",
    isSvg: true,
    paymentType: PaymentType.apple,
  );
  static final PaymentMethodType masterCard = PaymentMethodType._(
    name: 'MasterCard',
    asset: "assets/payment/mastercard-pay.svg",
    isSvg: true,
    paymentType: PaymentType.masterCard,
  );
  static final PaymentMethodType ec = PaymentMethodType._(
    name: 'EC',
    asset: "assets/payment/ec-pay.png",
    isSvg: false,
    paymentType: PaymentType.ec,
  );

  static final List<PaymentMethodType> available = [
    if (Platform.isIOS) apple else if (Platform.isAndroid) google,
    masterCard,
    ec,
  ];

  final String name;
  final String asset;
  final bool isSvg;
  final PaymentType paymentType;
  final double width = 64;

  PaymentMethodType._({
    required this.name,
    required this.asset,
    required this.isSvg,
    required this.paymentType,
  });

  Widget buildCard(BuildContext context) {
    Widget child = isSvg
        ? SvgPicture.asset(
            asset,
            width: width,
          )
        : Image.asset(
            asset,
            width: width,
          );
    return child;
  }
}
