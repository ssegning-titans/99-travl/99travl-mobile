part of ntravl;

class SearchOptionModal extends StatefulWidget {
  final SearchModel initialValue;

  SearchOptionModal({required this.initialValue});

  @override
  State<StatefulWidget> createState() => _SearchOptionModalState();
}

class _SearchOptionModalState extends State<SearchOptionModal> {
  MapPlace? startingPoint;
  MapPlace? endingPoint;
  DateTimeRange? range;
  int? startRadius;
  int? endRadius;

  bool get hasModified =>
      startingPoint != null ||
      endingPoint != null ||
      range != null ||
      startRadius != null ||
      endRadius != null;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      body: SafeArea(
        child: ListView(
          shrinkWrap: true,
          children: [
            AppText(
              text: 'Options',
              fontSize: 24.0,
              textAlign: TextAlign.start,
              suffix: SizedBox(
                width: 24.0,
                height: 24.0,
                child: IconButton(
                  padding: EdgeInsets.all(0.0),
                  icon: Icon(Icons.close),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
            ),
            PlacePicker(
              labelText: 'From',
              hintText: 'Where are you sending from?',
              initialValue: widget.initialValue.start.name,
              placeCallback: (place) {
                setState(() {
                  startingPoint = place;
                });
              },
            ),
            PlacePicker(
              labelText: 'To',
              hintText: 'Where are you sending to?',
              initialValue: widget.initialValue.end.name,
              placeCallback: (place) {
                setState(() {
                  endingPoint = place;
                });
              },
            ),
            AppWrapper(
              paddingVertical: 0,
              child: DatePicker(
                multiple: true,
                onChange: (range) {
                  if (range is DateTimeRange) {
                    if (mounted) {
                      setState(() {
                        this.range = range;
                      });
                    }
                  } else {
                    throw Exception('range is not DateTimeRange');
                  }
                },
              ),
            ),
            AppWrapper(child: Divider(), paddingVertical: 0),
            SizedBox(height: 24.0),
            RadiusPicker(
              label: 'Start radius',
              initialValue: widget.initialValue.startRadius,
              onRadiusChange: (value) {
                setState(() {
                  startRadius = value;
                });
              },
            ),
            SizedBox(height: 24.0),
            RadiusPicker(
              label: 'End radius',
              initialValue: widget.initialValue.endRadius,
              onRadiusChange: (value) {
                setState(() {
                  endRadius = value;
                });
              },
            ),
          ],
        ),
      ),
      bottomNavigationBar: AppWrapper(
        child: ElevatedButton(
          onPressed: !hasModified
              ? null
              : () {
                  final model = SearchModel(
                    startRadius: startRadius ?? widget.initialValue.startRadius,
                    endRadius: endRadius ?? widget.initialValue.endRadius,
                    start: startingPoint != null
                        ? SearchPoint(
                            name: startingPoint!.formattedAddress!,
                            latitude: startingPoint!.latitude!,
                            longitude: startingPoint!.longitude!,
                          )
                        : widget.initialValue.start,
                    end: endingPoint != null
                        ? SearchPoint(
                            name: endingPoint!.formattedAddress!,
                            latitude: endingPoint!.latitude!,
                            longitude: endingPoint!.longitude!,
                          )
                        : widget.initialValue.end,
                    range: range ?? widget.initialValue.range,
                  );
                  Navigator.pop(context, model);
                },
          child: Text('Done'),
          style: ElevatedButton.styleFrom(
            textStyle: theme.textTheme.headline5,
            shape: StadiumBorder(),
            padding: EdgeInsets.symmetric(horizontal: 24.0.w, vertical: 12.0.h),
          ),
        ),
      ),
    );
  }
}
