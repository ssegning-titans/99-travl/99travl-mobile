part of ntravl;

class TravelPath extends StatelessWidget {
  final String label;
  final String? subTitle;

  const TravelPath({
    required this.label,
    this.subTitle,
  });

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return ListTile(
      contentPadding: EdgeInsets.only(left: 32),
      title: Text(
        label,
        style: theme.textTheme.subtitle1,
        textAlign: TextAlign.start,
      ),
      subtitle: subTitle != null ? Text(subTitle!) : null,
    );
  }
}

class TravelItem extends StatelessWidget {
  final Travel travel;
  final List<TravelPoint> passagePoints;

  TravelPoint get start => passagePoints.first;

  TravelPoint get end => passagePoints.last;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return AppCard(
      paddingHorizontal: 18,
      child: Column(
        children: [
          TimelineTile(
            alignment: TimelineAlign.start,
            endChild: TravelPath(
              label: start.formattedName!,
              subTitle:
              '${df.format(
                  DateTime.fromMillisecondsSinceEpoch(start.passageTime!))}',
            ),
            isFirst: true,
            indicatorStyle: const IndicatorStyle(
              width: 24.0,
              height: 24.0,
            ),
          ),
          if (passagePoints.length == 3) TimelineTile(
            alignment: TimelineAlign.start,
            endChild: TravelPath(
              label: passagePoints[1].formattedName!,
            ),
            indicatorStyle: const IndicatorStyle(
              width: 12.0,
              height: 12.0,
              padding: EdgeInsets.only(left: 6.0, right: 6.0),
            ),
          ),
          if (passagePoints.length > 3) TimelineTile(
            alignment: TimelineAlign.start,
            endChild: TravelPath(
              label: '${passagePoints.length - 2}',
            ),
            indicatorStyle: const IndicatorStyle(
              width: 12.0,
              height: 12.0,
              padding: EdgeInsets.only(left: 6.0, right: 6.0),
            ),
          ),
          TimelineTile(
            alignment: TimelineAlign.start,
            endChild: TravelPath(
              label: end.formattedName!,
              subTitle:
              '${df.format(
                  DateTime.fromMillisecondsSinceEpoch(end.passageTime!))}',
            ),
            isLast: true,
            indicatorStyle: const IndicatorStyle(
              width: 24.0,
              height: 24.0,
            ),
          ),
          ListTile(
            contentPadding: EdgeInsets.zero,
            leading: CircleAvatar(
              child: Icon(iconFromTravelModeUnit(travel.transportMode!)),
            ),
            title: Text('${travel.luggageDimension.name} - luggage'),
            subtitle: Text('TODO 6 Stops'),
            trailing: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  '${travel.maxQuantity.weight} ${travel.maxQuantity.unit}',
                  style: theme.textTheme.headline6,
                ),
                Text(
                  '${travel.maxQuantity.pricePerUnit} €/${travel.maxQuantity
                      .unit}',
                  style: theme.textTheme.bodyText2!
                      .copyWith(color: theme.disabledColor),
                ),
              ],
            ),
          )
        ],
      ),
      onTap: () {
        // TODO Go to Details: Use Animations?
        print('see details of ${travel.id}');
      },
    );
  }

  TravelItem({
    required this.travel,
  }) : passagePoints = travel.passagePoints.toList()
    ..sort((a, b) => a.priority! - b.priority!);
}

class TravelSearch extends StatelessWidget {
  final SingleSearch search;

  const TravelSearch(this.search);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Row(
        children: [
          Expanded(
            child: Column(
              children: [
                TimelineTile(
                  alignment: TimelineAlign.start,
                  endChild: TravelPath(
                    label: search.startingPoint.formattedAddress!,
                    // TODO date: search.,
                  ),
                  isFirst: true,
                  indicatorStyle: IndicatorStyle(
                    width: 24.0,
                    height: 24.0,
                  ),
                ),
                TimelineTile(
                  alignment: TimelineAlign.start,
                  endChild:
                  TravelPath(label: search.endingPoint.formattedAddress!),
                  isLast: true,
                  indicatorStyle: IndicatorStyle(
                    width: 24.0,
                    height: 24.0,
                  ),
                ),
              ],
            ),
          ),
          Icon(Icons.arrow_forward_ios_sharp)
        ],
      ),
      onTap: () {
        gotoSearchResult(
          context,
          startingPoint: search.startingPoint,
          endingPoint: search.endingPoint,
          range: search.range,
        );
      },
    );
  }
}
