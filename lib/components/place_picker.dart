part of ntravl;

class PlacePicker extends StatefulWidget {
  final TextEditingController controller;
  final ValueChanged<MapPlace> placeCallback;
  final String labelText;
  final String hintText;
  final double paddingHorizontal;
  final double paddingVertical;

  PlacePicker({
    Key? key,
    required this.placeCallback,
    required this.labelText,
    required this.hintText,
    String? initialValue,
    this.paddingHorizontal = 24.0,
    this.paddingVertical = 12.0,
    TextEditingController? controller,
  })  : this.controller = TextEditingController(text: initialValue),
        super(key: key);

  @override
  State<StatefulWidget> createState() =>
      _PlacePickerState(controller: controller);
}

class _PlacePickerState extends State<PlacePicker> {
  final TextEditingController controller;

  _PlacePickerState({
    required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return AppWrapper(
      paddingHorizontal: widget.paddingHorizontal,
      paddingVertical: widget.paddingVertical,
      child: TextFormField(
        controller: controller,
        decoration: InputDecoration(
          labelText: widget.labelText,
          hintText: widget.hintText,
          border: OutlineInputBorder(
            borderRadius: const BorderRadius.all(Radius.circular(8.0)),
          ),
        ),
        readOnly: true,
        onTap: () => _onChooseAddress(context),
      ),
    );
  }

  void _onChooseAddress(BuildContext context) async {
    final place = await pickAddress(context);
    if (place != null && mounted) {
      setState(() {
        controller.text = place.formattedAddress!;
      });
      widget.placeCallback(place);
    }
  }
}
