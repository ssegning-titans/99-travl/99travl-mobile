part of ntravl;

class AccountLine extends StatelessWidget {
  final String? accountId;
  final Account? provider;
  final EdgeInsetsGeometry padding;

  AccountLine({
    Key? key,
    this.accountId,
    this.provider,
    EdgeInsetsGeometry? padding,
  })  : this.padding = padding ?? EdgeInsets.zero,
        super(key: key) {
    assert(accountId != null || provider != null);
  }

  @override
  Widget build(BuildContext context) {
    if (provider != null) {
      return AccountLineItem(account: provider!, padding: padding);
    }

    final apiProvider = ApiProvider.of(context);
    return FutureBuilder<Account>(
      future: _getAccount(apiProvider.accountsApi),
      builder: (context, snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
        if (snapshot.hasError) {
          return Container(
            child: AppText(
              text:
                  'An error occurred when trying to find your contacts. Please retry later',
              fontSize: 16.0,
              wrapper: (children) => Wrap(children: children),
            ),
          );
        }
        return AccountLineItem(account: snapshot.data!, padding: padding);
      },
    );
  }

  Future<Account> _getAccount(AccountsApi accountApi) async {
    final response =
        await accountApi.getAccountByIdentifier(id: this.accountId!);
    return response.data!;
  }
}

class AccountLineItem extends StatelessWidget {
  final EdgeInsetsGeometry padding;
  final Account account;

  AccountLineItem({
    Key? key,
    required this.account,
    required this.padding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: padding,
      leading: CircleAvatar(
        backgroundImage:
            account.avatarUrl != null ? NetworkImage(account.avatarUrl!) : null,
        child: account.avatarUrl == null
            ? Text('${account.firstName?[0]}${account.lastName?[0]}')
            : null,
      ),
      title: Text(account.firstName ?? account.email),
      subtitle: Text('Some stars here'),
      trailing: IconButton(
        onPressed: () {
          print("Show account's menu");
        },
        icon: Icon(Icons.more_vert),
      ),
    );
  }
}
