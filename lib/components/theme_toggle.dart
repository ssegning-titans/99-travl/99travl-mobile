part of ntravl;

class AppThemeToggle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final themeProvider = ThemeProvider.of(context);
    final current = themeProvider.getTheme();
    final theme = Theme.of(context);
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        TextButton.icon(
          icon: Icon(
            current == ThemeMode.system
                ? Icons.wb_auto
                : current == ThemeMode.dark
                    ? Icons.wb_cloudy
                    : Icons.wb_sunny,
          ),
          onPressed: () {
            if (current == ThemeMode.system) {
              themeProvider.setTheme(ThemeMode.dark);
            } else if (current == ThemeMode.dark) {
              themeProvider.setTheme(ThemeMode.light);
            } else {
              themeProvider.setTheme(ThemeMode.system);
            }
          },
          label: Text(
            getThemeName(current),
            style: theme.textTheme.headline5!.copyWith(
              color: theme.hintColor,
            ),
          ),
        ),
      ],
    );
  }

  String getThemeName(ThemeMode theme) {
    switch (theme) {
      case ThemeMode.system:
        return 'System';
      case ThemeMode.light:
        return 'Light';
      case ThemeMode.dark:
        return 'Dark';
    }
  }

  const AppThemeToggle();
}
