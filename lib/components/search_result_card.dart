part of ntravl;

class SearchResultCard extends StatefulWidget {
  final SearchTravel road;
  final List<TravelPoint> points;

  SearchResultCard({
    required this.road,
  }) : points = road.points.toList()..sort((a, b) => a.priority! - b.priority!);

  @override
  State createState() => _SearchResultCardState();
}

class _SearchResultCardState extends State<SearchResultCard> {
  List<TravelPoint> get points => widget.points;

  String get travelId => points.first.travelId!;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final travelsApi = ApiProvider.of(context).travelsApi;
    final accountsApi = ApiProvider.of(context).accountsApi;

    return FutureBuilder<Response<Travel>>(
      future: travelsApi.getTravelById(travelId: travelId),
      builder: (context, snapshot) {
        final canShowTravel = snapshot.data?.data != null;
        final travel = snapshot.data?.data;

        return AppCard(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TimelineTile(
                alignment: TimelineAlign.start,
                endChild: TravelPath(
                  label: points.first.formattedName!,
                  // TODO date: DateTime.now(),
                ),
                isFirst: true,
                indicatorStyle: IndicatorStyle(
                  width: 24.0,
                  height: 24.0,
                ),
              ),
              TimelineTile(
                alignment: TimelineAlign.start,
                endChild: TravelPath(
                  label: points.last.formattedName!,
                  // TODO date: DateTime.now(),
                ),
                isLast: true,
                indicatorStyle: IndicatorStyle(
                  width: 24.0,
                  height: 24.0,
                ),
              ),
              if (canShowTravel)
                ListTile(
                  contentPadding: EdgeInsets.zero,
                  leading: CircleAvatar(
                    child: Icon(iconFromTravelModeUnit(travel!.transportMode!)),
                  ),
                  title: Text('${travel.luggageDimension.name} - luggage'),
                  subtitle: Text('TODO 6 Stops'),
                  trailing: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        '${travel.maxQuantity.weight} ${travel.maxQuantity.unit}',
                        style: theme.textTheme.headline6,
                      ),
                      Text(
                        '${travel.maxQuantity.pricePerUnit} €/${travel.maxQuantity.unit}',
                        style: theme.textTheme.bodyText2!
                            .copyWith(color: theme.disabledColor),
                      ),
                    ],
                  ),
                ),
              if (canShowTravel) Divider(),
              if (canShowTravel)
                FutureBuilder<Response<Account>>(
                  future: accountsApi.getAccountByIdentifier(
                      id: travel!.providerAccountId),
                  builder: (context, snapshot) {
                    final provider = snapshot.data?.data;
                    if (provider == null) {
                      return Container();
                    }
                    return AccountLine(provider: provider);
                  },
                ),
            ],
          ),
          onTap: () {},
        );
      },
    );
  }
}
