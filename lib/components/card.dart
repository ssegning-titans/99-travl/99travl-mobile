part of ntravl;

class AppCard extends SingleChildStatelessWidget {
  final double paddingHorizontal;
  final double paddingVertical;
  final EdgeInsetsGeometry padding;
  final BorderRadius borderRadius;
  final VoidCallback? onTap;

  AppCard({
    Key? key,
    Widget? child,
    this.paddingHorizontal = 24.0,
    this.paddingVertical = 12.0,
    this.onTap,
    EdgeInsetsGeometry? padding,
    BorderRadius? borderRadius,
  })  : this.padding = padding ?? EdgeInsets.symmetric(horizontal: 12.0),
        this.borderRadius = borderRadius ?? BorderRadius.circular(12.0),
        super(key: key, child: child);

  @override
  Widget buildWithChild(BuildContext context, Widget? child) {
    return AppWrapper(
      paddingHorizontal: paddingHorizontal,
      paddingVertical: paddingVertical,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: borderRadius,
        ),
        child: InkWell(
          borderRadius: borderRadius,
          child: Container(
            padding: padding,
            child: child,
          ),
          onTap: onTap,
        ),
      ),
    );
  }
}
