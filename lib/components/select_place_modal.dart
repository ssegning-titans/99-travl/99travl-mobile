part of ntravl;

class SelectPlaceModal extends StatefulWidget {
  final MapsApi mapsApi;

  const SelectPlaceModal({required this.mapsApi});

  @override
  State<StatefulWidget> createState() => _SelectPlaceModalState();
}

class _SelectPlaceModalState extends State<SelectPlaceModal> {
  final _textController = TextEditingController();

  bool get hasNotTyped => _textController.text.isEmpty;

  final _debouncer = Debouncer(milliseconds: 256);
  final _sessionToken = Uuid().v4();
  CancelToken? _cancelToken;

  List<MapPlace> suggestions = [];
  List<MapPlace> previousSuggestions = [];

  @override
  void initState() {
    super.initState();
    _textController.addListener(_onTextChange);
  }

  @override
  void dispose() {
    _textController.removeListener(_onTextChange);
    super.dispose();
  }

  void _onTextChange() => _debouncer.run(
        () async {
          final text = _textController.text;
          if (text.isEmpty) {
            return;
          }
          if (_cancelToken != null) {
            _cancelToken!.cancel();
          }
          if (!mounted) {
            return;
          }

          final localeName = AppLocalizations.of(context)!.localeName;

          _cancelToken = CancelToken();
          final result = await widget.mapsApi.autocomplete(
            formattedAddress: text,
            acceptLanguage: localeName,
            sessionToken: _sessionToken,
            cancelToken: _cancelToken,
          );
          if (mounted)
            setState(() {
              suggestions = result.data!.toList();
            });
        },
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AppText(
                text: 'Pick address',
                fontSize: 24.0,
                textAlign: TextAlign.start,
                prefix: SizedBox(
                  width: 24.0,
                  height: 24.0,
                  child: IconButton(
                    padding: EdgeInsets.all(0.0),
                    icon: Icon(Icons.close),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ),
              ),
              AppWrapper(
                child: Column(
                  children: [
                    TextFormField(
                      controller: _textController,
                      decoration: InputDecoration(
                        labelText: AppLocalizations.of(context)!.city,
                        hintText: AppLocalizations.of(context)!.type_here,
                        border: OutlineInputBorder(
                          borderRadius: const BorderRadius.all(
                            Radius.circular(8.0),
                          ),
                        ),
                      ),
                    ),
                    if (hasNotTyped)
                      ListTile(
                        contentPadding: EdgeInsets.symmetric(horizontal: 0),
                        title: Text(AppLocalizations.of(context)!.use_location),
                        trailing: Icon(Icons.location_searching_outlined),
                        onTap: () => _useLocation(context),
                      ),
                    if (hasNotTyped) Divider(),
                  ],
                ),
              ),
              if (hasNotTyped)
                Expanded(
                  child: _buildPreviousSuggestions(context),
                ),
              if (!hasNotTyped)
                Expanded(
                  child: _buildList(
                    context: context,
                    suggestions: suggestions,
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildTile({
    required BuildContext context,
    required MapPlace place,
    ValueChanged<MapPlace>? onTake,
  }) {
    return MapPlaceListTile(
      padding: EdgeInsets.symmetric(horizontal: 24.0),
      place: place,
      onTap: () => onTake != null
          ? onTake(place)
          : _selectOne(
              context: context,
              placeId: place.placeId!,
            ),
      icon: Icon(Icons.arrow_forward_ios_sharp),
    );
  }

  void _selectOne({
    required BuildContext context,
    required String placeId,
  }) async {
    if (_cancelToken != null) {
      _cancelToken!.cancel();
    }

    _cancelToken = CancelToken();

    final localeName = AppLocalizations.of(context)!.localeName;
    final result = await widget.mapsApi.getAddressByPlaceId(
      placeId: placeId,
      acceptLanguage: localeName,
      cancelToken: _cancelToken,
    );
    final value = result.data;
    _takeValue(context, value!);
  }

  void _useLocation(BuildContext context) async {
    final position = await Geolocator.getLastKnownPosition();
    if (position != null) {
      final localeName = AppLocalizations.of(context)!.localeName;
      final result = await widget.mapsApi.getAddressByCoordinate(
        latitude: position.latitude,
        longitude: position.longitude,
        acceptLanguage: localeName,
      );
      final value = result.data;
      _takeValue(context, value!);
    }
  }

  void _takeValue(BuildContext context, MapPlace place) async {
    final provider = PreviousSuggestionProvider.of(context, listen: false);
    provider.put(MapPlaceEntity.fromMapPlace(place));
    Navigator.pop(context, place);
  }

  Widget _buildPreviousSuggestions(BuildContext context) {
    final provider = PreviousSuggestionProvider.of(context, listen: false);
    return _buildList(
      context: context,
      suggestions: provider.getAll(),
      onTake: (place) {
        Navigator.pop(context, place);
      },
    );
  }

  Widget _buildList({
    required BuildContext context,
    required List<MapPlace> suggestions,
    ValueChanged<MapPlace>? onTake,
  }) =>
      ListView.separated(
        shrinkWrap: true,
        separatorBuilder: (context, index) => Padding(
          padding: EdgeInsets.symmetric(horizontal: 24.0),
          child: Divider(height: 0),
        ),
        itemCount: suggestions.length,
        itemBuilder: (context, index) => buildTile(
          context: context,
          place: suggestions[index],
          onTake: onTake,
        ),
      );
}

class MapPlaceListTile extends StatelessWidget {
  final MapPlace place;
  final VoidCallback? onTap;
  final VoidCallback? onIconTap;
  final EdgeInsetsGeometry? padding;
  final Widget icon;

  MapPlaceListTile({
    required this.place,
    required this.icon,
    this.onTap,
    this.onIconTap,
    this.padding,
  });

  @override
  Widget build(BuildContext context) {
    final formattedAddress = place.formattedAddress!;
    final splitted = formattedAddress.split(', ');
    return ListTile(
      title: Text(formattedAddress),
      subtitle: Text(splitted.last),
      contentPadding: padding,
      trailing: IconButton(
        onPressed: onIconTap,
        icon: icon,
      ),
      onTap: onTap,
    );
  }
}
