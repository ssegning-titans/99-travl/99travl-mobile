part of ntravl;

class ChatItem extends StatefulWidget {
  final Chat chat;

  ChatItem({Key? key, required this.chat}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ChatItemState();
}

class _ChatItemState extends State<ChatItem> {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text('Some chat'),
    );
  }
}
