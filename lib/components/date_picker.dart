part of ntravl;

class DatePicker extends StatefulWidget {
  final bool multiple;
  final ValueChanged<dynamic> onChange;
  final Widget? hint;

  DatePicker({
    this.multiple = false,
    required this.onChange,
    this.hint,
  });

  @override
  State createState() {
    return _DatePickerState();
  }
}

class _DatePickerState extends State<DatePicker> {
  DateTimeRange dateRange = DateTimeRange(
    start: DateTime.now(),
    end: DateTime.now().add(Duration(days: 7)),
  );

  DateTime date = DateTime.now();

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (widget.hint != null) widget.hint!,
        ListTile(
          contentPadding: EdgeInsets.symmetric(horizontal: 0.0),
          title: Text(
            widget.multiple
                ? "${df.format(dateRange.start)} − ${df.format(dateRange.end)}"
                : "${df.format(date)}",
            style: theme.textTheme.headline6!.copyWith(
              color: theme.primaryColor,
            ),
            textScaleFactor: 1.0,
          ),
          onTap: () async {
            dynamic result = await pickDate(
              context,
              multiple: widget.multiple,
            );
            if (result != null && mounted) {
              setState(() {
                if (widget.multiple)
                  dateRange = result;
                else
                  date = result;
              });
              widget.onChange(result);
            }
          },
          trailing: Icon(
            Icons.calendar_today_outlined,
            color: theme.primaryColor,
          ),
        ),
      ],
    );
  }
}
