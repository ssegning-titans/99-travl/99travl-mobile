import 'package:openapi_generator_annotations/openapi_generator_annotations.dart';

@Openapi(
  additionalProperties: DioProperties(
    pubName: 'ntravl_api',
    pubAuthor: 'SSegning',
    pubAuthorEmail: 'dev@ssegning.com',
    pubVersion: '1.1.4',
    dateLibrary: DioDateLibrary.core,
    nullableFields: false,
  ),
  inputSpecFile: 'openapi.yaml',
  generatorName: Generator.dioNext,
  outputDirectory: 'api/ntravl_api',
  alwaysRun: true,
  apiPackage: 'ntravl_api',
  fetchDependencies: true,
  overwriteExistingFiles: true,
  runSourceGenOnOutput: true,
)
class NTravlConfig extends OpenapiGeneratorConfig {}
