part of ntravl;

class AnalyticProvider with ChangeNotifier {
  bool get init => MatomoTracker().initialized;

  bool get optIn => !_optOut;

  bool _optOut;

  AnalyticProvider() : this._optOut = MatomoTracker().optOut!;

  void setOptIn(bool value) {
    MatomoTracker().setOptOut(!value);
    _optOut = !value;

    notifyListeners();
  }
}
