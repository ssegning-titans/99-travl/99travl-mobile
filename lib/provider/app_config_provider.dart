part of ntravl;

class AppConfigProvider extends ChangeNotifier {
  late PackageInfo packageInfo;
  late DeviceInfoPlugin deviceInfo;
  bool ready = false;

  static AppConfigProvider of(BuildContext context) {
    return context.watch<AppConfigProvider>();
  }

  AppConfigProvider() {
    _init();
  }

  Future _init() async {
    packageInfo = await PackageInfo.fromPlatform();
    deviceInfo = DeviceInfoPlugin();
    this.ready = true;
    notifyListeners();
  }
}
