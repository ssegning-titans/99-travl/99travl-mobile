part of ntravl;

class ThemeProvider with ChangeNotifier {
  ThemeMode? _themeMode;

  ThemeMode? get themeMode => _themeMode;

  final Box<String> _box;

  ThemeProvider._(this._box) {
    _themeMode = getTheme();
  }

  static Future<ThemeProvider> create() async {
    final box = await Hive.openBox<String>(kBoxThemeKey);
    return ThemeProvider._(box);
  }

  static ThemeProvider of(BuildContext context) {
    return context.watch<ThemeProvider>();
  }

  Future<void> setTheme(ThemeMode mode) async {
    await _box.put(kThemeModeKey, mode.toString());
    _themeMode = mode;
    notifyListeners();
  }

  ThemeMode getTheme() {
    final found = _box.get(kThemeModeKey);
    if (found != null) {
      return ThemeMode.values.firstWhere(
        (element) => element.toString() == found,
        orElse: () => ThemeMode.system,
      );
    }
    return ThemeMode.system;
  }
}
