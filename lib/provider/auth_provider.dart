part of ntravl;

class AuthProvider extends ChangeNotifier {
  bool _isAuth = false;
  final OAuth2Helper oauth2Helper;

  bool get isAuth => _isAuth;

  AuthProvider._(this.oauth2Helper);

  static Future<AuthProvider> create() async {
    final client = SSegningOAuth2Client();

    OAuth2Helper oauth2Helper = OAuth2Helper(
      client,
      grantType: OAuth2Helper.AUTHORIZATION_CODE,
      clientId: EnvironmentConfig.clientId,
      clientSecret: EnvironmentConfig.secretId,
      scopes: ['offline_access', 'openid'],
      enablePKCE: true,
      enableState: true,
    );

    final service = AuthProvider._(oauth2Helper);
    await service._init();
    return service;
  }

  static AuthProvider of(BuildContext context) {
    return context.watch<AuthProvider>();
  }

  Future<void> _init() async {
    final response = await oauth2Helper.getTokenFromStorage();
    _isAuth = response != null;
  }

  Future<void> logout() async {
    try {
      await oauth2Helper.disconnect();
      _isAuth = false;

      notifyListeners();
    } on PlatformException catch (e, s) {
      print('auth error $e');
      print('auth stack $s');
    }
  }

  Future<void> login() async {
    try {
      final token = await oauth2Helper.getToken();
      if (token != null && token.accessToken != null) {
        _isAuth = true;
      }

      notifyListeners();
    } on PlatformException catch (e, s) {
      print('auth error $e');
      print('auth stack $s');
    } catch (e, s) {
      print('auth --> $e');
      print('auth --> $s');
    }
  }

  Future<String?> getToken() async {
    final token = await oauth2Helper.getToken();
    return token?.accessToken;
  }

  Future<String> getAccountId() async {
    final token = await getToken();
    final payload = Jwt.parseJwt(token!);
    final sub = payload['sub'] as String;
    return sub.split(':').last;
  }

  Future<Map<String, dynamic>> getMap() async {
    final token = await getToken();
    return Jwt.parseJwt(token!);
  }
}

class SSegningOAuth2Client extends OAuth2Client {
  SSegningOAuth2Client()
      : super(
          authorizeUrl: '$_ssegningOperationBaseUrl/auth',
          tokenUrl: '$_ssegningOperationBaseUrl/token',
          revokeUrl: '$_ssegningOperationBaseUrl/token',
          redirectUri: 'ntravlauth://oauth_redirect',
          customUriScheme: 'ntravlauth',
        );

  static final _ssegningOperationBaseUrl =
      '${EnvironmentConfig.oauth2Issuer}/protocol/openid-connect';
}
