part of ntravl;

class CurrentUserChatsProvider with ChangeNotifier {
  final ChatsApi chatsApi;
  final AuthProvider authProvider;
  bool ready = false;
  bool loading = true;
  bool error = false;
  Map<String, Chat> _previous = {};

  List<Chat> get previousList => List.unmodifiable(_previous.values);

  void _init() async {
    await _fetchTravels();
    ready = true;
    loading = false;
    notifyListeners();
  }

  void add(Chat chat) {
    _previous[chat.id!] = chat;
  }

  Future<void> _fetchTravels() async {
    try {
      final accountId = await authProvider.getAccountId();

      final result = await chatsApi.getAllChats(
          accountId: accountId, status: ChatStatus.ACTIVE);

      result.data!.forEach((p) {
        _previous[p.id!] = p;
      });
    } on DioError catch (e, s) {
      error = true;
      notifyListeners();
      rethrow;
    }
  }

  CurrentUserChatsProvider(this.chatsApi, this.authProvider) {
    _init();
  }
}
