part of ntravl;

class WelcomeProvider extends ChangeNotifier {
  final Box<bool> _box;

  WelcomeProvider(this._box);

  static WelcomeProvider of(BuildContext context, {bool listen = true}) {
    return Provider.of<WelcomeProvider>(context, listen: listen);
  }

  static Future<WelcomeProvider> create() async {
    final welcomeBox = await Hive.openBox<bool>(kBoxWelcomeKey);
    return WelcomeProvider(welcomeBox);
  }

  bool get hasSeenWelcome => _box.get(kSeenWelcomeKey) ?? false;

  Future<void> seenWelcome() async {
    await _box.put(kSeenWelcomeKey, true);
  }

  void clear() async {
    await _box.clear();
  }
}
