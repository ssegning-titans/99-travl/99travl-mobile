part of ntravl;

class PreviousSuggestionProvider extends ChangeNotifier {
  final Box<MapPlaceEntity> _box;

  PreviousSuggestionProvider._(this._box);

  static Future<PreviousSuggestionProvider> create() async {
    Hive.registerAdapter(MapPlaceAdapter());
    final box = await Hive.openBox<MapPlaceEntity>(kBoxPreviousSuggestionKey);
    return PreviousSuggestionProvider._(box);
  }

  static PreviousSuggestionProvider of(BuildContext context,
      {bool listen = true}) {
    return Provider.of<PreviousSuggestionProvider>(context, listen: listen);
  }

  List<MapPlace> getAll() =>
      _box.values.map((e) => e.toPlace()).toList();

  MapPlace? getOne(String placeId) => _box.get(placeId)?.toPlace();

  void put(MapPlaceEntity entity) {
    _box.put(entity.placeId, entity);

    notifyListeners();
  }
}

@HiveType(typeId: 0)
class MapPlaceEntity extends HiveObject {
  @HiveField(0)
  String placeId;

  @HiveField(1)
  double latitude;

  @HiveField(2)
  double longitude;

  @HiveField(3)
  String formattedAddress;

  MapPlaceEntity({
    required this.placeId,
    required this.latitude,
    required this.longitude,
    required this.formattedAddress,
  });

  MapPlaceEntity.fromMapPlace(MapPlace place)
      : placeId = place.placeId!,
        latitude = place.latitude!,
        longitude = place.longitude!,
        formattedAddress = place.formattedAddress!;

  MapPlace toPlace() {
    final builder = MapPlaceBuilder()
      ..placeId = placeId
      ..formattedAddress = formattedAddress
      ..latitude = latitude
      ..longitude = longitude;
    return builder.build();
  }
}

class MapPlaceAdapter extends TypeAdapter<MapPlaceEntity> {
  @override
  MapPlaceEntity read(BinaryReader reader) {
    final attributes = reader.readStringList();
    return MapPlaceEntity(
      placeId: attributes[0],
      latitude: double.parse(attributes[1]),
      longitude: double.parse(attributes[2]),
      formattedAddress: attributes[3],
    );
  }

  @override
  int get typeId => 0;

  @override
  void write(BinaryWriter writer, MapPlaceEntity obj) {
    final placeId = obj.placeId;
    final latitude = obj.latitude;
    final longitude = obj.longitude;
    final formattedAddress = obj.formattedAddress;
    writer.writeStringList(
        [placeId, '$latitude', '$longitude', formattedAddress]);
  }
}
