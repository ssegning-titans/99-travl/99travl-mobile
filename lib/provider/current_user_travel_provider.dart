part of ntravl;

class CurrentUserTravelProvider with ChangeNotifier {
  final TravelsApi travelsApi;
  final AuthProvider authProvider;
  bool ready = false;
  bool loading = true;
  bool error = false;
  Map<String, Travel> _previous = {};

  List<Travel> get previousList => List.unmodifiable(_previous.values);

  void _init() async {
    await _fetchTravels();
    ready = true;
    loading = false;
    notifyListeners();
  }

  void add(Travel travel) {
    _previous[travel.id!] = travel;
  }

  Future<void> _fetchTravels({
    int? size = 200,
    int? page = 0,
  }) async {
    try {
      final accountId = await authProvider.getAccountId();

      final result = await travelsApi.getAccountTravels(
        accountId: accountId,
        page: page,
        size: size,
      );

      result.data!.forEach((p) {
        _previous[p.id!] = p;
      });
    } on DioError catch(e, s) {
      error = true;
    }
  }

  CurrentUserTravelProvider(this.travelsApi, this.authProvider) {
    _init();
  }
}
