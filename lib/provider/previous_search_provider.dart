part of ntravl;

class PreviousSearchProvider extends ChangeNotifier {
  final Box<SingleSearchEntity> _box;
  final PreviousSuggestionProvider previousSuggestionProvider;

  PreviousSearchProvider._(this._box, this.previousSuggestionProvider);

  static Future<PreviousSearchProvider> create({
    required PreviousSuggestionProvider provider,
  }) async {
    Hive.registerAdapter(SingleSearchAdapter());
    final box = await Hive.openBox<SingleSearchEntity>(kBoxPreviousSearchKey);
    return PreviousSearchProvider._(box, provider);
  }

  static PreviousSearchProvider of(BuildContext context, {bool listen = true}) {
    return Provider.of<PreviousSearchProvider>(context, listen: listen);
  }

  List<SingleSearch> getAll() => _box.values
      .map((e) => SingleSearch.fromEntity(
            entity: e,
            previousSuggestionProvider: previousSuggestionProvider,
          ))
      .toList()
    ..sort();

  void put(SingleSearch search) async {
    final entity = SingleSearchEntity.fromModel(search);
    final key = '${entity.startingPlaceId}:${entity.endingPlaceId}';
    final one = _box.get(key);

    if (one != null) {
      one.tapCount++;
      one.startTime = entity.startTime;
      one.endTime = entity.endTime;
      one.startRadius = entity.startRadius;
      one.endRadius = entity.endRadius;
      await one.save();
    } else {
      previousSuggestionProvider
          .put(MapPlaceEntity.fromMapPlace(search.startingPoint));
      previousSuggestionProvider
          .put(MapPlaceEntity.fromMapPlace(search.endingPoint));
      _box.put(key, entity);
    }

    notifyListeners();
  }
}

class SingleSearch extends Comparable<SingleSearch> {
  final MapPlace startingPoint;
  final MapPlace endingPoint;
  final DateTimeRange range;
  final int tapCount;
  int startRadius;
  int endRadius;

  SingleSearch({
    required this.startingPoint,
    required this.endingPoint,
    required this.range,
    this.tapCount = 0,
    this.startRadius = 30,
    this.endRadius = 30,
  });

  factory SingleSearch.fromEntity({
    required SingleSearchEntity entity,
    required PreviousSuggestionProvider previousSuggestionProvider,
  }) {
    return SingleSearch(
      range: DateTimeRange(
        start: DateTime.fromMillisecondsSinceEpoch(entity.startTime),
        end: DateTime.fromMillisecondsSinceEpoch(entity.endTime),
      ),
      startingPoint: previousSuggestionProvider.getOne(entity.startingPlaceId)!,
      endingPoint: previousSuggestionProvider.getOne(entity.endingPlaceId)!,
    );
  }

  @override
  int compareTo(SingleSearch other) => tapCount - other.tapCount;
}

@HiveType(typeId: 1)
class SingleSearchEntity extends HiveObject {
  @HiveField(0)
  final String startingPlaceId;

  @HiveField(1)
  final String endingPlaceId;

  @HiveField(2)
  int startTime;

  @HiveField(3)
  int endTime;

  @HiveField(4)
  int tapCount;

  @HiveField(5)
  int startRadius;

  @HiveField(6)
  int endRadius;

  SingleSearchEntity({
    required this.startingPlaceId,
    required this.endingPlaceId,
    required this.startTime,
    required this.endTime,
    required this.tapCount,
    required this.startRadius,
    required this.endRadius,
  });

  factory SingleSearchEntity.fromModel(SingleSearch search) {
    return SingleSearchEntity(
      startingPlaceId: search.startingPoint.placeId!,
      endingPlaceId: search.endingPoint.placeId!,
      startTime: search.range.start.millisecondsSinceEpoch,
      endTime: search.range.end.millisecondsSinceEpoch,
      tapCount: search.tapCount,
      startRadius: search.startRadius,
      endRadius: search.endRadius,
    );
  }

  factory SingleSearchEntity.fromList(List<String> attributes) {
    while (attributes.length < 7) {
      attributes.add('');
    }

    return SingleSearchEntity(
      startingPlaceId: attributes[0],
      endingPlaceId: attributes[1],
      startTime: int.parse(attributes[2]),
      endTime: int.parse(attributes[3]),
      tapCount: int.tryParse(attributes[4]) ?? 0,
      startRadius: int.tryParse(attributes[5]) ?? 30,
      endRadius: int.tryParse(attributes[6]) ?? 30,
    );
  }

  List<String> toAttributes() {
    return [
      startingPlaceId,
      endingPlaceId,
      '$startTime',
      '$endTime',
      '$tapCount',
      '$startRadius',
      '$endRadius',
    ];
  }
}

class SingleSearchAdapter extends TypeAdapter<SingleSearchEntity> {
  @override
  SingleSearchEntity read(BinaryReader reader) {
    final attributes = reader.readStringList();
    return SingleSearchEntity.fromList(attributes);
  }

  @override
  int get typeId => 1;

  @override
  void write(BinaryWriter writer, SingleSearchEntity obj) {
    writer.writeStringList(obj.toAttributes());
  }
}
