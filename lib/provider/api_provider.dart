part of ntravl;

class ApiProvider extends ChangeNotifier {
  final NtravlApi api;

  //
  final AccountAddressApi addressApi;
  final AccountsApi accountsApi;
  final ChatMessagesApi messagesApi;
  final ChatsApi chatsApi;
  final CredentialsApi credentialsApi;
  final DocumentApi documentApi;
  final FavoritesApi favoritesApi;
  final FriendsApi friendsApi;
  final MapsApi mapsApi;
  final RatingsApi ratingsApi;
  final ReportsApi reportsApi;
  final ReservationsApi reservationsApi;
  final PropositionsApi propositionsApi;
  final TravelsApi travelsApi;

  //
  final Stream<bool> connectionStatus;

  ApiProvider(this.api)
      : addressApi = api.getAccountAddressApi(),
        accountsApi = api.getAccountsApi(),
        messagesApi = api.getChatMessagesApi(),
        chatsApi = api.getChatsApi(),
        credentialsApi = api.getCredentialsApi(),
        documentApi = api.getDocumentApi(),
        favoritesApi = api.getFavoritesApi(),
        friendsApi = api.getFriendsApi(),
        mapsApi = api.getMapsApi(),
        ratingsApi = api.getRatingsApi(),
        reportsApi = api.getReportsApi(),
        reservationsApi = api.getReservationsApi(),
        propositionsApi = api.getPropositionsApi(),
        travelsApi = api.getTravelsApi(),
        connectionStatus = Connectivity()
            .onConnectivityChanged
            .map((event) => event == ConnectivityResult.none);

  static Future<ApiProvider> create(
      {required AuthProvider authProvider}) async {

    final dir = await getApplicationDocumentsDirectory();

    // Global options
    final options = CacheOptions(
      // A default store is required for interceptor.
      store: HiveCacheStore('${dir.path}/.request_cache'),
      hitCacheOnErrorExcept: [401, 403, 500, 501],
      maxStale: const Duration(days: 1),
      allowPostMethod: false,
    );

    final api = NtravlApi(
      interceptors: [
        CookieManager(CookieJar()),
        await OauthInterceptor.create(authProvider: authProvider),
        DioCacheInterceptor(options: options),
      ],
      basePathOverride: r'https://api.99travl.com',
    );

    return ApiProvider(api);
  }

  static ApiProvider of(BuildContext context, {bool listen = true}) {
    return Provider.of<ApiProvider>(context, listen: listen);
  }
}

class OauthInterceptor extends Interceptor {
  final AuthProvider? _authProvider;
  final Box<String?> _headerBox;

  OauthInterceptor._(this._authProvider, this._headerBox);

  static Future<OauthInterceptor> create(
      {required AuthProvider authProvider}) async {
    final headerBox = await Hive.openBox<String>(kBoxHeaderKey);
    return OauthInterceptor._(authProvider, headerBox);
  }

  @override
  void onRequest(
    RequestOptions options,
    RequestInterceptorHandler handler,
  ) async {
    if (_authProvider!.isAuth) {
      final token = await _authProvider!.getToken();
      options.headers['Authorization'] = 'Bearer $token';
    }

    final xsrfToken = _getXsrf();
    if (xsrfToken != null) {
      options.headers['X-XSRF-TOKEN'] = xsrfToken;
    }

    options.extra.addAll({'withCredentials': true});
    return super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) async {
    extractXsrfToken(response.headers);
    return super.onResponse(response, handler);
  }

  void _setXsrf(String? value) {
    _headerBox.put('xsrf-token', value);
  }

  void extractXsrfToken(Headers headers) {
    if (headers.map.containsKey('set-cookie')) {
      List<String> csrfCookies = headers['set-cookie']!
          .where((element) => element.contains('XSRF-TOKEN'))
          .toList();

      List<String> rawCrsfCookies = [];

      csrfCookies.forEach((element) {
        final result = element
            .trim()
            .split(';')
            .where((val) => val.contains('XSRF-TOKEN'))
            .toList();

        rawCrsfCookies.addAll(result);
      });

      rawCrsfCookies.forEach((crsf) async {
        final keyValue = crsf.split('=');
        if (keyValue.length == 2 && keyValue[1].isNotEmpty) {
          _setXsrf(keyValue.length == 2 ? keyValue[1] : null);
        }
      });
    }
  }

  String? _getXsrf() {
    return _headerBox.get('xsrf-token');
  }
}
