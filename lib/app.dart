part of ntravl;

class MyApp extends TraceableStatefulWidget {
  @override
  State<StatefulWidget> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: kDebugMode,
      routeInformationParser: BeamerParser(),
      routerDelegate: _routerDelegate,
      backButtonDispatcher: _backButtonDispatcher,
      theme: AppThemeData.normalTheme,
      darkTheme: AppThemeData.darkTheme,
      themeMode: ThemeProvider.of(context).themeMode,
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      onGenerateTitle: (BuildContext context) =>
          AppLocalizations.of(context)!.title,
    );
  }
}

Future<void> app() async {
  WidgetsFlutterBinding.ensureInitialized();
  _initNotif();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  // FirebaseMessaging.onMessage;

  if (kDebugMode && !kIsWeb) Wakelock.enable();

  final appDocDir = await getApplicationDocumentsDirectory();

  CatcherOptions debugOptions = CatcherOptions(
    SilentReportMode(),
    [
      ConsoleHandler(),
    ],
    screenshotsPath: '${appDocDir.path}/.catcher/screenshots',
  );

  /// Release configuration. Same as above, but once user accepts dialog, user will be prompted to send email with crash to support.
  CatcherOptions releaseOptions = CatcherOptions(
    SilentReportMode(),
    [
      SentryHandler(
        SentryClient(
          SentryOptions(dsn: EnvironmentConfig.sentryDsn),
        ),
      ),
    ],
    screenshotsPath: '${appDocDir.path}/.catcher/screenshots',
  );

  Catcher(
    debugConfig: debugOptions,
    releaseConfig: releaseOptions,
    navigatorKey: _routerDelegate.navigatorKey,
    runAppFunction: () async {
      await initMatomo();

      await Hive.initFlutter('./configs');

      final previousSuggestionProvider =
          await PreviousSuggestionProvider.create();
      final authProvider = await AuthProvider.create();
      final apiProvider = await ApiProvider.create(authProvider: authProvider);
      final previousSearchProvider = await PreviousSearchProvider.create(
          provider: previousSuggestionProvider);
      final themeProvider = await ThemeProvider.create();
      final welcome = await WelcomeProvider.create();

      KiwiContainer container = KiwiContainer();
      container.registerInstance(apiProvider);
      container.registerInstance(authProvider);

      runApp(MultiProvider(
        providers: [
          ChangeNotifierProvider.value(value: welcome),
          ChangeNotifierProvider.value(value: themeProvider),
          ChangeNotifierProvider.value(value: previousSearchProvider),
          ChangeNotifierProvider.value(value: previousSuggestionProvider),
          ChangeNotifierProvider.value(value: authProvider),
          ChangeNotifierProvider.value(value: apiProvider),
          ChangeNotifierProvider(
            create: (_) => AppConfigProvider(),
          ),
          ChangeNotifierProvider(
            create: (c) => CurrentUserTravelProvider(
              c.read<ApiProvider>().travelsApi,
              c.read(),
            ),
          ),
          ChangeNotifierProvider(
            create: (c) => CurrentUserChatsProvider(
              c.read<ApiProvider>().chatsApi,
              c.read(),
            ),
          ),
        ],
        child: MyApp(),
      ));
    },
    enableLogger: kDebugMode,
  );
}
