class EnvironmentConfig {
  static const String serverUrl = 'https://api.99travl.com';

  static const String oauth2Issuer =
      'https://accounts.ssegning.com/auth/realms/99-travl';

  static const String clientId = '99travl-mobile';

  static const String secretId = '094293c2-bb1b-4fa2-bbd0-b880234c9c0e';

  static const String sentryDsn =
      'https://79878bd5698e4484969134a110a9c5a3@o557888.ingest.sentry.io/5813981';

  static const String matomoUrl = 'https://matomo.ssegning.com/matomo.php';

  static const num matomoSiteId = 4;
}
