# 99Travl

## Routing

We use Flutter navigator 2

### Deeplinking

This app wakes up when calling `ntravl://dl/${path}` where _path_ is the intern path.
Also when calling http(s)://

## Roadmap

1. Welcome (and slides).
2. Login/Auth
    - Ask phone number
    - Verify phone number and ask tan
    - After verification, check if account exists
      - If no, create an account
      - If yes, continue to next
    - Ask for information to complete account
    - 
   
3. Invite friends(as receiver)
4. 

## Script

1. Generate DB / OpenApi things
```bash
flutter packages pub run build_runner build --delete-conflicting-outputs
flutter pub run flutter_native_splash:create

```

2. Generate env things:
```bash
flutter pub run environment_config:generate
```

3. Generate icons
```bash
flutter pub run flutter_launcher_icons:main
```

## Credits

- <div>Icons made by <a href="https://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>

## Roadmap
### Auth
- Login feature
- Register
    - Provide pages

## Links
- https://medium.com/flutter-community/a-deep-dive-into-datepicker-in-flutter-37e84f7d8d6c
- https://pub.dev/packages/pattern_formatter
- https://www.raywenderlich.com/19457817-flutter-navigator-2-0-and-deep-links#toc-anchor-022
- https://flutter.dev/docs/development/accessibility-and-localization/internationalization#localizing-for-ios-updating-the-ios-app-bundle
